
size(ans)
setoutputlog ('file', 'xorlog1.mat','process', 'train','frequency', 'epoch','status', 'on','objects', {'epochno','tss'},'plot', 'on','plotlayout', [1 1]);
setplotparams ('file', 'xorlog1.mat', 'yvariables', {'tss'});
setoutputlog ('file', 'xoract1.mat','process', 'train','frequency', 'patsbyepoch','status', 'on','objects', {'epochno','pools(4).activation'},'plot', 'on','plotlayout', [1 1]);
setplotparams ('file', 'xoract1.mat', 'yvariables', {'pools_by_name('p3').activation'}, 'ylim', [0 1]);