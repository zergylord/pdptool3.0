function mya = get_activations(net)
mya = cell(0);
for i=1:length(net.pools)
        mya = [mya net.pools(i).activation];
end
save('mya.mat','mya');
        