function [pool index] = get_unit_from_pools(pools,abs_index)
prev_count = 0;
count = 0;
for i=1:length(pools)
    count = count + pools(i).unit_count; 
    if count >= abs_index
        index = abs_index - prev_count;
        pool = pools(i);
        return
    else
        prev_count = count;
    end
end
end
        