function mye = get_errors(net)
mye = cell(0);
for i=1:length(net.pools)
        mye = [mye net.pools(i).error];
end
save('mye.mat','mye');
        