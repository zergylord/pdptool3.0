function exp = get_exp(pic)
    exp_bar = pic(443,177:296,:);
    % imshow(exp_bar)
     for i =1:length(exp_bar)
         if exp_bar(1,i,:) < 200
             cutoff = i;
             break;
         end
     end
     exp = cutoff/120;
end