
function percent = circle_filled(center,radius,max_y)
    smaller_half = false;
    d = max_y - center(2);
    d = -d;    %only works on image coorinates i.e. x normal, y inverted (0 highest)
    if d < 0
        d = -d;
        smaller_half = true;
    end
    angle = 2*acos(d/radius);
    area = ((radius^2)/2)*(angle - sin(angle));
    total_area = pi*radius^2;
    if smaller_half
        percent = area/total_area;
    else
        percent = (total_area-area)/total_area;
    end
    
end