function [header remain] = get_header_toks(toks,delim_start,delim_end)
header_index = 0;
header_found = false;
for j=1:length(toks)
    if ~header_found
        same_str = strrep(toks{j},delim_start,'');
        header_found = ~strcmpi(same_str,toks{j});
        toks{j} = same_str;
        header_index = j;
    end
    if header_found
        same_str = strrep(toks{j},delim_end,'');
        end_header = ~strcmpi(same_str,toks{j});
        toks{j} = same_str;
        if end_header
            header = toks(header_index:j);
                if j+1 > length(toks)
                    remain = toks(1:header_index-1);
                else
                    remain = [toks(1:header_index-1); toks(j+1:end)];
                end
            return
        end
    end
end
%header not found
header = [];
remain = toks;
end