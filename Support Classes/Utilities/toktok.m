%seperates the tokens based on the delim
% e.g. my_toks = [{'a'} {'bat'} {'end'} {'a'} {'bird'}]
% toktok(my_toks,'end') returns [{'a'} {'bat'}]
function [first remain] = toktok(toks,delim)
    for i=1:length(toks)
        if strcmp(toks{i},delim)
            first = toks(1:i-1);
            remain = toks(i+1:end);
            return
        end
    end
    %no delim
    first = [];
    remain = toks;
end