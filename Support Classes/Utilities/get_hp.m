function hp = get_hp(pic)
hpfull = pic(end-100:end-1,25:124,:); %indexing y, then x
pt1 = [46.0514   46.3020];%indexing x, then y
pt2 = [48.5576    9.4599];
[hpfull crap1 crap2 r] = crop_circle(hpfull,pt1,pt2);
hpfull_bw = zeros(size(hpfull(:,:,1)));
max_y = -1;
found_red = false;
for i=numel(hpfull(1,:,1)):-1:1  
    first = true;
    crapfirst = true;
    for j=1:numel(hpfull(:,1,1))
        pixel= hpfull(i,j,:);
        if pixel(1)>40 && pixel(2) < 15 && pixel(3) < 15
            hpfull_bw(i,j) = 1;
            if crapfirst && j > 1 && hpfull_bw(i,j-1)
                max_y = i;
                crapfirst = false;
                found_red = true;
            end
            if first && j > 1 && hpfull_bw(i,j-1) && hpfull_bw(i,j-2)
                max_y = i;
                first = false;
            end
        end
    end
    if crapfirst && found_red
        hp = circle_filled(pt1,r,max_y);
        return
    end
end
if max_y == -1
    hp = 0;
else
    hp = circle_filled(pt1,r,max_y);
end
% if hp < .02
%     disp('shit');
%     load stored_pic;
%     if length(stored_pic) > 9
%         stored_pic = stored_pic(2:10);
%     end
%     stored_pic = [stored_pic {pic}];
%     save('stored_pic.mat','stored_pic');
% end
    load stored_pic;
    if length(stored_pic) > 9
        stored_pic = stored_pic(2:10);
    end
    stored_pic = [stored_pic {pic}];
    save('stored_pic.mat','stored_pic');
end
