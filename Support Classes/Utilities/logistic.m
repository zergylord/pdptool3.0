        function retval = logistic(val)
            retval = val;
            retval(retval > 15.9357739741644) = 15.9357739741644; % returns 0.99999988;
            retval(retval < -15.9357739741644) = -15.9357739741644; % returns 0.00000012
            retval = 1.0 ./(1.0 + exp(-1 * retval));
        end