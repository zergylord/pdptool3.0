function myw = get_weights(net)
myw = cell(0);
for i=1:length(net.pools)
    for j=1:length(net.pools(i).projections)
                if strcmpi(net.pools(i).projections(j).from.type,'bias')
            continue
        end
        myw = [myw net.pools(i).projections(j).using.weights];
    end
end
save('myw.mat','myw');
        