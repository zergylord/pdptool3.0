classdef annealing_process < handle
    properties
        ntemp;
        ctemp;
        ltemp;
        coolrate;
        schedule; %must set before use
        
        temp;%USE THIS RATHER THAN THE TR/TST OPTION VAR
    end
    methods
        function obj = annealing_process()
%              obj.coolrate = cool;
%             obj.schedule = sched;
        end
        function set_temp(obj,iter)
            %annealing stuff
            obj.ltemp = size(obj.schedule,1);
            obj.ntemp = 1;
            obj.ctemp=1;
            if ~isempty(obj.schedule) && obj.ltemp ~= obj.ntemp
                obj.ctemp=obj.ntemp;
                obj.ntemp = obj.ntemp+1;
                obj.coolrate = (obj.schedule(obj.ctemp,2) - obj.schedule(obj.ntemp,2))/obj.schedule(obj.ntemp,1);
                %annealing++++++++++++++++++++++++++++++
                obj.annealing(iter);
                %++++++++++++++++++++++++++++++
            elseif ~isempty(obj.schedule) && obj.ltemp == 1
                obj.temp = obj.schedule(1,2);
            end
        end
        
        function annealing(obj,iter)
            if iter >= obj.schedule(obj.ltemp,1)
                obj.temp = obj.schedule(obj.ltemp,2);
            end
            if iter >= obj.schedule(obj.ntemp,1)
                obj.temp = obj.schedule(obj.ntemp,2);
                if (obj.ntemp ~= obj.ltemp)
                    obj.ctemp = obj.ntemp;
                    obj.ntemp = obj.ntemp + 1;
                end
                obj.coolrate = (obj.schedule(obj.ctemp,2) - obj.schedule(obj.ntemp,2))/ ...
                    (obj.schedule(obj.ntemp,1) - obj.schedule(obj.ctemp,1));
            else
                obj.temp = obj.schedule(obj.ctemp,2) - (obj.coolrate * (iter - obj.schedule(obj.ctemp,1)));
            end
        end
        function set.schedule(obj,value)
            obj.schedule = value;
            obj.set_temp(0);
        end
    end
end