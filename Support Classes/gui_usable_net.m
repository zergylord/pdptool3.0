%handles screen, log, and plot updates through variable setters
classdef gui_usable_net < handle
    properties
        gui;
    end
    
    properties%( SetObservable = true)
        patno =0;
        seqno=1;
        epochno=0; %set to 0 instead of 1 because display was 1 when no training had occurred
        
        backtickno =1;
        
        updateno = 0;
        cycleno = 0;
        
        
        logoutflag;
        freqrange;
    end
    methods
		function obj = gui_usable_net()
			global net;
			if exist('net') && isfield(net,'gui')
				obj.gui = net.gui;
			else %if undefined
				obj.gui = false;
			end
		end
        function TF = done_updating_tickno(obj)
            global runparams PDPAppdata outputlog
            TF = false;
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag && ~isempty(strcmpi('tick',obj.freqrange))
                    writeoutput(runparams.process,'epoch');
                end
            end
            
            if obj.gui
                if strcmpi(runparams.granularity,'tick')
                    if obj.tickno == obj.numticks+1 %last tick checks for update at end of sequence
                        return
                    end
                    runparams.count_left = runparams.count_left - 1;
                    if obj.tickno == 1
                        runparams.count_left = 0;
                    end
                    if runparams.count_left == 0 
                        pause(.01);
                        %update_display(obj.seqno);%drawnow;
						update_display(obj.environment.true_sequence_index);
                        if PDPAppdata.stopprocess
                                TF = true;
                            return
                        else
                            runparams.count_left = runparams.count;
                        end
                    end
                end
            end
        end
        function TF = done_updating_patno(obj)
            global runparams PDPAppdata outputlog
            TF = false;
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag
                    freq_index = find(ismember(lower(obj.freqrange),{'pattern','patsbyepoch'}));
                    freq = cellstr(obj.freqrange);
                    for fq = 1: numel(freq_index)
                        writeoutput(runparams.process,freq{freq_index(fq)});
                    end
                end
            end
            
            if obj.gui
                if strcmpi(runparams.granularity,'pattern')
                    runparams.count_left = runparams.count_left - 1;
                    if runparams.count_left == 0
                        pause(.01);
						%                         if strcmpi(obj.type,'rbp')
						%                             update_display(obj.seqno);
						%                         else
						%                             update_display(obj.patno);%drawnow;
						%                         end
						update_display(obj.environment.true_sequence_index);
                        if PDPAppdata.stopprocess
                            TF = true;
                            return
                        else
                            runparams.count_left = runparams.count;
                        end
                    end
                end
            end
        end
        function TF = done_updating_stepno(obj)
            global runparams PDPAppdata outputlog
            TF = false;
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag
                    freq_index = find(ismember(lower(obj.freqrange),{'pattern','patsbyepoch'}));
                    freq = cellstr(obj.freqrange);
                    for fq = 1: numel(freq_index)
                        writeoutput(runparams.process,freq{freq_index(fq)});
                    end
                end
            end
            
            if obj.gui
                if strcmpi(runparams.granularity,'step')
                    runparams.count_left = runparams.count_left - 1;
                    if runparams.count_left <= 0
                        update_display(1);%drawnow;
                        if PDPAppdata.stopprocess
                            TF = true;
                            return
                        else
                            runparams.count_left = runparams.count;
                        end
                    end
                end
            end
        end
        function TF = done_updating_seqno(obj)
            global runparams PDPAppdata outputlog
            TF = false;
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag && ~isempty(strcmpi('sequence',obj.freqrange))
                    writeoutput(runparams.process,'epoch');
                end
            end
            if obj.gui
                if strcmpi(runparams.granularity,'sequence') || strcmpi(runparams.granularity,'tick')
                    runparams.count_left = runparams.count_left - 1;
                    if runparams.count_left == 0
						%                         if strcmpi(obj.type,'rbp')
						%                             update_display(obj.seqno);
						%                         else
						%                             update_display(obj.patno);%drawnow;
						%                         end
						update_display(obj.environment.true_sequence_index);
                        if PDPAppdata.stopprocess
                            TF = true;
                            return
                        else
                            runparams.count_left = runparams.count;
                        end
                    end
                end
            end
        end
        
        function TF = done_updating_epochno(obj)
            TF = false;
            global runparams PDPAppdata outputlog
			if obj.tss < obj.train_options.ecrit
				TF = true;
			end
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag && ~isempty(strmatch('epoch',lower(obj.freqrange)))
                    writeoutput(runparams.process,'epoch');
                end
            end
            
            if ~obj.gui
                obj.tce = 0;
                obj.tss = 0;
                return;
            end
%             if strcmpi(runparams.process,'train')
%                 epoch_cap = obj.train_options.nepochs;
%             else
%                 epoch_cap = obj.test_options.nepochs;
%             end
            if strcmpi(runparams.granularity,'epoch') %&& obj.epochno <= epoch_cap
                runparams.count_left = runparams.count_left - 1;
                if runparams.count_left == 0
					if isempty(obj.environment)
						update_display(1);%drawnow;
					else
						update_display(obj.environment.true_sequence_index);
					end
                    if PDPAppdata.stopprocess
                        TF = true;
                        return
                    else
                        runparams.count_left = runparams.count;
                    end
                end
            end
            obj.tce = 0;
            obj.tss = 0;
        end
        function TF = done_updating_cycleno(obj)
            TF = false;
            global runparams PDPAppdata outputlog
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag && ~isempty(strmatch('cycle',lower(obj.freqrange)))
                    writeoutput(runparams.process,'cycle');
                end
            end
            obj.tce = 0;
            obj.tss = 0;
            if ~obj.gui
                return;
            end
            
            if strcmpi(runparams.granularity,'cycle') %&& obj.cycleno <= obj.test_options.ncycles
                runparams.count_left = runparams.count_left - 1;
                if runparams.count_left == 0
                    if isempty(obj.environment)
						update_display(1);%drawnow;
					else
						update_display(obj.environment.true_sequence_index);
					end
                    if PDPAppdata.stopprocess
                        TF = true;
                        return
                    else
                        runparams.count_left = runparams.count;
                    end
                end
            end
        end
        function TF = done_updating_updateno(obj)
            TF = false;
            global runparams PDPAppdata outputlog
            if ~isempty(outputlog) || (exist('out','var') && ~isempty(out.outputfile))
                [obj.logoutflag obj.freqrange] = getloggingdetails(runparams.process);
                
                if obj.logoutflag && ~isempty(strmatch('update',lower(obj.freqrange)))
                    writeoutput(runparams.process,'update');
                end
			end
            if ~obj.gui
                return;
            end
            
            if strcmpi(runparams.granularity,'update')
                runparams.count_left = runparams.count_left - 1;
                if runparams.count_left == 0
                    if isempty(obj.environment)
						update_display(1);%drawnow;
					else
						update_display(obj.environment.true_sequence_index);
					end
                    if PDPAppdata.stopprocess
                        TF = true;
                        return
                    else
                        runparams.count_left = runparams.count;
                    end
                end
            end
        end
        
    end
end