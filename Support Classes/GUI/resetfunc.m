function resetfunc
global net PDPAppdata closer
script = PDPAppdata.netscript{1};
prescript_ws = who;
eval(script); %run the script
postscript_ws = who;
possible_nets = setxor(prescript_ws,postscript_ws);
for i = 1:length(possible_nets)
    if isa(eval(possible_nets{i}),'net')
        net = eval(possible_nets{i});
        PDPAppdata.networks = {net};
        closer = loop_closer(net);
        break
    end
end
end