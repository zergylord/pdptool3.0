function setup(driver,template)
% pdptool;
global net
net.gui = 1;
initsystem(1);
pdp
% pdptool
loadscript(driver);
% net.train_options.nepochs = 1000;
if nargin == 2 && exist(strcat(template,'.tem'),'file')
   loadtemplate (strcat(template,'.tem'));
   launchnet;
   
else
    structexplore(net)
end   

 
% filename = getfilename('xorlog','.mat');
% setoutputlog ('file', filename,'process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','tss'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
% setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'tss'});
% filename = getfilename('xoract','.mat');
% setoutputlog ('file', filename,'process', 'train','frequency', 'patsbyepoch','status', 'on','writemode', 'binary','objects', {'epochno','pools(4).activation'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
% setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'pools(4).activation'}, 'ylim', [0 1]);