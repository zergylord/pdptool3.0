function pdpinit(guistr)
global net PDPAppdata
if nargin <1%default setting
	guistr = 'gui';
end
net.gui = strcmpi(guistr,'gui');
initsystem(net.gui);
[ST,I] = dbstack;
try
    PDPAppdata.netscript = {ST(I+1).file};
catch err
    disp('Please run script in Command Window, not Editor, if you want PDPtool to see your file for logging purposes');
end
% pdp