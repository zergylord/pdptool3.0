%WHEN OPENING THIS GUI, THE CALLER SHOULD GET THE STRING/VALUE FIELDS TO
%THEIR CURRENT VALUES

function optfig = options_gui(mode)
global PDPAppdata net
controls = [];
train_mode = strcmpi(mode,'train'); %test mode when fals
%process continuations '<-'
if train_mode
cont = PDPAppdata.train_options_disp(find(strcmp(PDPAppdata.train_options_disp,'<-'))+1); %variable after the continuation
args = PDPAppdata.train_options_disp(~strcmp(PDPAppdata.train_options_disp,'<-'));
else
cont = PDPAppdata.test_options_disp(find(strcmp(PDPAppdata.test_options_disp,'<-'))+1); %variable after the continuation
args = PDPAppdata.test_options_disp(~strcmp(PDPAppdata.test_options_disp,'<-'));  
end

%change cell array to struct array
for i=1:2:length(args)
    name = args{i};
    cur = args{i+1};
    values = [];
    switch class(cur)
        case 'char'
            switch cur
                case 'num'
                    type = 'edit';
                case 'bool'
                    type = 'checkbox';
            end       
        case 'cell'
            type = 'popupmenu';
            values = cur;
    end
    controls = [controls {struct('name',name,'type',type)}];
    controls{end}.values = values;
end
offset = 10;
height = 22;
pos = 50;
if train_mode
    figure_title = 'Train Options';
else
    figure_title = 'Test Options';
end
h.fig = figure('CloseRequestFcn',@my_closereq,'Visible','off','NumberTitle','off','Name',figure_title,'MenuBar','none',...
    'Color',get(0,'defaultUicontrolBackgroundColor'),...
    'position',[500 500 200 (offset+height+50)]);%[dis from screen-left, dis from screen-bottom, width, height]
h.controls = uicontrol('style','pushbutton','String','Apply','Position',[0 0 100 40]);
h.train_mode = train_mode;
horoff = 0;
for i=1:length(controls)
    if ~isempty(cont) && strcmp(controls{i}.name,cont{1})%if continue hit
        cont = cont(2:end);%continues are alwasy in order
        horoff = horoff + 200+offset;
        cursize = get(h.fig,'position');
        if cursize(3) < horoff
            cursize(3) = cursize(3) +200+offset;
        end
        set(h.fig,'position',cursize)
    elseif i ~= 1
        horoff = 0;
        pos = pos +offset + height;
        cursize = get(h.fig,'position');
        cursize(4) = cursize(4) + (offset+height);
        set(h.fig,'position',cursize)
    end
    %make the controls
    uicontrol('style','text','String',controls{i}.name,'position',[1+horoff pos 100 height]);%static text
    h.controls = [h.controls uicontrol('KeyPressFcn','set(gcbo,''BackgroundColor'',''yellow'')','userdata',controls{i}.name,'style',controls{i}.type,...
        'String',controls{i}.values,'position',[101+horoff pos 100 height])];%control
    if train_mode
        if isfield(net.train_options,controls{i}.name)
            val = net.train_options.(controls{i}.name);
        elseif isprop(net.environment,controls{i}.name)
            val = net.environment.(controls{i}.name);
        end
    else
        if isfield(net.test_options,controls{i}.name)
            val = net.test_options.(controls{i}.name);
        elseif isprop(net.environment,controls{i}.name)
            val = net.environment.(controls{i}.name);
        end
    end
    switch get(h.controls(end),'style')
        case 'checkbox'
            set(h.controls(end),'horizontalAlignment','left');
            set(h.controls(end),'Value',val);
            set(h.controls(end),'callback',{@popup_callback h});
        case 'popupmenu'
            set(h.controls(end),'backgroundColor','white');
            ind = find(strcmpi(get(h.controls(end),'String'),val));
            set(h.controls(end),'Value',ind);
            set(h.controls(end),'callback',{@popup_callback h});
        case 'edit'
            set(h.controls(end),'backgroundColor','white');
            set(h.controls(end),'String',val);
    end
%     set(h.controls(end),'callback',{@callback h});
end
set(h.controls(1),'Callback',{@apply h});
optfig = h.fig;

function h = apply(hObject,eventdata,h)
    for i=2:length(h.controls)
        set_new_value(h.controls(i),h.train_mode);
    end
function h = popup_callback(hObject,eventdata,h)
    set(hObject,'BackgroundColor','yellow')
function set_new_value(hObject,train_mode)
global net
if strcmpi(get(hObject,'style'),'checkbox') 
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'))
else
    set(hObject,'BackgroundColor','white')
end
var = get(hObject,'userdata');
% disp(var);
switch(get(hObject,'style'))
    case 'popupmenu'
        s = get(hObject,'String');
        i = get(hObject,'Value');
        val = s{i};        
    case 'checkbox'
        val = get(hObject,'Value');
    case 'edit'
        val = str2double(get(hObject,'String'));
end
% disp(val);
if train_mode
    if isfield(net.train_options,var)
        net.train_options.(var) = val;
    elseif isprop(net.environment,var)
        net.environment.(var) = val;
    end
else
    if isfield(net.test_options,var)
        net.test_options.(var) = val;
    elseif isprop(net.environment,var)
        net.environment.(var) = val;
    end
end
function my_closereq(src,evnt)

if isempty(findobj('tag','netdisplay'))
    delete(gcf)
else
    set(gcf,'Visible','off');
end