function change_pattern_file(filename)
global net PDPAppdata
net.environment = file_environment(filename);
set_pattern_display();
trpattern_lbox = findobj('tag','trpatlist');
tstpattern_lbox = findobj('tag','tstpatlist');
set(trpattern_lbox,'String',PDPAppdata.trainData,'Value',1);
set(tstpattern_lbox,'String',PDPAppdata.testData,'Value',1);