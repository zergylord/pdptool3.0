classdef dynamic_plot < handle
    properties
        xname;
        yname;
        x;
        y;
        h;
    end
    methods
        function obj = dynamic_plot(xobj,xname,yobj,yname)
            obj.xname = xname;
            obj.yname = yname;
            obj.x = eval(['xobj.' xname]);
            obj.y = eval(['yobj.' yname]);
            addlistener(xobj,xname,'PostSet',...
                @(src,event)listenX(obj,src,event));
            addlistener(yobj,yname,'PostSet',...
                @(src,event)listenY(obj,src,event));
            figure;
            obj.h = plot(obj.x,obj.y);
            set(obj.h,'XDataSource','obj.x');
            set(obj.h,'YDataSource','obj.y');
%             linkdata on
        end
        function listenX(obj,src,event)
            disp('watsssss');
            newx = eval(['event.AffectedObject.' obj.xname]);
            obj.x = [obj.x newx];
            refreshdata(obj.h); 
        end
        function listenY(obj,src,event)
            disp('uppp!');
            newy = eval(['event.AffectedObject.' obj.yname]);
            obj.y = [obj.y newy];
            refreshdata(obj.h);
        end
    end
end

%update only on x axis update; assume only incraments in 1s, check for
%jumps. If jumps, do nothing, else get current value of y and refresh.
%NO