function prepare_gui(guiflag)
global net PDPAppdata
if nargin <1%default setting
	guiflag = 1;
end
net.gui = guiflag;
initsystem(guiflag);
[ST,I] = dbstack;
PDPAppdata.netscript = {ST(I+1).file};
% pdp