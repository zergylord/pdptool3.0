%rewrite in environment?
%update highlights to be in line with ordering
function set_pattern_display()

global PDPAppdata net
e = net.environment;
switch class(e)
    case {'manual_environment','double'}%double for blank environment []
        PDPAppdata.trainData = [1];
        return	
end
if ~e.showPat
	PDPAppdata.trainData = [1];
        return	
end
switch net.type
    case {'bp','pa'}
        
        str = cell(1,length(e.sequences));
        for i=1:length(e.sequences)
            [ipat opat] = e.sequences(i).intervals(1).clamps.pattern;
            str{i} = sprintf('%s : %s | %s', e.sequences(i).intervals(1).name,num2str(ipat),num2str(opat));
        end
        e.sequence_index = 1;
    case 'srn'
        str = cell(1,length(e.sequences));
        for i=1:length(e.sequences)
            npats = length(e.sequences(i).intervals);
            str{(i-1)*npats +1} = sprintf('%s:',e.sequences(i).name);
            for j=1:npats
                [ipat opat] = e.sequences(i).intervals(j).clamps.pattern;
                str{(i-1)*npats +1 + j} = sprintf('%s | %s',num2str(ipat),num2str(opat));
            end
        end
        e.sequence_index = 1;
    case 'cs'
        if ~isempty(e)
        end
        str = [];
    case 'iac'
        if ~isempty(e)
        end
        str = [];
    case 'rbp'
        str = cell(1,length(e.sequences));
        for i=1:length(e.sequences)
            e.sequence_index = i;
            ipat = e.current_patterns.pattern;
            str{i} = sprintf('%s : %s', e.sequences(i).name,num2str(ipat));
        end
        e.sequence_index = 1;
    case 'tdbp'
        str = 1;
end
PDPAppdata.trainData = str;
PDPAppdata.testData = str;