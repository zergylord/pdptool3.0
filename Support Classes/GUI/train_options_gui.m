%WHEN OPENING THIS GUI, THE CALLER SHOULD GET THE STRING/VALUE FIELDS TO
%THEIR CURRENT VALUES

function varargout = train_options_gui(mode)
global PDPAppdata net
controls = [];
%process continuations '<-'
cont = PDPAppdata.train_options_disp(find(strcmp(PDPAppdata.train_options_disp,'<-'))+1); %variable after the continuation

args = PDPAppdata.train_options_disp(~strcmp(PDPAppdata.train_options_disp,'<-'));

%change cell array to struct array
for i=1:2:length(args)
    name = args{i};
    cur = args{i+1};
    values = [];
    switch class(cur)
        case 'char'
            switch cur
                case 'num'
                    type = 'edit';
                case 'bool'
                    type = 'checkbox';
            end       
        case 'cell'
            type = 'popupmenu';
            values = cur;
    end
    controls = [controls {struct('name',name,'type',type)}];
    controls{end}.values = values;
end
offset = 10;
height = 22;
pos = 10;
h.fig = figure('NumberTitle','off','Name','Train Options','MenuBar','none',...
    'Color',get(0,'defaultUicontrolBackgroundColor'),...
    'position',[500 500 200 (offset+height)]);%[dis from screen-left, dis from screen-bottom, width, height]
h.controls = [];
horoff = 0;
for i=1:length(controls)
    if ~isempty(cont) && strcmp(controls{i}.name,cont{1})%if continue hit
        cont = cont(2:end);%continues are alwasy in order
        horoff = horoff + 200+offset;
        cursize = get(h.fig,'position');
        if cursize(3) < horoff
            cursize(3) = cursize(3) +200+offset;
        end
        set(h.fig,'position',cursize)
    elseif i ~= 1
        horoff = 0;
        pos = pos +offset + height;
        cursize = get(h.fig,'position');
        cursize(4) = cursize(4) + (offset+height);
        set(h.fig,'position',cursize)
    end
    %make the controls
    uicontrol('style','text','String',controls{i}.name,'position',[1+horoff pos 100 height]);%static text
    h.controls = [h.controls uicontrol('userdata',controls{i}.name,'style',controls{i}.type,...
        'String',controls{i}.values,'position',[101+horoff pos 100 height])];%control
    if isfield(net.train_options,controls{i}.name)
        val = net.train_options.(controls{i}.name);
    elseif isprop(net.environment,controls{i}.name)
        val = net.environment.(controls{i}.name);
    end
    switch get(h.controls(end),'style')
        case 'checkbox'
            set(h.controls(end),'horizontalAlignment','left');
            set(h.controls(end),'Value',val);
        case 'popupmenu'
            set(h.controls(end),'backgroundColor','white');
            ind = find(strcmpi(get(h.controls(end),'String'),val));
            set(h.controls(end),'Value',ind);
        case 'edit'
            set(h.controls(end),'backgroundColor','white');
            set(h.controls(end),'String',val);
    end
    set(h.controls(end),'callback',{@callback h});
    
end

function h = callback(hObject,eventdata,h)
global net
var = get(hObject,'userdata');
disp(var);
switch(get(hObject,'style'))
    case 'popupmenu'
        s = get(hObject,'String');
        i = get(hObject,'Value');
        val = s{i};        
    case 'checkbox'
        val = get(hObject,'Value');
    case 'edit'
        val = str2double(get(hObject,'String'));
end
disp(val);
if isfield(net.train_options,var)
    net.train_options.(var) = val;
elseif isprop(net.environment,var)
    net.environment.(var) = val;
end
    
