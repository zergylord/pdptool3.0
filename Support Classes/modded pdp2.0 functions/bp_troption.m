function varargout = bp_troption(varargin)
% BP_TROPTION M-file for bp_troption.fig
% This is called when user clicks on
% (i)  'Set Training options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Train panel of the network viewer window.
% It presents training parameters for 'bp' type of network that can be 
% modified and saved.  

% Last Modified 03-May-2007 10:13:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bp_troption_OpeningFcn, ...
                   'gui_OutputFcn',  @bp_troption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bp_troption is made visible.
function bp_troption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bp_troption (see VARARGIN)

% The GUI opening function sets current/existing values of training 
% parameters to the corresponding GUI controls.
movegui('center');
setcurrentvalues();

% Choose default command line output for bp_troption
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bp_troption wait for user response (see UIRESUME)
% uiwait(handles.bp_troption);


% --- Outputs from this function are returned to the command line.
function varargout = bp_troption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function lrateedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function lgrainpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function lgrainedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function nepochsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ecritedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function momentumedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function wrangeedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function muedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tmaxedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function crateedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ncyclesedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function clearvaledit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function trmodepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function nstepsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function istartedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function iduredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function troptnfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
trpatfile = trainpatternspopup_cfn(hObject);
setappdata(findobj('tag','bp_troption'),'patfiles',trpatfile);


function tpsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function errorfnpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tduredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tstartedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function troptnfilepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow')

function lgrainpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow')

function trmodepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function errorfnpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function okbtn_Callback(hObject, eventdata, handles)
apply_options();
delete(handles.bp_troption);

function cancelbtn_Callback(hObject, eventdata, handles)

% Exits without making any changes
delete (handles.bp_troption);


function troptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.troptnfilepop,handles.bp_troption);

function applybtn_Callback(hObject, eventdata, handles)
apply_options();


function writeoutchk_Callback(hObject, eventdata, handles)
writeout_cb(hObject,handles);


function setwritebtn_Callback(hObject, eventdata, handles)
% Calls the module that presents options for writing network output logs.
% Once the setwriteopts window is up, this makes 'train' as the default 
% logging process.

% setwriteopts;
create_edit_log;
logproc = findobj('tag','logprocpop');
if ~isempty(logproc)
    set(logproc,'Value',1);
end


function apply_options()
% This reads in the new parameters and applies changes to the network
% trainopts structure.
% EDITED BY ME
handles = guihandles;
global net PDPAppdata;
prevmode=[];
n = PDPAppdata.networks;

% if isfield(net,'trainopts') 
   tropts = net.train_options;
   tempopts = tropts;
   prevmode = net.train_options.trainmode;
% end
tropts.nepochs = str2double(get(handles.nepochsedit,'String'));
tropts.ncycles = str2double(get(handles.ncyclesedit,'String'));
tmval = get(handles.trmodepop,'Value');
tmstr = cellstr(get(handles.trmodepop,'String'));
tropts.trainmode = tmstr{tmval};
tropts.lflag = get(handles.learnchk,'Value');
tropts.lrate = str2double(get(handles.lrateedit,'String'));
tropts.follow = get(handles.followchk,'Value');
tropts.cascade = get(handles.cascadechk,'Value');
tropts.ecrit = str2double(get(handles.ecritedit,'String'));
tropts.crate = str2double(get(handles.crateedit,'String'));
tropts.wrange = str2double(get(handles.wrangeedit,'String'));
grainstr = get(handles.lgrainpop,'String');
grainval= get(handles.lgrainpop,'Value');
tropts.lgrain = lower(grainstr{grainval});
tropts.lgrainsize = str2double(get(handles.lgrainedit,'String'));
emval = get(handles.errmeaspop,'Value');
emstr = cellstr(get(handles.errmeaspop,'String'));
emopts = cellfun(@(x) x(1:3),emstr,'UniformOutput',false);
tropts.errmeas = emopts{emval};
tropts.wdecay = str2double(get(handles.wdecayedit,'String'));
tropts.momentum = str2double(get(handles.momentumedit,'String'));
tropts.mu = str2double(get(handles.muedit,'String'));
tropts.tmax = str2double(get(handles.tmaxedit,'String'));
tropts.clearval = str2double(get(handles.clearvaledit,'String'));
tropts.fastrun = get(handles.fastrunchk,'Value');
patlist = get(handles.troptnfilepop,'String');
pfiles = getappdata(handles.bp_troption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window    
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','train');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.troptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
    end
end
plistval = get(handles.troptnfilepop,'Value');
tropts.trainset = patlist{plistval};

% -- Calls settrainopts command with only the modified fields of the 
%    trainopts structure. 
update_params = compare_struct(tropts,tempopts);
if ~isempty(update_params)
   args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
   args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
   settrainopts(args{1:end});
   if PDPAppdata.lognetwork
      trstmt = 'settrainopts (';
      for i=1:size(update_params,1)
          field_name = update_params{i,1};
          field_val = update_params{i,2};
          if ischar(field_val)
             trstmt = sprintf('%s''%s'',''%s'',',trstmt,field_name,field_val);
          else
             trstmt = sprintf('%s''%s'',%g,',trstmt,field_name,field_val);
          end
      end
      trstmt = sprintf('%s);',trstmt(1:end-1));
      updatelog(trstmt);
   end
end

% tempopts = orderfields(tempopts,tropts);
% fields = fieldnames(tempopts);
% tempA = struct2cell(tempopts);
% tempB = struct2cell(tropts);
% diffind = cellfun(@isequal,tempA,tempB); 
% diffind = find(~diffind);
% if ~isempty(diffind)
%    trstmt = 'settrainopts (';
%    args = cell(1,numel(diffind)*2);
%    [args(1:2:end-1)] = fields(diffind);   
%    for i = 1 : numel(diffind)
%        fval =  tropts.(fields{diffind(i)});
%        args{i*2} = fval;   
%        if ischar(fval)
%           trstmt = sprintf('%s''%s'',''%s'',',trstmt,...
%                    fields{diffind(i)},fval);
%        else
%           trstmt = sprintf('%s''%s'',%g,',trstmt,fields{diffind(i)},fval);
%        end
%    end
%    settrainopts(args{1:end});
%    if PDPAppdata.lognetwork
%       trstmt = sprintf('%s);',trstmt(1:end-1));
%       updatelog(trstmt);
%    end    
% end
set(findobj('tag','trpatpop'),'String',patlist,'Value',plistval);

% -- Reset interrupt flag if figure module is invoked from within
%    train panel of network viewer and training mode has been altered.
netdisp = findobj('tag','netdisplay');
if ~isempty(netdisp)
   if ~isempty(prevmode) && ~strcmpi(prevmode,tropts.trainmode)
      PDPAppdata.trinterrupt_flag = 0;
   end  
end
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.bp_troption,'patfiles',pfiles);

% -- All edited uicontrols that have KeypressFcn (done through guide) set  
%    to change background color to yellow is changed back to white.This 
%    indicates that modified fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');


function setcurrentvalues
% This sets the current training parameter values to the corresponding GUI
% controls.
%HEAVILY EDITED BY ME
global net;
handles = guihandles;
tropts = net.train_options;
trmode =1;
set(handles.nepochsedit,'String',tropts.nepochs);
set(handles.ncyclesedit,'String',tropts.ncycles);
modeopts = cellstr(get(handles.trmodepop,'String'));
ind = find(strncmpi(tropts.trainmode,modeopts,length(tropts.trainmode)));
if ~isempty(ind)
    trmode = ind;
end
tropts.trainmode = modeopts{trmode};
set(handles.trmodepop,'Value',trmode);
set(handles.learnchk,'Value',tropts.lflag);
set(handles.lrateedit,'String',tropts.lrate);
set(handles.followchk,'Value',tropts.follow);
set(handles.cascadechk,'Value',tropts.cascade);
set(handles.crateedit,'String',tropts.crate);
set(handles.ecritedit,'String',tropts.ecrit);
val = 1;
findgrain = find(strcmpi(get(handles.lgrainpop,'String'),tropts.lgrain));
if ~isempty(findgrain)
    val = findgrain;
end
set (handles.lgrainpop,'Value',val);
set (handles.lgrainedit,'String',tropts.lgrainsize);
errmval = 1;
errmstr =  cellstr(get(handles.errmeaspop,'String'));
errmopts = cellfun(@(x) x(1:3),errmstr,'UniformOutput', false); %get first 3 characters of each errmstr string
finderrm = find(strcmpi(errmopts,tropts.errmeas));
if ~isempty(finderrm)
   errmval = finderrm;
end
tropts.errmeas = errmopts{errmval};
set(handles.errmeaspop,'Value',errmval);
set(handles.wdecayedit,'String',tropts.wdecay);
set (handles.wrangeedit,'String',tropts.wrange);
set (handles.momentumedit,'String',tropts.momentum);
set (handles.muedit,'String',tropts.mu);
set (handles.tmaxedit,'String',tropts.tmax);
set (handles.clearvaledit,'String',tropts.clearval);
set(handles.fastrunchk,'Value',tropts.fastrun);
net.train_options = tropts;  % in case some fields were changed from invalid to valid



% Produces strange behavior is callback function to checkbox not defined.
% So  this must be left as is.
function cascadechk_Callback(hObject, eventdata, handles)





function errmeaspop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow')

function errmeaspop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function wdecayedit_Callback(hObject, eventdata, handles)


function wdecayedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% Produces strange behavior is callback function to checkbox not defined.
% So  this must be left as is.
function learnchk_Callback(hObject, eventdata, handles)



% Produces strange behavior is callback function to checkbox not defined.
% So  this must be left as is.
function followchk_Callback(hObject, eventdata, handles)




% --- Executes on button press in fastrunchk.
function fastrunchk_Callback(hObject, eventdata, handles)
% hObject    handle to fastrunchk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fastrunchk
