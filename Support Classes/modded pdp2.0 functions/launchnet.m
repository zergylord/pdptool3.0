function launchnet()
global net PDPAppdata;
if isempty(net.templatefile)
   PDPAppdata.dispobjects = [];
   structexplore(net);
else
    pause(0.01);
    if PDPAppdata.gui
        if ~isempty(findobj('tag','netdisplay'))
%             openfig('netdisplay.fig','reuse');
            netdisplay;
        else
            netdisplay;
        end
    end
end