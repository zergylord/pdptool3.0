function structexplore(structure)
%TODO:rename to createtemplate
global PDPAppdata;
Subs_Width=55;  % Width of the left pane 
Field_Width=55; % Width of the right pane 
Panel_height=35;
Default_cellsize = 30;
Default_temp_row = 20;
Default_temp_col = 20;
XX.Margin=1; 
XX.Txt.Height=1.4; 
XX.But.Height=1.5; 


Fig_Width=3*XX.Margin+Subs_Width+Field_Width; 
Fig_Height=3*XX.Margin+Panel_height;

Units=get(0,'units'); 
set(0,'units','characters'); 
ss = get(0,'ScreenSize'); 
set(0,'units',Units); 
swidth = ss(3); 
sheight = ss(4);
left = (swidth-Fig_Width)/2; 
bottom = (sheight-Fig_Height)/2; 
rect = [left bottom Fig_Width Fig_Height]; 

fig= figure('visible','off','numbertitle','off',...
           'units', 'characters','Position',rect,...
           'tag','strexplore','Color',get(0,'defaultUicontrolBackgroundColor'),...
           'toolbar','none','Menubar','none','name','Select Display items',...
           'CloseRequestfcn',{@exit_cb});

%left panel
rect(1) = XX.Margin;
rect(2) = XX.But.Height +XX.Margin;
rect(3) = Subs_Width;
rect(4) = Panel_height;
leftpanel = uipanel(fig,'Units','characters','Position',rect,...
                    'BorderType','beveledin','BorderWidth',1,...
                    'title','Select and Add Items');
%right panel                
rect(1) = rect(1) +rect(3);
rect(3) = Field_Width;
rect(4) = Panel_height;
rightpanel = uipanel(fig,'Units','characters','Position',rect,...
                    'BorderType','beveledin','BorderWidth',1,...
                    'title','Selected Items','tag','rightpanel');                

%left tree view
rect(1)= 0.00;
rect(2)=0.305;
rect(3) = 0.99; 
rect(4) = 0.7; 
structname=inputname(1);
handles = guihandles;
setappdata(handles.strexplore,'rootstruct',structure);
Str1={sprintf('+ %s : <struct>',structname)};
currlist(1).parents = [];
currlist(1).sub = [1];
currlist(1).fname = structname;
setappdata(handles.strexplore,'currentlist',currlist);
listview = uicontrol(leftpanel,'style','listbox','String',Str1,...
                     'Units','normalized','Position', rect,...
                     'backgroundcolor',[1 1 1],'horizontalalignment','left',...
                     'tag','treeviewlist','callback',{@edittree,structure,structname});

% right list box
listselview = uicontrol(rightpanel,'style','listbox','Units','normalized',...
                     'Position', rect,'tag','selectedlist',...
                     'backgroundcolor',[1 1 1],'horizontalalignment','left',...
                     'callback',{@selectedlist_change},'Createfcn',{@listselected});   

pos(1) = 0.05;
pos(2) = 0.13;
pos(3) = 0.92;
pos(4) = 0.15;
layoutpanel = uipanel('parent',leftpanel,'Units','normalized','position',pos,'BorderType','etchedin',...
                      'BorderWidth',1,'Title','Layout');
pos(1)= 0.04;
pos(2)= 0.3;
pos(3) = 0.15;
pos(4) = 0.35;
% pdp_handle = findobj('tag','mainpdp');
tempsz = PDPAppdata.templatesize; %getappdata(pdp_handle,'templatesize');
cellsz = PDPAppdata.cellsize; %getappdata(pdp_handle,'cellsize');
if ~isempty(tempsz)
   temp_row = tempsz(1);
   temp_col = tempsz(2);
else
   temp_row = Default_temp_row;
   temp_col = Default_temp_col;
end
if isempty(cellsz)
   tempcell = Default_cellsize;
else
   tempcell = cellsz;
end
layoutrowtxt = uicontrol(layoutpanel,'style','text','HorizontalAlignment','left','Units','normalized',...
                     'Position',pos,'String','Rows :');
                 
pos(1) = pos(1) + pos(3);
pos(3) = 0.12;
pos(4) = 0.45;
layrowedit = uicontrol(layoutpanel,'style','edit','tag','layrowedit','HorizontalAlignment','left',...
                     'Units','normalized','Position',pos,'BackgroundColor','white',...
                     'String',temp_row,'UserData',Default_temp_row);
                 
pos(1) = pos(1) + pos(3)+0.03;
pos(4)=0.35;
laycoltxt = uicontrol(layoutpanel,'style','text','HorizontalAlignment','left','Units','normalized',...
                     'Position',pos,'String','Cols :');
                 
pos(1) = pos(1) + pos(3);
pos(4) = 0.45;
laycoledit = uicontrol(layoutpanel,'style','edit','tag','laycoledit','HorizontalAlignment','left',...
                     'Units','normalized','Position',pos,'BackgroundColor','white',...
                     'String',temp_col,'UserData',Default_temp_col);
                 
pos(1) = pos(1) + pos(3) + 0.03;
% pos(2)= 0.13;
pos(3)=0.18;
pos(4)=0.35;
cellsizetxt = uicontrol(layoutpanel,'style','text','HorizontalAlignment','left','Units','normalized',...
                     'Position',pos,'String','Cell size :');

pos(1) = pos(1) + pos(3) + 0.02;
pos(3)=0.13;
pos(4)=0.45;
cellsize=uicontrol(layoutpanel,'style','edit','tag','cellszedit','HorizontalAlignment','left',...
                     'Units','normalized','Position',pos,'BackgroundColor','white',...
                     'String',tempcell,'UserData',Default_cellsize);

pos(1) = 0.05;
pos(2)=0.03;
pos(3) = 0.25; 
pos(4) = 0.07;
add_display_item = uicontrol(leftpanel,'style','pushbutton','String','Add >>',...
                             'Units','normalized','Position',pos,'enable','off',...
                             'tag','additembtn','callback',{@select_display_item});
pos(1) = pos(1) + pos(3) + 0.05;                         
rem_display_item = uicontrol(leftpanel,'style','pushbutton','String','Remove <<',...
                             'Units','normalized','Position',pos,'enable','off',...
                             'tag','remitembtn','callback',{@delete_selected_item});
pos(1) = pos(1) + pos(3) + 0.05;
save_display_item = uicontrol(leftpanel,'style','pushbutton','String','Done',...
                             'Units','normalized','Position',pos,'enable','on',...
                             'tag','donebtn','callback',{@save_selected_items,structure});
pos(1)=0.04;
pos(2)=0.22;
pos(3)=0.13; 
pos(4)=0.05;

object_descr_lbl = uicontrol(rightpanel,'style', 'text','String', 'Object : ',...
                         'Units','normalized','Position',pos,'tag','objdesclbl',...
                         'HorizontalAlignment','left','visible','off');

pos(1) = pos(1)+ pos(3);
pos(3)= 0.26;
object_descr_name =  uicontrol(rightpanel,'style', 'text','String', ' <object name>',...
                         'Units','normalized','Position',pos,'tag','objdesctxt',...
                         'HorizontalAlignment','left','visible','off');
                     
pos(1) = pos(1) + pos(3);
pos(3) = 0.23;
select_txt = uicontrol(rightpanel,'style','text','String','Select :', ...
                       'Units','normalized','Position',pos,'tag','seltxt',...
                       'visible','off');
                         
pos(1) = pos(1) + pos(3);
pos(2)=pos(2) + 0.01;
pos(3)= 0.29;
select_drop = uicontrol(rightpanel,'style','popupmenu','String',{'Label','Value'},...
                                 'Units','normalized','Position',pos,'tag','selmenu',...
                                  'BackgroundColor','white','visible','off',...
                                  'callback',{@selection_change,structure});
% label related controls                              
pos(1) = 0.05;                              
pos(2) = 0.15;      
pos(3)= 0.2;
label_txt = uicontrol(rightpanel,'style','text','String','Label :',...
                     'Units','normalized','Position',pos,'tag','labeltxt',...
                     'HorizontalAlignment','left','visible','off'); 
uicontrol(rightpanel,'style','checkbox','String','Scalar','Value',1,...
          'Units','normalized','Position',pos,'tag','scalarchk','Visible','off')                 
                 
pos(1) = pos(1) + 0.15;
pos(2) = pos(2) + 0.01;
pos(4) = 0.052;
label_edit = uicontrol(rightpanel,'style','edit','String',' ','Units','normalized',...
                      'Position',pos,'tag','labeledit','HorizontalAlignment','left',...
                      'BackgroundColor','white','visible','off');
pos(1) = 0.66;
pos(2) = 0.17;
pos(3)= 0.29;
label_ok_btn = uicontrol(rightpanel,'style','pushbutton','String','Ok',...
                        'Units','normalized','Position',pos,'tag','okbtn',...
                        'callback',{@create_object,structure},'visible','off');
%end of label related controls
pos(2) = 0.12;
pos(3) = 0.16;
pos(4) = 0.04;
uicontrol(rightpanel,'Style','text','String','vcslope :','tag','vcslopetxt',...
         'Units','normalized','Position',pos,'HorizontalAlignment','left',...
         'visible','off');
pos(1) = pos(1) + pos(3) + 0.01;
pos(3) = 0.12;
uicontrol(rightpanel,'Style','edit','String','1','tag','vcslopeedit',...
         'Units','normalized','Position',pos,'HorizontalAlignment','left',...
         'BackgroundColor','white','visible','off');
% vector related controls
pos(1) = 0.05;                              
pos(2) = 0.16;
pos(3) = 0.23;
pos(4) = 0.05;
vec_orient_txt = uicontrol(rightpanel,'style','text','String','Orientation : ',...
                     'Units','normalized','Position',pos,'tag','vecorienttxt',...
                     'HorizontalAlignment','left','visible','off'); 
pos(2) = 0.1;
vec_dir_txt = uicontrol(rightpanel,'style','text','String','Direction    : ',...
                     'Units','normalized','Position',pos,'tag','vecdirtxt',...
                     'HorizontalAlignment','left','visible','off'); 
pos(1) = pos(1) + pos(3)+ 0.05;
pos(3) = 0.28;
pos(2)=0.18;
pos(4)=0.04;
vec_orient_popup = uicontrol(rightpanel,'style','popupmenu','String',{'Horiz','Vertical','Diagonal'},...
                     'Value',1,'Units','normalized','Position',pos,'tag','vecorientpopup',...
                     'BackgroundColor','white','visible','off');
pos(2) = 0.12;
vec_dir_popup = uicontrol(rightpanel,'style','popupmenu','String',{'Normal','Backwards'},'Value',1,...
                     'Units','normalized','Position',pos,'tag','vecdirpopup',...
                     'BackgroundColor','white','visible','off');
pos(1) = 0.05;                 
pos(2)= 0.05;
uicontrol(rightpanel,'style','text','String','Elements        : ',...
                     'Units','normalized','Position',pos,'tag','vectxt',...
                     'HorizontalAlignment','left','visible','off');
pos(1) = pos(1) + pos(3)+0.01;
pos(2) = pos(2) + 0.01;
pos(3) = 0.15;
pos(4) = 0.04;
uicontrol(rightpanel,'style','popupmenu','String',' ','Units','normalized', ...
          'Position',pos,'tag','vec_cpop_s','BackgroundColor','white',...
          'visible','off');
                      
pos(1) = pos(1) + pos(3);
pos(2) = 0.045;
pos(3) = 0.13;
% pos(4) = 0.052;
uicontrol(rightpanel,'style','text','String','To',...
         'Units','normalized','Position',pos,'tag','vectotxt','visible','off');
                 
pos(1) = pos(1) + pos(3);
pos(2) = pos(2) + 0.015;
pos(3) = 0.15;
% pos(4) = 0.04;
uicontrol(rightpanel,'style','popupmenu','String',' ', ...
                          'Units','normalized','Position',pos,'tag','vec_cpop_e',...
                          'BackgroundColor','white','visible','off');                 
% pos(1) = 0.05;                 
% pos(2) = 0.05; 
pos(1) = 0.05;                              
pos(2) = 0.16;
pos(3) = 0.13;
matrix_sub_r = uicontrol(rightpanel,'style','text','String','Row   : ',...
                     'Units','normalized','Position',pos,'tag','matrowtxt',...
                     'HorizontalAlignment','left','visible','off');
                 
pos(1) = pos(1) + pos(3)+0.01;
pos(2) = pos(2) + 0.01;
pos(3) = 0.15;
pos(4) = 0.04;
matrix_start_r = uicontrol(rightpanel,'style','popupmenu','String',' ', ...
                          'Units','normalized','Position',pos,'tag','mat_rpop_s',...
                          'BackgroundColor','white','visible','off');
                      
pos(1) = pos(1) + pos(3);
pos(2) = 0.16;
pos(3) = 0.13;
% pos(4) = 0.052;
matrix_to_txt = uicontrol(rightpanel,'style','text','String','To',...
                     'Units','normalized','Position',pos,'tag','mattotxt','visible','off');
                 
pos(1) = pos(1) + pos(3);
pos(2) = pos(2) + 0.01;
pos(3) = 0.15;
pos(4) = 0.04;
matrix_end_r = uicontrol(rightpanel,'style','popupmenu','String',' ', ...
                          'Units','normalized','Position',pos,'tag','mat_rpop_e',...
                          'BackgroundColor','white','visible','off');
pos(1) = 0.05;                              
pos(2) = 0.1;
pos(3) = 0.13;
% pos(2) = 0.1
matrix_sub_c = uicontrol(rightpanel,'style','text','String','Col      : ',...
                     'Units','normalized','Position',pos,'tag','matcoltxt',...
                     'HorizontalAlignment','left','visible','off');
                 
pos(1) = pos(1) + pos(3)+0.01;
pos(2) = pos(2) + 0.01;
pos(3) = 0.15;
pos(4) = 0.04;
matrix_start_c = uicontrol(rightpanel,'style','popupmenu','String',' ', ...
                          'Units','normalized','Position',pos,'tag','mat_cpop_s',...
                          'BackgroundColor','white','visible','off');
pos(1) = pos(1) + pos(3);
pos(2) = 0.1;
pos(3) = 0.13;
matrix_to_txt = uicontrol(rightpanel,'style','text','String','To',...
                     'Units','normalized','Position',pos,'tag','mattotxtc','visible','off');
                 
pos(1) = pos(1) + pos(3);
pos(2) = pos(2) + 0.01;
pos(3) = 0.15;
pos(4) = 0.04;
matrix_end_c = uicontrol(rightpanel,'style','popupmenu','String',' ', ...
                          'Units','normalized','Position',pos,'tag','mat_cpop_e',...
                          'BackgroundColor','white','visible','off');
pos(1) = 0.05;
pos(2) = 0.04;
pos(3) = 0.28;
mat_tranpose = uicontrol(rightpanel,'style','checkbox','String','Transpose',...
                        'Units','normalized','Position',pos,'tag','mat_trans_chk',...
                        'HorizontalAlignment','left','visible','off');
                    
pos(1) = pos(1) + pos(3) + 0.06;
pos(3)=0.23;
mat_flip_lr = uicontrol(rightpanel,'style','checkbox','String','Flip-L/R',...
                        'Units','normalized','Position',pos,'tag','mat_lrflip_chk',...
                        'visible','off');
                    
pos(1) = pos(1) + pos(3) + 0.06;                    
mat_flip_ud = uicontrol(rightpanel,'style','checkbox','String','Flip-U/D',...
                        'Units','normalized','Position',pos,'tag','mat_udflip_chk',...
                        'visible','off');                    
                    
set(fig, 'visible','on');

function edittree(hObject,eventdata,structure,structname)
h=guihandles;
v=get(hObject,'Value');
str1=get(hObject,'String');
numpads = getleadspaces(str1{v});
padding(1:numpads)=' ';
index = 1;
currlist = getappdata(h.strexplore,'currentlist');
parents = currlist(v).parents;
fn = currlist(v).fname;
subs = currlist(v).sub;
[t,r]= strtok(str1{v});
if t(1) == '+'
    ind = find(r==':');
    if ~isempty(ind)
        r = r(1:ind-1);
    end
    item = strtrim(r);
    s = structure;
    if ~strcmp(structname, item)
      array_mark = regexp(item,'[()]'); 
      if ~isempty(array_mark)
          index = str2num(item(array_mark(1)+1 : array_mark(2) -1));
      end
      for i=2:numel(parents)
          s=getfield(s,parents{i},{subs(i)});
      end
      s = s.(fn);
    end
    str2 = expandstruct(s,v,index,padding);
    namecollap = {sprintf('%s- %s',padding,item)};
    newstring= [str1{1:v-1},namecollap,str2,str1{v+1:end}];
    set(hObject,'String',newstring);
    set(h.additembtn,'enable','off');    
    return;
end
if t(1) == '-'
    item=strtrim(r);
    numremove = collapsestruct(v);
    nameexp = {sprintf('%s+ %s : <struct>',padding,item)};
    newstring= [str1{1:v-1},nameexp,str1{v+numremove+1:end}];
    set(hObject,'String',newstring);
    set(h.additembtn,'enable','off');   
    currlist(v+1 : v+ numremove)=[];
    setappdata(h.strexplore,'currentlist',currlist);
    return;
end
set(h.additembtn,'enable','on');

function num = collapsestruct(v)
handles = guihandles;
currlist = getappdata(handles.strexplore,'currentlist');
checkp = currlist(v).parents;
num = 0;
if isempty(checkp)
    num = size(currlist,2) - 1;
    return;
end
for i = v+1:size(currlist,2)
    temp = currlist(i).parents{end};
    if ~isempty(strmatch(temp,checkp,'exact'))
       return;
    else       
        num = num + 1;
    end
end
    
function additems = expandstruct(expstruct,val,ind,pad)
handles = guihandles;
currentlist =  getappdata(handles.strexplore,'currentlist');
% root = getappdata(handles.strexplore,'rootstruct');
parents  = currentlist(val).parents;
par_ind = size(parents,2) + 1;
% subs = currentlist(val).sub;
parents{par_ind} = currentlist(val).fname;
additems='';
allitems = fieldnames(expstruct(ind));
k=1;
for i=1:size(allitems,1)
    suff='';
    field = allitems{i};
    f= expstruct(ind).(field);
    if isstruct(f) || isa(f,'net') || isa(f,'pool') || isa(f,'projection')
       suff = '+';
       for j=1:numel(f)  % for array of structures
          item = sprintf('%s(%d) : %s',field,j,'<struct>');
          additems{k} = sprintf('%s  %s %s',pad,suff,item);
          if val  > 1
              currentlist(val+k+1 : end+1) = currentlist(val+k:end);
          end
          currentlist(val+k).parents = parents;
          currentlist(val+k).fname = field; %fieldname
          currentlist(val+k).sub = [currentlist(val).sub,j];
          k = k +1;
       end
    else
       if ~ischar(f)
          if isscalar(f) && isnumeric(f)
             f = num2str(f);
          else
            if isempty(f)
                f='[ ]';
            else    
                f= sprintf('%d x %d array',size(f,1),size(f,2));
            end
          end
       end
       item = sprintf('%s : %s',field,f);   
       additems{k} =sprintf('%s  %s %s',pad,suff,item);
       if (val > 1)
          currentlist(val+k+1 : end+1) = currentlist(val+k:end);
       end    
       currentlist(val+k).parents = parents;
       currentlist(val+k).fname = field; %fieldname
       currentlist(val+k).sub = [currentlist(val).sub,1];
       k = k +1;
    end
end
setappdata(handles.strexplore,'currentlist',currentlist);


function num=getleadspaces(str)
num=0;
k = strfind(str,'+');
l = strfind(str,'-');
if ~isempty(k)
    num = k -1;
end
if ~isempty(l)
    num = l -1;
end
if num == 0
    k= regexp(str,'\w');
    num = k(1) - 1;
end 
    
function select_display_item(hObject, event)
h=guihandles;
index =  get(h.treeviewlist,'Value');
currlist =  getappdata(h.strexplore,'currentlist');
name = currlist(index).fname;
set(h.objdesclbl,'visible','on');
set(h.objdesctxt,'String',name,'visible','on');
set(h.seltxt,'visible','on');
set(h.selmenu,'visible','on','Value',1);
set(h.labeledit,'String',name);
set(h.okbtn,'visible','on');
set(h.mat_rpop_s,'String', ' ');
set(h.mat_rpop_e,'String', ' ');
set(h.mat_cpop_s,'String', ' ');
set(h.mat_cpop_s,'String', ' ');
setvisible(1,0,0,0);

function selection_change(hObject,event,structure)
h=guihandles;
val = get(hObject,'value');
currlist = getappdata(h.strexplore,'currentlist');
index = get(h.treeviewlist,'Value');
structs= currlist(index).parents;
indices = currlist(index).sub;
field = currlist(index).fname;
f = structure;
for i= 2:numel(structs)
    f=getfield(f,structs{i},{indices(i)});
end
obj = f.(field);
mindic = 0;
vecindic = 0;
scindic = 0;
if isscalar(obj)
    scindic = 1;
    set(h.scalarchk,'Value',1);
end
if ~ischar(obj) && ~isscalar(obj)
   if isvector(obj)
       vecindic =1;
       x=numel(obj);
       set(h.vec_cpop_s,'String',1:x,'Value',1);
       set(h.vec_cpop_e,'String',1:x,'Value',x);       
    else
        [x,y]=size(obj);
        set(h.mat_rpop_s,'String',1:x,'Value',1);
        set(h.mat_rpop_e,'String',1:x,'Value',x);        
        set(h.mat_cpop_s,'String',1:y,'Value',1);
        set(h.mat_cpop_e,'String',1:y,'Value',y);        
        mindic = 1;
    end
end
setvisible(val,scindic,vecindic,mindic);


function setvisible(selected,scchk,vecindicator,matindicator)
vecpat = 'vec*';
matpat='mat*';
labelpat='label*';
setonpat='';
setoffpat='';
if (selected == 1)
    setoffpat=sprintf('%s|%s',vecpat,matpat); %'val* ';
    setonpat=labelpat;
else
    set(findobj('-regexp','tag','vcslope*'),'Visible','on');    
    if vecindicator == 1
        setoffpat=sprintf('%s|%s',labelpat,matpat);
        setonpat=vecpat;
    else
        setoffpat=sprintf('%s|%s',labelpat,vecpat);
        if matindicator == 1
            setonpat=matpat;
        end     
    end
end
if scchk==1
   setonpat=sprintf('%s|scalar*',setonpat);
else
   setoffpat=sprintf('%s|scalar*',setoffpat);
end
handles_on= findobj(gcf,'-regexp','tag',setonpat);
handles_off = findobj(gcf,'-regexp','tag',setoffpat);
set(handles_on,'Visible','on');
set(handles_off,'Visible','off');

 
function create_object(hObject,event,structure)
handles = guihandles;
val =  get(handles.treeviewlist,'Value');
selected = get(handles.selmenu,'Value');
currlist =  getappdata(handles.strexplore,'currentlist');
displayobj = getappdata(handles.strexplore,'displayobjects');
index  = numel(displayobj) + 1;
structs= currlist(val).parents;
indices = currlist(val).sub;
field = currlist(val).fname;
cstart=1;
cend=1;
f = structure;
panelitem = sprintf('%s.',structs{1});
for i= 2:numel(structs)
    f=getfield(f,structs{i},{indices(i)});
    panelitem = sprintf('%s%s(%d).',panelitem,structs{i},indices(i));
end
panelitem=sprintf('%s%s',panelitem,field);
rangestr='';
obj = f.(field);
displayobj(index).source ={structs(2:end),indices(2:end),field};
displayobj(index).position = [];
displayobj(index).orient ='Horiz';
displayobj(index).dir = 'Normal';
displayobj(index).labeltxt = '';
displayobj(index).rowrange=[];
displayobj(index).colrange=[];
displayobj(index).vcslope = 1;
if (selected == 1)
    displayobj(index).disptype = 'label';
    displayobj(index).labeltxt = get(handles.labeledit,'String');
    panelitem= sprintf('label : %s | %s',displayobj(index).labeltxt,panelitem);
    displayobj(index).source=[];
else
    if ischar(obj) || (isscalar(obj) && (get(handles.scalarchk,'Value') ==1)) %isscalar(obj)
        displayobj(index).disptype = 'scalar';
    else
        if isvector(obj)
           if iscell(obj)
              displayobj(index).disptype='label';
           else
              displayobj(index).disptype='vector'; 
           end
           if strcmpi(get(handles.vecorientpopup,'Visible'),'on')
              orientstrs =  get(handles.vecorientpopup,'String');
              orientind = get(handles.vecorientpopup,'Value');
              dirstrs = get(handles.vecdirpopup,'String');
              dirind = get(handles.vecdirpopup,'Value');
              displayobj(index).orient = orientstrs{orientind};        
              displayobj(index).dir = dirstrs{dirind};
              veccol_s = cellstr(get(handles.vec_cpop_s,'String'));
              veccol_e = cellstr(get(handles.vec_cpop_e,'String'));
              veccol_sv = get(handles.vec_cpop_s,'Value');
              cstart = str2double(veccol_s{veccol_sv});
              veccol_ev = get(handles.vec_cpop_e,'Value');
              cend = str2double(veccol_e{veccol_ev});
           end
            displayobj(index).colrange =[cstart cend];
            rangestr=mat2str([cstart cend]);
        else
            displayobj(index).disptype='matrix';
            if get(handles.mat_trans_chk,'Value') == 1
               displayobj(index).orient='Transpose';
            else
               displayobj(index).orient='Normal';
            end
            displayobj(index).dir = 'Normal';
            lrchk = get(handles.mat_lrflip_chk,'Value');
            udchk = get(handles.mat_udflip_chk,'Value');
            if lrchk == 1 && udchk ==1
               displayobj(index).dir = 'Both';
            else
               if lrchk == 1
                  displayobj(index).dir ='Left/Right flip';
               end
               if udchk == 1
                  displayobj(index).dir ='Up/Down flip';
               end
            end
            matrow_s = cellstr(get(handles.mat_rpop_s,'String'));
            matrow_sv = get(handles.mat_rpop_s,'Value');
            rstart = str2double(matrow_s{matrow_sv});
            matrow_e = cellstr(get(handles.mat_rpop_e,'String'));
            matrow_ev = get(handles.mat_rpop_e,'Value');
            rend = str2double(matrow_e{matrow_ev});
            matcol_s = cellstr(get(handles.mat_cpop_s,'String'));
            matcol_sv = get(handles.mat_cpop_s,'Value');
            cstart = str2double(matcol_s{matcol_sv});
            matcol_e = cellstr(get(handles.mat_cpop_e,'String'));
            matcol_ev = get(handles.mat_cpop_e,'Value');
            cend = str2double(matcol_e{matcol_ev});
            displayobj(index).rowrange=[rstart rend];
            displayobj(index).colrange =[cstart cend];
            rangestr=sprintf('%s ;%s',mat2str([rstart rend]),mat2str([cstart cend]));
        end
        vcs = str2double(get(handles.vcslopeedit,'String'));
        if ~isnan(vcs)
           displayobj(index).vcslope = vcs;
        end
    end
    panelitem = sprintf('%s : %s | %s %s',displayobj(index).disptype,field,panelitem,rangestr);
end
setappdata(handles.strexplore,'displayobjects',displayobj);
rpanelstrs = get(handles.selectedlist,'String');
newitem = 1;
simitems =find(strcmpi(rpanelstrs,panelitem));
if ~isempty(simitems)
    panelitem=sprintf('%s (%d)',panelitem,numel(simitems));
end
if ~all(isspace((rpanelstrs{1})))
   newitem = numel(rpanelstrs) + 1;
end
rpanelstrs{newitem} = panelitem;
set(handles.selectedlist,'String',rpanelstrs);

% set(handles.objdesclbl,'visible','off');
% set(handles.objdesctxt,'visible','off');
% set(handles.seltxt,'visible','off');
% set(handles.selmenu,'visible','off','Value',1);
% set(handles.okbtn,'visible','off');
% set(handles.mat_rpop_s,'String', ' ');
% set(handles.mat_rpop_e,'String', ' ');
% set(handles.mat_cpop_s,'String', ' ');
% set(handles.mat_cpop_s,'String', ' ');
% handles_lbl = findobj(gcf,'-regexp','tag','label*|scalar*');
% handles_val = findobj(gcf,'-regexp','tag','vec*');
% handles_mat = findobj(gcf,'-regexp','tag','mat*');
% set(handles_lbl,'visible','off');
% set(handles_val,'visible','off');
% set(handles_mat,'visible','off');
panelobjs = findobj(allchild(handles.rightpanel),'-not','tag','selectedlist');
set(panelobjs,'Visible','off');

function selectedlist_change(hObject,event)
handles = guihandles;
set(handles.remitembtn,'enable','on');

function delete_selected_item(hObject,event)
handles = guihandles;
dispobj = getappdata(handles.strexplore,'displayobjects');
index  = get(handles.selectedlist,'Value');
rpanelstrs = get(handles.selectedlist,'String');
rpanelstrs(index)=[];
dispobj(index)=[];
if numel(rpanelstrs) == 0
   rpanelstrs = {' '};
   set(hObject,'enable','off');
end
set(handles.selectedlist,'String',rpanelstrs,'Value',1);
setappdata(handles.strexplore,'displayobjects',dispobj);

function save_selected_items(hObject,event,structure)
handles = guihandles;
list =  get(handles.selectedlist,'String');
tempsz = [str2double(get(handles.layrowedit,'String')) str2double(get(handles.laycoledit,'String'))];
cellsz= str2double(get(handles.cellszedit,'String'));
if ~isnumeric(tempsz) || isempty(tempsz)
    tempsz=[get(handles.layrowedit,'UserData') get(handles.laycoledit,'UserData')];
end
if ~isnumeric(cellsz) || isempty(cellsz)
    cellsz=get(handles.cellszedit,'UserData');
end
setdisplay(list,tempsz,cellsz);

function listselected(hObject,event)
global PDPAppdata;
% pdp_handle = findobj('tag','mainpdp');
if isempty (PDPAppdata.dispobjects) %getappdata(pdp_handle,'dispobjects'))
   rpanelstrs={' '};
else
   dispobj = PDPAppdata.dispobjects; %getappdata(pdp_handle,'dispobjects');    
   for i=1:numel(dispobj)
       src= dispobj(i).source;
       if isempty(src)
          panelitem= sprintf('%s : %s |static',dispobj(i).disptype,dispobj(i).labeltxt);
       else
           panelitem = 'net.';
           structs = src{1};
           indices = src{2};
           field = src{3};       
           for j=1:numel(structs)
               panelitem = sprintf('%s%s(%d).',panelitem,structs{j},indices(j));
           end
           panelitem = sprintf('%s : %s | %s%s',dispobj(i).disptype,field,panelitem,field);
           if ~isempty(dispobj(i).rowrange)
               panelitem = sprintf('%s%s',panelitem,mat2str(dispobj(i).rowrange));
           end
           if ~isempty(dispobj(i).colrange)
               panelitem = sprintf('%s%s',panelitem,mat2str(dispobj(i).colrange));
           end
       end
       rpanelstrs{i}=panelitem;
   end
   setappdata(findobj('tag','strexplore'),'displayobjects',dispobj);
end
set(hObject,'String',rpanelstrs);

function exit_cb(hObject,event)
dispfig = findobj('tag','dispfig');
if ~isempty(dispfig)
    delete(dispfig);
end
delete(hObject);
