function runprocess(varargin)
global closer net CALLIND runparams trpatn tstpatn PDPAppdata binarylog;
runparams.process = 'test';
runparams.mode = 'run';
runparams.granularity = 'pattern';
runparams.count = 1;
runparams.pattest = 0;
runparams.alltest = 1;
runparams.fastrun = 0;
runparams.nepochs = [];
runparams.range= [];
runparams = parse_pairs(runparams,varargin);

runparams.count_left = runparams.count;

if strcmpi(runparams.mode,'step')
%      closer.step_listen.Enabled = true;
    PDPAppdata.stopprocess = 1;
end

fastfuncname = sprintf('%s_run_fast',net.type);
options = [runparams.process '_options'];
if isfield(net.(options),'fastrun') && net.(options).fastrun==1 && exist(fastfuncname,'file')==2
   runfuncname = fastfuncname;
else
   runfuncname = sprintf('%s_run',net.type);
end   
% if runparams.fastrun == 1 && exist(fastfuncname,'file')==2
%    runfuncname = fastfuncname;
% else
%    runfuncname = sprintf('%s_run',net.type);
% end
if ~PDPAppdata.gui
   if ~isempty(trpatn)
      PDPAppdata.trinterrupt_flag = 1;
   end
   if ~isempty(tstpatn)
      PDPAppdata.tstinterrupt_flag = 1;
   end
   net.(runparams.process);
   fprintf(1,'%s process completed for range %s\n',runparams.process,...
           mat2str(runparams.range));    
   return;
end
if isempty(CALLIND)  %global variable is empty if runprocess is called from logfile instead of button callback
    trpanel = findobj('tag','trainpanel');
    tstpanel = findobj('tag','testpanel');
    trpanelobjs = findobj(allchild(trpanel),'-not','tag','trainradio');
    tstpanelobjs = findobj(allchild(tstpanel),'-not','tag','testradio');
    runparams.mode = 'run';
    switch runparams.process
        case 'train'
              set(findobj('tag','countedit'),'String',runparams.count);
              granlist = get(findobj('tag','granlist'),'String');
              x = find(strncmp(runparams.granularity,granlist,length(runparams.granularity)));
              if ~isempty(x)
                 set(findobj('tag','granlist'),'Value',x(1));
              end
              set(trpanelobjs,'enable','on');
              set(tstpanelobjs,'enable','off');
              set(findobj('tag','testradio'),'Value',0);
              set(findobj('tag','trainradio'),'Value',1);
              set(findobj('tag','fastchk'),'Value',runparams.fastrun);
              if ~isempty(trpatn)
                 PDPAppdata.trinterrupt_flag = 1;
              end
        case 'test'
             set(findobj('tag','testcountedit'),'String',runparams.count);
             set(findobj('tag','tallchk'),'Value',runparams.alltest);
             set(findobj('tag','tstpatchk'),'Value',runparams.pattest);
             set(get(findobj('tag','testpanel'),'children'),'enable','on');
             set(trpanelobjs,'enable','off');
             set(findobj('tag','trainradio'),'Value',0);
             set(findobj('tag','testradio'),'Value',1); 
             if ~isempty(tstpatn)
                PDPAppdata.tstinterrupt_flag = 1;
             end         
        otherwise 
             set(findobj('tag','testcountedit'),'String',runparams.count);
             set(findobj('tag','tallchk'),'Value',runparams.alltest);
             set(findobj('tag','tstpatchk'),'Value',runparams.pattest);       
    end
end
setvisibility(runparams.process,runparams.mode,'pre');
if strcmpi(runparams.process,'test')
    if ~runparams.alltest
        net.next_seqno = get(findobj('tag','tstpatlist'),'Value');
    elseif ~net.alltest %if alltest was previously off but is now on, reset indices
        net.next_seqno = 1;
        net.next_tickno = 2;
    end
        
    net.alltest = runparams.alltest;
end
% if last tick
%     patno = ppatno +1;
%     tickno = 1
%     update screen;
%     else
    net.(runparams.process);
if PDPAppdata.stopprocess == 1
    PDPAppdata.stopprocess = 0;
%     closer.restore_values;
%      net.restore_values;
end
% global tester;
% tester = dummy(5000000);
% tester.test;
% tester.train;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end
setvisibility(runparams.process,runparams.mode,'post');

function setvisibility(process,mode,when)
persistent modeon nmodeoff modeoff cpbtns optbtn;
% persistent btns modeon nmodeoff modeoff optbtn cpbtns;
switch when
    case 'pre'
%         clear btns modeon nmodeoff modeoff;
    case 'post'
        set(modeon,'Visible','off');
        set(nmodeoff,'enable','on');
        set(modeoff,'Visible','on');
        set(optbtn,'enable','on');
        set(cpbtns,'enable','on');
        return;
    otherwise
        disp('should not happen in runprocess,setvisibility');
end
cpbtns =findobj('tag','resetbtn','-or','tag','newstartbtn','-or','tag',...
               'loadwtbtn','-or','tag','savewtbtn');
set(cpbtns,'enable','off');
trpanel = findobj('tag','trainpanel');
tstpanel = findobj('tag','testpanel');
if strcmpi(process,'train')
    optbtn=findobj(allchild(trpanel),'-not','style','pushbutton');
    set(optbtn,'enable','inactive');
    if strcmpi(mode,'run')
        modeon = findobj('tag','runbtn_on');
        set(modeon,'Visible','on');
        nmodeoff=findobj('tag','stepbtn_off');
        set(nmodeoff,'Visible','on','enable','off');
        set(findobj('tag','stepbtn_on'),'Visible','off');
        modeoff=findobj('tag','runbtn_off');
        set(modeoff,'Visible','off');
        pause(0.05);
%         try 
%             uiwait(findobj('tag','netdisplay'),0.05);
%         catch
%             disp('uiwait failed');
%         end
    else
        modeon = findobj('tag','stepbtn_on');        
        set(modeon,'Visible','on');
        nmodeoff=findobj('tag','runbtn_off');        
        set(nmodeoff,'Visible','on','enable','off');
        set(findobj('tag','runbtn_on'),'Visible','off');
        modeoff=findobj('tag','stepbtn_off');        
        set(modeoff,'Visible','off');
        pause(0.05);
    end
else

    optbtn=findobj(allchild(tstpanel),'-not','style','pushbutton');
    set(optbtn,'enable','inactive');    
    if strcmpi(mode,'run')
        modeon=findobj('tag','testrunbtn_on');
        set(modeon,'Visible','on');
        nmodeoff=findobj('tag','teststepbtn_off');
        set(nmodeoff,'Visible','on','enable','off');
        set(findobj('tag','teststepbtn_on'),'Visible','off');
        modeoff=findobj('tag','testrunbtn_off');
        set(modeoff,'Visible','off');
        pause(0.05);        
%         try
%            uiwait(findobj('tag','netdisplay'),0.05);
%         catch
%         end
    else
        modeon=findobj('tag','teststepbtn_on');
        set(modeon,'Visible','on');
        nmodeoff=findobj('tag','testrunbtn_off');
        set(nmodeoff,'Visible','on','enable','off');
        set(findobj('tag','testrunbtn_on'),'Visible','off');
        modeoff=findobj('tag','teststepbtn_off');
        set(modeoff,'Visible','off');
    end
end
