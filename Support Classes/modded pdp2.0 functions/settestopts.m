function settestopts (varargin)
global net PDPAppdata;
switch net.type
    case 'pa'
         validobjs={'nepochs';'trainmode';'lflag';'lrule';'lrate';'ecrit'; ...
                    'noise';'actfunction';'temp';'testset'};
    case {'bp','srn'}
         validobjs={'nepochs';'ncycles';'trainmode';'lflag';'cascade';'ecrit';...
                    'crate';'mu';'tmax';'clearval';'lrate';'lgrain';...
                    'lgrainsize';'errmeas';'wdecay';'follow';'momentum';...
                    'fastrun';'testset'};  
    case 'rbp'
         validobjs={'nepochs';'trainmode';'lflag';'cascade';'ecrit';...
                    'errmargin';'lrate';'lgrain';'lgrainsize';'errmeas';...
                    'wdecay';'follow';'momentum';'fastrun';'testset'};                   
    case 'cs'
         validobjs={'nupdates';'ncycles';'actfunction';'istr';'estr';'clamp';...'harmony';'kappa';...
                    'temp';'annealsched';'testset'};
    case 'cl'
         validobjs={'nepochs';'trainmode';'lflag';'lrate';'testset'};
    case 'iac'
         validobjs={'ncycles';'actfunction';'estr';'alpha';'gamma';'max';'min';...
                    'decay';'rest';'testset'};
    case 'tdbp'
         validobjs={'nepochs';'stepcutoff';'mu';'clearval';'testset';...
                    'policy';'epsilon';'showvals';'temp';'runstats'};                
    otherwise
       fprintf(1,'ERROR: Invalid network type\n');
        return;
end
objs = varargin(1:2:end-1);
values = varargin(2:2:end);
X = find(~(ismember(objs,validobjs)));
Y = find(ismember(objs,validobjs));
if ~isempty(X)
    invobjs = objs(X);
    fprintf(1,'ERROR : invalid object ''%s'' in settestopts command\n',invobjs{1:end});
end
tstopts = net.test_options;
tempopts = tstopts;
for i=1:numel(Y)
    ind =Y(i);
    tstopts.(objs{ind})=values{ind};
    if strcmpi(net.type,'tdbp') && strcmpi(net.netmode,'sarsa') && strcmpi(values{ind},'defer')
        fprintf(1,'ERROR: Policy type ''defer'' is not allowed for SARSA networks.  Switching to ''egreedy''.\n');
    end
    outpools = find(strcmpi({net.pools.type},'output'));
    if strcmpi(net.type,'tdbp') && strcmpi(net.pool(outpools(1)).actfunction,'linear') && strcmpi(values{ind},'linearwt')
        fprintf(1,'ERROR: Policy type ''linearwt'' is not allowed for linear outputs.  Switching to ''egreedy''.\n');
    end    
    if strcmpi(objs{ind},'actfn')
        actshort = {'st','li','cs','lt'};
        actlong = {'stochastic','linear','continuous sigmoid',...
                  'linear threshold'};
        actfn =1;
        ind = find(strcmpi(actshort,tstopts.actfn));
        if isempty(ind)
           ind = find(strncmpi(tstopts.actfn,actlong,length(tstopts.actfn)));
           if ~isempty(ind)
              actfn = ind(1);
           end
        else
            actfn = ind;
        end
        tstopts.actfn = actshort{actfn};           
    end    
end
netdisp = findobj('tag','netdisplay');
net.test_options = tstopts;
n = PDPAppdata.networks;
n{net.num} = net;
PDPAppdata.networks = n;
if strcmpi(net.type,'cs') && ~isempty(net.schedule)
    net.annealing(net.cycleno);
    if ~isempty(netdisp)
        update_display(0);
    end
end
if ismember('testset',objs)
   tset = net.test_options.testset;
   dat = [];
   patn = [];
   pfiles = PDPAppdata.patfiles;   
   if ~strcmpi(tset,'none')
       patn = find(strcmpi(tset,pfiles(:,1)));
       if isempty(patn) % if filename instead of setname
          patn = find(strcmpi(tset,pfiles(:,2))); 
       end
       if isempty(patn)
          fprintf(1,'ERROR : %s - Testset not defined\n',tset);
          return;
       end
       patn = patn(1); %filename need not be unique       
       dat = PDPAppdata.patfiles{patn,3};
   end
   PDPAppdata.testData = dat;
   if PDPAppdata.gui && ~strcmp(net.type,'tdbp')    % no patterns for net type 'tdbp'
      patlist = get(findobj('tag','tstpatpop'),'String');
      pn = 1;
      if ~isempty(patn)
         pn = find(strcmpi(patlist,pfiles(patn,1)));          
      end
      set(findobj('tag','tstpatpop'),'Value',pn);             
      if ~isempty(netdisp)
         tstbuttons = findobj(findobj('tag','testpanel'),'Style','pushbutton');       
         if isempty(dat)
            set(findobj('tag','tstpatlist'),'String',{' '},'Value',1);
            set(tstbuttons,'enable','off');
         else
            if get(findobj('tag','testradio'),'Value')
               set(tstbuttons,'enable','on');
            end                         
            fill_patterns(findobj('tag','tstpatlist'),PDPAppdata.testData);
         end
      end  
   end
end
if ~PDPAppdata.gui
   objstr = sprintf(', %s',objs{1:end});
   fprintf(1,'Testing option(s) %s set to specified value(s)\n',objstr);
   if PDPAppdata.lognetwork
      update_params = compare_struct(tstopts,tempopts);
      if ~isempty(update_params)
           args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
           args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
           tststmt = 'settestopts (';
           for i=1:size(update_params,1)
               field_name = update_params{i,1};
               field_val = update_params{i,2};
               if ischar(field_val)
                  tststmt = sprintf('%s''%s'',''%s'',',tststmt,field_name,field_val);
               else
                  tststmt = sprintf('%s''%s'',%g,',tststmt,field_name,field_val);
               end
           end
           tststmt = sprintf('%s);',tststmt(1:end-1));
           updatelog(tststmt);
      end
   end   
end