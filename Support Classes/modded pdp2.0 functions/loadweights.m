function loadweights(varargin)
if nargin < 1
   error('File name not specified');
end
loadparams.file = '';
loadparams.type='';
if nargin > 1
   if ~strncmpi('file',varargin{1},length(varargin{1}))
       fprintf(1,'ERROR: First argument must be property name ''file''\n');
       return;
   end
   loadparams = parse_pairs(loadparams,varargin);
else
   loadparams.file = varargin{1};
end
global net PDPAppdata;
netdisp = findobj('tag','netdisplay');
lasterror('reset');
try
    X= load(loadparams.file,'-mat');
    struc =fieldnames(X);
    Wtstruct= X.(struc{1});
    for i=1:numel(Wtstruct)
        plnum = Wtstruct(i).pooln;
        prnum = Wtstruct(i).projn;
        net.pools(plnum).projections(prnum).using.weights = Wtstruct(i).weight;
    end
catch
    e = lasterror;
    if isempty(regexp(e.message,'not a binary MAT-file','once'))
       fprintf(1,'ERROR: %s',e.message);
       return;
    else
        fid = fopen(loadparams.file,'r');
        numlines = 0;
        while ~feof(fid)
            tline = fgetl(fid);
            numlines = numlines +1;
        end
        fclose(fid);
        r = 0;
        c = 0;
        try
            if strcmpi(loadparams.type,'old-ascii')
               [lines] = textread(weightFile,'%s','delimiter','\n');
               for i = 1 : 3 : numel(lines)-2
                   np = str2double(lines{i,1});
                   nj = str2double(lines{i+1,1});
                   net.pool(np).proj(nj).weight(:) = str2num(lines{i+2});
               end                
            else
                while(1)
                    if (r >= numlines)
                        break;
                    end
                    np = dlmread(loadparams.file,'\t',[r,c,r,c]);
                    r = r+1;
                    nj = dlmread(loadparams.file,'\t',[r,c,r,c]);
                    r = r+1;
                    sz = size(net.pools(np).projections(nj).using.weights);
                    range =[r,c, r+sz(1)-1, c + sz(2)-1];
                    net.pools(np).projections(nj).using.weights = dlmread(loadparams.file,'\t',range);
                    r = r+sz(1);
                    c = 0;
                end
            end
         catch 
            e=lasterror;
            fprintf(1,['Loadweights Error : %s\nThe weight file was possibly saved in a version of pdptool older than 2.02.\n'...
                'If so, try using command loadweights(<your_weight_file>,''type'',''old-ascii'')\n'...
                'Check your version with command pdptool_version'],e.message);
        end
    end
end
if ~isempty(netdisp)
    update_display(1);
end
if ~PDPAppdata.gui
   fprintf(1,'Weight file %s loaded\n',loadparams.file);
end
