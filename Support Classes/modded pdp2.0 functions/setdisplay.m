function setdisplay(left_panel_list,temp_size,cell_size)
Panel_width = 240;
Margin = 15;
Button_height = 50;
% % Get screensize in pixels
Units=get(0,'units'); 
set(0,'units','pixels'); 
ss = get(0,'ScreenSize'); 
set(0,'units',Units);

grid_width = temp_size(2) * cell_size;
grid_height = temp_size(1) * cell_size;
Panel_height = grid_height;
Fig_Width = 3*Margin + Panel_width + grid_width;
Fig_Height = 3*Margin + Button_height+ grid_height;
swidth = ss(3); 
sheight = ss(4);
left = (swidth-Fig_Width)/2;   % for center alignment
bottom = (sheight-Fig_Height)/2; % for center alignment
rect = [left bottom Fig_Width Fig_Height];
dispfig = figure('Name','Set display Positions','Visible','on','MenuBar','none',...
                'Color',get(0,'defaultUicontrolBackgroundColor'),'tag','dispfig',...
                'Toolbar','figure', 'NumberTitle','off','Units','pixels','Position',rect,...
                 'ResizeFcn',{@dispfig_resize},'WindowStyle','normal',...
                 'Interruptible','off','DockControls','off','WindowButtonMotionFcn',{@showtextlabel},...'WindowButtonDownFcn',{@setplotoff},...
                 'CloseRequestFcn',{@cancel_template});

setappdata(dispfig,'originalsize',rect);          
unitsize=cell_size - 2;             
setappdata(dispfig,'unitsize',unitsize);
setappdata(dispfig,'templatesize',temp_size);

%left panel
rect(1) = Margin;
rect(2) = Button_height+2*Margin;
rect(3) = Panel_width;
rect(4) = Panel_height;
leftpanel=uipanel(dispfig,'Units','pixels','Position',rect,...
                  'BorderType','beveledin','BorderWidth',1,...
                  'tag','objpanel','title','Display Objects');

%right panel grid
rect(1) = rect(1) + rect(3)+ Margin;
rect(2) = Button_height + 2* Margin;
rect(3)=grid_width;
rect(4)=grid_height;
gridtick = 1/cell_size;
xlimit = [0 temp_size(2)/cell_size];
ylimit = [0 temp_size(1)/cell_size];
obj_grid = axes('Tag','gridpanel','Units','pixels','Box','on',...
             'Position',rect,'GridLineStyle','-','Xgrid','on','Ygrid','on',...
             'Color',get(0,'defaultUicontrolBackgroundColor'),'XtickLabel',[],'YtickLabel',[],...
             'XLim',xlimit,'YLim',ylimit,'Xtick',(0:gridtick:xlimit),'Ytick',(0:gridtick:ylimit),...
             'XColor',[0.502,0.502,0.502],'YColor',[0.502,0.502,0.502],...
             'Selectionhighlight','off','ButtonDownFcn',{@dispfig_buttondown});
                     
uicontrol('Style','pushbutton','Units','pixels',...
          'Position',[Margin Margin+5 Panel_width Button_height-10],...
          'String','Add static label','tag','addlabelbtn','Callback',{@set_btn_appdata});           

%Button group
rect(1)= 2*Margin+Panel_width;
rect(2) =Margin;
rect(3)=grid_width;
rect(4) = Button_height;
btngroup = uipanel('tag','buttons','Units','pixels','Position',rect,...
                         'BorderType','none');                   
%save button  
rect(1) = 0.025;
rect(2) = 0.1;
rect(3)= 0.2;
rect(4)= 0.8;
uicontrol(btngroup,'Style','pushbutton','Units','normalized',...
                   'Position',rect,'String','Save','tag','savebtn',...
                   'Callback',{@save_display_objects},...
                   'TooltipString','Click to save template.Toggle tool for editing annotations must be in off state for this');
              
%Reset button
rect(1) = rect(1) + rect(3) + 0.05;
uicontrol(btngroup,'Style','pushbutton','Units','normalized',...
                   'Position',rect,'String','Reset','tag','resetbtn',...
                   'Callback',{@reset_display_object},...
                   'TooltipString','Click to reset selected display object.Toggle tool for editing annotations must be in off state for this');

%Reset all button               
rect(1) = rect(1) + rect(3) + 0.05;
uicontrol(btngroup,'Style','pushbutton','Units','normalized',...
                   'Position',rect,'String','Reset All','tag','resetallbtn',...
                   'Callback',{@reset_all_display_objects},...
                   'TooltipString','Click to reset all display objects.Toggle tool for editing annotations must be in off state for this');

%Cancel button
rect(1) = rect(1) + rect(3) + 0.05;
uicontrol(btngroup,'Style','pushbutton','Units','normalized',...
                   'Position',rect,'String','Cancel','tag','cancelbtn',...
                   'Callback',{@cancel_template},...
                   'TooltipString','Click to cancel template creation and exit.Toggle tool for editing annotations must be in off state for this');
             
% get(obj_grid,'Position');      
% set(obj_grid,'Units','pixels');
% get(obj_grid,'Position');
% set(obj_grid,'Units','normalized');
%left tree view
rect(1)= 0.02;
rect(2)=0.02;
rect(3) = 0.95; 
rect(4) = 0.98;
% size(left_panel_list)
% list = cell(size(left_panel_list,1)+1,1);
% list{1} = 'Select item,click on right to position';
% list(2:end)=left_panel_list;
setappdata(dispfig,'mainlist',left_panel_list);
listview = uicontrol(leftpanel,'style','listbox',... 'String',list,...
                     'tag','objlist','Units','normalized','Position', rect,...
                     'backgroundcolor',[1 1 1],'horizontalalignment','left',...
                     'TooltipString','Click to select display object.Toggle tool for editing annotations must be in off state for this',...
                     'Callback',{@set_fig_buttonup},'createfcn',{@makeobjectlist,left_panel_list});
% toggleuicontrolbgcompatibility(dispfig,1,false);
t = findobj(findall(dispfig),'Type','uitoggletool','-or','Type','uipushtool','-or','Type','uitogglesplittool');
ep = findobj(t,'tag','Standard.EditPlot');
x = t~=ep;
delete(t(x));
tstring = sprintf('%s\n%s\n%s\n%s','Toggle on to edit annotations and ', ' off to edit display items.','Must be in off state for ','pushbuttons and list control to work');
set(ep,'TooltipString',tstring);
insertm = uimenu('tag','insertmenu','Label','Insert','Callback',{@setplotoff});
uimenu(insertm,'tag','line','Label','Line','Callback',{@insert_annotation});
uimenu(insertm,'tag','rect','Label','Rectangle','Callback',{@insert_annotation});
uimenu(insertm,'tag','arrow','Label','Arrow','Callback',{@insert_annotation});
uimenu(insertm,'tag','dblarrow','Label','Double Arrow','Callback',{@insert_annotation});
uimenu(insertm,'tag','ellipse','Label','Ellipse','Callback',{@insert_annotation});


function set_fig_buttonup(hObject,event)
handles = guihandles;
setappdata(handles.addlabelbtn,'buttonstatus',0);
val = get(hObject,'Value');
if (val ==1)
    return;
end
set(handles.dispfig,'WindowButtonUpFcn',{@dispfig_btnup});

function makeobjectlist(hObject,event,panellist)
global PDPAppdata;
Panel_width = 240;
Margin = 15;
Button_height = 50;
strexplore = findobj('tag','strexplore');
dispobjs = getappdata(strexplore,'displayobjects');
posind = cellfun('isempty',{dispobjs.position});
listobjind = find(posind);
list = cell(1,numel(listobjind) +1);
list{1} = 'Select item,click on right to position';
for i=1:numel(listobjind)
    y=listobjind(i);
    list(i+1)=panellist(y);
end
set(hObject,'String',list);
gridobjind = find(~posind);
for i=1:numel(gridobjind)
    x=gridobjind(i);
    pos = dispobjs(x).position;
    pos(1) = pos(1) + Panel_width + Margin;
    pos(2)=  pos(2) + pos(4) + Button_height+Margin;
    framepos=[pos(1) pos(2)];
    create_position_objects(dispobjs(x),framepos,x);
end
annote_obj = PDPAppdata.annoteobjects; %getappdata(pdp_handle,'annoteobjects');
if ~isempty(annote_obj)
    figpos = get(gcf,'Position');
    for anum=1:numel(annote_obj)
        annote_obj(anum).astring = regexprep(annote_obj(anum).astring,'''''','''');
         t= eval(annote_obj(anum).astring);
         switch annote_obj(anum).type
             case {1,3}
               lx = get(t,'X');
               ly = get(t,'Y');
               lx = (lx+Panel_width+Margin)/figpos(3);
               ly = (ly+Button_height+Margin)/figpos(4);
               set(t,'X',lx,'Y',ly);
             case {2,4}
               p = get(t,'Position');
               p(1) = (p(1)+Panel_width+Margin)/figpos(3);
               p(2) = (p(2)+Button_height+Margin)/figpos(4);
               p(3) = p(3)/figpos(3);
               p(4) = p(4)/figpos(4);
               set(t,'Position',p);
        end
    end
    plotedit('on');
end

function dispfig_btnup(hObject,event)
handles = guihandles;
set(handles.dispfig,'Units','pixels');
cpt=get(handles.dispfig,'CurrentPoint');
set(findobj('Selected','on'),'SelectionHighlight','off','Selected','off');
if getappdata(handles.addlabelbtn,'buttonstatus')
   index = addstaticobject();
   setappdata(handles.addlabelbtn,'buttonstatus',0)
else
    val = get(handles.objlist,'Value'); 
    objectlist = get(handles.objlist,'String');
    mainlist = getappdata(handles.dispfig,'mainlist');
    index = find(strcmpi(objectlist{val},mainlist));
    objectlist(val) = [];    
    set(handles.objlist,'String',objectlist,'Value',1);    
end
strexplore = findobj('tag','strexplore');
dispobjs = getappdata(strexplore,'displayobjects');
create_position_objects(dispobjs(index),cpt,index);%1st list item is not on the object list
set(handles.dispfig,'WindowButtonUpFcn',{});

function create_position_objects(currentobject,currentpt,ind)
fpos=[0 0 0 0];
handles = guihandles;
fontunits = get(handles.objlist,'FontUnits');
set(handles.objlist,'FontUnits','pixels');
fontsz=get(handles.objlist,'Fontsize');
set(handles.objlist,'FontUnits',fontunits);
frame_arr = getappdata(handles.dispfig,'frames');
findex = numel(frame_arr) + 1;
propmenu = uicontextmenu;
uimenu(propmenu,'Label', 'Properties', 'Callback',{@showproperty});
mainlist = getappdata(handles.dispfig,'mainlist');     
unitsize = getappdata(handles.dispfig,'unitsize');
switch currentobject.disptype
    case {'label','staticlabel'}
        if isempty(currentobject.source)
           cobject{1} = currentobject.labeltxt;
        else
           obj = getfieldvalue(currentobject);
           if isscalar(obj)
              cobject = obj;
           else
               range = currentobject.colrange(1) : currentobject.colrange(2);
               cobject = obj(range);
           end
        end
        p = size(cobject);
        switch currentobject.orient
            case 'Horiz'
                 if p(1) > p(2)
                     p = fliplr(p);
                 end
            case 'Vertical'
                  if p(1) < p(2)
                     p = fliplr(p);
                  end
            case 'Diagonal'
                  p(1:2) = max(p);                  
        end
%         switch currentobject.dir
%             case 'Normal'
%                  lstart = 1;
%                  lstep = 1;
%                  lend = max(p);
%             case 'Backwards'
%                  lstart = max(p);
%                  lstep = -1;
%                  lend = 1;
%         end 
        sz = max(cellfun('size',cobject,2));
        fpos(3) = ceil(0.45* fontsz * p(2)*sz) + 20;                    
        fpos(4) = p(1)*(unitsize+2);
        fpos(1) = currentpt(1,1);
        fpos(2) = currentpt(1,2)-fpos(4);
        [fp,tagstr] = strtok(mainlist{ind},'|');
        tagstr = tagstr(2:end);
        labelstr = sprintf('%s ',cobject{:}); %cell array of strings
        labelstr(end)=[]; %last space
        frame_arr(findex).fhandle = uicontrol('Tag',tagstr,'Style','text',...
                                    'String',labelstr,'Units','pixels',...
                                    'Visible','on','Enable','inactive',...
                                    'Position',fpos,...
                                    'ButtonDownFcn',{@selectandmove,1},...
                                    'BackgroundColor','white',...
                                    'TooltipString',mainlist{ind},...
                                    'UIContextMenu', propmenu);                      
    case 'scalar'
        obj = getfieldvalue(currentobject);
        if isnumeric(obj)
            obj = num2str(obj);
        end
        fpos(1) = currentpt(1,1);
        fpos(4) = unitsize + 2;        
        fpos(2) = currentpt(1,2) - fpos(4);
        fpos(3) = fix (4*fontsz);
        [fp,tagstr] = strtok(mainlist{ind},'|');
        tagstr = tagstr(2:end);        
        frame_arr(findex).fhandle = uicontrol('Tag',tagstr,'Style','text',...
                                    'String',obj,'Units','pixels',...
                                    'Visible','on','Enable','inactive',...
                                    'Position',fpos,...
                                    'ButtonDownFcn',{@selectandmove,0},...
                                    'BackgroundColor','white',...
                                    'TooltipString',mainlist{ind},...
                                    'UIContextMenu', propmenu); 
    case 'vector'
        obj = getfieldvalue(currentobject);
        cols = currentobject.colrange(1) : currentobject.colrange(2);
        obj = obj(cols(:));
        p = size(obj);
        switch currentobject.orient
            case 'Horiz'
                if p(1) > p(2)
                    p = fliplr(p);
                end
            case 'Vertical'
                if p(1) < p(2)
                    p = fliplr(p);
                end
            case 'Diagonal'
                p(1:2) = max(p);
        end
%         switch currentobject.dir
%             case 'Normal'
%                 lstart = 1;
%                 lstep = 1;
%                 lend = max(p);
%             case 'Backwards'
%                 lstart = max(p);
%                 lstep = -1;
%                 lend = 1;
%         end
        [fpos(1) fpos(2)] = snaptogrid(currentpt);
        fpos(4) = p(1)*(unitsize + 2); 
        fpos(2) = fpos(2) - fpos(4);
        fpos(3)= p(2)*(unitsize + 2) + 1;
        [fp,tagstr] = strtok(mainlist{ind},'|');
        tagstr = tagstr(2:end);
        frame_arr(findex).fhandle = uicontrol('Tag',tagstr,'Style','text',...
                                    'Units','pixels','Visible','on',...
                                    'Enable','inactive','Position',fpos,...
                                    'ButtonDownFcn',{@selectandmove,0},...
                                    'TooltipString',mainlist{ind},...
                                    'SelectionHighlight','on',...
                                    'BackgroundColor','white',...
                                    'UIContextMenu', propmenu);             
    case 'matrix'
        obj = getfieldvalue(currentobject);
        rows =currentobject.rowrange(1) : currentobject.rowrange(2);
        cols =currentobject.colrange(1) : currentobject.colrange(2);
        obj = obj(rows(:),:);
        obj = obj(:,cols(:));
        p=size(obj);
        if strcmpi(currentobject.orient,'Transpose')
            p= fliplr(p);
        end
        switch currentobject.dir
            case 'Left/Right flip'
                obj = fliplr(obj);
            case 'Up/Down flip'   
                obj = flipud(obj);
        end
        [fpos(1) fpos(2)] = snaptogrid(currentpt);        
        fpos(4) = p(1)*(unitsize + 2);        
        fpos(2) = fpos(2) - fpos(4);
        fpos(3) = p(2)*(unitsize + 2) + 1;
        [fp,tagstr] = strtok(mainlist{ind},'|');
        tagstr = tagstr(2:end);        
        frame_arr(findex).fhandle = uicontrol('Tag',tagstr,'Style','frame',...
                                    'Units','pixels','Visible','on',...
                                    'Enable','inactive','Position',fpos,...
                                    'ButtonDownFcn',{@selectandmove,0},...
                                    'TooltipString',mainlist{ind},...
                                    'BackgroundColor','white',...
                                    'UIContextMenu', propmenu);
end
frame_arr(findex).listpos = ind;
setappdata(handles.dispfig,'frames',frame_arr)
settextlabels(frame_arr(findex).fhandle);



function selectandmove(hObject,event,mflag)
global currtext;
framepos = get(hObject,'Position');
plotedit('off');
A = selectmoveresize;
if ~isempty(currtext)
   delete(currtext);
   currtext = [];
end
switch A.Type
    case 'Select'
         set(hObject,'SelectionHighlight','on');
         if (mflag) %for label objects only
             labelstr = get(hObject,'String');
             tooltipstr = get(hObject,'TooltipString');
             if isempty(regexp(tooltipstr,labelstr, 'once')) && ~isempty(regexp(tooltipstr,'static text', 'once'))
                tooltipstr = regexprep(tooltipstr,'static text',labelstr);
                set (hObject,'TooltipString',tooltipstr);
             end
         end    
         return;
    case 'Resize'
         if ~mflag
            set(hObject,'Position',framepos); %does not allow resize, put it back in its position
            return;
         end
    case 'Move'
         mframes = getselectedframes();
         for i = 1 : numel(mframes)
             framenewpos(i,:) = get(mframes(i),'Position');
             newpos = [framenewpos(i,1),framenewpos(i,2)];
             [framenewpos(i,1),framenewpos(i,2)] = snaptogrid(newpos);
             set(mframes(i),'Position',framenewpos(i,:));
             settextlabels(mframes(i));
        end
    case 'Copy'
         for i = 1 : size(A.Handles,1)
             newf = A.Handles(i,2);
             set(A.Handles(i,1),'Position',get(newf,'Position'),...
                'SelectionHighlight','on','Selected','on');
             delete(A.Handles(i,2));
         end
         % Get frames with 'selectionhighlight' on
         mframes = getselectedframes();
         for i = 1:numel(mframes)
             framenewpos(i,:) = get(mframes(i),'Position');
             newpos = [framenewpos(i,1),framenewpos(i,2)];
             [framenewpos(i,1),framenewpos(i,2)] = snaptogrid(newpos);
             set(mframes(i),'Position',framenewpos(i,:));
         end
end


function field = getfieldvalue(cobject)
global net;
mainstruct = net;
structs = cobject.source{1};
indices = cobject.source{2};
fname = cobject.source{3};
f =  mainstruct;
for i= 1:numel(structs)
    f=getfield(f,structs{i},{indices(i)});
end
field = f.(fname);

function reset_display_object(hObject, event)
global textlabels;
handles = guihandles;
frames= getappdata(handles.dispfig,'frames');
if isempty(frames)
    return;
end
mainlist = getappdata(handles.dispfig,'mainlist');
f = [frames.fhandle];
mframes = getselectedframes();
if isempty(mframes)
    return;
end
ind = [];
currentlist = get(handles.objlist,'String');
for nf =  1:numel(mframes)
    rem_frame = mframes(nf);    
    index = find(f == rem_frame);
    ind = [ind,index];
    listindex = frames(index).listpos; % to stuff back into current list on leftpanel
    currentlist{end+1} = mainlist{listindex}; 
    delete(rem_frame);
end
frames(ind) = [];
tf = [textlabels.fhandle];
textlabels(tf == mframes) = [];
setappdata(handles.dispfig,'frames', frames);    
set(handles.objlist,'String',currentlist,'Value',1);


function reset_all_display_objects(hObject, event)
global textlabels;
handles = guihandles;
frames= getappdata(handles.dispfig,'frames');
if isempty(frames)
    return;
end
currentlist= get(handles.objlist,'String');
mainlist = getappdata(handles.dispfig,'mainlist');
for i=1:numel(frames)
    listindex = frames(i).listpos;
    currentlist{end+1} = mainlist{listindex};
    delete(frames(i).fhandle);
end
textlabels = [];
setappdata(handles.dispfig,'frames',[]);
set(handles.objlist,'String',currentlist,'Value',1);    


function selframes = getselectedframes()
handles = guihandles;
selframes=[];
frames = getappdata(handles.dispfig,'frames');
for i = 1 : numel(frames)
    if strcmpi(get(frames(i).fhandle,'Selected'),'on')
       selframes = [selframes, frames(i).fhandle];
    end
end

function dispfig_resize(hObject,event)
handles = guihandles;
dispfig = gcbo;
rect = getappdata(dispfig,'originalsize');
set(dispfig,'Units','pixels');
figrect = get(dispfig,'Position');
if isempty(rect)  %figure resize fn is called at the time of creation also 
   return;
end
Panel_width = 240;
Margin = 15;
Button_height = 50;
grid_width = figrect(3) - 3*Margin-Panel_width;
grid_height = figrect(4)- 3*Margin - Button_height;
cellsz = getappdata(dispfig,'unitsize') + 2;
temp_xsize = grid_width/cellsz;
temp_ysize = grid_height/cellsz;
gridtick = 1/cellsz;
xlimit = [0 temp_xsize/cellsz];
ylimit = [0 temp_ysize/cellsz];
obj_grid_pos = get(handles.gridpanel,'Position');
obj_grid_pos(3) = grid_width;
obj_grid_pos(4) = grid_height;
set(handles.gridpanel,'Units','pixels','Position',obj_grid_pos,...
   'XLim',xlimit,'YLim',ylimit,'Xtick',(0:gridtick:xlimit(2)),...
   'Ytick',(0:gridtick:ylimit(2)));

Panel_height = grid_height ;
panel_pos = get(handles.objpanel,'Position');
panel_pos(4) = Panel_height;
set(handles.objpanel,'Position',panel_pos);

btn_grp_pos = get(handles.buttons,'Position');
btn_grp_pos(3) = grid_width;
set(handles.buttons,'Position', btn_grp_pos);
setappdata(dispfig,'templatesize',[temp_ysize temp_xsize]);

function [xframe,yframe] = snaptogrid(cpoint)
xframe = cpoint(1,1);
yframe = cpoint(1,2);
handles =  guihandles;
gridpos = get(handles.gridpanel,'Position');
cellsize = getappdata(handles.dispfig,'unitsize') + 2;
snapsize  =  cellsize/5+1;
xdiff = mod(xframe - gridpos(1),cellsize);
ydiff = mod(yframe - gridpos(2),cellsize);

if (cellsize - xdiff) <= snapsize
   xframe = xframe + (cellsize-xdiff);
end
if xdiff <= snapsize
   xframe = xframe - xdiff;
end
if (cellsize - ydiff) <= snapsize
   yframe = yframe + (cellsize-ydiff);
end
if ydiff <= snapsize
   yframe = yframe - ydiff;
end

function save_display_objects(hObject,eventData)
global net a_objects PDPAppdata;
Panel_width = 240;
Margin = 15;
Button_height = 50;
handles = guihandles;
frames= getappdata(handles.dispfig,'frames');
scripts = PDPAppdata.netscript;
scriptname = scripts{net.num};
[path,fname,ext]=fileparts(scriptname);
fname = sprintf('%s.tem',fname);
[filename,path] = uiputfile('*.tem','Save Template File',fname);
if isequal([filename,path],[0,0])
	return;
end
fid =fopen(filename,'w');
fprintf(fid,'setdisplay\n');
cellsize = getappdata(handles.dispfig,'unitsize') + 2;
templatesz = getappdata(handles.dispfig,'templatesize');
fprintf(fid,'cellsize %d;\n',cellsize);
fprintf(fid,'template [%g %g];\n',templatesz(1),templatesz(2));
if isempty(frames)
    fclose(fid)
    return;
end
strexplore = findobj('tag','strexplore');
dispobjs = getappdata(strexplore,'displayobjects');
for i = 1 : numel(frames)
    f = frames(i).fhandle;
    index = frames(i).listpos;
    fpos = get(f,'Position');
    fpos(1) = fpos(1) - Panel_width-Margin;
    fpos(2)= fpos(2) - Button_height-Margin;
    dispobjs(index).position = fpos;
    if strcmpi(dispobjs(index).disptype ,'label') || strcmpi(dispobjs(index).disptype ,'staticlabel')
       dispobjs(index).disptype = 'label';
       dispobjs(index).labeltxt = get(f,'String');
    end
end
allpos = {dispobjs.position}; %eliminates dispobjs that were never selected and drawn from the object list
findempty = cellfun('isempty',allpos);
dispobjs(findempty) = [];
fields = fieldnames(dispobjs);
for i = 1 : numel(dispobjs)
    setstmt = '';
    source = dispobjs(i).source;
    srcname = '';
    if isempty(source)
        srcname='{}';
    else
        for k = 1:numel(source{1})
            srcname=sprintf('%s.%s(%d)',srcname,source{1}{k},source{2}(k));
        end
        srcname=sprintf('%s.%s',srcname,source{3});
        srcname=srcname(2:end);
    end
    for j = 1:numel(fields)
        if strcmp(fields{j},'source')
            continue;
        end
        property = dispobjs(i).(fields{j});
        if ~isempty(property)
           if ischar(property)
              setstmt = sprintf('%s,%s,%s',fields{j},property,setstmt);
           else
              setstmt = sprintf('%s,%s,%s',fields{j},mat2str(property),setstmt);
           end
        end
    end
    setstmt(end) = []; %last comma
    setstmt = sprintf('object (%s,%s);',srcname,setstmt);
    fprintf(fid,'\n%s',setstmt);
end
% save annotation objects 
annote_obj = PDPAppdata.annoteobjects;
next = numel(annote_obj);
if ~isempty(annote_obj)
   fprintf(fid,'\n%s',annote_obj.astring);
end
if ~isempty(a_objects)
   fig_pos = get(findobj('tag','dispfig'),'Position');
   arrow_property.names = { 'Color','HeadLength','HeadStyle','HeadWidth',...
                            'LineStyle','LineWidth'};
 %hardcoded default values for list in arrow_property.names , in the same order.This can be removed once MATLAB exposes factory defaults in future release                        
   arrow_property.defaults = {[0 0 0],10,'vback2',10,'-',0.5};
   
   dblarrow_property.names = { 'Color','Head1Length','Head2Length',...
                               'Head1Style','Head2Style',...
                               'Head1Width','Head2Width',...
                               'LineStyle','LineWidth'};
 %hardcoded default values for list in dblarrow_property.names , in the same order.This can be removed once MATLAB exposes factory defaults in future release
   dblarrow_property.defaults = {[0 0 0],10,10,'vback2','vback2',10,10,'-',0.5};
   line_properties = {'Color','LineStyle','LineWidth'};
   rect_properties = {'EdgeColor','FaceColor','LineStyle','LineWidth'};
   for anum = 1 : numel(a_objects)
        ano = a_objects(anum);
        try 
            findobj(ano.handle);
        catch
            continue;
        end
        next = next +1;
        switch ano.type
               case 'line'
                    lh = get(ano.handle,'children');
                    if isempty(lh)
                       cc = ano.handle;
                    else
                       cc = lh(end);
                    end
                    l = get(cc,line_properties);
                    fact_p = strcat('FactoryLine',line_properties);
                    fl = get(0,fact_p);
                    compare = find(~cellfun(@isequal,l,fl));
                    stmt = 'annotation(gcf,''''line''''';
                    for si = 1 : numel(compare)
                        ind = compare(si);
                        x = l{ind};
                        if ischar(x)
                           x = sprintf('''''%s''''',x);
                        else
                           x = mat2str(x,2);
                        end
                        stmt = sprintf('%s,''''%s'''',%s',stmt,...
                              line_properties{ind},x);
                    end
                    lx = (get(cc,'X')* fig_pos(3) - ...
                         Panel_width - Margin);
                    ly = (get(cc,'Y')* fig_pos(4) - ...
                         Button_height - Margin);
                    stmt = sprintf('%s, ''''X'''', %s, ''''Y'''', %s);',...
                           stmt,mat2str(lx),mat2str(ly));
                    annote_obj(next).type = 1;
                case {'rectangle','ellipse'}
                     cc = get(ano.handle,'children');
                     if isempty(cc)
                        r = ano.handle;
                     else
                        r = findobj(cc,'Type','rectangle');
                     end
                     rp = get(r,rect_properties);
                     fact_p = strcat('FactoryRectangle',rect_properties);
                     fr = get(0,fact_p);
                     compare = find(~cellfun(@isequal,rp,fr));
                     if strcmpi(ano.type,'rectangle')
                        stmt = 'annotation(gcf,''''rectangle''''';
                        annote_obj(next).type = 2;                   
                     else
                        stmt = 'annotation(gcf,''''ellipse''''';
                        annote_obj(next).type=4;
                     end
                     for si = 1 : numel(compare)
                         ind = compare(si);
                         x = rp{ind};
                         if ischar(x)
                            x = sprintf('''''%s''''',x);
                         else
                            x = mat2str(x,2);
                         end
                         stmt = sprintf('%s,''''%s'''',%s',stmt,...
                                rect_properties{ind},x);
                     end
                    rpos = get(r,'Position');
                    rx = (rpos(1)* fig_pos(3)-  Panel_width - Margin);
                    rw =  rpos(3)* fig_pos(3);
                    ry = (rpos(2) * fig_pos(4) - Button_height - Margin);
                    rh = rpos(4) * fig_pos(4);
                    stmt = sprintf('%s, ''''Position'''',%s);',stmt,...
                           mat2str([rx ry rw rh],2));
                case 'arrow'
                    ap = get(ano.handle,arrow_property.names);
                    fact_p = strcat('FactoryArrow',arrow_property.names);
                    try
                        fa = get(0,fact_p);
                        compare = find(~cellfun(@isequal,ap,fa));
                    catch
                        compare = find(~cellfun(@isequal,ap,arrow_property.defaults));
                    end
                    stmt = 'annotation(gcf,''''arrow''''';
                    for si = 1 : numel(compare)
                        ind = compare(si);
                        x = ap{ind};
                        if ischar(x)
                            x = sprintf('''''%s''''',x);
                        else
                            x = mat2str(x,2);
                        end
                        stmt = sprintf('%s,''''%s'''',%s',stmt,...
                               arrow_property.names{ind},x);
                    end    
                    lx = (get(ano.handle,'X') * fig_pos(3) - ...
                         Panel_width - Margin);
                    ly = (get(ano.handle,'Y') * fig_pos(4) - ...
                         Button_height - Margin);
                    stmt = sprintf('%s, ''''X'''', %s, ''''Y'''', %s);',...
                           stmt,mat2str(lx,2),mat2str(ly,2));
                    annote_obj(next).type = 3;
                case 'dblarrow'
                    ap = get(ano.handle,dblarrow_property.names);
                    fact_p = strcat('FactoryDoubleArrow',dblarrow_property.names);
                    try
                        fa = get(0,fact_p);
                        compare = find(~cellfun(@isequal,ap,fa));
                    catch
                        compare = find(~cellfun(@isequal,ap,dblarrow_property.defaults));
                    end
                    stmt = 'annotation(gcf,''''doublearrow''''';
                    for si = 1 : numel(compare)
                        ind = compare(si);
                        x = ap{ind};
                        if ischar(x)
                            x = sprintf('''''%s''''',x);
                        else
                            x = mat2str(x,2);
                        end
                        stmt = sprintf('%s,''''%s'''',%s',stmt,...
                               dblarrow_property.names{ind},x);
                    end    
                    lx = (get(ano.handle,'X') * fig_pos(3) - ...
                         Panel_width - Margin);
                    ly = (get(ano.handle,'Y') * fig_pos(4) - ...
                         Button_height - Margin);
                    stmt = sprintf('%s, ''''X'''', %s, ''''Y'''', %s);',...
                           stmt,mat2str(lx,2),mat2str(ly,2));
                    annote_obj(next).type = 4;                    
        end
        annote_obj(next).astring = stmt;
        fprintf(fid,'\n%s',stmt);
   end
   PDPAppdata.annoteobjects = annote_obj;
end
fclose(fid);
if PDPAppdata.lognetwork
   stmt = sprintf('loadtemplate (''%s'');',filename);
   updatelog(stmt);
end
net.templatefile = filename;
PDPAppdata.dispobjects = dispobjs;
PDPAppdata.cellsize = cellsize;
PDPAppdata.templatesize = ceil(templatesz);
clear global textlabels;
clear global currtext;
close(strexplore);
%STRAIGHT INTO THE NEXT WINDOW!
loadtemplate (filename);
launchnet;

function set_btn_appdata(hObject,event)
handles = guihandles;
setappdata(hObject,'buttonstatus',1);
set(handles.dispfig,'WindowButtonUpFcn',{@dispfig_btnup});

function index = addstaticobject()
handles = guihandles;
strexplore = findobj('tag','strexplore');
dispobjs = getappdata(strexplore,'displayobjects');
index = numel(dispobjs)+1;
dispobjs(index).source = [];
dispobjs(index).position = [];
dispobjs(index).orient = 'Horiz';
dispobjs(index).dir = 'Normal';
dispobjs(index).labeltxt = 'static text';
dispobjs(index).rowrange = [];
dispobjs(index).colrange = [];
dispobjs(index).disptype = 'staticlabel';
mlist = getappdata(handles.dispfig,'mainlist');
listmem = numel(mlist) + 1;
mlist{listmem} = 'label : static text |static';
setappdata(handles.dispfig,'mainlist',mlist);
setappdata(strexplore,'displayobjects',dispobjs);

function cancel_template(hObject, event)
handles=guihandles;
strexplore = findobj('tag','strexplore');
dispobjs = getappdata(strexplore,'displayobjects');
disp_types = {dispobjs.disptype};
statindex = strcmp(disp_types,'staticlabel');
dispobjs(statindex) = [];
setappdata(strexplore,'displayobjects',dispobjs);
clear global textlabels;
clear global currtext;
clear global a_objects;
delete(handles.dispfig);


function showproperty(hObject, event)
selectedobjs = findobj('Selected','on');
inspect(selectedobjs);

function settextlabels(fh)
global textlabels;
if isempty(textlabels)
   textlabels = struct('fhandle',[],'boundxv',[],'boundyv',[]);
   nextitem=1;
else
   h=cell2mat({textlabels.fhandle});
   hindex = find(h==fh);
   if isempty(hindex)
       nextitem = numel(textlabels) +1;
   else
       nextitem = hindex;
   end
end
fpos = get(fh,'Position');
textlabels(nextitem).fhandle = fh;
boundxv(1) = fpos(1);
boundxv(2)= fpos(1)+fpos(3);
boundxv(3) = boundxv(2);
boundxv(4) = boundxv(1);
boundyv(1) = fpos(2);
boundyv(2) = fpos(2);
boundyv(3) = fpos(2)+fpos(4);
boundyv(4) = boundyv(3);
textlabels(nextitem).boundxv = boundxv;
textlabels(nextitem).boundyv = boundyv; 

function showtextlabel(hObject, event)
global textlabels currtext;
handles=guihandles;
mousetype = get(findobj('tag','dispfig'),'SelectionType');
if isempty(textlabels) || ~strcmpi(mousetype,'normal')
    return;
end
cpt = get(hObject,'CurrentPoint');
if ~isempty(currtext)
    delete (currtext);
    currtext=[];
end
bx= {textlabels.boundxv};
by = {textlabels.boundyv};
cpx = cell(1,numel(textlabels));
cpy = cell(1,numel(textlabels));
[cpx{:}] = deal(cpt(1));
[cpy{:}] = deal(cpt(2));
inarr = cellfun(@inpolygon,cpx,cpy,bx,by);
i = find(inarr,1);
if ~isempty(i)
   fp = get(textlabels(i).fhandle,'Position');
   txpos = textlabels(i).boundxv(1)-300;
   typos = fp(2) + fp(4)-60;
   currtext = text(txpos,typos,get(textlabels(i).fhandle,'Tooltipstring'),...
              'Color','black','Units','pixels','EdgeColor','red',...
              'Fontsize',8,'BackgroundColor','white',...
              'Parent',handles.gridpanel);
end

function dispfig_buttondown(hObject,event)
global annotation_flag a_objects;
set(findobj(allchild(gcf),'Selected','on'),'Selected','off','SelectionHighlight','off');
if isempty(annotation_flag)
   plotedit('off');
   return;
end
figpos = get(gcf,'Position');
p = get(gcf,'CurrentPoint');
p(1) = p(1)/figpos(3);
p(2) = p(2)/figpos(4);
q(1) = p(1) + 0.05;
q(2) = p(2);
k = numel(a_objects)+1;
plotedit('on');
switch annotation_flag
    case 1
        h = annotation(gcf,'Line',[p(1) q(1)],[p(2) q(2)]);
        t='line';
    case 2
        h = annotation(gcf,'Rectangle',[p(1) p(2) 0.05 0.05]);
        t='rectangle';
    case 3        
        h = annotation(gcf,'Arrow',[p(1) q(1)],[p(2) q(2)]);
        t='arrow';
    case 4        
        h = annotation(gcf,'DoubleArrow',[p(1) q(1)],[p(2) q(2)]);
        t='dblarrow';         
    case 5        
        h = annotation(gcf,'Ellipse',[p(1) p(2) 0.05 0.05]);
        t='ellipse';           
end
a_objects(k).handle =h;
a_objects(k).type = t;
annotation_flag=[];
% set(findobj(gcf,'style','text'),'ButtonDownFcn',{@selectandmove,0});


function insert_annotation(hObject, event)
global annotation_flag;
whichmenu = get(hObject,'tag');
switch whichmenu
    case 'line'
        annotation_flag = 1;
    case 'rect'
        annotation_flag = 2;
    case 'arrow'
        annotation_flag = 3;
    case 'dblarrow'
        annotation_flag = 4;        
    case 'ellipse'
        annotation_flag = 5;        
    otherwise
        annotation_flag = [];
end
set(findobj(allchild(gcf),'Selected','on'),'SelectionHighlight','off');
plotedit('off');

function setplotoff(hObject,event)
handles = guihandles;
plotedit('off');
t = findobj(findall(handles.dispfig),'Type','uitoggletool','-or','Type','uipushtool','-or','Type','uitogglesplittool');
ep = findobj(t,'tag','Standard.EditPlot');
set(ep,'State','off');
