function draw_network_objects(parent)
global PDPAppdata displayscale;
dispobjs = PDPAppdata.dispobjects;
% fontunits = get(0,'DefaultUicontrolFontUnits');
% set(0,'DefaultUiControlFontUnits','pixels');
% fontsz=get(0,'DefaultUiControlFontSize');
fontsz = 8;
% set(0,'DefaultUicontrolFontUnits',fontunits);
unitsize = PDPAppdata.cellsize - 2;
frame_arr = getappdata(parent,'frames');
for i=1:numel(dispobjs)
    object=dispobjs(i);
    cobject={};
    childuis = 0;
    num = 1;
    fpos = object.position;
%     fpos(1) = fpos(1) * displayscale(2);
%     fpos(2) = fpos(2) * displayscale(1);
    findex = numel(frame_arr) + 1;
    frame_arr(findex).scalarind =0;        
    switch object.disptype
        case 'label'
             if isempty(object.source)
                cobject{1} = object.labeltxt;
             else
                obj= getfieldvalue(object);
                range =object.colrange(1) : object.colrange(2);
                cobject = obj(range);
             end
             p = size(cobject);
             switch object.orient
                 case 'Horiz'
                      if p(1) > p(2)
                         p = fliplr(p);
                      end
                case 'Vertical'
                      if p(1) < p(2)
                         p= fliplr(p);
                      end
                case 'Diagonal'
                      p(1:2)=max(p);                  
             end
             switch object.dir
                 case 'Normal'
                      lstart = 1;
                      lstep = 1;
                      lend = max(p);
                case 'Backwards'
                     lstart = max(p);
                     lstep = -1;
                     lend = 1;
             end 
             pos(1)=fpos(1)+2;
             pos(2)= fpos(2)+(fpos(4)-unitsize-1);
             pos(4) = unitsize-1;
             frame_arr(findex).fhandle =uicontrol('Tag','matp',...
                                                 'Style','frame',...
                                                 'Units','pixels',...
                                                 'Visible','on',...
                                                 'Enable','inactive',...
                                                 'Position',fpos,...
                                                 'BackgroundColor','white',...
                                                 'ForegroundColor','white'); 
             for uictrl = lstart : lstep : lend
                 txt = cobject{uictrl};
                 pos(3) = ceil(0.45 * fontsz * numel(txt))+ 24 ;
                 childuis(num)=uicontrol('Style','text','String',txt,...
                                      'Units','pixels','Position',pos,...
                                      'HorizontalAlignment','left',...
                                      'FontWeight','normal','Parent',parent,...'FontUnits','points','FontSize',fontsz,...
                                      'BackgroundColor','white',...
                                      'Visible','on');
                 if p(1) >= p(2)
                    pos(2) = pos(2) - pos(4)-3;
                 end
                 if p(1) <= p(2)
                    pos(1) = pos(1) + pos(3)+4;
                 end              
              num = num+1;
            end                                  
        case 'scalar'
            frame_arr(findex).scalarind =1;                        
            obj = getfieldvalue(object);
            if isnumeric(obj)
                obj = num2str(obj);
            end
            pos = fpos;
            pos(2)=fpos(2) + 3;
            pos(3) = fpos(3) - 3;
            pos(4) = fpos(4)-3;
            frame_arr(findex).fhandle = uicontrol('Tag','matp',...
                                        'Style','frame','Units','pixels',...
                                        'Visible','on','Enable','inactive',...
                                        'Position',fpos,...
                                        'ForegroundColor','white',...
                                        'BackgroundColor','white');       
            childuis(num) = uicontrol('Style','text','String',obj,...
                            'Units','pixels','Position',pos,...
                            'HorizontalAlignment','left',...
                            'FontAngle','Oblique','Parent',parent,...
                            'BackgroundColor','white','UserData',1);  
        case 'vector'
            obj = getfieldvalue(object);
            if isempty(object.colrange)
                object.colrange= size(obj);
            end
            cols =object.colrange(1) : object.colrange(2);
            %if ~isnan(obj)
                obj = obj(cols(:)); 
            %else
            %    obj = zeros(size(cols)); %treating NaN as 0 for display purposes
            %end
                       
            p=size(obj);
            switch object.orient
                case 'Horiz'
                    if p(1) > p(2)
                        p = fliplr(p);
                    end
%                    if fix(fpos(4)) ~= PDPAppdata.cellsize
%                       fpos(4) = PDPAppdata.cellsize;
%                    end
%                    if fix(fpos(3)/numel(cols)) ~= PDPAppdata.cellsize
%                       fpos(3) = PDPAppdata.cellsize * numel(cols);
%                    end                    
                case 'Vertical'
                    if p(1) < p(2)
                        p= fliplr(p);
                    end
%                     if fix(fpos(3)) ~= PDPAppdata.cellsize
%                        fpos(3) = PDPAppdata.cellsize;
%                     end
%                     if fix(fpos(4)/numel(cols)) ~= PDPAppdata.cellsize
%                        fpos(4) = PDPAppdata.cellsize * numel(cols);
%                     end                    
                case 'Diagonal'
                    p(1:2)=max(p);
            end
            switch object.dir
                case 'Normal'
                    lstart = 1;
                    lstep = 1;
                    lend = max(p);
                case 'Backwards'
                    lstart = max(p);
                    lstep = -1;
                    lend = 1;
            end 
            pos(1)=fpos(1)+1;
            pos(2)= fpos(2)+(fpos(4)-unitsize-1);
            pos(3) = unitsize;
            pos(4) = unitsize;
            frame_arr(findex).fhandle = uicontrol('Tag','matp',...
                                        'Style','frame','Units','pixels',...
                                        'Visible','on','Position',fpos,...
                                        'ForegroundColor','white');
            for uictrl = lstart : lstep : lend
                [value, bcolor] = get_display_attributes(obj(uictrl));
                childuis(num)= uicontrol('Style','text','String',value,...
                               'Units','pixels','Position',pos,...
                               'HorizontalAlignment','center',...
                               'Parent',parent,'UserData',cols(uictrl),...
                               'TooltipString',num2str(obj(uictrl)),...
                               'ButtonDownFcn',{@editobject,object.source});
                  if p(1) >= p(2)
                      pos(2) = pos(2) - pos(4)-2;
                  end
                  if p(1) <= p(2)
                      pos(1) = pos(1) + pos(3)+2;
                  end
                num =num +1;
            end
        case 'matrix'
            obj = getfieldvalue(object);
            rows =object.rowrange(1) : object.rowrange(2);
            cols =object.colrange(1) : object.colrange(2);
            r=sort(repmat(rows,1,numel(cols)));
            c=repmat(cols,1,numel(rows));
            Ind = sub2ind(size(obj),r,c);
            cobj = obj(rows(:),:);
            cobj = cobj(:,cols(:));
            p=size(cobj);
            if strcmpi(object.orient,'Transpose')
                p= fliplr(p);
                cobj = cobj';
                Ind = sub2ind(size(obj'),c,r);
            end
            switch object.dir
                case 'Left/Right flip'
                    cobj = fliplr(cobj);
                    c = repmat(cols(end:-1:1),1,numel(rows));
                    Ind = sub2ind(size(obj),r,c);
                case 'Up/Down flip'   
                    cobj = flipud(cobj);
                    Ind = sub2ind(size(obj),r(end:-1:1),c);
            end
            pos(2)= fpos(2)+fpos(4)-(unitsize+2);        
            pos(1)=fpos(1)+1;        
            pos(3) = unitsize;
            pos(4) = unitsize;
            frame_arr(findex).fhandle = uicontrol('Tag','matp',...
                                        'Style','frame','Units','pixels',...
                                        'Visible','on','Position',fpos,...
                                        'ForegroundColor','white');
            xpos = pos(1);
            for mi = 1:p(1)
                uictrl = mi;
                for j=1:p(2)
                     [value, bcolor] =get_display_attributes(cobj(uictrl));                            
                     childuis(num) = uicontrol('Style','text',...
                                     'String',value,'Units','pixels',...
                                     'Position',pos,...
                                     'HorizontalAlignment','center',...
                                     'Parent',parent,'UserData',Ind(num),...
                                     'SelectionHighlight','off',...
                                     'TooltipString',num2str(cobj(uictrl)),... 
                                     'ButtonDownFcn',...
                                     {@editobject,object.source});
                     pos(1) = pos(1) + pos(3)+ 2;
                     uictrl = uictrl + p(1);
                     num = num +1;
                end
                pos(1) = xpos;
                pos(2) = pos(2) - pos(4) - 2;
            end
    end
    set(frame_arr(findex).fhandle,'UserData',childuis)
    if strcmp(object.disptype,'label')
       frame_arr(findex).source = [];
    else
       frame_arr(findex).source = object.source;
    end
    if isempty(object.vcslope) || ~isnumeric(object.vcslope)
       frame_arr(findex).vcslope = 1;
    else
       frame_arr(findex).vcslope = object.vcslope;
    end
end
setappdata(parent,'frames',frame_arr);
annote_obj = PDPAppdata.annoteobjects; 
if ~isempty(annote_obj)
    figpos = get(parent,'Position');
    for anum=1:numel(annote_obj)
        annote_obj(anum).astring = regexprep(annote_obj(anum).astring,'''''','''');
         t= eval(annote_obj(anum).astring);
         switch annote_obj(anum).type
             case {1,3,4}
               lx = get(t,'X');
               ly = get(t,'Y');
               lx = lx/figpos(3);
               ly = ly/figpos(4);
               set(t,'X',lx,'Y',ly);
             case {2,5}
               p = get(t,'Position');
               p(1) = p(1)/figpos(3);
               p(2) = p(2)/figpos(4);
               p(3) = p(3)/figpos(3);
               p(4) = p(4)/figpos(4);
               set(t,'Position',p);
        end
    end
end

function field =getfieldvalue(cobject)
global net;
structs = cobject.source{1};
indices = cobject.source{2};
fname = cobject.source{3};
f =  net;
for i= 1:numel(structs)
    f=getfield(f,structs{i},{indices(i)});
end
field = f.(fname);

function set_foregroundcolor(hObject,eventData)
value = str2double(get(hObject,'tooltip'));
curr_bgcolor = get(hObject,'BackgroundColor');
curr_fgcolor = get(hObject,'ForegroundColor');
fcolor = [0 0 0];
if value > 0
   fcolor = [1 1 1];
end
if (curr_bgcolor == curr_fgcolor)
    set(hObject,'ForegroundColor',fcolor);
else
    set(hObject,'ForegroundColor',curr_bgcolor);
end

function editobject(hObject,eventData,objsrc)
seltype = get(findobj('tag','netdisplay'),'SelectionType');
switch seltype
    case 'open'
         return;
    case 'normal'
         set_foregroundcolor(hObject,eventData);
         return;
    case 'alt'
         alldispobjs = findobj('style','edit');
         editunits = findobj(alldispobjs,'tag','');
         if ~isempty(editunits)
%              set(editunits,'Style','text'); //why would this ever be here
             update_display(0);             
         end
         set(hObject,'Style','edit','foregroundColor','black',...
         'BackgroundColor','white','Callback',{@change_to_button,objsrc});
         uicontrol(hObject);
end


function change_to_button(hObject, eventData,objsrc)
global net PDPAppdata;
val = get(hObject,'String');
v=get(hObject,'UserData');
structs = objsrc{1};
indices = objsrc{2};
fname = objsrc{3};
s='net';
for i= 1:numel(structs)
    s= sprintf('%s.%s(%d)',s,structs{i},indices(i));
end
s=sprintf('%s.%s(%d) = %f;',s,fname,v,str2double(val));
evalc(s);
if PDPAppdata.lognetwork 
   updatelog(s);
end
update_display(0);
set(hObject,'style','text','tooltipstr',num2str(val));