function writeoutput(proc,freq)
global outputlog logindex PDPAppdata;
ind=logindex;
for i=1:numel(ind)
    index=ind(i);
    if ~any(strcmpi(freq,outputlog(index).frequency)) || ...
       ~any(strcmpi(outputlog(index).process,{'both',proc}))
       continue;
    end
%     if strcmpi(freq,'patsbyepoch')
%        collectpatsbyepoch(index);
%        return;
%     end
    gindicator = 0;    
    if PDPAppdata.gui && strcmpi(outputlog(index).plot,'on')
       if isempty(outputlog(index).graphdata(1).fighandle) || ~ishandle(outputlog(index).graphdata(1).fighandle)
          outputlog(index).graphdata(1).fighandle = figure;
%           hold on;
       end
       for np = 1:numel(outputlog(index).graphdata)
           outputlog(index).graphdata(np).fighandle = outputlog(index).graphdata(1).fighandle;
           if isempty(outputlog(index).graphdata(np).axhandle) || ~ishandle(outputlog(index).graphdata(np).axhandle)
              axh = subplot(outputlog(index).plotlayout(1),outputlog(index).plotlayout(2),outputlog(index).graphdata(np).plotnum);
              outputlog(index).graphdata(np).axhandle =  axh;
           end
       end
       gindicator = 1;
    end 
    if  outputlog(index).writemode(1) == 'b'
        if strcmpi(freq,'patsbyepoch')        
           gindicator = writepatsbyepoch(index,gindicator);
        else
           writebinaryoutput(index,gindicator);
        end
    else
        writetextoutput(index,gindicator);
    end
    if PDPAppdata.gui && gindicator > 0 && ishandle(outputlog(index).graphdata(1).fighandle)
      for np = 1:numel(outputlog(index).graphdata)
          plotax = outputlog(index).graphdata(np).axhandle;
          cla(plotax);
          xdata = outputlog(index).graphdata(np).px;
          zdata = outputlog(index).graphdata(np).py;
          ydata = repmat([1:size(zdata,1)]',1,size(zdata,2));
%           plot(plotax,outputlog(index).graphdata(np).px,outputlog(index).graphdata(np).py);
          if outputlog(index).graphdata(np).plot3d && ~isscalar(xdata)
            plot3(plotax,xdata,ydata,zdata);
            if ~isempty(outputlog(index).graphdata(np).ylim)
               set(plotax,'zlim',outputlog(index).graphdata(np).ylim);
            end
            az = outputlog(index).graphdata(np).az;
            el = outputlog(index).graphdata(np).el;
            view(plotax,az,el);            
          else
            plot(plotax,xdata,zdata);
            if ~isempty(outputlog(index).graphdata(np).ylim)
                set(plotax,'ylim',outputlog(index).graphdata(np).ylim);
            end             
          end
          title(plotax,outputlog(index).graphdata(np).title,'Color','r');
          xlabel(plotax,outputlog(index).graphdata(np).xlabel);
          ylabel(plotax,outputlog(index).graphdata(np).ylabel);
          set(plotax,'tag',outputlog(index).graphdata(np).tag);
      end
    end    
end

function writetextoutput(outindex,gind)
global net outputlog;
logstruct = outputlog(outindex);
lfile = logstruct.file;
if exist(lfile,'file')
   lfid=fopen(lfile,'a');
else
   lfid=fopen(lfile,'w');
   if strcmpi(outputlog(outindex).logopts,'on')
      if strcmpi(outputlog(outindex).process,'train')
         opts = net.trainopts;
      else
         opts = net.testopts;
      end
      fields = fieldnames(opts);
      for i = 1:numel(fields)
          f = opts.(fields{i});
          if ischar(f)
             fprintf(lfid,'%% %s :''%s''\n',fields{i},f);
          else
             if isnumeric(f)
                fprintf(lfid,'%% %s:%s\n',fields{i},num2str(f));
             end
          end
      end
   end
   fprintf(lfid,'\n%% ');
   fprintf(lfid,'%s\t',logstruct.objects{1:end});
   fprintf(lfid,'\n');
end
varsubs = logstruct.varstruct;
pyarray = [];
for j=1:numel(varsubs)
    T=evalc(varsubs{j});
    f=subsref(net,S);
    if ischar(f)
       fprintf(lfid,'%s\t',f);            
    else
       fprintf(lfid,'%g\t',f');  %transpose useful for writing matrices
    end
    if gind > 0
       if j == 1
          outputlog(outindex).graphdata.px = [outputlog(outindex).graphdata.px f];
       else
          pyarray = [pyarray;f'];
       end 
    end     
end
if gind > 0
   outputlog(outindex).graphdata.py = [outputlog(outindex).graphdata.py pyarray];    
end
fprintf(lfid,'\n');
fclose(lfid);

function writebinaryoutput(logn,gind)
global net binarylog outputlog;
bindex = find(cell2mat({binarylog.logind})== logn);
output = binarylog(bindex).output;
logstruct = outputlog(logn);
varsubs = logstruct.varstruct;
% pyarray = [];
pycellarr = cell(1,numel(outputlog(logn).graphdata));
for j=1:numel(varsubs)
    ncount =1;
    evalc(varsubs{j});
    f=subsref(net,S);
   if numel(output) >=j
      ncount = size(output{j},3)+1;
   end
 if ischar(f)
    output{j}(:,:,ncount)={f};
 else
   output{j}(:,:,ncount)=f(1:end,:);
 end
 if gind > 0
    if j == 1
       px = [outputlog(logn).graphdata(1).px f];
       [outputlog(logn).graphdata(1:end).px] = deal(px);
    else
       for np = 1:numel(outputlog(logn).graphdata)
           yindex = find(ismember(outputlog(logn).graphdata(np).yvariables,outputlog(logn).objects{j}),1);
           if ~isempty(yindex)
              pyarray = pycellarr{np};
              pyarray = [pyarray;f'];
              pycellarr{np} = pyarray;
           end
       end
%        pyarray = [pyarray;f'];
    end 
 end
end
if gind > 0
   for np = 1:numel(outputlog(logn).graphdata)
       outputlog(logn).graphdata(np).py = [outputlog(logn).graphdata(np).py pycellarr{np}];   
   end
%    [outputlog(logn).graphdata(1:end).py] = deal(pycellarr{1:end});
%    outputlog(logn).graphdata.py = [outputlog(logn).graphdata.py pyarray];
end
binarylog(bindex).output=output;
outputlog(logn).binout = binarylog(bindex).output;

function g = writepatsbyepoch(ind,gind)
global net binarylog outputlog runparams PDPAppdata;
global bypatsarray;
g = 0;
bindex = find(cell2mat({binarylog.logind})== ind);
output = binarylog(bindex).output;
logstruct = outputlog(ind);
varsubs = logstruct.varstruct;
if strncmpi(runparams.process,'train',length(runparams.process))
   cpatnum = net.patno;
   npats = numel(PDPAppdata.trainData);
else
   cpatnum = net.patno;
   npats = numel(PDPAppdata.testData);
end
for j=1:numel(varsubs)
    evalc(varsubs{j});
    f=subsref(net,S);
%     ncount = (cpatnum - 1) * nepochs + (net.epochno - 1) + 1;

 if ischar(f)
    output{j}(net.epochno,cpatnum)={f};
 else
   output{j}(net.epochno,cpatnum,:)=f;
 end
 if gind > 0
       for np = 1:numel(outputlog(ind).graphdata)
           yindex = find(ismember(outputlog(ind).graphdata(np).yvariables,outputlog(ind).objects{j}),1);
           if ~isempty(yindex)
               bypatsarray{np}(net.epochno,cpatnum,:) = f;
           end
       end
%     end 
 end
end
if gind > 0 && cpatnum == npats
   for np = 1:numel(outputlog(ind).graphdata)
       xy = bypatsarray{np}(net.epochno,:,:);
       xy = reshape(xy,1,npats*size(bypatsarray{np},3));
       outputlog(ind).graphdata(np).py = [outputlog(ind).graphdata(np).py; xy];
       px = repmat(net.epochno,1, npats*size(bypatsarray{np},3));
       outputlog(ind).graphdata(np).px = [outputlog(ind).graphdata(np).px ;px];       
   end
   g = 1;
end
binarylog(bindex).output=output;
outputlog(ind).binout = binarylog(bindex).output;