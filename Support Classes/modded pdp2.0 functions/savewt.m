function varargout = savewt(varargin)
% SAVEWT M-file for savewt.fig
%      SAVEWT, by itself, creates a new SAVEWT or raises the existing
%      singleton*.
%
%      H = SAVEWT returns the handle to a new SAVEWT or the handle to
%      the existing singleton*.
%
%      SAVEWT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SAVEWT.M with the given input arguments.
%
%      SAVEWT('Property','Value',...) creates a new SAVEWT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before savewt_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to savewt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help savewt

% Last Modified by GUIDE v2.5 26-Sep-2006 15:19:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @savewt_OpeningFcn, ...
                   'gui_OutputFcn',  @savewt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before savewt is made visible.
function savewt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to savewt (see VARARGIN)

% Choose default command line output for savewt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
uicontrol(handles.filepathedit);
% UIWAIT makes savewt wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = savewt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function filepathedit_Callback(hObject, eventdata, handles)


function filepathedit_CreateFcn(hObject, eventdata, handles)
pdp_handle=findobj('tag','mainpdp');
global net PDPAppdata;
script = PDPAppdata.netscript; %getappdata(pdp_handle,'netscript');
filename =script{net.num};
[fpath fname fext]=fileparts(filename);
wtfile=sprintf('%s.wt',fname);
if ~isempty(fpath)
    wtfile = sprintf('%s%s%s.wt',fpath,filesep,fname);
end
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',wtfile);


function locationbtn_Callback(hObject, eventdata, handles)
[filename, pathname] = uiputfile('*.wt', 'Save weight file');
if isequal([filename,pathname],[0,0])
    return;
end
File = fullfile(pathname,filename);
set(handles.filepathedit,'String',File);

function binarychk_Callback(hObject, eventdata, handles)


function donebtn_Callback(hObject, eventdata, handles)
global PDPAppdata;
filename = get(handles.filepathedit,'String');
bchk = get(handles.binarychk,'Value');
if bchk==1
   format = 'b';
else
   format = 't';
end
saveweights('file',filename,'mode',format);
if PDPAppdata.lognetwork
   statement = sprintf('saveweights (''file'',''%s'',''mode'',''%s'');',...
               filename,format);
   updatelog(statement);
end
delete(handles.savewtfig);

function cancelbtn_Callback(hObject, eventdata, handles)
delete(handles.savewtfig);

