function settrainopts (varargin)
global net PDPAppdata;
switch net.type
    case 'pa'
         validobjs={'nepochs';'trainmode';'lflag';'lrule';'lrate';'ecrit'; ...
                    'noise';'actfunction';'temp';'trainset'};
    case 'cl'
         validobjs={'nepochs';'trainmode';'lflag';'lrate';'trainset'};
    case {'bp','srn'}
         validobjs={'nepochs';'ncycles';'trainmode';'lflag';'lrate';'follow';...
                    'cascade';'ecrit';'crate';'wrange';'lgrain';...
                    'lgrainsize';'errmeas';'wdecay';'momentum';'mu';'tmax';...
                    'clearval';'fastrun';'trainset'};
    case 'rbp'
         validobjs={'nepochs';'trainmode';'lflag';'lrate';'follow';'ecrit';...
                    'wrange';'lgrain';'lgrainsize';'errmeas';'wdecay';...
                    'momentum';'errmargin';'fastrun';'trainset'};  
    case 'tdbp'
         validobjs={'nepochs';'lflag';'lrate';'wrange';'lgrain';...
                    'lgrainsize';'wdecay';'mu';'clearval';'trainset';...
                    'gamma' ; 'lambda' ; 'policy' ; 'epsilon';...
                    'showvals' ; 'annealsched' ; 'temp' ; 'runstats'};                
    otherwise
        return;
end
objs = varargin(1:2:end-1);
values = varargin(2:2:end);
X = find(~(ismember(objs,validobjs)));
Y = find(ismember(objs,validobjs));
if ~isempty(X)
    invobjs = objs(X);
    fprintf(1,'ERROR: invalid object ''%s'' in settrainopts command\n',invobjs{1:end});
end
tropts = net.train_options;
tempopts = tropts;
for i = 1 : numel(Y)
    ind = Y(i);
    tropts.(objs{ind}) = values{ind};
    if strcmpi(net.type,'tdbp') && strcmpi(net.netmode,'sarsa') && strcmpi(values{ind},'defer')
        fprintf(1,'ERROR: Policy type ''defer'' is not allowed for SARSA networks.  Switching to ''egreedy''.\n');
    end
    outpools = find(strcmpi({net.pools.type},'output'));
    if strcmpi(net.type,'tdbp') && strcmpi(net.pool(outpools(1)).actfunction,'linear') && strcmpi(values{ind},'linearwt')
        fprintf(1,'ERROR: Policy type ''linearwt'' is not allowed for linear outputs.  Switching to ''egreedy''.\n');
    end    
    if strcmpi(objs{ind},'actfn')
        actshort = {'st','li','cs','lt'};
        actlong = {'stochastic','linear','continuous sigmoid',...
                  'linear threshold'};
        actfn =1;
        ind = find(strcmpi(actshort,tropts.actfn));
        if isempty(ind)
           ind = find(strncmpi(tropts.actfn,actlong,length(tropts.actfn)));
           if ~isempty(ind)
              actfn = ind(1);
           end
        else
            actfn = ind;
        end
        tropts.actfn = actshort{actfn};           
    end
end
net.train_options = tropts;
n = PDPAppdata.networks;
n{net.num} = net;
PDPAppdata.networks = n;
if ismember('trainset',objs)
   tset = net.train_options.trainset;
   dat = [];
   patn = [];
   pfiles = PDPAppdata.patfiles;   
   if ~strcmpi(tset,'none')
      patn = find(strcmpi(tset,pfiles(:,1)));
      if isempty(patn)
         patn = find(strcmpi(tset,pfiles(:,2)));
      end
      if isempty(patn)
          fprintf(1,'ERROR : %s - Trainset not defined\n',tset);
          return;
      end
      patn = patn(1); %filename need not be unique       
      dat = PDPAppdata.patfiles{patn,3};
   end
   PDPAppdata.trainData = dat;
   if PDPAppdata.gui && ~strcmp(net.type,'tdbp')    % no patterns for net type 'tdbp'
       patlist = get(findobj('tag','trpatpop'),'String');
       pn = 1;
       if ~isempty(patn)
           pn = find(strcmpi(patlist,pfiles(patn,1)));
       end
       set(findobj('tag','trpatpop'),'Value',pn);
       netdisp = findobj('tag','netdisplay');
      if ~isempty(netdisp)
         trbuttons = findobj(findobj('tag','trainpanel'),'Style',...
                     'pushbutton');       
         if isempty(dat)
            set(findobj('tag','trpatlist'),'String',{' '},'Value',1);
            set(trbuttons,'enable','off');
         else
            if get(findobj('tag','trainradio'),'Value')
               set(trbuttons,'enable','on');
            end
            fill_patterns(findobj('tag','trpatlist'),PDPAppdata.trainData);
         end
      end
   end
end 
if ~PDPAppdata.gui
   objstr = sprintf(', %s',objs{1:end});
   fprintf(1,'Training option(s) %s set to specified value(s)\n',objstr);
   if PDPAppdata.lognetwork
      update_params = compare_struct(tropts,tempopts);
      if ~isempty(update_params)
         args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
         args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
         trstmt = 'settrainopts (';
         for i=1:size(update_params,1)
             field_name = update_params{i,1};
             field_val = update_params{i,2};
             if ischar(field_val)
                trstmt = sprintf('%s''%s'',''%s'',',trstmt,field_name,field_val);
             else
                trstmt = sprintf('%s''%s'',%g,',trstmt,field_name,field_val);
             end
          end
          trstmt = sprintf('%s);',trstmt(1:end-1));
          updatelog(trstmt);
      end
    end   
end
