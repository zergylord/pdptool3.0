function resetlog
global net outputlog binarylog;
if isempty(net.outputfile)
   return
end
%first find global outputlog entries that match the current network's outputlogs
[tf, lmatch] = ismember({outputlog.file},net.outputfile); 
if ~any(lmatch)
   return;
end
matchindex = find(lmatch);
onindex = [];
% then find outputlogs that have 'on' status
for i = 1:numel(matchindex);
    ind = matchindex(i);
    if strcmpi(outputlog(ind).status,'on')
       onindex = [onindex ind];
    end
end
if isempty(onindex)
   return;
end
%now check the onreset option of the filtered logs
for i = 1:numel(onindex)
    ind = onindex(i);
    lfile = outputlog(ind).file;    
    if strcmpi(outputlog(ind).onreset,'clear')
       if exist(lfile,'file') == 2
          delete(lfile);
       end
       if outputlog(ind).writemode(1) == 'b' && ~isempty(binarylog) 
          bindex = find(cell2mat({binarylog.logind})== ind);
          if ~isempty(bindex)
             binarylog(bindex).output = [];
          end
          outputlog(ind).binout = [];
       end
       if ~isempty(outputlog(ind).graphdata) && ~isempty(outputlog(ind).graphdata(1).fighandle) && ishandle(outputlog(ind).graphdata(1).fighandle)
          plotax = findobj(outputlog(ind).graphdata(1).fighandle,'type','axes');
          for np = 1:numel(plotax)
              cla(plotax(np),'reset');
              outputlog(ind).graphdata(np).px = [];
              outputlog(ind).graphdata(np).py = [];
          end
       end
    else
       x = regexp(lfile,'_\d*\.');
       if isempty(x)
          fnamepre = lfile(1:end-4); %without extension .mat or .out           
          fnamepre = sprintf('%s_',fnamepre);       
       else
          fnamepre = lfile(1:x);
       end
       if outputlog(ind).writemode(1) == 'b'
          newfile = getfilename(fnamepre,'.mat');
       else
          newfile = getfilename(fnamepre,'.out');
       end
       outputlog(ind).file = newfile;
       netout = find(strcmp(net.outputfile,lfile));
       net.outputfile{netout} = newfile;
       if outputlog(ind).writemode(1) == 'b' && ~isempty(binarylog) 
          bindex = find(cell2mat({binarylog.logind})== ind);
          if ~isempty(bindex)
             binarylog(bindex).output = [];
          end
          outputlog(ind).binout = [];
       end       
       if ~isempty(outputlog(ind).graphdata)
          for np = 1:numel(outputlog(ind).graphdata)
              outputlog(ind).graphdata(np).px = [];
              outputlog(ind).graphdata(np).py = [];
              outputlog(ind).graphdata(np).fighandle = [];
              outputlog(ind).graphdata(np).axhandle = [];              
          end           
%           outputlog(ind).graphdata = struct('px',[],'py',[],'fighandle',[]);  %do not clear old figure handle in this case
       end       
    end
end
       