function loadscript(varargin)
%LOADSCRIPT This function reads a .net file and performs network setup.
%
%           Syntax :
%           (a) loadscript('<filename>')
%           (b) loadscript ('file','<filename>') 
%
%           Syntax (b) is rarely used and is available here only to adhere 
%           to the convention of using �property-value� pair specification 
%           used in MATLAB. Using <filename> alone is sufficient for this. 
%           <filename> should be a valid network script file with .net 
%           extension. If <filename> is not in the current directory, the 
%           entire path must be specified. <filename>  should be within 
%           single quotes as it is expected to be a string value.
%           This function is invoked internally when user clicks on
%           'Load Script' button in the main pdp window when running
%           pdptool in gui mode. 
if nargin < 1
   error('File name not specified');
end
if nargin > 1
   if ~strncmpi('file',varargin{1},length(varargin{1}))
       fprintf(1,'ERROR: First argument must be property name ''file''\n');
       return;
   end
   file = varargin{2};
else
   file = varargin{1};
end
if evalin('base','exist(''net'',''var'')')==1   % workspace variable
   clear global net;
end
global net PDPAppdata closer resetfunc;

PDPAppdata.netscript = {file};
resetfunc = @()evalin('base',file);
prescript_ws = who;
eval(file); %run the script
postscript_ws = who;
% evalin('base',file); %sooo slow...
possible_nets = postscript_ws;%setxor(prescript_ws,postscript_ws);
for i = 1:length(possible_nets)
    if isa(eval(possible_nets{i}),'net')
        net = eval(possible_nets{i});
        PDPAppdata.networks = {net};
%         closer = loop_closer(net);
        break
    end
end
% [fid,fopenmesg] = fopen (file,'rt');
% if fid < 0
%    fprintf(1,'ERROR : %s - %s\n',file,fopenmesg);
%    return;
% end
% delim='.';
% prefix='';
% lines = cell(1,10);
% c = 1;
% while (feof(fid) ~= 1)
%     tline = strtrim(fgetl(fid));
%     if isempty(tline)
%         continue;
%     end
%     f = regexp(tline,'\.');
%     if ~isempty(f) && f(1) > 1  % if line does not begin with '.' save that as prefix for the next lines
%        [prefix, rem] = strtok(tline,delim);
%     else
%        tline = sprintf('%s%s',prefix,tline);
%     end
%     lines{c} = tline;
%     c = c +1;
% end
% fclose(fid);
n = PDPAppdata.networks;
netind = numel(n)+1;
evalin('base','global net');
% try
%     ev_lines = removelinebreaks(lines);
%     cellfun(@eval,ev_lines);
% catch
%     error(lasterror);
% end    
% if exist('net','var')==1 || evalin('base','exist(''net'',''var'')')==1   % workspace variable
%    nx = net;
% end
% if exist('pool','var')==1
%    nx.pool = pool;
% else
%     if evalin('base','exist(''pool'',''var'')')==1
%        nx.pool = evalin('base','pool');
%        evalin('base','clear(''pool'')');
%     end
% end  
% if exist('trainopts','var')== 1 
%    nx.trainopts = trainopts;
% else
%     if evalin('base','exist(''trainopts'',''var'')')==1
%        nx.trainopts = evalin('base','trainopts');
%        evalin('base','clear(''trainopts'')');
%     end
% end
% if exist('testopts','var')==1 
%     nx.testopts = testopts;
% else
%     if evalin('base','exist(''testopts'',''var'')')==1
%        nx.testopts = evalin('base','testopts');
%        evalin('base','clear(''trainopts'')')    
%     end 
% end
% nx = definenetwork(nx);

% NO MAIN PDP WINDOW--------------------------

% if PDPAppdata.gui
%    pdp_handle = findobj('tag','mainpdp');
%    npop = findobj(pdp_handle,'tag','netpop');
%    currentnets = get(npop,'String');
%    netlist=cellstr(currentnets);
%    sz=size(netlist,1);
%    if ~strcmp(netlist{1},'none')
%       sz = sz+1;
%    end
%    netlist{sz} = PDPAppdata.netscript{1};
%    set(npop,'String',netlist,'Value',sz);
%    set(findobj('tag','loadpatbtn'),'enable','on');
% end
if PDPAppdata.lognetwork && ~isempty(PDPAppdata.logfilename)
    if netind > 1 
       PDPAppdata.logfilename = getfilename('pdplog','.m');
       logfd = fopen(PDPAppdata.logfilename,'w');
       fprintf(logfd,'pdp;\n');
       fclose(logfd);
    end
%     nx.logfile= PDPAppdata.logfilename;
end
% nx.num = netind;
% net=nx;
% n{netind} = nx;
script = PDPAppdata.netscript;
script{netind}=file;
PDPAppdata.netscript = script;
PDPAppdata.networks = n;
if isfield(net,'templatefile') && isempty(net.templatefile)
    PDPAppdata.dispobjects = [];
    PDPAppdata.templatesize = [];
    PDPAppdata.cellsize = [];
end
if ~PDPAppdata.gui
   fprintf(1,'Script %s loaded and network initialized ...\n',file);
end