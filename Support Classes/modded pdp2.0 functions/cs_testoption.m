function varargout = cs_testoption(varargin)
% CS_TESTOPTION M-file for cs_testoption.fig
% This is called when user clicks on
% (i)  'Set Testing options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Test panel of the network viewer window.
% It presents testing parameters for 'cs' type of network that can be 
% modified and saved. 

% Last Modified 04-May-2007 16:50:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cs_testoption_OpeningFcn, ...
                   'gui_OutputFcn',  @cs_testoption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cs_testoption is made visible.
function cs_testoption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cs_testoption (see VARARGIN)

% The GUI opening function sets current/existing values of testing 
% parameters to the corresponding GUI controls.
movegui('center');
setcurrentvalues();

% Choose default command line output for cs_testoption
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cs_testoption wait for user response (see UIRESUME)
% uiwait(handles.cs_testoption);


% --- Outputs from this function are returned to the command line.
function varargout = cs_testoption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function nupdatesedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function modelpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function istredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function estredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function extinputpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    
function kappaedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tempedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tstoptnfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% This gets appdata for cs_testoption figure handle by extracting details 
% of test patterns currently in the Test pattern list of the main window.
testpatfiles = testpatternspopup_cfn(hObject);
setappdata (findobj('tag','cs_testoption'),'patfiles',testpatfiles);


function ncyclesedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function annealbtn_CreateFcn(hObject, eventdata, handles)
global net;
if isempty(net)
   return;
end
setappdata(hObject,'annealsched',net.test_options.annealsched);

function modelpop_Callback(hObject, eventdata, handles)

% This examines the selected value of the model popup menu and enables 
% temperature related uicontrols if selected model is boltzmann and
% disables them if selected model is Schema.
set (hObject,'BackgroundColor','yellow');
if get(hObject,'Value') == 2
   set (findobj('tag','tempedit'),'Enable','on');
else
%    if get (handles.harmonychk,'Value') ~= 1
   set (findobj('tag','tempedit'),'Enable','off');
%    end
end

function harmonychk_Callback(hObject, eventdata, handles)
if get(hObject,'Value') == 1
   set(findobj('tag','kappaedit'),'Enable','on');
   set(findobj('tag','tempedit'),'Enable','on');
else
   if get(handles.modelpop,'Value') == 2
      set(findobj('tag','tempedit'),'Enable','on');
   else
      set(findobj('tag','tempedit'),'Enable','off');
   end
   set(findobj('tag','kappaedit'),'Enable','off');
end


function extinputpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');
if get(hObject,'Value') == 2
   set(handles.estredit,'enable','off');
else
   set(handles.estredit,'enable','on');    
end


function okbtn_Callback(hObject, eventdata, handles)
% Applies current options and exits.

apply_options();
delete(handles.cs_testoption);

function cancelbtn_Callback(hObject, eventdata, handles)
% Exits without making any changes

delete(handles.cs_testoption);

function applybtn_Callback(hObject, eventdata, handles)
% Calls subroutine that sets network parameters.Does not exit from window.
apply_options();


function apply_options()

% This reads in the new parameters and applies changes to the network
% testopts structure.
handles = guihandles;
global net PDPAppdata;
n = PDPAppdata.networks;
prevset = 'none';
% if isfield(net,'test_options') 
   tstopts = net.test_options;
   tempopts = tstopts;
   prevset = net.test_options.testset;
% end
tstopts.nupdates = str2double(get(handles.nupdatesedit,'String'));
tstopts.ncycles = str2double(get(handles.ncyclesedit,'String'));
mval = get(handles.modelpop,'Value');
mstr = cellstr(get(handles.modelpop,'String'));
tstopts.actfunction = mstr{mval};
tstopts.istr = str2double(get(handles.istredit,'String'));
if get(handles.extinputpop,'Value') ==2
   tstopts.clamp=1;
else
   tstopts.clamp=0;
   tstopts.estr = str2double(get(handles.estredit,'String'));
end
% if get(handles.harmonychk,'Value') ~= 0
%    tstopts.harmony = 1;
% end
% tstopts.kappa = str2double(get(handles.kappaedit,'String'));
tstopts.temp = str2double(get(handles.tempedit,'String'));
tstopts.annealsched = getappdata(handles.annealbtn,'annealsched'); 

patlist = get(handles.tstoptnfilepop,'String');
pfiles = getappdata(handles.cs_testoption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window    
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','test');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.tstoptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
     end 
end
plistval = get(handles.tstoptnfilepop,'Value');
tstopts.testset = patlist{plistval};

% -- Calls settestopts command with only the modified fields of the testopts
%    structure. 
update_params = compare_struct(tstopts,tempopts);
if ~isempty(update_params)
   args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
   args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
   settestopts(args{1:end});
   if PDPAppdata.lognetwork
      tststmt = 'settestopts (';
      for i=1:size(update_params,1)
          field_name = update_params{i,1};
          field_val = update_params{i,2};
          if ischar(field_val)
             tststmt = sprintf('%s''%s'',''%s'',',tststmt,field_name,field_val);
          else
            if ndims(field_val) > 1  %annealsched
                 tststmt = sprintf('%s''%s'',%s,',tststmt,field_name,mat2str(field_val));
            else              
                 tststmt = sprintf('%s''%s'',%g,',tststmt,field_name,field_val);
            end
          end
      end
      tststmt = sprintf('%s);',tststmt(1:end-1));
      updatelog(tststmt);
   end
end

% tempopts = orderfields(tempopts,tstopts);
% fields = fieldnames(tempopts);
% tempA = struct2cell(tempopts);
% tempB = struct2cell(tstopts);
% diffind = cellfun(@isequal,tempA,tempB);
% diffind = find(~diffind);
% if ~isempty(diffind)   
%    tststmt = 'settestopts (';
%    args = cell(1,numel(diffind)*2);
%    [args(1:2:end-1)] = fields(diffind);    
%    for i = 1 : numel(diffind)
%        fval =  tstopts.(fields{diffind(i)});
%        args{i*2} = fval;       
%        if ischar(fval)
%           tststmt = sprintf('%s''%s'',''%s'',',tststmt,fields{diffind(i)},...
%                     fval);
%        else
%           if ndims(fval) > 1  %annealsched
%              tststmt = sprintf('%s''%s'',%s,',tststmt,fields{diffind(i)},mat2str(fval));
%           else
%              tststmt = sprintf('%s''%s'',%g,',tststmt,fields{diffind(i)},fval);
%           end
%        end
%    end
%    settestopts(args{1:end});   
%    if PDPAppdata.lognetwork
%       tststmt = sprintf('%s);',tststmt(1:end-1));
%       updatelog(tststmt);
%    end 
% end

set(findobj('tag','testpatpop'),'String',patlist,'Value',plistval);

% -- Updates network viewer window if figure module is invoked from within
%    test panel and new test patterns need to be loaded in network viewer.
netdisp = findobj('tag','netdisplay');
if ~isempty(netdisp) && ~strcmpi(prevset,net.test_options.testset)
    if strcmpi(net.test_options.testset,'none')
       set(findobj('tag','tallchk'),'Visible','off');
       set(findobj('tag','tstpatchk'),'Visible','off');
       set(findobj('tag','testgranlist'),'String',{'cycle','update'});
    else
%        glistpos = get(findobj('tag','testgranlist'),'Position');
%        pos = get(findobj('tag','tallchk'),'Position');
%        pos(1) = glistpos(1) + glistpos(3);
%        set(findobj('tag','tallchk'),'Position',pos,'Visible','on');
       set(findobj('tag','tstpatchk'),'Visible','on','Value',0);
       set(findobj('tag','tstpatlist'),'Visible','on','enable','off');
    end
    set(findobj(findobj('tag','testpanel'),'Style','pushbutton'),...
       'enable','on');  
end
if ~isequal(tstopts.annealsched,tempopts.annealsched)
    cssettemp();
    if ~isempty(netdisp)
        update_display(0);
    end
end
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.cs_testoption,'patfiles',pfiles);

% -- All edited uicontrols that have KeypressFcn set to change background  
%    color to yellow is changed back to white.This indicates that modified 
%    fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');

function tstoptnfilepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function tstoptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.tstoptnfilepop,handles.cs_testoption);


function setwritebtn_Callback(hObject, eventdata, handles)

% Calls the module that presents options for writing network output logs.
create_edit_log;
% setwriteopts;


function setcurrentvalues
% This sets the current testing parameter values to the corresponding GUI
% controls. 
global net;
handles = guihandles;
tstopts = net.test_options;
mode =1;
set (handles.nupdatesedit,'String',tstopts.nupdates);
set (handles.ncyclesedit,'String',tstopts.ncycles);
modeopts = cellstr(get(handles.modelpop,'String'));
ind = find(strncmpi(tstopts.actfunction,modeopts,length(tstopts.actfunction)));
if ~isempty(ind)
   mode = ind;
end
set (handles.modelpop,'Value',mode);
set (handles.istredit,'String',tstopts.istr);
set (handles.estredit,'String',tstopts.estr);

if tstopts.clamp == 0;
   set (handles.extinputpop,'Value',1);
   set (handles.estredit,'enable','on');
else
   set (handles.extinputpop,'Value',2);
   set (handles.estredit,'enable','off');
end
% if tstopts.harmony ~= 0 
%    set (findobj('-regexp','tag','kappa*'),'visible','on');
%    set (findobj('-regexp','tag','temp*'),'Visible','on');
% end
if mode == 2
   set (findobj('-regexp','tag','temp*'),'Visible','on','Enable','on');
end
% set (handles.harmonychk,'Value',tstopts.harmony);
% set (handles.kappaedit,'String',tstopts.kappa);
set (handles.tempedit,'String',tstopts.temp);

function annealbtn_Callback(hObject, eventdata, handles)
getannealschedule;


function nupdatesedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function ncyclesedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function istredit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function estredit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function tempedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function kappaedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');


