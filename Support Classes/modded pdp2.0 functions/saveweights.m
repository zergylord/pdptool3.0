function saveweights(varargin)
global net PDPAppdata;
saveparams.file = getfilename('pdpweight','.wt');
saveparams.mode = 'binary';
saveparams = parse_pairs(saveparams,varargin);
wtstruct=struct('weight',{},'pooln',{},'projn',{});
recnum =1;
for i=1:length(net.pools)
    p = net.pools(i);
    for j=1: numel(p.projections)
        if ~isempty(p.projections(j).using.weights)
            wtstruct(recnum).weight = p.projections(j).using.weights;
            wtstruct(recnum).pooln = i;
            wtstruct(recnum).projn =j;
            recnum = recnum+1;
        end
    end
    
end
if isempty(wtstruct)
    fprintf(1,'ERROR: No weights to save');
    return;
else
    if strncmpi(saveparams.mode,'text',numel(saveparams.mode))
        %       fid = fopen(saveparams.file,'wt');
        for i = 1: size(wtstruct,2)
            %           fprintf(fid,'%d\n',wtstruct(i).pooln);
            %           fprintf(fid,'%d\n',wtstruct(i).projn);
            %           fprintf(fid,'%.4f ',wtstruct(i).weight);
            %           fprintf(fid,'\n');
            dlmwrite(saveparams.file,wtstruct(i).pooln,'-append','delimiter','\t');
            dlmwrite(saveparams.file,wtstruct(i).projn,'-append','delimiter','\t');
            %           size(wtstruct(i).weight)
            dlmwrite(saveparams.file,wtstruct(i).weight,'-append','delimiter','\t');
        end
        %       fclose(fid);
    else
        save(saveparams.file,'wtstruct');
    end
end
if ~PDPAppdata.gui
    fprintf(1,'Weights saved in %s mode onto %s\n',saveparams.mode,...
        saveparams.file);
end
