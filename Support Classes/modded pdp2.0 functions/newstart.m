function newstart
global net PDPAppdata;
if exist('RandStream','file') == 2 % for version 7.8 and above    
   stream = RandStream('mt19937ar','Seed',sum(100*clock));
   RandStream.setGlobalStream(stream);
   net.seed = stream.Seed;
else
   rand('seed',sum(100*clock));
   net.seed = rand('seed');
end



% net.seed = sum(100*clock);
% Save lognetwork flag ,set it to 0 for call to reset function so that
% newstart and reset do not get logged together. Reset lognetwork flag
% after call to reset function returns.
logind = PDPAppdata.lognetwork;
PDPAppdata.lognetwork = 0;
net.reset_net();
PDPAppdata.lognetwork = logind;
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end