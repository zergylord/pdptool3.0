function varargout = pdp(varargin)
% PDP Parallel Distributed Processing graphical user interface 
%      for simulating neural network models, as discussed in 
%      Parallel distributed processing: Explorations in the 
%      microstructure of cognition (McClelland, J. L., Rumelhart,
%      D. E., and the PDP research group. (1986)).
%      
%      Syntax : 
%      pdp <network script(s)> 
%   
%      Description :
%      pdp by itself opens the main pdp window which allows the user to
%      create ,load and run neural network simulations. It can also be
%      invoked with input arguments that are names of existing network
%      script files.This will load all the corresponding networks in the 
%      pdp interface, making the last loaded network the current network.
%      The script files need to have a .net extension.
%
% Last Modified by GUIDE v2.5 25-Oct-2010 12:21:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pdp_OpeningFcn, ...
                   'gui_OutputFcn',  @pdp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pdp is made visible.
function pdp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pdp (see VARARGIN)

global PDPAppdata;
if isempty(PDPAppdata)
   initsystem(1);
end
if ~PDPAppdata.gui   
    delete(findobj('tag','mainpdp'));
    return;
end
for i=1:size(varargin,2)
    file=varargin{i};
    [path,nam,ext]= fileparts(file);
    if isempty(ext)
        ext='.net';
        file = strcat(path,nam,ext);        
    end
    if ~strcmp(ext,'.net') &&  ~strcmp(ext,'.m')
        fprintf(1,'ERROR : File should be a network(.net) file or matlab script (.m) file\n');
    else
        if exist(file,'file') ~=2
           fprintf(1,'ERROR : File %s does not exist\n',file);
        else    
           loadscript(file);
        end
    end
end
movegui('center');
fontctrls = findobj(findall(hObject),'-property','FontSize');
set(fontctrls,'FontName','MS Sans Sherif','FontUnits','pixels','FontSize',11);
set(0,'DefaultUicontrolFontName','MS Sans Sherif');
set(0,'DefaultUicontrolFontUnits','pixels');
set(0,'DefaultUicontrolFontSize',11);
set(0,'DefaultUipanelFontName','MS Sans Sherif');
set(0,'DefaultUipanelFontUnits','pixels');
set(0,'DefaultUipanelFontSize',11);
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pdp wait for user response (see UIRESUME)
% uiwait(handles.mainpdp);


% --- Outputs from this function are returned to the command line.
function varargout = pdp_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
global PDPAppdata;
if PDPAppdata.gui
   varargout{1} = handles.output;
else
   varargout{1} = [];
end


function netpop_Callback(hObject, eventdata, handles)
% callback for pop-up menu containing all loaded networks on the main pdp
% window. This changes the global net variable to point to the currently 
% selected network from the list.

global net PDPAppdata;
networks = PDPAppdata.networks;
if isempty(networks)
   return;
end
ind = get(hObject,'Value');
net = networks{ind};
if isempty(net.templatefile)
   PDPAppdata.dispobjects = [];
   PDPAppdata.templatesize = [];
   PDPAppdata.cellsize = [];
else
    loadtemplate(net.templatefile);
    if PDPAppdata.lognetwork
       statement = sprintf('loadtemplate (''%s'')',net.templatefile);
       updatelog(statement);
    end
end
PDPAppdata.logfilename = net.logfile;


function netpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function trpatpop_Callback(hObject, eventdata, handles)
global PDPAppdata;
networks = PDPAppdata.networks;
if isempty(networks)
   return;
end
pfiles = cellstr(get(hObject, 'String'));
val = get(hObject,'Value');
settrainopts('trainset',pfiles{val});
if PDPAppdata.lognetwork
   stmt = sprintf('settrainopts (''trainset'',''%s'');',pfiles{val});
   updatelog(stmt);
end

function trpatpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function testpatpop_Callback(hObject, eventdata, handles)
global PDPAppdata;
networks = PDPAppdata.networks;
if isempty(networks)
   return;
end
pfiles = cellstr(get(hObject, 'String'));
val = get(hObject,'Value');
settestopts('testset',pfiles{val});
if PDPAppdata.lognetwork
   stmt = sprintf('settestopts (''testset'',''%s'');',pfiles{val});
      updatelog(stmt);
end       
   

function testpatpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function loadscriptbtn_Callback(hObject, eventdata, handles)
%get script file with fileopen dialog
global PDPAppdata;
[filename, pathname findex] = uigetfile({'*.net', 'Network scripts(*.net)';...
                       '*.m','Matlab scripts (*.m)'}, 'Load Network Script');
% If "Cancel" is selected then return
if isequal([filename,pathname],[0,0])
	return
% Otherwise construct the fullfilename and Check and load the file.
end
File = fullfile(pathname,filename);
if strcmpi(pathname(1:end-1),pwd)
    File = filename;
end
loadscript(filename(1:end-2));
if PDPAppdata.lognetwork
    statement= sprintf('loadscript (''%s'');',File);
    updatelog(statement);
end

function loadpatbtn_Callback(hObject, eventdata, handles)
global PDPAppdata;
[filename, pathname] = uigetfile({'*.pat','Pattern Files (*.pat)'}, ...
                       'Load Pattern File');
if isequal([filename,pathname],[0,0])
	return
% Otherwise construct the fullfilename and Check and load the file.
else
    File = fullfile(pathname,filename);
    [p,name,e] = fileparts(filename); %to get name without extension
    if strcmpi(pathname(1:end-1),pwd)
       File = filename;
    end 
    loadpattern('file',File,'setname',name,'usefor','both');
end
% if PDPAppdata.lognetwork
%    stmt = sprintf('loadpattern (''%s'',''%s'',''both'');',File,name);
%    updatelog(stmt);
% end

% Exit button - delete handles to exit
function exitbtn_Callback(hObject, eventdata, handles)
pdpquit;

%--------------------------------------------------------

function filenew_Callback(hObject, eventdata, handles)
edit;
%--------------------------------------------------------

function fileopen_Callback(hObject, eventdata, handles)
% Use UIGETFILE to allow for the selection of a custom address book.
[filename, pathname] = uigetfile({'*.*','All Files (*.*)'}, 'Open');
% If "Cancel" is selected then return
if isequal([filename,pathname],[0,0])
	return
% Otherwise construct the fullfilename and Check and load the file.
else
	File = fullfile(pathname,filename);
    if exist(File, 'file') == 2
        edit(File);
    end
end
%--------------------------------------------------------

% File 'exit' menu item. Delete handles to exit
function filexit_Callback(hObject, eventdata, handles)
pdpquit;


% function exitall()
% % Delete any windows that might be open
% p =mfilename('fullpath');
% l=length(mfilename);
% p=p(1:end-l);
% pd = dir(p);
% allf = {pd.name};
% findfigs = strfind(allf,'.fig');
% cfigs = cellfun('isempty',findfigs);
% figtags = regexprep(allf(~cfigs),'.fig','');
% figtags=[figtags ,{'mainpdp','dispfig','strexplore','setwriteopts'}];
% for i=1:numel(figtags)
%     h= findobj('tag',figtags{i});
%     if ~isempty(h)
%         delete(h);
%     end
% end
% clear global;
% fclose('all');
% delf = sprintf('%s%stemp.m',pwd,filesep);
% if exist(delf,'file') ==2
%    delete(delf);
% end

%--------------------------------------------------------

function nwmenu_Callback(hObject, eventdata, handles)
global PDPAppdata;
networks = PDPAppdata.networks;
global net;
allmenu = get(hObject,'children');
if ~isempty(networks)
    set(allmenu,'Visible','on','enable','on');
    if strcmpi(net.type,'cs') || strcmpi(net.type,'iac')
        set(handles.nwtroptsmenu,'enable','off');
    end       
else
    crmenu=findobj('tag','nwcreatemenu');
    h_include=allmenu(allmenu ~= crmenu);
    set(h_include,'enable','off');
end

function nwcreatemenu_Callback(hObject, eventdata, handles)
createnetwork;
% networkcreate;

function nwtroptsmenu_Callback(hObject, eventdata, handles)
% Load training options from GUI application data
global PDPAppdata;
networks = PDPAppdata.networks;
ind = get(handles.netpop,'Value');
if strcmpi(networks{ind}.type,'srn')
   t = 'bp';
else
   t = networks{ind}.type;
end
tfunc = str2func(sprintf('%s_troption',t));
tfunc();

function nwdeletemenu_Callback(hObject, eventdata, handles)
if isempty(findobj('tag','netdisplay'))
   set(get(hObject,'children'),'enable','on');    
else
   set(get(hObject,'children'),'enable','off');
end

function selectdispmenu_Callback(hObject, eventdata, handles)
global net PDPAppdata;
if isempty(net.templatefile)
   PDPAppdata.dispobjects = [];
else
   if isempty(PDPAppdata.dispobjects)
      loadtemplate(net.templatefile);
   end
end
structexplore(net);

function displaymenu_Callback(hObject, eventdata, handles)
global net PDPAppdata;
if isempty(PDPAppdata.networks)
   set(get(hObject,'children'),'enable','off');
else
   set(get(hObject,'children'),'Visible','on','enable','on');
   if isempty(net.templatefile)
      set(handles.editdispmenu,'enable','off');
   end
end

function loaddisptempmenu_Callback(hObject, eventdata, handles)
global PDPAppdata;
[filename, pathname] = uigetfile({'*.tem','Template Files (*.tem)'}, 'Load Template File');
if isequal([filename,pathname],[0,0])
	return
% Otherwise construct the fullfilename and Check and load the file.
end
File = fullfile(pathname,filename);
if strcmpi(pathname(1:end-1),pwd)
   File = filename;
end 
loadtemplate (File);
if PDPAppdata.lognetwork
   statement = sprintf('loadtemplate (''%s'')',File);
   updatelog(statement);
end

function helpmenu_Callback(hObject, eventdata, handles)


function helpaboutmenu_Callback(hObject, eventdata, handles)
about;

function nwlaunchmenu_Callback(hObject, eventdata, handles)
global PDPAppdata;
launchnet;
if PDPAppdata.lognetwork
    updatelog('launchnet;');
end


function nwresetmenu_Callback(hObject, eventdata, handles)
global net;
fname = sprintf('%s_reset',net.type);
resetfunc = str2func(fname);
resetfunc();



function nwnewstartmenu_Callback(hObject, eventdata, handles)
newstart;



function nwtstoptsmenu_Callback(hObject, eventdata, handles)
% Load testing options from GUI application data
global PDPAppdata;
h=guihandles;
net = PDPAppdata.networks;
ind = get(h.netpop,'Value');
if strcmpi(net{ind}.type,'srn')
   t = 'bp';
else
   t = net{ind}.type;
end
tfunc = str2func(sprintf('%s_testoption',t));
tfunc();



function nwloadwtmenu_Callback(hObject, eventdata, handles)
global PDPAppdata;
[filename, pathname] = uigetfile({'*.wt','Weight Files (*.wt)'}, ...
                       'Load Weight File');
if isequal([filename,pathname],[0,0])
	return;
% Otherwise construct the fullfilename and Check and load the file.
end
File = fullfile(pathname,filename);
if strcmpi(pathname(1:end-1),pwd)
   File = filename;
end 
loadweights(File);
if PDPAppdata.lognetwork
   statement = sprintf('loadweights (''%s'');',File);
   updatelog(statement);
end



function nwsavewtmenu_Callback(hObject, eventdata, handles)
savewt;


function mainpdp_CloseRequestFcn(hObject, eventdata, handles)
pdpquit;


function filelog_Callback(hObject, eventdata, handles)
global PDPAppdata;
status = 'off';
if strcmpi(get(hObject,'checked'),'off')
   status = 'on';
end
if ~isdeployed && isempty(PDPAppdata.history)
   fprintf(1,['WARNING : Logging not enabled possibly because pdptool cannot locate '...
   'matlab history file\n']);
else
    enablelog(status);   
end


function nwmanagelogmenu_Callback(hObject, eventdata, handles)
create_edit_log;
% setwriteopts;



function delsubnw_Callback(hObject, eventdata, handles)
global net PDPAppdata;
nets = PDPAppdata.networks;
currnets = get(handles.netpop,'String');
currnets(net.num)=[];
nets(net.num)=[];
if isempty(nets)
   clear global net;
   currnets={'none'};
else
    for i=1:numel(nets)
        nets{i}.num=i;
    end
    net = nets{1};
    PDPAppdata.dispobjects = [];
    PDPAppdata.templatesize = [];
    PDPAppdata.cellsize = [];    
    if ~isempty(net.templatefile)
        loadtemplate(net.templatefile);
    end
end
set(handles.netpop,'String',currnets,'Value',1);
PDPAppdata.networks = nets;



function delsubtrp_Callback(hObject, eventdata, handles)
global net PDPAppdata;
trpats = cellstr(get(handles.trpatpop,'String'));
v = get(handles.trpatpop,'Value');
pat_to_delete = trpats{v};
if strcmpi(pat_to_delete,'none')
    return;
end
n = PDPAppdata.networks;
pfiles = PDPAppdata.patfiles;
pfileindex = find(strcmpi(pfiles(:,1),pat_to_delete));
for i = 1 : numel(n)
    cnet = n{i};
    if find(strcmpi(cnet.trainopts.trainset,{pfiles{pfileindex,1:2}}))
       cnet.trainopts.trainset = 'none';
       n{i} = cnet;
       if i == net.num
          net = cnet;
       end
    end
end
PDPAppdata.networks = n;
tstfiles = get(handles.testpatpop,'String');
testfind = strcmpi(tstfiles,pat_to_delete);
if isempty(find(testfind,1))
    pfiles(pfileindex,:) = [];
end
trpats(v) = [];
PDPAppdata.trainData = [];
set(handles.trpatpop,'String',trpats,'Value',1);
PDPAppdata.patfiles = pfiles;

function delsubtstp_Callback(hObject, eventdata, handles)
global net PDPAppdata;
tstpats = cellstr(get(handles.testpatpop,'String'));
v = get(handles.testpatpop,'Value');
pat_to_delete = tstpats{v};
if strcmpi(pat_to_delete,'none')
    return;
end
n = PDPAppdata.networks;
pfiles = PDPAppdata.patfiles;
pfileindex = find(strcmpi(pfiles(:,1),pat_to_delete));
for i=1:numel(n)
    cnet = n{i};
    if find(strcmpi(cnet.testopts.testset,{pfiles{pfileindex,1:2}}))
       cnet.testopts.testset='none';
       n{i} = cnet;
       if i == net.num
          net = cnet;
       end       
    end
end
PDPAppdata.networks = n;
trfiles = get(handles.trpatpop,'String');
trfind = strcmpi(trfiles,pat_to_delete);
if isempty(find(trfind,1))
    pfiles(pfileindex,:) = [];
end
tstpats(v) = [];
PDPAppdata.testData = [];
set(handles.testpatpop,'String',tstpats,'Value',1);
PDPAppdata.patfiles = pfiles;




function nweditobjmenu_Callback(hObject, eventdata, handles)
launchobjectviewer;



function editdispmenu_Callback(hObject, eventdata, handles)
edittemplateparams;



% --- Executes during object creation, after setting all properties.
function filenew_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filenew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if isdeployed
   set(hObject,'Enable','off');
end


% --- Executes during object creation, after setting all properties.
function fileopen_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fileopen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if isdeployed
   set(hObject,'Enable','off');
end
