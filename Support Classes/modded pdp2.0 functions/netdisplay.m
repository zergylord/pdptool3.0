function varargout = netdisplay(varargin)
% NETDISPLAY M-file for netdisplay.fig
%      NETDISPLAY, by itself, creates a new NETDISPLAY or raises the existing
%      singleton*.
%
%      H = NETDISPLAY returns the handle to a new NETDISPLAY or the handle to
%      the existing singleton*.
%
%      NETDISPLAY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NETDISPLAY.M with the given input arguments.
%
%      NETDISPLAY('Property','Value',...) creates a new NETDISPLAY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before netdisplay_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to netdisplay_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help netdisplay

% Last Modified by GUIDE v2.5 14-Aug-2013 11:14:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @netdisplay_OpeningFcn, ...
                   'gui_OutputFcn',  @netdisplay_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before netdisplay is made visible.
function netdisplay_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to netdisplay (see VARARGIN)

% Choose default command line output for netdisplay
global net troptfunc testoptfunc testrunfunc teststepfunc resetfunc;
global PDPAppdata trainrunfunc trainstepfunc displayscale;
net.gui = true;
handles.output = hObject;
total_margin=45;
margin=15;
pat_panel_width=240;
layout=[0 0];
templatesz = PDPAppdata.templatesize; %getappdata(pdp_handle,'templatesize');
cellsize = PDPAppdata.cellsize; %getappdata(pdp_handle,'cellsize');
figpos = get(hObject,'Position');
ctrlpnlpos = get(handles.controlpnl,'Position');
if ~isempty(templatesz) && ~isempty(cellsize)
    layout = templatesz * cellsize;
end
orig_layout = layout;
if ((layout(2)+total_margin+pat_panel_width) - figpos(3) > 0 )
    fig_width = layout(2)+total_margin+pat_panel_width;
else
    fig_width = ctrlpnlpos(3) + total_margin + pat_panel_width; %figpos(3);
    layout(2) = fig_width - (total_margin + pat_panel_width);
end
% fig_height = figpos(4)- 1.5*margin + layout(1);
fig_height = ctrlpnlpos(4)+2*margin+layout(1);

% % Get screensize in pixels
Units=get(0,'units'); 
set(0,'units','pixels'); 
ss = get(0,'ScreenSize');
set(0,'units',Units);
swidth = ss(3); 
sheight = ss(4);
if fig_width > swidth
    layout(2) = layout(2) - (fig_width - swidth);        
    fig_width = swidth;
end
if fig_height > sheight
    layout(1) = layout(1) - (fig_height - sheight);    
    fig_height = sheight;
end
displayscale = 1;
if all(orig_layout)
   displayscale = layout ./ orig_layout;
end
left = (swidth-fig_width)/2; 
bottom = (sheight-fig_height)/2;
figrect = [left bottom fig_width fig_height];
panelpos = get(handles.layoutpnl,'Position');
panelpos(1)=margin/fig_width;
panelpos(2)=margin/fig_height;
if all(layout)
    panelpos(4) = layout(1)/fig_height;
    panelpos(3)=layout(2)/fig_width;
end
set(handles.layoutpnl,'Position',panelpos);
set(handles.layoutpnl,'Units','normalized');
panelpos=get(handles.layoutpnl,'Position');
ctrlpnlpos = get(handles.controlpnl,'Position');
ctrlpnlpos(2)=panelpos(2) + panelpos(4) + margin/fig_height;
ctrlpnlpos(1)=margin/fig_width;
ctrlpnlpos(3)=ctrlpnlpos(3)/fig_width;
ctrlpnlpos(4)= ctrlpnlpos(4)/fig_height;
patpanelpos=get(handles.patpanel,'Position');
patpanelpos(1)=panelpos(1) + panelpos(3) + margin/fig_width;
patpanelpos(2)=panelpos(2);
patpanelpos(3)=pat_panel_width/fig_width;
patpanelpos(4)=0.98; 
set(handles.patpanel,'Position',patpanelpos);
set(handles.controlpnl,'Units','normalized');
set(handles.controlpnl,'Position',ctrlpnlpos);
set(hObject,'Position',figrect,'Visible','off','Resize','off');
if any(strcmpi(net.type,{'bp','srn'}))
   t = 'bp';
else
   t = net.type;
end
troptfunc = str2func(sprintf('%s_troption',t));
testoptfunc = str2func(sprintf('%s_testoption',t));
testrunfunc = str2func(sprintf('%s_testrun',net.type));
teststepfunc = str2func(sprintf('%s_teststep',net.type));
trainrunfunc = str2func(sprintf('%s_trainrun',net.type));
trainstepfunc = str2func(sprintf('%s_trainstep',net.type));
resetfunc = str2func(sprintf('%s_reset',net.type));
% MY MOD
% PDPAppdata.testData =1;

%TAKES CARE OF TRDATA/TSTDATA
%net.set_pattern_display
set_pattern_display();
tstdata = PDPAppdata.testData;
trdata = PDPAppdata.trainData;
PDPAppdata.train_options_disp = {};
PDPAppdata.test_options_disp = {};
%call net.init_options
%then comment out the switch
switch net.type
    case {'pa','cl'}  
        if strcmpi(net.type,'pa')
        PDPAppdata.train_options_disp = {'ecrit','num','<-','temp','num','<-','noise','num',...
            'lflag','bool','<-','lrate','num','<-','lrule',{'Hebb','Delta'},...
            'nepochs','num','<-','trainmode',{'s','r','p'},...
                };
            PDPAppdata.test_options_disp = {'ecrit','num','<-','temp','num','<-','noise','num',...
                };
        end
         set(handles.testgranlist,'Visible','off');
         set(handles.tallchk,'Visible','on');
         tstpanelobjs = findobj(allchild(handles.testpanel),'-not','tag',...
                       'testradio');
         set(tstpanelobjs,'enable','off');
         if isempty(trdata)
            trbuttons =findobj(handles.trainpanel,'Style','pushbutton');
            set(trbuttons,'enable','off');
         end
        if isempty(tstdata)
           tstbuttons =findobj(handles.testpanel,'Style','pushbutton');
           set(tstbuttons,'enable','off');
        end
    case {'srn','rbp'}
        if strcmpi(net.type,'rbp')
            PDPAppdata.train_options_disp = {'errmargin','num','<-','wrange','num','<-','ecrit','num','<-','fastrun','bool'...
                'errmeas',{'sse','cee'},'<-','wdecay','num','<-','follow','bool',...
                'lflag','bool','<-','lrate','num','<-','lgrain',{'tick','pattern','sequence','epoch'},'<-','lgrainsize','num',...
                'nepochs','num','<-','trainmode',{'s', 'p','r'},'<-','momentum','num'...
                };
            PDPAppdata.test_options_disp = {'fastrun','bool'};
        else
        PDPAppdata.train_options_disp = {'wrange','num','<-','ecrit','num','<-','fastrun','bool',...
                'errmeas',{'sse','cee'},'<-','wdecay','num','<-','follow','bool','<-','tmax','num',...
                'cascade','bool','<-','ncycles','num','<-','crate','num','<-','mu','num'...
                'lflag','bool','<-','lrate','num','<-','lgrain',{'tick','pattern','sequence','epoch'},'<-','lgrainsize','num',...
                'nepochs','num','<-','trainmode',{'s', 'p','r'},'<-','momentum','num'...
                };
            PDPAppdata.test_options_disp = {'ecrit','num','<-','fastrun','bool','<-','tmax','num',...
                'cascade','<-','bool','ncycles','num','<-','crate','num','<-','mu','num'...
                };
        end
         set(handles.tallchk,'Visible','on');
         tstpanelobjs = findobj(allchild(handles.testpanel),'-not','tag',...
                       'testradio');
         set(tstpanelobjs,'enable','off');
         if isempty(trdata)
            trbuttons =findobj(handles.trainpanel,'Style','pushbutton');
            set(trbuttons,'enable','off');
         end
        if isempty(tstdata)
           tstbuttons =findobj(handles.testpanel,'Style','pushbutton');
           set(tstbuttons,'enable','off');
        end        
        set(findobj('tag','testgranlist'),'Visible','on');
        glistpos = get(findobj('tag','testgranlist'),'Position');
        pos = get(findobj('tag','tallchk'),'Position');
        pos(1) = glistpos(1) + glistpos(3);
        set(findobj('tag','tallchk'),'Position',pos,'Visible','on');
    case 'bp'
        PDPAppdata.train_options_disp = {'wrange','num','<-','ecrit','num','<-','fastrun','bool',...
                'errmeas',{'sse','cee'},'<-','wdecay','num','<-','follow','bool','<-','tmax','num',...
                'cascade','bool','<-','ncycles','num','<-','crate','num','<-','mu','num'...
                'lflag','bool','<-','lrate','num','<-','lgrain',{'tick','pattern','sequence','epoch'},'<-','lgrainsize','num',...
                'nepochs','num','<-','trainmode',{'s', 'p','r'},'<-','momentum','num'...
                };
            PDPAppdata.test_options_disp = {'ecrit','num','<-','fastrun','bool','<-','tmax','num',...
                'cascade','<-','bool','ncycles','num','<-','crate','num','<-','mu','num'...
                };
         set(handles.tallchk,'Visible','on');
         tstpanelobjs = findobj(allchild(handles.testpanel),'-not','tag',...
                       'testradio');
         set(tstpanelobjs,'enable','off');
         if isempty(trdata)
            trbuttons =findobj(handles.trainpanel,'Style','pushbutton');
            set(trbuttons,'enable','off');
         end
        if isempty(tstdata)
           tstbuttons =findobj(handles.testpanel,'Style','pushbutton');
           set(tstbuttons,'enable','off');
        end 
        set(findobj('tag','testgranlist'),'Visible','off');      
       if (net.test_options.cascade)
          set(findobj('tag','testgranlist'),'Visible','on');
          glistpos = get(findobj('tag','testgranlist'),'Position');
          pos = get(findobj('tag','tallchk'),'Position');
          pos(1) = glistpos(1) + glistpos(3);
          set(findobj('tag','tallchk'),'Position',pos,'Visible','on'); 
       end
%     case 'cs'
%          set(get(findobj('tag','trainpanel'),'children'),'enable','off');
%          set(findobj('tag','testgranlist'),'Visible','on');
%          set(findobj('tag','testradio'),'Visible','off');         
%          set(findobj('tag','tallchk'),'Visible','off');
%          if ~isempty(tstdata) 
%             set(findobj('tag','tstpatchk'),'Visible','on');
%          end
%          set(findobj('tag','tstpatlist'),'Visible','on','enable','off'); 
    case {'cs' 'iac'}
        if strcmpi(net.type,'cs')
            PDPAppdata.test_options_disp = {'actfunction',{'schema','boltzmann'},'<-','temp','num','<-','istr','num','<-','estr','num','<-','nupdates','num','<-','ncycles','num'...
                'clamp','bool'};
        else
            PDPAppdata.test_options_disp = {'alpha','num','<-','gamma','num','<-','ncycles','num','<-','decay','num','<-','estr','num'...
                'actfunction',{'st','gr'}};
        end
         set(get(findobj('tag','trainpanel'),'children'),'enable','off');
         set(findobj('tag','testgranlist'),'Visible','on');
         set(findobj('tag','testradio'),'Visible','off');            
         if isempty(tstdata) 
            set(findobj('tag','tstpatchk'),'Visible','off');
            set(findobj('tag','tallchk'),'Visible','off');
         else
            glistpos = get(findobj('tag','testgranlist'),'Position');
            pos = get(findobj('tag','tallchk'),'Position');
            pos(1) = glistpos(1) + glistpos(3);              
            set(findobj('tag','tstpatchk'),'Visible','on');
            set(findobj('tag','tallchk'),'Position',pos,'Visible','on',...
            'Enable','off');              
         end
         set(findobj('tag','tstpatlist'),'Visible','on','enable', ...
                           'off');
    case 'tdbp'
        PDPAppdata.train_options_disp = {'lrate','num','<-','wdecay','num','<-','wrange','num',...
            'gamma','num','<-','lgrain',{'pattern','epoch'},'<-','lflag','bool',...
            'lambda','num','<-','epsilon','num',...
            'nepochs','num','<-','policy',{'egreedy','greedy','softmax','defer','userdef'}};
       PDPAppdata.test_options_disp = {'epsilon','num',...
            'policy',{'egreedy','greedy','softmax','defer','userdef'}};
         set(handles.tallchk,'Visible','off');
         tstpanelobjs = findobj(allchild(handles.testpanel),'-not','tag',...
                       'testradio');
         set(tstpanelobjs,'enable','off');
         if isempty(trdata)
            trbuttons =findobj(handles.trainpanel,'Style','pushbutton');
            set(trbuttons,'enable','off');
         end
        if isempty(tstdata)
           tstbuttons =findobj(handles.testpanel,'Style','pushbutton');
           set(tstbuttons,'enable','off');
        end 
        %set(findobj('tag','testgranlist'),'Visible','off');   
        fastfile = sprintf('%s_run_fast',net.type);
%         if exist(fastfile,'file')==2
%            set(handles.fastchk,'visible','on');
%         else
%            set(handles.fastchk,'visible','off');     
%            set(findobj('tag','testgranlist'),'Visible','on');
%         end
end

if ~isempty(PDPAppdata.dispobjects) %~isempty(getappdata(pdp_handle,'dispobjects'))
    draw_network_objects(hObject);
end

PDPAppdata.train_options_figure = options_gui('train');
PDPAppdata.test_options_figure = options_gui('test');

dirname =mfilename('fullpath');
dd= regexp(dirname,mfilename);
mapfname = sprintf('%sdefaultmap.mat',dirname(1:dd-1));
if exist(mapfname,'file')==2
   setcolormap(mapfname);
end
trpattern_lbox = findobj('tag','trpatlist');
tstpattern_lbox = findobj('tag','tstpatlist');
if ~strcmp(net.type,'tdbp')     %... but not if this is type 'tdbp'
    set(trpattern_lbox,'String',trdata,'Value',1);
    set(tstpattern_lbox,'String',tstdata,'Value',1);
%    fill_patterns(tstpattern_lbox,tstdata);
end
set(hObject,'Visible','on');
update_display(0);
PDPAppdata.trinterrupt_flag = 0;
PDPAppdata.tstinterrupt_flag = 0;
% setappdata(pdp_handle,'trinterrupt_flag',0);
% setappdata(pdp_handle,'tstinterrupt_flag',0);
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes netdisplay wait for user response (see UIRESUME)
% uiwait(handles.netdisplay);


% --- Outputs from this function are returned to the command line.
function varargout = netdisplay_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;
varargout{1} =hObject;

function countedit_Callback(hObject, eventdata, handles)
count = str2double(get(hObject,'String'));
if isempty(count) || isnan(count)
    set(hObject,'String',1);
end
set(handles.trpatlist,'Visible','on');
set(handles.tstpatlist,'Visible','off');

function countedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function granlist_Callback(hObject, eventdata, handles)

function granlist_CreateFcn(hObject, eventdata, handles)
global net;
if isempty(net)
   return;
end
t = net.type;
switch t
    case 'pa'
         liststr={'epoch','cycle','pattern'};
    case 'cs'
         liststr={'cycle','update'};         
    case 'bp'
         liststr={'epoch','pattern'}; 
    case 'rbp'
         liststr = {'epoch','sequence','pattern','tick'};         
    case 'srn'
         liststr = {'epoch','pattern','sequence'};       
    case 'cl'
         liststr={'epoch','pattern'};
    case 'iac'
         liststr={'cycle'};  
    case 'tdbp'
         liststr={'epoch','step'};
    otherwise
         disp('net.type not set correctly');
end
set(hObject,'String',liststr);

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function trainoptsbtn_Callback(hObject, eventdata, handles)
global PDPAppdata
% global troptfunc;
set(handles.trpatlist,'Visible','on');
set(handles.tstpatlist,'Visible','off');
% set(handles.trpatradio,'Value',1);
% set(handles.patpanel,'Title','Training patterns');
% troptfunc();
set(PDPAppdata.train_options_figure,'Visible','on');


function curr_epochedit_Callback(hObject, eventdata, handles)


function curr_epochedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function stopbtn_Callback(hObject, eventdata, handles)
global PDPAppdata;
PDPAppdata.stopprocess = 1;
uiresume(handles.netdisplay);

function runbtn_off_Callback(hObject, eventdata, handles)
global trainrunfunc;
trainrunfunc(handles);

function stepbtn_off_Callback(hObject, eventdata, handles)
global trainstepfunc;
trainstepfunc(handles);

function pa_trainstep(handles)
global CALLIND;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','mode','step','granularity',procstep,'count',...
        proccount};
runprocess(args{1:end});


function bp_trainstep(handles)
global CALLIND;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','mode','step','granularity',procstep,'count',...
        proccount};
runprocess(args{1:end});

function tdbp_trainstep(handles)
global CALLIND;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','mode','step','granularity',procstep,'count',...
        proccount};
runprocess(args{1:end});

function srn_trainstep(handles)
global CALLIND;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','mode','step','granularity',procstep,'count',...
        proccount};
runprocess(args{1:end});

function rbp_trainstep(handles)
global CALLIND;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','mode','step','granularity',procstep,'count',...
        proccount};
% profile on -history
runprocess(args{1:end});
% profile viewer

function testcountedit_Callback(hObject, eventdata, handles)
set(handles.trpatlist,'Visible','off');
set(handles.tstpatlist,'Visible','on');
% set(handles.tstpatradio,'Value',1);
% set(handles.patpanel,'Title','Testing patterns');

function testcountedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function testoptsbtn_Callback(hObject, eventdata, handles)
global PDPAppdata;
set(handles.trpatlist,'Visible','off');
set(handles.tstpatlist,'Visible','on');
% set(handles.tstpatradio,'Value',1);
% set(handles.patpanel,'Title','Testing patterns');
% testoptfunc();
set(PDPAppdata.test_options_figure,'Visible','on');



function testrunbtn_off_Callback(hObject, eventdata, handles)
global testrunfunc;
testrunfunc(handles);

function pa_trainrun(handles)
uiresume(handles.netdisplay);
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
steplist = get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','granularity',procstep,'count',proccount};
runprocess(args{1:end});    
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
   else
      rangemat = [rangelog;rangemat];
   end   
   statement = sprintf(['runprocess (''process'',''train'',''granularity'','...
               '''%s'',''count'',%d,''range'',%s);'],procstep,proccount,...
               mat2str(rangemat));
   updatelog(statement);
end
rangelog = [];

function bp_trainrun(handles)
uiresume(handles.netdisplay);
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','granularity',procstep,'count',proccount};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];   
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
   else
      rangemat = [rangelog;rangemat];
   end   
   statement = sprintf(['runprocess (''process'',''train'',''granularity'''...
               ',''%s'',''count'',%d,''range'',%s);'],procstep,proccount,...
               mat2str(rangemat));
   updatelog(statement);
end
rangelog = [];

function srn_trainrun(handles)
uiresume(handles.netdisplay);
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','granularity',procstep,'count',proccount};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.seqno net.epochno net.patno];
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
   else
      rangemat = [rangelog;rangemat];
   end  
   statement = sprintf(['runprocess (''process'',''train'',''granularity'''...
               ',''%s'',''count'',%d,''range'',%s);'],procstep,proccount,...
               mat2str(rangemat));   
   updatelog(statement);
end
rangelog = [];

function rbp_trainrun(handles)
uiresume(handles.netdisplay);
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','granularity',procstep,'count',proccount};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];
    if ~isequal(size(rangelog),size(rangemat))
       rangemat = [];
    else
       rangemat = [rangelog;rangemat];
    end   
   statement = sprintf(['runprocess (''process'',''train'',''granularity'''...
               ',''%s'',''count'',%d,''range'',%s);'],procstep,proccount,...
               mat2str(rangemat));
   updatelog(statement);
end
rangelog = [];


function pa_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
proccount = 1;
tall_indic = get(handles.tallchk,'Value'); % 0 for test pattern,1 for test all
if tall_indic
   proccount = str2double(get(handles.testcountedit,'String'));
end
procstep = 'pattern';
args = {'mode','run','granularity',procstep,'count',proccount,...
        'alltest', tall_indic};
runprocess(args{1:end});
if PDPAppdata.lognetwork
    rangemat = [net.patno net.epochno]; %changed testpatno to patno
    if ~isequal(size(rangelog),size(rangemat))
       rangemat = [];
    else
       rangemat = [rangelog;rangemat];
    end
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'',%d,'...
               '''alltest'',%d,''range'',%s);'],procstep,proccount,...
               tall_indic,mat2str(rangemat));
   updatelog(statement);
end
rangelog = [];

function cs_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND=1;
proccount = str2double(get(handles.testcountedit,'String'));
steplist =get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
pattestind=0;
tallind=1;
if strcmpi(get(handles.tstpatchk,'Visible'),'on')
   pattestind = get(handles.tstpatchk,'Value');
   tallind = get(handles.tallchk,'Value');
end
args = {'mode','run','granularity',procstep,'count',proccount,'pattest',...
        pattestind,'alltest', tallind};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'','...
               '%d '],procstep,proccount);    
    rangemat = [net.cycleno net.updateno];
    if pattestind
       statement = sprintf('%s,''pattest'',1',statement);
       if tallind
          statement = sprintf('%s,''alltest'', 1',statement);
       end        
       rangemat(end+1) = net.patno;
    end
    if ~isequal(size(rangelog),size(rangemat))
        rangemat = [];
    else
        rangemat = [rangelog;rangemat];
    end
    statement = sprintf('%s,''range'', %s);',statement,mat2str(rangemat));
    updatelog(statement);
end
rangelog = [];

function iac_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
proccount = str2double(get(handles.testcountedit,'String'));
steplist =get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
pattestind = 0;
tallind = 1;
if strcmpi(get(handles.tstpatchk,'Visible'),'on')
   pattestind = get(handles.tstpatchk,'Value');
   tallind = get(handles.tallchk,'Value');
end
if pattestind && ~tallind
   net.patno = get(findobj('tag','tstpatlist'),'Value');
end
args = {'mode','run','granularity',procstep,'count',proccount,'pattest',...
        pattestind,'alltest', tallind};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'','...
               '%d '],procstep,proccount);
    rangemat = net.cycleno;
    if pattestind
       statement = sprintf('%s,''pattest'',1',statement);
       if tallind
          statement = sprintf('%s,''alltest'',1',statement);
       end       
       rangemat(end+1) = net.patno;
    end
    if ~isequal(size(rangelog),size(rangemat))
        rangemat = [];
    else
        rangemat = [rangelog;rangemat];
    end    
    statement = sprintf('%s, ''range'', %s);',statement,mat2str(rangemat));
    updatelog(statement);
end
rangelog = [];

function bp_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
proccount = 1;
tall_indic = get(handles.tallchk,'Value'); % 0 for test pattern,1 for test all
if tall_indic
   proccount = str2double(get(handles.testcountedit,'String'));
end
procstep = 'pattern';
if strcmpi(get(handles.testgranlist,'Visible'),'on')
   steplist = get(handles.testgranlist,'String');
   val = get(handles.testgranlist,'Value');
   procstep = steplist{val};
end
args = {'granularity',procstep,'count',proccount,'alltest', tall_indic};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];
    if ~isequal(size(rangelog),size(rangemat))
       rangemat = [];
    else
       rangemat = [rangelog;rangemat];
    end   
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'',%d,'...
               '''alltest'',%d,''range'',%s);'],procstep,proccount,...
               tall_indic,mat2str(rangemat));   
   updatelog(statement);
end
rangelog = [];

function tdbp_trainrun(handles)
uiresume(handles.netdisplay);
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','granularity',procstep,'count',proccount};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];   
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
   else
      rangemat = [rangelog;rangemat];
   end   
   statement = sprintf(['runprocess (''process'',''train'',''granularity'''...
               ',''%s'',''count'',%d,''range'',%s);'],procstep,...
               proccount,mat2str(rangemat));
   updatelog(statement);
end
rangelog = [];

function tdbp_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
proccount = 1;
tall_indic = get(handles.tallchk,'Value'); % 0 for test pattern,1 for test all
if tall_indic
   proccount = str2double(get(handles.testcountedit,'String'));
end
procstep = 'pattern';
if strcmpi(get(handles.testgranlist,'Visible'),'on')
   steplist = get(handles.testgranlist,'String');
   val = get(handles.testgranlist,'Value');
   procstep = steplist{val};
end
args = {'granularity',procstep,'count',proccount,'alltest', tall_indic};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];
    if ~isequal(size(rangelog),size(rangemat))
       rangemat = [];
    else
       rangemat = [rangelog;rangemat];
    end   
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'',%d,'...
               '''alltest'',%d,''range'',%s);'],procstep,proccount,...
               tall_indic,mat2str(rangemat));   
   updatelog(statement);
end
rangelog = [];

function srn_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
proccount = 1;
tall_indic = get(handles.tallchk,'Value'); % 0 for test pattern,1 for test all
if tall_indic
   proccount = str2double(get(handles.testcountedit,'String'));    
end  
steplist =get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
args = {'granularity',procstep,'count',proccount,'alltest', tall_indic};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.seqno net.epochno net.patno];
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
   else
      rangemat = [rangelog;rangemat];
   end   
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'',%d,'...
               '''alltest'',%d,''range'',%s);'],procstep,proccount,...
               tall_indic,mat2str(rangemat));    
   updatelog(statement);
end
rangelog = [];


function rbp_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
tall_indic = get(handles.tallchk,'Value'); % 0 for test pattern,1 for test all
proccount = 1;
if tall_indic
   proccount = str2double(get(handles.testcountedit,'String'));
end
steplist = get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
args = {'granularity',procstep,'count',proccount,'alltest', tall_indic};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
    else
      rangemat = [rangelog;rangemat];
    end   
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'',%d,'...
               '''alltest'',%d,''range'',%s);'],procstep,proccount,...
               tall_indic,mat2str(rangemat));
   updatelog(statement);
end
rangelog = [];


function cl_testrun(handles)
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
proccount = 1;
tall_indic = get(handles.tallchk,'Value'); % 0 for test pattern,1 for test all
if tall_indic
   proccount = str2double(get(handles.testcountedit,'String'));    
end
procstep = 'pattern';
args = {'granularity',procstep,'count',proccount,'alltest', tall_indic};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.testpatno net.epochno];
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
    else
      rangemat = [rangelog;rangemat];
    end
   statement = sprintf(['runprocess (''granularity'',''%s'',''count'',%d,'...
               '''alltest'',%d,''range'',%s);'],procstep,proccount,...
               tall_indic,mat2str(rangemat));    
   updatelog(statement);
end
rangelog = [];

function cl_trainrun(handles)
uiresume(handles.netdisplay);
global net CALLIND PDPAppdata rangelog;
CALLIND = 1;
steplist =get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','granularity',procstep,'count',proccount};
runprocess(args{1:end});
if PDPAppdata.lognetwork
   rangemat =[net.patno net.epochno];
   if ~isequal(size(rangelog),size(rangemat))
      rangemat = [];
   else
      rangemat = [rangelog;rangemat];
   end    
   statement = sprintf(['runprocess (''process'',''train'',''granularity'''...
               ',''%s'',''count'',%d,''range'',%s);'],procstep,proccount,...
               mat2str(rangemat));   
   updatelog(statement);
end
rangelog = [];

function cl_trainstep(handles)
global CALLIND;
CALLIND = 1;
steplist = get(handles.granlist,'String');
val = get(handles.granlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.countedit,'String'));
args = {'process','train','mode','step','granularity',procstep,'count',...
        proccount};
runprocess(args{1:end});


function teststepbtn_off_Callback(hObject, eventdata, handles)
global teststepfunc;
teststepfunc(handles);

function pa_teststep(handles)
global CALLIND;
CALLIND = 1;
tallind = get(handles.tallchk,'Value');
procstep = 'pattern';
proccount = str2double(get(handles.testcountedit,'String'));
args = {'mode','step','granularity',procstep,'count',proccount,...
        'alltest', tallind};
runprocess(args{1:end});

function cs_teststep(handles)
global net CALLIND;
CALLIND = 1;
proccount = str2double(get(handles.testcountedit,'String'));
steplist = get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
pattestind = 0;
tallind = 1;
if strcmpi(get(handles.tstpatchk,'Visible'),'on')
   pattestind = get(handles.tstpatchk,'Value');
%    tallind = get(handles.tallchk,'Value');
end
if pattestind && ~tallind 
   net.patno = get(findobj('tag','tstpatlist'),'Value');
end
args = {'mode','step','granularity',procstep,'count',proccount,'pattest',...
        pattestind,'alltest', tallind};
runprocess(args{1:end});

function iac_teststep(handles)
global net CALLIND PDPAppdata;
CALLIND = 1;
proccount = str2double(get(handles.testcountedit,'String'));
steplist =get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
pattestind = 0;
tallind = 1;
if strcmpi(get(handles.tstpatchk,'Visible'),'on')
   pattestind = get(handles.tstpatchk,'Value');
   tallind = get(handles.tallchk,'Value');
end
if pattestind && ~tallind
   net.patno = get(findobj('tag','tstpatlist'),'Value');
end
args = {'mode','step','granularity',procstep,'count',proccount,'pattest',...
        pattestind,'alltest', tallind};
runprocess (args{1:end});


function cl_teststep(handles)
global CALLIND;
CALLIND = 1;
tallind = get(handles.tallchk,'Value');
procstep = 'pattern';
proccount = str2double(get(handles.testcountedit,'String'));
args = {'mode','step','granularity',procstep,'count',proccount,'alltest',...
        tallind};
runprocess(args{1:end});


function bp_teststep(handles)
global CALLIND;
CALLIND = 1;
tallind = get(handles.tallchk,'Value');
procstep = 'pattern';
if strcmpi(get(handles.testgranlist,'Visible'),'on')
   steplist = get(handles.testgranlist,'String');
   val = get(handles.testgranlist,'Value');
   procstep = steplist{val};
end
proccount = str2double(get(handles.testcountedit,'String'));
args = {'mode','step','granularity',procstep,'count',proccount,'alltest',...
        tallind};
runprocess(args{1:end});

function tdbp_teststep(handles)
global CALLIND;
CALLIND = 1;
tallind = get(handles.tallchk,'Value');
procstep = 'pattern';
if strcmpi(get(handles.testgranlist,'Visible'),'on')
   steplist = get(handles.testgranlist,'String');
   val = get(handles.testgranlist,'Value');
   procstep = steplist{val};
end
proccount = str2double(get(handles.testcountedit,'String'));
args = {'mode','step','granularity',procstep,'count',proccount,'alltest',...
        tallind};
runprocess(args{1:end});

function srn_teststep(handles)
global CALLIND;
CALLIND = 1;
tallind = get(handles.tallchk,'Value');
steplist = get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.testcountedit,'String'));
args = {'mode','step','granularity',procstep,'count',proccount,'alltest',...
        tallind};
runprocess(args{1:end});

function rbp_teststep(handles)
global CALLIND;
CALLIND = 1;
tallind = get(handles.tallchk,'Value');
steplist = get(handles.testgranlist,'String');
val = get(handles.testgranlist,'Value');
procstep = steplist{val};
proccount = str2double(get(handles.testcountedit,'String'));
args = {'mode','step','granularity',procstep,'count',proccount,'alltest',...
        tallind};
runprocess(args{1:end});


function tallchk_Callback(hObject, eventdata, handles)
set(handles.trpatlist,'Visible','off');
set(handles.tstpatlist,'Visible','on');
% set(handles.tstpatradio,'Value',1);
% set(handles.patpanel,'Title','Testing patterns');

function resetbtn_Callback(hObject, eventdata, handles)
global net
net.reset_net;

function newstartbtn_Callback(hObject, eventdata, handles)
newstart;
% global net
% net.seed = randi(10000);
% net.reset_net;


%see newstart for logging stuff I should do 
% newstart;

function runbtn_on_Callback(hObject, eventdata, handles)


function stepbtn_on_Callback(hObject, eventdata, handles)
uiresume(handles.netdisplay);

function testrunbtn_on_Callback(hObject, eventdata, handles)


function teststepbtn_on_Callback(hObject, eventdata, handles)
uiresume(handles.netdisplay);

function trpatlist_Callback(hObject, eventdata, handles)


function trpatlist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tstpatlist_Callback(hObject, eventdata, handles)


function tstpatlist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function test_stopbtn_Callback(hObject, eventdata, handles)
global PDPAppdata;
PDPAppdata.stopprocess = 1;
uiresume(handles.netdisplay)



function loadwtbtn_Callback(hObject, eventdata, handles)
global PDPAppdata;
[filename, pathname] = uigetfile({'*.wt','Weight Files (*.wt)'}, 'Load Weight File');
if isequal([filename,pathname],[0,0])
	return
% Otherwise construct the fullfilename and Check and load the file.
end
File = fullfile(pathname,filename);
if strcmpi(pathname(1:end-1),pwd)
   File = filename;
end 
loadweights(File);
if PDPAppdata.lognetwork
   statement = sprintf('loadweights (''%s'');',File);
   updatelog(statement);
end

function savewtbtn_Callback(hObject, eventdata, handles)
savewt;

function trainpanel_CreateFcn(hObject, eventdata, handles)


function controlpnl_CreateFcn(hObject, eventdata, handles)




function testgranlist_Callback(hObject, eventdata, handles)


function testgranlist_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
global net;
liststr={'cycle','pattern'}; 
switch net.type
    case 'cs'
         liststr={'cycle','update'};
    case 'iac'
         liststr={'cycle'};
    case 'bp'
         liststr={'pattern','cycle'};         
    case 'srn'
         liststr={'sequence','pattern'};
    case 'rbp'
         liststr={'sequence','pattern','tick'}; 
    case 'tdbp'
         liststr={'epoch','step'};
end
set(hObject,'String',liststr);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function netdisplay_CloseRequestFcn(hObject, eventdata, handles)
% Hint: delete(hObject) closes the figure
delete(hObject);




function tstpatchk_Callback(hObject, eventdata, handles)
global net PDPAppdata;
grlist = cellstr(get(handles.testgranlist,'String'));
if get(hObject,'Value') == 0
   set(handles.tstpatlist,'enable','off');
   set(handles.tallchk,'enable','off','Value',0);   
   pindex = find(~cellfun('isempty',strfind(grlist,'pattern')));
   if ~isempty(pindex)
       grlist(pindex) = [];
   end
   if any(strcmpi(net.type,{'cs','iac'}))
      for i = 1:numel(net.pool)
          net.pool(i).extinput = repmat(0,1,net.pool(i).nunits);
      end
      if PDPAppdata.gui
         update_display(0);
      end
   end
else
   set(handles.tstpatlist,'enable','on');
   set(handles.tallchk,'enable','on','Value',0);
   grlist{end+1}='pattern';
end
set(handles.testgranlist,'String',grlist,'Value',1);



function cbarmenu_Callback(hObject, eventdata, handles)
if isempty(getappdata(handles.cbaronmenu,'Colorbaron'))
   return;
end
cb=findobj('tag','colbar');
if isempty(cb) && getappdata(handles.cbaronmenu,'Colorbaron')==1
   set(handles.cbaronmenu,'checked','off');
   setappdata(hObject,'Colorbaron',0);       
end
   


function cbaronmenu_Callback(hObject, eventdata, handles)
status='off';
if strcmpi(get(hObject,'checked'),'off')
    status='on';
%     set(hObject,'checked','on');
%     axes('Units','normalized','Position',[0.1 0.1 0.5 0.5]);
%     colorbar('tag','colbar','Location','Southoutside','ButtonDownFcn',@colorbarselect);
%     setappdata(hObject,'Colorbaron',1);
%     axis off;
% else
%     cb=findobj('tag','colbar');         
%     if ~isempty(cb)
%         delete(cb);
%     end
%     set(hObject,'checked','off');
end
set(hObject,'checked',status);
setcolorbar(status);


function colorbarselect(hObject,event)
handles=guihandles;
stype = get(handles.netdisplay,'SelectionType');
if strcmpi(stype,'normal')
   selectmoveresize;
else
   set(hObject,'SelectionHighlight','off');
end


function netdisplay_CreateFcn(hObject, eventdata, handles)




function nwseedmenu_Callback(hObject, eventdata, handles)
setnwseed;



function loadcmapmenu_Callback(hObject, eventdata, handles)

[filename, pathname] = uigetfile({'*.mat','Matlab Mat Files (*.mat)'}, 'Load Colormap matrix');
if isequal([filename,pathname],[0,0])
	return
% Otherwise construct the fullfilename and Check and load the file.
end
File = fullfile(pathname,filename);
setcolormap(File);


function procpnl_SelectionChangeFcn(hObject, eventdata, handles)
if strcmpi(get(hObject,'tag'),'trpatradio')
   paton = findobj('tag','trpatlist');
   patoff = findobj('tag','tstpatlist');
else
   paton = findobj('tag','tstpatlist');
   patoff = findobj('tag','trpatlist');
end
set(paton,'Visible','on');
set(patoff,'Visible','off');


function trainradio_Callback(hObject, eventdata, handles)
global net PDPAppdata;
val = get(hObject,'Value');
trpanelobjs = findobj(allchild(handles.trainpanel),'-not','tag',...
              'trainradio');
tstpanelobjs = findobj(allchild(handles.testpanel),'-not','tag',...
              'testradio');
if val == 1
   set(trpanelobjs,'enable','on');
   set(tstpanelobjs,'enable','off');
   set(handles.testradio,'Value',0);
   set(handles.tstpatlist,'Visible','off');
   set(handles.trpatlist,'Visible','on');
   set(handles.patpanel,'Title','Training patterns');
  if ismember(net.type,{'pa','cl','bp','srn','rbp'}) && ...
      isempty(PDPAppdata.trainData)
      trbuttons =findobj(handles.trainpanel,'Style','pushbutton');
      set(trbuttons,'enable','off');
   end  
else
   set(trpanelobjs,'enable','off');
   set(tstpanelobjs,'enable','on');
   set(handles.testradio,'Value',1);
   set(handles.tstpatlist,'Visible','on');
   set(handles.trpatlist,'Visible','off');
   set(handles.patpanel,'Title','Testing patterns');
  if ismember(net.type,{'pa','cl','bp','srn','rbp'}) && ...
      isempty(PDPAppdata.testData)
      tstbuttons = findobj(handles.testpanel,'Style','pushbutton');
      set(tstbuttons,'enable','off');
   end   
end


function testradio_Callback(hObject, eventdata, handles)
global net PDPAppdata;
val = get(hObject,'Value');
tstpanelobjs = findobj(allchild(handles.testpanel),'-not','tag','testradio');
trpanelobjs = findobj(allchild(handles.trainpanel),'-not','tag','trainradio');
if val == 1
   set(trpanelobjs,'enable','off');
   set(tstpanelobjs,'enable','on');
   set(handles.trainradio,'Value',0);
   set(handles.tstpatlist,'Visible','on');
   set(handles.trpatlist,'Visible','off');
   set(handles.patpanel,'Title','Testing patterns');
 if ismember(net.type,{'pa','cl','bp','srn','rbp'}) && ...
      isempty(PDPAppdata.testData)
      tstbuttons =findobj(handles.testpanel,'Style','pushbutton');
      set(tstbuttons,'enable','off');
   end   
else
   set(trpanelobjs,'enable','on');
   set(tstpanelobjs,'enable','off');
   set(handles.trainradio,'Value',1);
   set(handles.tstpatlist,'Visible','off');
   set(handles.trpatlist,'Visible','on');
   set(handles.patpanel,'Title','Training patterns');
   if ismember(net.type,{'pa','cl','bp','srn','rbp'}) && ...
      isempty(PDPAppdata.trainData)
      trbuttons = findobj(handles.trainpanel,'Style','pushbutton');
      set(trbuttons,'enable','off');
   end    
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over runbtn_on.
function runbtn_on_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to runbtn_on (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on runbtn_on and none of its controls.
function runbtn_on_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to runbtn_on (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function runbtn_on_CreateFcn(hObject, eventdata, handles)
% hObject    handle to runbtn_on (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over testradio.
function testradio_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to testradio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function filemenu_Callback(hObject, eventdata, handles)
% hObject    handle to filemenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function closemenu_Callback(hObject, eventdata, handles)
% hObject    handle to closemenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% pdpquit
close


% --------------------------------------------------------------------
function edittemplatemenu_Callback(hObject, eventdata, handles)
% hObject    handle to edittemplatemenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global net
structexplore(net);


% --------------------------------------------------------------------
function quitmenu_Callback(hObject, eventdata, handles)
% hObject    handle to quitmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
pdpquit
close
