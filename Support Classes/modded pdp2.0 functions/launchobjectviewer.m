function varargout = launchobjectviewer(varargin)
% LAUNCHOBJECTVIEWER M-file for launchobjectviewer.fig
%      LAUNCHOBJECTVIEWER, by itself, creates a new LAUNCHOBJECTVIEWER or raises the existing
%      singleton*.
%
%      H = LAUNCHOBJECTVIEWER returns the handle to a new LAUNCHOBJECTVIEWER or the handle to
%      the existing singleton*.
%
%      LAUNCHOBJECTVIEWER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LAUNCHOBJECTVIEWER.M with the given input arguments.
%
%      LAUNCHOBJECTVIEWER('Property','Value',...) creates a new LAUNCHOBJECTVIEWER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before launchobjectviewer_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to launchobjectviewer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help launchobjectviewer

% Last Modified by GUIDE v2.5 04-Jun-2007 09:45:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @launchobjectviewer_OpeningFcn, ...
                   'gui_OutputFcn',  @launchobjectviewer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before launchobjectviewer is made visible.
function launchobjectviewer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to launchobjectviewer (see VARARGIN)
leftstr=cellstr('+ net : <struct>');
leftitemlist(1).parents = [];
leftitemlist(1).sub = 1;
leftitemlist(1).fname = 'net';
setappdata(hObject,'leftlist',leftitemlist);
set(handles.vartree,'String',leftstr);

% Choose default command line output for launchobjectviewer
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes launchobjectviewer wait for user response (see UIRESUME)
% uiwait(handles.launchobjectviewer);


% --- Outputs from this function are returned to the command line.
function varargout = launchobjectviewer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function vartree_Callback(hObject, eventdata, handles)
global net editable;
if isempty(net)
   return;
end
v=get(hObject,'Value');
listitems=get(hObject,'String');
numpads = getleadspaces(listitems{v});
padding(1:numpads)=' ';
index = 1;
currlist = getappdata(handles.launchobjectviewer,'leftlist');
parents = currlist(v).parents;
fn = currlist(v).fname;
subs = currlist(v).sub;
[t,r]= strtok(listitems{v});  % for struct items t will be + or - and 'r' will be rest of line
if t(1) == '+'
    ind = find(r==':');
    if ~isempty(ind)
        r = r(1:ind-1);
    end
    item = strtrim(r);
    structure =net;
    if ~strcmpi('net', item)
      array_mark = regexp(item,'[()]'); 
      if ~isempty(array_mark)
          index = str2num(item(array_mark(1)+1 : array_mark(2) -1));
      end
      for i=2:numel(parents)
          structure=getfield(structure,parents{i},{subs(i)});
      end
      structure = structure.(fn);
    end
    expstring = expandtree(structure,v,index,padding);
    namecollap = cellstr(sprintf('%s- %s',padding,item));
    newstring= [listitems{1:v-1},namecollap,expstring,listitems{v+1:end}];
    set(hObject,'String',newstring);
    set(handles.editbtn,'enable','off'); 
    set(handles.datatypetxt,'String','Structure');
    set(handles.desctxt,'String','No object Selected');
   set(findobj(handles.editpanel,'Style','edit'),'enable','off');    
    return;
end
if t(1) == '-'
    item=strtrim(r);
    numremove = collapsetree(v);
    nameexp = {sprintf('%s+ %s : <struct>',padding,item)};
    newstring= [listitems{1:v-1},nameexp,listitems{v+numremove+1:end}];
    set(hObject,'String',newstring);
    currlist(v+1 : v+ numremove)=[];
    setappdata(handles.launchobjectviewer,'leftlist',currlist);
    set(handles.editbtn,'enable','off');
    set(handles.datatypetxt,'String','Structure');
    set(handles.desctxt,'String','No object Selected');
   set(findobj(handles.editpanel,'Style','edit'),'enable','off');    
    return;
end
structure =net;
for i=2:numel(parents)
    structure=getfield(structure,parents{i},{subs(i)});
end
structure = structure.(fn);
edit = 0;
% switch parents{end}
%     case 'net'
%         if any (strcmp(editable.net,fn))
%            edit = 1;
%         end
%     case 'pool'
%         if any (strcmp(editable.pool,fn))
%             edit =1;
%         end
%     case 'proj'
%         if any (strcmp(editable.proj,fn))
%             edit = 1;
%         end
%     case 'trainopts'
%         if any (strcmp(editable.trainopts,fn))
%             edit = 1;
%         end
%     case 'testopts'
%         if any (strcmp(editable.testopts,fn))
%             edit = 1;
%         end
% end
edit = 1;
enablestatus = 'on';
if ischar(structure) %|| (edit ==0 && numel(structure) ==1)
   set(handles.datatypetxt,'String','Scalar');
   set(handles.desctxt,'String',structure);
   enablestatus = 'off';
else
    if isvector(structure) %|| (edit ==1 && numel(structure) == 1)
       set(handles.datatypetxt,'String','Vector');
    else
      set(handles.datatypetxt,'String','matrix');
    end
    set(handles.desctxt,'String',sprintf('[%d x %d] array',...
       size(structure,1),size(structure,2)));
   if numel(structure) == 0 %|| edit ==0
      enablestatus = 'off';
   end
end
setappdata(handles.editbtn,'editproperty',edit);
if (edit)
    btnlabel = 'Edit';
else
    btnlabel = 'View';
end
set(handles.editbtn,'enable',enablestatus,'String',btnlabel);
set(findobj(handles.editpanel,'Style','edit'),'enable',enablestatus);
if strcmpi (enablestatus,'on')
   sz = size(structure);
   sz(sz > 10) = 10;
   set(handles.startrowedit,'String',1);
   set(handles.startcoledit,'String',1);
   set(handles.numrowsedit,'String',size(structure,1));
   set(handles.numcolsedit,'String',size(structure,2));
   set(handles.maxrowsedit,'String',sz(1));
   set(handles.maxcolsedit,'String',sz(2));
end


function newitems = expandtree(expstruct,val,ind,pad)
handles = guihandles;
currentlist =  getappdata(handles.launchobjectviewer,'leftlist');
parents  = currentlist(val).parents;
par_ind = numel(parents) + 1;
parents{par_ind} = currentlist(val).fname;
newitems='';
allitems = fieldnames(expstruct(ind));
k=1;
for i=1:numel(allitems)
    suff='';
    field = allitems{i};
    f= expstruct(ind).(field);
    if isstruct(f) || isa(f,'net')
       suff = '+';
       for j=1:numel(f)  % for array of structures
          item = sprintf('%s(%d) : %s',field,j,'<struct>');
          newitems{k} = sprintf('%s  %s %s',pad,suff,item);
          if val  > 1
              currentlist(val+k+1 : end+1) = currentlist(val+k:end);
          end
          currentlist(val+k).parents = parents;
          currentlist(val+k).fname = field; %fieldname
          currentlist(val+k).sub = [currentlist(val).sub,j];
          k = k +1;
       end
    else
       if ~ischar(f)
          if isscalar(f) && isnumeric(f)
             f = num2str(f,'%0.5g');
          else
            if isempty(f)
                f='[ ]';
            else    
                f= sprintf('%d x %d array',size(f,1),size(f,2));
            end
          end
       end
       item = sprintf('%s : %s',field,f);   
       newitems{k} =sprintf('%s  %s %s',pad,suff,item);
       if (val > 1)
          currentlist(val+k+1 : end+1) = currentlist(val+k:end);
       end    
       currentlist(val+k).parents = parents;
       currentlist(val+k).fname = field; %fieldname
       currentlist(val+k).sub = [currentlist(val).sub];
       k = k +1;
    end
end
setappdata(handles.launchobjectviewer,'leftlist',currentlist);

function numtoremove = collapsetree(v)
handles = guihandles;
currlist = getappdata(handles.launchobjectviewer,'leftlist');
checkp = currlist(v).parents;
numtoremove = 0;
if isempty(checkp)
    numtoremove = numel(currlist) - 1;
    return;
end
for i = v+1:numel(currlist)
    temp = currlist(i).parents{end};
    if ~isempty(strmatch(temp,checkp,'exact'))
       return;
    else       
        numtoremove = numtoremove + 1;
    end
end
    

function num=getleadspaces(str)
num=0;
k = strfind(str,'+');
l = strfind(str,'-');
if ~isempty(k)
    num = k -1;
end
if ~isempty(l)
    num = l -1;
end
if num == 0
    k= regexp(str,'\w');
    num = k(1) - 1;
end 

function vartree_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function editbtn_Callback(hObject, eventdata, handles)
global net properties;
properties=[];
currlist = getappdata(handles.launchobjectviewer,'leftlist');
v = get(handles.vartree,'Value');
parents = currlist(v).parents;
fn = currlist(v).fname;
subs = currlist(v).sub;
arraystr ='net';
array = net;
for i=2:numel(parents)
    arraystr = sprintf('%s.%s(%d)',arraystr,parents{i},subs(i));
    array=getfield(array,parents{i},{subs(i)});
end
arr = array.(fn);
properties.startrow = str2double(get(handles.startrowedit,'String'));
properties.startcol = str2double(get(handles.startcoledit,'String'));
properties.numrows = str2double(get(handles.numrowsedit,'String'));
properties.numcols = str2double(get(handles.numcolsedit,'String'));
properties.maxrows = str2double(get(handles.maxrowsedit,'String'));
properties.maxcols = str2double(get(handles.maxcolsedit,'String'));
properties.cellheight = str2double(get(handles.cellheightedit,'String'));
properties.cellwidth = str2double(get(handles.cellwidthedit,'String'));
properties.cellborder = str2double(get(handles.cellborderedit,'String'));
enablestatus = 'off';
if (getappdata(hObject,'editproperty'))
   enablestatus = 'on';
end
properties.enableedit = enablestatus;
properties.array = arr;
properties.hedits=[];
% properties.numrows = size(arr,1);
% properties.numcols = size(arr,2);
if ~isnumeric(properties.startrow) || properties.startrow < 1 || ...
   properties.startrow >= size(arr,1)
   properties.startrow = 1;
end
if ~isnumeric(properties.numrows) || properties.numrows < 1 || ...
   properties.startrow + properties.numrows -1 > size(arr,1)
   properties.numrows = size(arr,1)-properties.startrow +1;
end
if ~isnumeric(properties.startcol) || properties.startcol < 1 || ...
   properties.startcol >= size(arr,1)
   properties.startcol = 1;
end
if ~isnumeric(properties.numcols) || properties.numcols < 1 || ...
   properties.startcol + properties.numcols -1 > size(arr,2)
   properties.numcols = size(arr,2) - properties.startcol + 1;
end
arraystr = sprintf('%s.%s',arraystr,fn);
if ~isscalar(arr)
    srow = properties.startrow;
    scol = properties.startcol;
    erow = srow + properties.numrows - 1;
    ecol = scol + properties.numcols - 1;
    arr = arr(srow : erow,scol : ecol);
    arraystr=sprintf('%s(%d:%d,%d:%d)',arraystr,srow,erow,scol,ecol);    
end
properties.out=arr;
% arraystr=sprintf('%s.%s(%d:%d,%d:%d)',arraystr,fn,srow,erow,scol,ecol);
versionstr = version('-date');
releaseyear = str2double(datestr(versionstr,'yyyy'));
if releaseyear < 2008
   draweditor(arr,arraystr);
else
   drawuitable(arr,arraystr);
end

function descedit_Callback(hObject, eventdata, handles)


function descedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function numrowsedit_Callback(hObject, eventdata, handles)


function numrowsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function numcolsedit_Callback(hObject, eventdata, handles)


function numcolsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cellheightedit_Callback(hObject, eventdata, handles)


function cellheightedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cellwidthedit_Callback(hObject, eventdata, handles)


function cellwidthedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cellborderedit_Callback(hObject, eventdata, handles)


function cellborderedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function maxrowsedit_Callback(hObject, eventdata, handles)


function maxrowsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function maxcolsedit_Callback(hObject, eventdata, handles)


function maxcolsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function launchobjectviewer_CloseRequestFcn(hObject, eventdata, handles)
clear global properties;
% Hint: delete(hObject) closes the figure
delete(hObject);




function startcoledit_Callback(hObject, eventdata, handles)


function startcoledit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function startrowedit_Callback(hObject, eventdata, handles)


function startrowedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


