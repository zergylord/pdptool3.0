function rbp_reset
global net PDPAppdata;
if exist('RandStream','file') == 2 % for version 7.8 and above    
   stream = RandStream.getDefaultStream;
   reset(stream);
else
   rand('seed',net.seed);
   randn('seed',net.seed);
end
net.epochno = 0;
net.pss = 0.0;
net.pce = 0.0;
net.tss = 0.0;
net.tce = 0.0;
net.css = 0.0;
net.gcor = 0.0;
net.cpname = '';
net.trainpatno = 0;
net.testpatno = 0;
net.tickno = 0;
net.stateno = 0;
net.intervalno = 0;
PDPAppdata.trinterrupt_flag = 0;
PDPAppdata.tstinterrupt_flag = 0;
% nstates = net.nticks + 1;
for i=1:numel(net.pool)
%     vecsize = [1 net.pool(i).nunits];
    net.pool(i).netinput(:) = 0; %repmat(0,vecsize);     
    net.pool(i).activation(:) = 0; %repmat(0,vecsize);    
    net.pool(i).acthistory(:) = 0; %repmat(0,net.pool(i).nunits,nstates);
    net.pool(i).dEdnet(:) = 0; %repmat(0,vecsize);     
    net.pool(i).dEdnethistory(:) = 0; %repmat(0,net.pool(i).nunits,nstates);
    net.pool(i).extinput(:) = nan; %repmat(nan,nstates,net.pool(i).nunits);
    net.pool(i).target(:) = 0; %repmat(0,vecsize);
    net.pool(i).exttarget(:) = nan; %repmat(nan,nstates,net.pool(i).nunits);     
    if ~isempty(net.pool(i).proj)
       matx = net.pool(i).nunits;
       pools = {net.pool.name};
       for j=1:numel(net.pool(i).proj)
           from = net.pool(i).proj(j).frompool;
           [tf,np] = ismember(from,pools);
           maty = [net.pool(np).nunits];
           matsz = [matx maty];
           switch lower(net.pool(i).proj(j).constraint_type)
               case 'scalar'
                    net.pool(i).proj(j).weight(:) = net.pool(i).proj(j).constraint; %repmat(1,matsz) .* ...
%                                             net.pool(i).proj(j).constraint;
               case 'random'
                    net.pool(i).proj(j).weight = (rand(matsz)-0.5) * ...
                                                      net.trainopts.wrange;
               case 'prandom'
                    net.pool(i).proj(j).weight = rand(matsz) * ...
                                                      net.trainopts.wrange;
                    w = net.pool(i).proj(j).weight;
                    net.pool(i).proj(j).weight(w < 0) = 0.0;                                           
               case 'nrandom'
                    net.pool(i).proj(j).weight = (rand(matsz)-1) * ... 
                                                      net.trainopts.wrange;
                    w = net.pool(i).proj(j).weight;
                    net.pool(i).proj(j).weight(w > 0) = 0.0;
           end
           net.pool(i).proj(j).dweight(:) = 0; %repmat(0,matsz);                                      
           net.pool(i).proj(j).dEdw(:) = 0; %repmat(0,matsz);
           net.pool(i).proj(j).pwed(:) = 0; %repmat(0,matsz);  
       end
    end    
end
for i=1:numel(net.pool)
    for k=1:numel(net.pool(i).oproj)
        tp = net.pool(i).oproj(k).toindex;
        pindex = net.pool(i).oproj(k).projindex;
        net.pool(i).oproj(k).weight = net.pool(tp).proj(pindex).weight;
    end    
end
net.pool(1).activation = 1;  %bias 
net.pool(1).acthistory(:) = 1; %repmat(1,size(net.pool(1).acthistory));
if ~isempty(findobj('tag','netdisplay'))
    update_display(0);
end
if ~isempty(net.outputfile)
   resetlog;
end
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end