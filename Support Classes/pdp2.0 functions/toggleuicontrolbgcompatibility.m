function toggleuicontrolbgcompatibility(figure, mode, warningson)
% TOGGLEUICONTROLBGCOMPATIBILITY(FIGURE, MODE, WARNINGSON)
% toggles uicontrol background color compatibility mode.  The
% Backgroundcolor property of any uicontrol of style pushbutton created in
% the specified figure while background compatibility mode is on will
% behave in backward compatible mode for its entire lifetime.  That is, the
% backgrounds of such push buttons will be colored according to the value
% of their Backgroundcolor properties.
%
% FIGURE = The figure upon which to change the push button background color
% compatibility mode
%
% MODE = 0    - turns off compatibility.
%        1    - turns on backwards compatibility for pushbutton
%               Backgroundcolor property, if possible.
%
% WARNINGSON = true  - allows warnings to be shown.
%              false - turns warnings off (silent mode).
%

% Copyright 2005 The MathWorks, Inc.
% Version 1.0.

% Check to see if we display warnings.
if (nargin < 3)  % Didn't specify, turn them off
    warningson = false;
end

if (warningson)
    % Some messages.
    message1 = 'This function requires MATLAB R2006a';
    message2 = 'You are running MATLAB ';
    message3 = 'Invalid mode';
end

% Check version - this file is only for R14SP3+.
v = ver('MATLAB');
vstring_temp = v.Version;
vstring_arr = strfind(vstring_temp, '.');
if (length(vstring_arr) > 1)  % We have a version e.g. 7.0.1, so must truncate to 7.0
    vstring = vstring_temp(1:vstring_arr(2)-1);
else % Version is ok as is
    vstring = vstring_temp;
end
vstring = str2double(vstring);

% translate the mode - if used anything other than valid values, turn off
if ischar(mode)
    if (strcmpi(mode, 'all'))
        modeval = -1;
    else
        if (warningson)
            warning(message3);
        end
        modeval = 0;
    end
else
    if (isnumeric(mode) && isscalar(mode))
        modeval = mode;
    else
        if (warningson)
            warning(message3);
        end
        modeval = 0;
    end
end

if (vstring < 7.2)
    % Version is older that R2006a.
    % This function works only with MATLAB R2006a
    if (warningson)
        warning('MATLAB:UIControlBackgroundCompatibility:VersionNotSupported', ...
            '%s or later.\n%s%s.\n\n', message1, message2, v.Release(2:end-1));
    end
else
    setbgcompatibility(figure, modeval);
end
