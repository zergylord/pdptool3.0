% In nogui mode, this script can be run by typing 'pdptool nogui topo.m'
% on the matlab command line.
pdp
loadscript ('3jets.net');
loadtemplate ('3jets.tem');
loadpattern ('file','JETS.PAT','usefor','both');
launchnet;
setcolormap 'jmap.mat';
setcolorbar 'on';
settrainopts ('trainmode','ptrain','nepochs',20,'lrate',0.05);
cl_reset;
