function [valid, data,errmsg] = read_nonpair_data(Filename)
global net clspecial;
valid=0;
data=[];
errmsg='';
if numel(net.pool) <=1
   return;
end
pools = net.pool(2:end);
z= find(strcmpi({pools.type},'input'));
if isempty(z)
   disp('No input pools');
   return;
end
ninputs = sum(cell2mat({pools(z).nunits}));
fid = fopen (Filename,'r');
tline = fgetl(fid);
tline =strsplit(' ',tline,'omit');
tline = strtrim(tline);
zz=cellfun('isempty',tline);
tline=tline(~zz);
num = ninputs;
if tline{2}=='x'
   clspecial = 1;
   num =3;
end
textreadcmd='[pname';
for i=1:num
    textreadcmd= sprintf('%s,c%i',textreadcmd,i);
end
textreadcmd= sprintf('%s] = textread(''%s'' ,''',textreadcmd,Filename);
if clspecial ==1
   textreadcmd=sprintf('%s%%s%%*s',textreadcmd);
else
   textreadcmd = sprintf('%s%%s',textreadcmd);
end
for i=1:num
    textreadcmd=sprintf('%s%%f',textreadcmd);
end
textreadcmd=sprintf('%s'',''emptyvalue'',1);',textreadcmd);
try
    eval(textreadcmd);
catch
    e = lasterror;
    errmsg = sprintf(' %s ,%s',Filename,e.message);
    return;
end    
ip=c1;
arraycatcmd= sprintf('ip =[ip ');
for i=2:num
    arraycatcmd = sprintf('%s c%d',arraycatcmd,i);
end
arraycatcmd = sprintf('%s];',arraycatcmd);
eval(arraycatcmd);
for i=1:numel(pname)
    data(i).pname=pname{i};
    data(i).ipattern = ip(i,:);
end



% lineno=1;
% while tline ~= -1
%     clspecial=0;
%     [pname rem] = strtok(tline);
%     patt_string = strsplit(' ',rem,'omit');
%     patt_string = strtrim(patt_string);
%     zz = cellfun('isempty',patt_string);
%     patt_string = patt_string(~zz);
% %     patt_string = regexp(rem,'\w+','match');  % get a list of non-white space entries of the line;
%     if patt_string{1}=='x'
%        clspecial=1;
%     end
%     %replace special single character entries
%     patt_string = regexprep(patt_string,'\.','0.0');    
%     patt_string = regexprep(patt_string,'+','+1.0');
%     patt_string = regexprep(patt_string,'-','-1.0');
%     if clspecial ==0 && size(patt_string,2) ~= ninputs
%         errmsg='Pattern length mismatch';
%         data=[];
%         return;
%     end
%     if clspecial == 0 
%        ipattern=(str2num(char(patt_string(1:ninputs))))'; %char function on cell array of string gives column vector
%     else
%        ipattern=(str2num(char(patt_string(2:4))))';
%     end
%     data(lineno).pname = pname;
%     data(lineno).ipattern = ipattern;
%     tline = fgetl(fid);
%     lineno=lineno + 1;
% end
valid = 1;
