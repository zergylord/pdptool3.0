function definednet = cl_define(net)
% This function is called for setting up competitive learning networks.
% 'n' is the network struct variable with valid fieldnames for 
% 'cl' networks. 'basepool' defines a valid pool structure. 
% 'baseproj' defines valid projection structure.'trainopts' defines valid 
% training paramters. 'testopts' defines valid testing parameters.
% fillfields routine populates these structure variables with values from
% the given input parameter 'net'.setcldefaultvars initializes the vectors 
% and matrices with default values.
%
% Global variable PDPAppdata contains  runtime information for the
% current pdptool session. PDPAppdata.networks is a list of all currently 
% loaded networks (feature supported only for pdptool in gui mode).
% 'editable' variable can be used to specify user-modifiable network
% fields. These are then used by the object viewer module when invoked from
% pdptool mainwindow menu item 'Launch object viewer'.
% This is also used only when running pdptool in gui mode.
%
% **Important note for enhancement** : To add a new network type, you would
% need to define struct variables specific to the new type by creating a
% file like this with exactly similar function signature. The easiest method 
% would be copying the file content in its entirety into a new file, change
% function name to <newtype>_define , save it as <newtype>_define.m file. In
% the new file all you have to do is change fields of struct variables 
% 'n','basepool','baseproj','tropts','tstopts' - add/remove fieldnames
% according to your needs for the new type.Make corresponding changes to
% set<newtype>defaultvars subroutine. Type 'help struct' on matlab command
% prompt to get more information on syntax of MATLAB struct function.

global editable PDPAppdata;
networks = PDPAppdata.networks;
namedef = sprintf('net%d',numel(networks)+1);
n = struct('name',namedef,'type','cl','seedmode',1,'seed',0,'pool',[],...
          'num',1,'standardcl','on','nunits',0,'ninputs',0,'nhidden',0,...
          'epochno',0,'cpname','','trainpatno',0,'testpatno',0,... 
          'nancolor',[.97 .97 .97],'templatefile','','outputfile','',...
          'logfile','','trainopts',[],'testopts',[]);
basepool = struct('name','pool','type','input','nunits',1,'unames',[],...
                  'geometry',[],'lrange',1.0,'winner',1,'swan',0,...
                  'winRelPos',[],'proj',[],'activation',[],'netinput',[]);
baseproj = struct('frompool','bias','constraint',0.0,'weight',[],...
                 'constraint_type','scalar','topbias',1.0,'ispread',0.2);
tropts = struct('nepochs',20,'trainmode','strain','lflag',1,'lrate',0.2,...
                'trainset','none');
tstopts =struct('nepochs',1,'trainmode','strain','lflag',0,'lrate',0.2,...
                'testset','none');           
n = fillfields(n,net);
net = bias_define(net);   
p(1:length(net.pool)) = basepool;
for i = 1:numel(net.pool)
    p(i) = fillfields(p(i),net.pool(i));
    prj = baseproj;        
    if isfield(net.pool(i),'proj') && numel(net.pool(i).proj) > 0
       for j = 1:numel(net.pool(i).proj)
           prj(j) = fillfields(baseproj,net.pool(i).proj(j));
       end
       p(i).proj = prj;            
    end
end
n.pool = p;
n.trainopts = tropts;
if isfield(net,'trainopts')
    n.trainopts = fillfields(n.trainopts,net.trainopts);
end
n.testopts = tstopts;
if isfield(net,'testopts')
    n.testopts = fillfields(n.testopts,net.testopts);
end
if n.seedmode == 0       % 0 - seed provided, 1 - seed randomly
    if exist('RandStream','file') == 2 % for version 7.8 and above
        stream = RandStream('mt19937ar','Seed',n.seed);
        RandStream.setDefaultStream(stream);
    else
        rand('seed',n.seed); % for backward compatibility
        randn('seed',n.seed);                
    end
else
    if exist('RandStream','file') == 2 % for version 7.8 and above    
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        RandStream.setDefaultStream(stream);
        n.seed = stream.Seed;
    else
       n.seed = rand('seed');
       randn('seed',n.seed);
    end
end
editable.net = {'seed'};
editable.pool = {'netinput','activation','geometry','lrange'};
editable.proj = {'constraint','weight','topbias','ispread'};
editable.trainopts = fieldnames(rmfield(tropts,'trainset'));
editable.testopts = fieldnames(rmfield(tstopts,'testset'));
definednet = setcldefaultvars(n); 

function net = setcldefaultvars(net)
types={net.pool.type};
inpools = find(strcmpi(types,'input'));
hidpools = find(strcmpi(types,'hidden'));
net.inindx = inpools;
net.hidindx = hidpools;
if ~isempty(inpools)
    net.ninputs= sum(cell2mat({net.pool(inpools).nunits}));
end
if ~isempty(hidpools)
    net.nhidden = sum(cell2mat({net.pool(hidpools).nunits}));
end
for i = 1:numel(net.pool)
    vecsize = [1 net.pool(i).nunits];
    net.pool(i).activation = repmat(0,vecsize);
    net.pool(i).netinput = repmat(0,vecsize);
    shape = net.pool(i).geometry;         
    if ~all(shape) || numel(shape) ~= 2 || ...
       prod(shape) ~= net.pool(i).nunits
       shape =[1 net.pool(i).nunits];
    end
    net.pool(i).geometry = shape;
    try
    np=0;
    if ~isempty(net.pool(i).proj)
       matx = net.pool(i).nunits;
       pools = {net.pool.name};
       for j = 1:numel(net.pool(i).proj)
           from = net.pool(i).proj(j).frompool;
           [tf,np] = ismember(from,pools);
           maty = [net.pool(np).nunits];
           matsz = [matx maty];
           switch lower(net.pool(i).proj(j).constraint_type)
            case 'scalar'
                 net.pool(i).proj(j).weight = repmat(1,matsz) .* ...
                                              net.pool(i).proj(j).constraint;
            case 'random'
                 net.pool(i).proj(j).weight = rand(matsz);
                 sumwt = sum(net.pool(i).proj(j).weight,2);
                 ncols = size(net.pool(i).proj(j).weight,2);
                 net.pool(i).proj(j).weight = net.pool(i).proj(j).weight ./...
                                              repmat(sumwt,1,ncols);                  
            case 'tbias'
                  tbias = net.pool(i).proj(j).topbias;
                  sc = 2*net.pool(i).proj(j).ispread^2; 
                  nconst = 2*pi*net.pool(i).proj(j).ispread^2; %normalizing constant for Gaussian                       
                  %Absolute distance
                  [si,sj] = ind2sub(net.pool(np).geometry,...
                            (1:net.pool(np).nunits));
                  [ri,rj] = ind2sub(net.pool(i).geometry,...
                                     (1:net.pool(i).nunits));                           
                 %If the either grid is larger, then center the other grid
                 %on the larger grid
                 if ~isequal(net.pool(np).geometry,net.pool(i).geometry)
                    diffgeo = net.pool(np).geometry - net.pool(i).geometry;
                    if diffgeo(1) > 0 %if input grid is larger in X dim
                        ri = ri + diffgeo(1)/2;
                    else              %if input grid is smaller in X dim
                        si = si - diffgeo(1)/2;
                    end
                    if diffgeo(2) > 0 %if input grid is larger in Y dim
                        rj = rj + diffgeo(2)/2;
                    else             %if input grid is smaller in Y dim
                        sj = sj - diffgeo(2)/2;
                    end
                 end
                 %Non-topograhic part of the weights, uniform then
                 %normalized
                 net.pool(i).proj(j).weight = rand(matsz);
                 sumwt = sum(net.pool(i).proj(j).weight,2);
                 ncols = size(net.pool(i).proj(j).weight,2);
                 net.pool(i).proj(j).weight = net.pool(i).proj(j).weight ./...
                                              repmat(sumwt,1,ncols);          
                 %Add topographic part of the weights
                 p1 = repmat(si,numel(ri),1);
                 q1 = repmat(ri',1,numel(si));
                 p2 = repmat(sj,numel(rj),1);
                 q2 = repmat(rj',1,numel(sj));
                 d = (p1 - q1) .^2 + (p2 - q2).^2;
                 net.pool(i).proj(j).weight = (1. - tbias) .* ...
                                              net.pool(i).proj(j).weight + ...
                                              tbias .* exp(-d./sc)./ nconst;
           end
       end
    end
    catch
        if (np == 0)
           fprintf(1,['net.pool(%d).proj(%d).frompool is not a valid pool ' ...
                      'name'],i,j);
        else
           fprintf(1,'%s',lasterror);
        end
    end
end
