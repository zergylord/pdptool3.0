% In nogui mode, this script can be run by typing 'pdptool nogui jets.m'
% on the matlab command line.
pdp ;
loadscript ('jets.net');
loadtemplate ('jets.tem');
settestopts ('estr',0.4);
launchnet;
filename = getfilename('Jetslog','.mat');
myel = 4; myaz = 4;
setoutputlog ('file', filename,'process', 'test','frequency', 'cycle','status', 'on','writemode', 'binary',...
    'objects', {'cycleno','pool(2).activation(1:2)','pool(2).activation(3:5)','pool(2).activation(6:8)','pool(2).activation(9:11)',...
    'pool(2).activation(12:14)','pool(2).activation(15:41)','pool(3).activation(1:15)','pool(3).activation(16:27)'},...
    'onreset', 'clear','plot', 'on','plotlayout', [5 3]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'pool(2).activation(1:2)'},  'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Gang', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 2, 'yvariables', {'pool(2).activation(15:41)'},'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Name', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 3, 'yvariables', {'pool(3).activation(1:15)'}, 'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Jets-Inst', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 4, 'yvariables', {'pool(2).activation(3:5)'},  'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Age', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 7, 'yvariables', {'pool(2).activation(6:8)'},  'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Edu', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 10,'yvariables', {'pool(2).activation(9:11)'}, 'plot3d', 1,'az', myaz, 'el', myel, 'title', 'MarStat', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 13,'yvariables', {'pool(2).activation(12:14)'},'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Occ', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 6, 'yvariables', {'pool(3).activation(16:27)'},'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Shk-Inst', 'ylim', [-0.2 1]);
setcolormap 'jmap.mat';
setcolorbar 'on';
