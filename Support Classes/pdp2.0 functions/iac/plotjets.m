function plotjets(outfile,p2units,p3units)
% must have iac directory in path or as current directory
% loads the output log
% usage:
% plotjets('logfile.mat',{pool2.unitlist},{pool3 unitlist});
% unitlist may be a list of integer unit indices or a list of unit names in
% single quotes.
% Examples
% plotjets('jetlog.mat',{'Ken','in20s','in30s','HS','Single','burglar'},{'_Ken'});
% plotjets('jetlog.mat',[1,3,7],[13,15]};
f = load(outfile);
vunames = textread('jetsvisunames','%s');
hunames = textread('jetshidunames','%s');
if iscell(p2units)  % if units argument is a cell array of unames
   p2index = find(ismember(lower(vunames),lower(p2units)));
else
   p2index = p2units;
end
if iscell(p3units)
   p3index = find(ismember(lower(hunames),lower(p3units)));
else
   p3index = p3units;
end
pf = [f.pool2_activation(p2index,:);f.pool3_activation(p3index,:)]; % This is incase p3index or p2index is empty
figure;
plot(f.cycleno,pf); %...
if iscell(p2units) && iscell(p3units)
   legend(vunames{p2index},hunames{p3index}); %if unames are passed are arguments , make legend from them directlt
else                                      %else get the unames from the unames files.
   legend(vunames{p2units},hunames{p3units});
end