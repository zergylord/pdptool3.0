function bp_reset
global net PDPAppdata;
if exist('RandStream','file') == 2 % for version 7.8 and above    
   stream = RandStream.getDefaultStream;
   reset(stream);
else
   rand('seed',net.seed);
   randn('seed',net.seed);
end
net.epochno = 0;
net.pss=0.0;
net.tss=0.0;
net.css=0;
net.pce=0.0;
net.tce=0.0;
net.gcor = 0.0;
net.cpname='';
net.trainpatno=0;
net.testpatno=0;
net.cycleno = 0;
PDPAppdata.trinterrupt_flag = 0;
PDPAppdata.tstinterrupt_flag = 0;
for np = 1: numel(net.pool)
    sz=[1 net.pool(np).nunits];
    net.pool(np).netinput(:) = 0; %repmat(0,sz);
    net.pool(np).activation(:) = 0; %repmat(0,sz);
    net.pool(np).delta(:) = 0; %repmat(0,sz);
    net.pool(np).error(:) = 0; %repmat(0,sz);
    net.pool(np).target(:) = 0; %repmat(0,sz);
    for nc=1:numel(net.pool(np).proj)
        psz = size(net.pool(np).proj(nc).weight);
        switch lower(net.pool(np).proj(nc).constraint_type)
               case 'scalar'
                    net.pool(np).proj(nc).weight(:) = net.pool(np).proj(nc).constraint; %repmat(1,psz) .* ...
%                                           net.pool(np).proj(nc).constraint;
               case 'random'
                    net.pool(np).proj(nc).weight = net.trainopts.wrange * ...
                                          (rand(psz)-0.5);
               case 'prandom'
                    net.pool(np).proj(nc).weight = net.trainopts.wrange *...
                                          rand(psz);
                    w = net.pool(np).proj(nc).weight;
                    net.pool(np).proj(nc).weight(w < 0) = 0.0;                                           
               case 'nrandom'
                    net.pool(np).proj(nc).weight = net.trainopts.wrange *...
                                          (rand(psz)-1);
                    w = net.pool(np).proj(nc).weight;
                    net.pool(np).proj(nc).weight(w > 0) = 0.0;                                     
               case 'copyback'
                    net.pool(np).proj(nc).weight(:) = 0; %repmat(0,psz);
        end        
        net.pool(np).proj(nc).dweight(:) = 0; %repmat(0,psz);
        net.pool(np).proj(nc).wed(:) = 0; %repmat(0,psz);
        net.pool(np).proj(nc).pwed(:) = 0; %repmat(0,psz);
    end
end
net.pool(1).activation = 1;
if ~isempty(findobj('tag','netdisplay'))
    update_display(0);
end
if ~isempty(net.outputfile)
   resetlog;
end
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end