function trpatfile = trainpatternspopup_cfn(popuphandle)

% This is a common function for all <net_type>_troption M files that have
% a pattern list popup menu item.It is called when the pop-up menu labelled
% 'Pattern files' is created. The purpose of this routine is to create
% appdata for <net_type>_troption figure handle from pdp handle 
% appdata by extracting details of all training patterns currently in the  
% Train pattern list of the main window. 'patfiles' appdata is a cell array 
% of strings ,each row of which contains a pattern set name and its
% corresponding file path. The Pattern file popup menu is then populated 
% with the current list.

global PDPAppdata;
pdp_handle = findobj ('tag','mainpdp');
if isempty(pdp_handle)
   disp('Can only be called when running pdp');
   return;
end
trainlist = findobj('tag','trpatpop');
currentpats = cellstr(get(trainlist,'String'));
val = get(trainlist,'Value');
patfiles = PDPAppdata.patfiles;
trpatfile = [];
if ~isempty(patfiles)
    patsetnames = patfiles(:,1);
    [foundmem, memindex] = ismember (currentpats,patsetnames);
    memindex = memindex(memindex > 0 );    
    trpatfile = patfiles(memindex,:); 
end
set(popuphandle,'String',currentpats,'Value',val);