function temp = annealing(iter)
% Adjusts temperature for cs network based on the annealing schedule 
% specified by user. It linearly interpolates between temperature-time
% milestones given by 'anneal' matrix and returns temperature for current
% iteration.

temp = 0.0;
global net ntemp ctemp ltemp coolrate;
anneal = net.test_options.annealsched;
if isempty (anneal)
   return;
end
if iter >= anneal(ltemp,1) 
   temp = anneal(ltemp,2);
end
if iter >= anneal(ntemp,1)
   temp = anneal(ntemp,2);
   if (ntemp ~= ltemp)
       ctemp = ntemp;
       ntemp = ntemp + 1;
   end
   coolrate = (anneal(ctemp,2) - anneal(ntemp,2))/ ...
              (anneal(ntemp,1) - anneal(ctemp,1));
else
   temp = anneal(ctemp,2) - (coolrate * (iter - anneal(ctemp,1)));
end