function refresh_iac()
global net;
net.cycleno=0;
for i=1:numel(net.pool)
    vecsize = [1 net.pool(i).nunits];
    net.pool(i).activation(:) = 0; %repmat(0,vecsize);
    net.pool(i).inhibition(:) = 0; %repmat(0,vecsize);
    net.pool(i).netinput(:) = 0; %repmat(0,vecsize);
    net.pool(i).activation(:) = net.testopts.rest; %repmat(net.testopts.rest,vecsize);
end