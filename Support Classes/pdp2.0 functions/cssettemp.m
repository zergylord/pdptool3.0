function cssettemp()
global net ltemp ctemp ntemp coolrate;
%annealing stuff
ltemp = size(net.test_options.annealsched,1); %changed testopts to test_options
ntemp = 1;
ctemp=1;
anneal = net.test_options.annealsched;
net.goodness = 0;
net.updateno = 0;
net.cuname='';
if ~isempty(net.test_options.annealsched) && ltemp ~= ntemp
   ctemp=ntemp;
   ntemp = ntemp+1;
   coolrate = (anneal(ctemp,2) - anneal(ntemp,2))/anneal(ntemp,1);
   net.test_options.temp = annealing(net.cycleno);
end