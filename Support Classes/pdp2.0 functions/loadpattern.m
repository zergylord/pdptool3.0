function loadpattern(varargin)
%loads pattern file
%
global net PDPAppdata;
patparams.file = '';
patparams.setname = '';
patparams.usefor = 'none';
patparams = parse_pairs(patparams,varargin);
if exist(patparams.file,'file') ~= 2
   fprintf(1,'ERROR: Filename %s does not exist\n',patparams.file);
   return;
end
tf = ismember({'train','test','none','both'},patparams.usefor);
if ~any(tf)
    fprintf(1,['ERROR: %s - Incorrect value for ''usefor'' property, '...
            'switching to default (''none'')\n'],patparams.usefor);
    patparams.usefor = 'none';
end
if isempty(strtrim(patparams.setname))
   [pth nm ext] = fileparts(patparams.file);
   patparams.setname = nm;
end
% return;
% patfile = varargin{1};
% if nargin==1
%    [pth nm ext] = fileparts(patfile);
%    patname = nm;
%    pattype='both';   
% else
%     if nargin < 3
%        pattype='both';
%     else
%        pattype=varargin{3};
%     end
%     patname=varargin{2};
% end
patfiles = PDPAppdata.patfiles;
next = size(patfiles,1) +1;
dat = [];
if ~isempty(patfiles)
    if any(strcmpi(patfiles(:,1),patparams.setname))
       fprintf(1,'ERROR: Setname %s already exists\n',patparams.setname);
       return;
    end
%     [tf,patnum] = ismember(patparams.setname,lower(patfiles(:,1)));
end
dat = readpatterns(patparams.file);
if isempty(dat)
   return;
end
patfiles{next,1} = patparams.setname;
patfiles{next,2} = patparams.file;
patfiles{next,3} = dat;

n = PDPAppdata.networks;
% n = getappdata(pdp_handle,'networks');
if ~isempty(net)
    ind = net.num;
end
PDPAppdata.patfiles = patfiles;
% setappdata(pdp_handle,'patfiles',patfiles);
switch patparams.usefor
    case 'train'
         if PDPAppdata.gui
            trpop = findobj('tag','trpatpop');
            patlist = update_patlist(trpop,patparams.setname);
            set(trpop,'String',patlist,'Value',numel(patlist));
         end
         settrainopts('trainset',patparams.setname);
%          net.trainopts.trainset = patparams.file;
%           PDPAppdata.trainData  = dat;
    case 'test'
         if PDPAppdata.gui
            tstpop = findobj('tag','testpatpop');
            patlist = update_patlist(tstpop,patparams.setname);
            set(tstpop,'String',patlist,'Value',numel(patlist));
         end
         settestopts('testset',patparams.setname);
%           PDPAppdata.testData = dat;
    case 'both'
         if PDPAppdata.gui
            trpop = findobj('tag','trpatpop');
            patlist = update_patlist(trpop,patparams.setname);
            set(trpop,'String',patlist,'Value',numel(patlist));          
            tstpop = findobj('tag','testpatpop');
            patlist = update_patlist(tstpop,patparams.setname);          
            set(tstpop,'String',patlist,'Value',numel(patlist));
         end
         settrainopts('trainset',patparams.setname);
         settestopts('testset',patparams.setname);
%           PDPAppdata.trainData = dat;
%           PDPAppdata.testData = dat;
    case 'none'
         if PDPAppdata.gui
            trpop = findobj('tag','trpatpop');
            patlist = update_patlist(trpop,patparams.setname);
            set(trpop,'String',patlist,'Value',1);          
            tstpop = findobj('tag','testpatpop');
            patlist = update_patlist(tstpop,patparams.setname);          
            set(tstpop,'String',patlist,'Value',1);
         end 
          PDPAppdata.trainData = [];
          PDPAppdata.testData = [];        
end
if PDPAppdata.lognetwork
   stmt = sprintf(['%s (''file'',''%s'',''setname'',''%s'',''usefor'','...
                  '''%s'');'],mfilename,patparams.file,patparams.setname,...
                  patparams.usefor);
   updatelog(stmt);
end
if ~isempty(net)
    n{ind} = net;
end
PDPAppdata.networks = n;
if ~PDPAppdata.gui
   fprintf(1,'Pattern file %s successfully loaded\n',patparams.file);
end