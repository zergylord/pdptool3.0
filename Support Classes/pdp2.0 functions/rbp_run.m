function rbp_run 
global net outputlog inpools outpools hidpools patn binarylog runparams;
global tstpatn trpatn PDPAppdata;
[options,data,cpno,N,cepoch lepoch,patfield,sttick,orig_tss,orig_tce,...
 appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
inpools =find(~cellfun('isempty',regexp({net.pool.type},'input|inout')));
hidpools = find(strcmpi({net.pool.type},'hidden'));
outpools = find(~cellfun('isempty',regexp({net.pool.type},'output|inout')));
if runparams.alltest==0
    clear_dEdw;
end
updatecount = 0;
lgraincount = 1;
breakval = 0;
for iter = cepoch:lepoch
    abort = PDPAppdata.stopprocess; 
    if ~isempty(abort)
       PDPAppdata.stopprocess = [];         
       PDPAppdata.(appflag) = 1;           
       return;       
    end
    net.epochno = iter;
    net.tss = orig_tss;
    net.tce = orig_tce;    
    if iter > cepoch || isempty(patn)
       patn = getpatternrange(data,options);  %gets fresh pattern range for next epoch          
       if strcmpi(runparams.process,'test')    
          tstpatn = patn;
       else
          trpatn = patn;
       end
    end
    if iter == lepoch && ~isempty(runparams.range)
        N = runparams.range(2,1);
    end
    for p = cpno: N
        abort = PDPAppdata.stopprocess; 
        if ~isempty(abort)
           PDPAppdata.stopprocess = [];         
           PDPAppdata.(appflag) = 1;           
           return;       
        end        
        pno = patn(p);
        if net.tickno == 1 % tickno may not be 1 when running test process in 'step' mode
           net.stateno = 1;
           setupextinputandtarget(data,pno);
           setinitialstate;
           if logoutflag && ~isempty(strmatch('tick',lower(freqrange)))
              writeoutput(runparams.process,'tick');     
           end
           if logoutflag && ~isempty(strmatch('interval',lower(freqrange)))
              writeoutput(runparams.process,'interval');
           end        
          if PDPAppdata.gui && strncmpi(runparams.granularity,'tick',...
             length(runparams.granularity))
             updatecount = update_display_step(updatecount,net.(patfield));
             if updatecount < 0
                PDPAppdata.(appflag) = 1;                 
                return;
             end
          end            
        end       
        net.(patfield) = p;        
        breakval = doforwardpass(sttick,options,pno,logoutflag,freqrange);                           
        if breakval
           PDPAppdata.stopprocess = [];
           if PDPAppdata.gui
              update_display (pno);
           end
           PDPAppdata.(appflag) = 1;
           if net.tickno < net.nticks+1
              return;
           end
        end
        if strncmpi(runparams.process,'train',length(runparams.process))
           breakval = dobackwardpass(logoutflag,freqrange);
        end
        sumstats();                
        if (options.lflag)
           compute_dEdw;
           if isequal(lower(options.lgrain(1)),'p')
              if isequal(options.lgrainsize,lgraincount)
                 if (options.follow)
                    change_weights_follow(options);
                 else
                    change_weights(options);
                 end
                    lgraincount = 1;                 
              else
                 lgraincount = lgraincount + 1;
              end
              for i =1:numel(net.pool)
                  for k=1:numel(net.pool(i).oproj)
                      tp = net.pool(i).oproj(k).toindex;
                      pindex = net.pool(i).oproj(k).projindex;
                      net.pool(i).oproj(k).weight = net.pool(tp).proj(pindex).weight;
                  end
              end              
           end
        end
        if logoutflag && ~isempty(strmatch('pattern',lower(freqrange)))
            writeoutput(runparams.process,'pattern');
        end
        if breakval && p ~= N % on last tick
           if PDPAppdata.gui
              update_display (pno);
           end
           PDPAppdata.(appflag) = 1;
           return;
        end
        granopts = {'pattern','interval','tick'};
        if PDPAppdata.gui && any((strncmpi(granopts,runparams.granularity,...
           length(runparams.granularity))))
           updatecount = update_display_step(updatecount,pno);
           if updatecount < 0 && p ~= N % 'step' mode
              PDPAppdata.(appflag) = 1;
              return;
           end
        end
        net.tickno = 1;        
        sttick = 2;        
    end
    cpno = 1;
    trpatn = [];
    tstpatn = [];
    if (options.lflag) && isequal(lower(options.lgrain(1)),'e')
        if isequal(options.lgrainsize,lgraincount)
           if (options.follow)
              change_weights_follow(options);
           else
              change_weights(options);
           end
           lgraincount = 1;
        else
           lgraincount = lgraincount + 1;
        end
        for i =1:numel(net.pool)
            for k=1:numel(net.pool(i).oproj)
                tp = net.pool(i).oproj(k).toindex;
                pindex = net.pool(i).oproj(k).projindex;
                net.pool(i).oproj(k).weight = net.pool(tp).proj(pindex).weight;
            end
        end         
    end
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
       writeoutput(runparams.process,'epoch');
    end    
    if PDPAppdata.gui && strncmpi(runparams.granularity,'epoch',...
       length(runparams.granularity))
       updatecount = update_display_step(updatecount,pno);
    end
    if net.tss < options.ecrit
       disp('Error criterion reached');
       if PDPAppdata.gui
          update_display(net.(patfield));
       end
       net.(patfield)=0;
       break;
    end
    if breakval || (PDPAppdata.gui && updatecount < 0) %when p==N for 'step' mode or 'stepping' through each epoch 
       break; % to check if binary output files need to be written
    end
    orig_tss = 0.0;    
    orig_tce = 0.0;    
end
if PDPAppdata.gui
   update_display(-1);
end
PDPAppdata.(appflag) = 0;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end

function [opts,dat,spat,npat,sepoch,lepoch,pfield,stick,otss,otce,...
flag]= initialize_params
global net runparams patn trpatn tstpatn PDPAppdata rangelog;
otss = 0.0;
otce = 0.0;
flag ='tstinterrupt_flag';
stick = 2;
if PDPAppdata.gui && (~isequal(size(net.nancolor),[1 3]) || ...
   any(net.nancolor > 1) || any(net.nancolor < 0))
   fprintf(1,'net.nancolor has an invalid value,switching to default RGB triplet [0.97 0.97 0.97]\n');
   net.nancolor = [0.97 0.97 0.97];
end
if strncmpi(runparams.process,'test',length(runparams.process))
   opts = net.testopts;
   dat = PDPAppdata.testData;
   pfield = 'testpatno';
   if PDPAppdata.tstinterrupt_flag ~= 1
      net.tickno = 0;
      tstpatn = getpatternrange(dat,opts);
   end
   patn = tstpatn;
   pnum = get(findobj('tag','tstpatlist'),'Value');   
   if strncmpi(runparams.mode,'run',length(runparams.mode))
      net.tickno = 1;
      if runparams.alltest
         net.testpatno = 1;
         npat = numel(dat);
      else
         net.testpatno = find(patn==pnum,1);
         npat = net.testpatno;         
      end
   else
       net.tickno = net.tickno + 1;
       if net.tickno > net.nticks+1
          net.tickno = 1;
       end
       if runparams.alltest
          if net.tickno == 1
             net.testpatno = net.testpatno + 1;
             net.pss = 0;
             net.pce = 0;             
             net.intervalno = 0;
          end
          npat = numel(dat);
          if net.testpatno > npat
             net.testpatno = 1;
          end
          if net.testpatno == 1
             net.tss = 0;
             net.tce = 0;             
          end
          otss = net.tss;       
          otce = net.tce;          
       else
         net.testpatno = find(patn==pnum,1);
         net.pss = 0;        
         net.pce = 0;         
         npat = net.testpatno;
       end
       stick = max(2,net.tickno);
   end    
   if isempty(runparams.range)
      sepoch = net.epochno;
      lepoch = net.epochno;
      spat = net.testpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);
   end
else
   opts = net.trainopts;
   pfield = 'trainpatno';
   dat = PDPAppdata.trainData;
   npat = numel(dat);
   if PDPAppdata.trinterrupt_flag
      otss = net.tss; % in interrupt mode, start with current tss
      otce = net.tce;      
   else
      trpatn = getpatternrange(dat,opts);
   end
   patn = trpatn;
   flag = 'trinterrupt_flag';
   net.tickno = 1;
   net.trainpatno = net.trainpatno + 1;
   if net.trainpatno > numel(dat)
      net.trainpatno = 1;
   end
   if net.trainpatno == 1
      net.epochno = net.epochno + 1;
      otss = 0;
      otce = 0;
   end
   if isempty(runparams.range)
      sepoch = net.epochno ;
      if isempty(runparams.nepochs)
         lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;          
%          lepoch = sepoch + opts.nepochs - 1;
      else
         net.trainpatno = 1;
         lepoch = sepoch + runparams.nepochs - 1;
      end
      spat = net.trainpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);
   end
end
rangelog = [spat sepoch];

function breakind = doforwardpass(tstart,options,pno,logflag,freq)
global net runparams PDPAppdata;
breakind = 0;
updatecount = 0;
for ticknum = tstart : net.nticks + 1
    abort = PDPAppdata.stopprocess;
    if ~isempty(abort)
        breakind = 1;
        return;
    end
    settarget(ticknum);    
    net.intervalno = (ticknum - 1)/net.ticksperinterval;
    net.tickno =  ticknum;
    compute_output(ticknum,options);
    net.stateno = ticknum;    
    if logflag && ~isempty(strmatch('tick',lower(freq)))
       writeoutput(runparams.process,'tick');
    end
    if logflag && ~isempty(strmatch('backtick',lower(freq)))
       writeoutput(runparams.process,'backtick');
    end
    if logflag && (~isempty(strmatch('interval',lower(freq))) && ...
       net.intervalno == round(net.intervalno))
       writeoutput(runparams.process,'interval');
    end
    if PDPAppdata.gui && strncmpi(runparams.process,'test',...
       length(runparams.process))
       if (strncmpi(runparams.granularity,'tick',...
          length(runparams.granularity))) || ...
          (strncmpi(runparams.granularity,'interval',...
          length(runparams.granularity)) && ...
          net.intervalno == round(net.intervalno))
          updatecount = update_display_step(updatecount,pno);
          if updatecount < 0
             breakind = 1;
             return;
          end
       end
    end     
end

function breakind = dobackwardpass(logflag,freq)
global net runparams PDPAppdata;
breakind = 0;
for ticknum = net.nticks : -1 : 2
    abort = PDPAppdata.stopprocess;
    if ~isempty(abort)
        breakind = 1;
        return;
    end
    net.tickno = ticknum;
    compute_error(ticknum);
    if logflag && (~isempty(strmatch('backtick',lower(freq))))
       writeoutput(runparams.process,'backtick');
    end 
end


function setupextinputandtarget(dat,pnum)
global net inpools outpools;
d = dat(pnum);
for i=inpools
    net.pool(i).extinput(:) = nan;
    net.pool(i).clampstates(:) = 0;
    net.pool(i).target(:) = nan;
    f= ([d.inpools.poolnum] == i);
    if any(f)
        for j=1: size(d.inpools(f).tickpatterns,1)
            r = d.inpools(f).tickpatterns{j,1};
            v = d.inpools(f).tickpatterns{j,2};
            net.pool(i).extinput(r,:)= repmat(v,numel(r),1);
        end
        net.pool(i).clampstates = d.inpools(f).hclamps;
    end
end
for i=outpools
    net.pool(i).exttarget(:) = nan;
    net.pool(i).target(:) = nan;
    net.pool(i).hastargets(:) = 0.0;
    f= ([d.outpools.poolnum] == i);
    if any(f)
        for j=1: size(d.outpools(f).tickpatterns,1)
            r = d.outpools(f).tickpatterns{j,1};
            v = d.outpools(f).tickpatterns{j,2};
            net.pool(i).exttarget(r,:)= repmat(v,numel(r),1);
            net.pool(i).hastargets(r) = 1.0;
        end
    end
end
net.cpname = dat(pnum).pname;

%in the alt version of setinitialstate, the net input of all units
%is initialized to the bias, even if they are hard-clamped.
%this version goes with compute_output_alt, in which whatever influences are
%present come in to the net input and it evolves over time, even if its
%activation is hard clamped.
% function setinitialstate_alt
% global net;
% for ind = 2 : numel(net.pool)
%        biaswt = repmat(0,1,net.pool(ind).nunits);
%        if ~isempty(net.pool(ind).proj)
%           projind = [net.pool(ind).proj.fromindex]==1; %bias
%           if any(projind)
%              biaswt = net.pool(ind).proj(projind).weight';
%           end
%        end
%        net.pool(ind).netinput = biaswt;
%        if strcmpi(net.pool(ind).type,'input') || strcmpi(net.pool(ind).type,'inout')
%           if ~isnan(net.pool(ind).extinput(1,1))
%              net.pool(ind).netinput = net.pool(ind).netinput + ...
%                                    net.pool(ind).extinput(1,:);
%           end
%        end
%        if net.pool(ind).clampstates(1)
%          net.pool(ind).activation = net.pool(ind).extinput(1,:);
%        else
%          net.pool(ind).activation = logistic(net.pool(ind).netinput);
%        end
%     net.pool(ind).acthistory(:,1) = net.pool(ind).activation;
% end

function setinitialstate
global net;
for ind = 2 : numel(net.pool)
    if net.pool(ind).clampstates(1)
       net.pool(ind).activation = net.pool(ind).extinput(1,:);
       act = net.pool(ind).activation;
       act(act == 1) = 0.99999988;
       act(act == 0) = 0.00000012;
       net.pool(ind).netinput = log (act ./(1 - act));
    else
       biaswt = repmat(0,1,net.pool(ind).nunits);
       if ~isempty(net.pool(ind).proj)
          projind = [net.pool(ind).proj.fromindex]==1; %bias
          if any(projind)
             biaswt = net.pool(ind).proj(projind).weight';
          end
       end
       net.pool(ind).netinput = biaswt;
       if strcmpi(net.pool(ind).type,'input') || strcmpi(net.pool(ind).type,'inout')
          if ~isnan(net.pool(ind).extinput(1,1))
             net.pool(ind).netinput = net.pool(ind).netinput + ...
                                   net.pool(ind).extinput(1,:);
          end
       end
       net.pool(ind).activation = logistic(net.pool(ind).netinput);
    end
    net.pool(ind).acthistory(:,1) = net.pool(ind).activation;
end

function settarget(ticknum)
global net outpools;
%set target
for i=1:numel(outpools)
    outindx = outpools(i);
    net.pool(outindx).target = net.pool(outindx).exttarget(ticknum,:);
end


function retval = logistic(val)
retval = val;
retval(retval > 15.9357739741644) = 15.9357739741644; % returns 0.99999988;
retval(retval < -15.9357739741644) = -15.9357739741644; % returns 0.00000012
retval = 1.0 ./(1.0 + exp(-1 * retval));

function clear_dEdw()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        net.pool(i).proj(j).dEdw(:) = 0.0;
    end
end

%code below is the old version of compute-output, which does not keep the
%net input at extreme values.  The version below does keep the net input at
%extremal values, and is the one that should be emulated in the timesequence code.
% function compute_output_alt(ticknum,procoptions)
% global net;
% dt = 1/net.ticksperinterval; 
% for i = 1:numel(net.pool)
%     prevnet = net.pool(i).netinput;
%     ninput = repmat(0,1,net.pool(i).nunits);
%     if ~net.pool(i).clampstates(ticknum) && ~isnan(net.pool(i).extinput(ticknum,1))
%        ninput = net.pool(i).extinput(ticknum,:);
%     end
%     for j=1:numel(net.pool(i).proj)
%         sender = net.pool(net.pool(i).proj(j).fromindex);
%         ninput = (net.pool(i).proj(j).weight * sender.acthistory(:,ticknum-1))' + ninput; 
%     end
%     net.pool(i).netinput = dt*(ninput - prevnet) + prevnet;
%     if net.pool(i).clampstates(ticknum)
%        net.pool(i).activation = net.pool(i).extinput(ticknum,:);
%     else
%        net.pool(i).activation = logistic(net.pool(i).netinput);
%     end
%     net.pool(i).acthistory(:,ticknum) = net.pool(i).activation;
%     if net.pool(i).hastargets(ticknum)
% %     if ~isnan(net.pool(i).exttarget(ticknum,:))
%        t = net.pool(i).exttarget(ticknum,:);
%        d = zeros(1,net.pool(i).nunits);
%        a = net.pool(i).activation;
%        if (procoptions.errmargin >0)
%           emg = procoptions.errmargin;
%           ome = 1 - procoptions.errmargin;
%           %when target is 1.0;
%           u = find(t == 1.0);
%           x = find(a(u) <= ome);
%           d(u(x)) = ome - a(u(x));
%           %when target is 0.0
%           u = find(t == 0.0);
%           x = find(a(u) >= emg);
%           d(u(x)) = emg - a(u(x));
%           %when target is -1.0
%           u = find(t == -1.0);
%           x =  find(a(u) >= -ome);
%           d(u(x)) = -ome - a(u(x));
%           %when target is none of the above
%           u = find(~ismember(t,[1.0 0.0 -1.0]));
%           d(u) = t(u) - a(u);
%        else
%           d = t - a;
%        end
% %            t = getmargin(t,procoptions.errmargin);
%        if strcmpi(procoptions.errmeas,'cee')
%            net.pool(i).dEdnet = dt .* d;
% %           net.pool(i).dEdnet = (t - net.pool(i).activation).*dt;
%        else
% %           actd = net.pool(i).activation .* (1 - net.pool(i).activation);
%           net.pool(i).dEdnet = dt .* d .* a .* (1-a);   
% %           net.pool(i).dEdnet = (t - net.pool(i).activation) .* dt .* actd;
%        end
%     else
%        net.pool(i).dEdnet = repmat(0,size(net.pool(i).dEdnet));
%     end
%     net.pool(i).dEdnethistory(:,ticknum) = net.pool(i).dEdnet;
% end

%this version makes the net input conform to the activation, and goes with
%the standard version of setinitialstate
function compute_output(ticknum,procoptions)
global net;
dt = 1/net.ticksperinterval; 
for i = 1:numel(net.pool)
    prevnet = net.pool(i).netinput;
    ninput = repmat(0,1,net.pool(i).nunits);
    if net.pool(i).clampstates(ticknum)
       net.pool(i).activation = net.pool(i).extinput(ticknum,:);
       act = net.pool(i).activation;
       act(act == 1) = 0.99999988;
       act(act == 0) = 0.00000012;
       net.pool(i).netinput = log (act ./(1 - act));
    else 
      if ~isnan(net.pool(i).extinput(ticknum,1))
         ninput = net.pool(i).extinput(ticknum,:);
      end
      for j=1:numel(net.pool(i).proj)
        sender = net.pool(net.pool(i).proj(j).fromindex);
        ninput = (net.pool(i).proj(j).weight * sender.acthistory(:,ticknum-1))' + ninput; 
      end
      net.pool(i).netinput = dt*(ninput - prevnet) + prevnet;
      net.pool(i).activation = logistic(net.pool(i).netinput);
    end
    net.pool(i).acthistory(:,ticknum) = net.pool(i).activation;
    if net.pool(i).hastargets(ticknum)
       t = net.pool(i).exttarget(ticknum,:);
       d = zeros(1,net.pool(i).nunits);
       a = net.pool(i).activation;
       if (procoptions.errmargin >0)
          emg = procoptions.errmargin;
          ome = 1 - procoptions.errmargin;
          %when target is 1.0;
          u = find(t == 1.0);
          x = find(a(u) <= ome);
          d(u(x)) = ome - a(u(x));
          %when target is 0.0
          u = find(t == 0.0);
          x = find(a(u) >= emg);
          d(u(x)) = emg - a(u(x));
          %when target is -1.0
          u = find(t == -1.0);
          x =  find(a(u) >= -ome);
          d(u(x)) = -ome - a(u(x));
          %when target is none of the above
          u = find(~ismember(t,[1.0 0.0 -1.0]));
          d(u) = t(u) - a(u);
       else
          d = t - a;
       end
       if strcmpi(procoptions.errmeas,'cee')
           net.pool(i).dEdnet = dt .* d;
       else
          net.pool(i).dEdnet = dt .* d .* a .* (1-a);   
       end
    else
       net.pool(i).dEdnet(:) = 0; %repmat(0,size(net.pool(i).dEdnet));
    end
    net.pool(i).dEdnethistory(:,ticknum) = net.pool(i).dEdnet;
end

function outt = getmargin(target,margin)
outt = target;
outt(target >  margin) = target(target > margin) - margin;
outt(target < -margin) = target(target < -margin) + margin;
outt(abs(target) <= margin) = 0.0;
   
   
function compute_error(ticknum)
global net;
dt = 1/net.ticksperinterval;
for i = 1:numel(net.pool)
    net.pool(i).dEdA(:) = 0; %repmat(0,1,net.pool(i).nunits);
end
for i=1:numel(net.pool)    
    if ~net.pool(i).clampstates(ticknum)
        for j=1:numel(net.pool(i).oproj)
            r = net.pool(i).oproj(j).toindex;
            if ~net.pool(r).clampstates(ticknum+1)
                net.pool(i).dEdA = net.pool(i).dEdA + ...
                                   net.pool(r).dEdnethistory(:,ticknum+1)' * ...
                                   net.pool(i).oproj(j).weight;
            end 
        end      
        activ = net.pool(i).acthistory(:,ticknum)';    
        net.pool(i).dEdnet = net.pool(i).dEdnethistory(:,ticknum)' +...
                             (dt * activ .* (1 - activ) .* net.pool(i).dEdA) +... 
                             (1-dt).*net.pool(i).dEdnethistory(:,ticknum+1)';
     else % if state is clamped dEdnet = 0
         net.pool(i).dEdnet = zeros(size(net.pool(i).dEdnet));
     end
     net.pool(i).dEdnethistory(:,ticknum) = net.pool(i).dEdnet;
end


function compute_dEdw()
global net;
for i = 1: numel(net.pool)
    for j=1: numel(net.pool(i).proj)
        s = net.pool(i).proj(j).fromindex;
        net.pool(i).proj(j).dEdw = net.pool(i).proj(j).dEdw + ...
                                   net.pool(i).dEdnethistory(:,2:end) * ...
                                   net.pool(s).acthistory(:,1:end-1)';
    end
end

function sumstats()
global net outpools;
net.pss = 0.0;
net.pce = 0.0;
dt = 1/net.ticksperinterval;
for i=1:numel(outpools)
    ind = outpools(i);
    %t should be an array with a 1 where ever t is specified
    t = find(net.pool(ind).hastargets ==1);
    ext = net.pool(ind).exttarget(t,:)';
    tacthist = net.pool(ind).acthistory(:,t);
    err = dt*sum(sum((ext - tacthist) .^ 2));    
    ton = (ext==1);
    toff = (ext==0);
    %to prevent ce from blowing up, cut off act hist at .999 and .001:
    tacthist(tacthist < .001) = .001;
    tacthist(tacthist > .999) = .999;
    ce = dt*(sum(log(tacthist(ton))) + sum(log(1-tacthist(toff))));
    net.pss = net.pss + err;
    net.pce = net.pce - ce; % to make pce positive
end
net.tss = net.tss+ net.pss;
net.tce = net.tce+ net.pce; 

function change_weights(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
for ind=non_input
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'scalar')
           continue;
        end
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
           lr = opts.lrate;
        end
        if (opts.wdecay)
        net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).dEdw - ... 
                                        opts.wdecay * net.pool(ind).proj(j).weight + ...
                                        opts.momentum * net.pool(ind).proj(j).dweight;
        else
        net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).dEdw + ... 
                                        opts.momentum * net.pool(ind).proj(j).dweight;
        end
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
                                       net.pool(ind).proj(j).dweight;
        net.pool(ind).proj(j).dEdw(:) = 0; %repmat(0,...
%                                      size(net.pool(ind).proj(j).dEdw));      
    end
end

function change_weights_follow(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
p_css = net.css;
net.css = 0.0;
dp =0;
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'scalar')
           continue;
        end        
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
           lr = opts.lrate;
        end
        if (opts.wdecay)
            net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).dEdw - ... 
                                        opts.wdecay * net.pool(ind).proj(j).weight + ...
                                        opts.momentum * net.pool(ind).proj(j).dweight;
        else
            net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).dEdw + ...
                                        opts.momentum * net.pool(ind).proj(j).dweight;
        end
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
                                       net.pool(ind).proj(j).dweight;
        sqwed = net.pool(ind).proj(j).dEdw  .^ 2;
        net.css = net.css + sum(sqwed(1:end));
        dpprod = net.pool(ind).proj(j).dEdw  .*  net.pool(ind).proj(j).pwed;       
        dp = dp + sum(dpprod(1:end));
        net.pool(ind).proj(j).pwed = net.pool(ind).proj(j).dEdw ;
        net.pool(ind).proj(j).dEdw(:) = 0; %repmat(0,...
%                                      size(net.pool(ind).proj(j).dEdw));         
    end
end
den = p_css * net.css;
if den > 0.0
   net.gcor = dp/(sqrt(den));
else
   net.gcor = 0.0;
end
constrain_weights();


function constrain_weights()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        w = net.pool(i).proj(j).weight; %just for readability
        switch lower(net.pool(i).proj(j).constraint_type)
            case 'prandom'
                 net.pool(i).proj(j).weight(w < 0) = 0.0;
            case 'nrandom'
                 net.pool(i).proj(j).weight(w > 0) = 0.0;
        end
    end
end

function currcount = update_display_step(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmpi(runparams.mode,'step',length(runparams.mode))
       currcount = -1;
    end
end


function range = getpatternrange(patterns,procoptions)
N = numel(patterns);
switch procoptions.trainmode(1)
    case 's'  %strain
        range = 1:N;
    case 'p'  %ptrain
        range = randperm(N);
    case 'r' %rtrain
        range = ceil(N * rand(1,N));
    otherwise
        range =1: N;
end
 