function [flag range] = getloggingdetails(proc)
global net outputlog logindex binarylog;
logindex=[];
% if isempty(binarylog)
   binarylog = struct('logind',[],'output',{});
% else
%    [binarylog.logind] = deal(0);
% end
flag=0;
range={};
[tf, lmatch] = ismember({outputlog.file},net.outputfile);
bnum=1;
k=1;
for i=1:numel(lmatch)
  pp = outputlog(lmatch(i));
  [t,m]= ismember({proc,'both'},pp.process); % using ismember as pp.process can be {'train','test'}
  if any(m)
      if strcmpi(pp.status,'on')
         range{k} = pp.frequency;
         logindex(k) = lmatch(i);
         if pp.writemode(1) =='b'
            binarylog(bnum).logind = logindex(k);
            binarylog(bnum).output = outputlog(logindex(k)).binout;
            bnum = bnum+1;
         end
         k=k+1;
      end
  end
end
if ~isempty(range)
    flag=1;
end