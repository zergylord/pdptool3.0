function cl_reset
global net PDPAppdata;
net.cpname='';
net.trainpatno=0;
net.testpatno=0;
net.epochno=0;
if exist('RandStream','file') == 2 % for version 7.8 and above    
   stream = RandStream.getDefaultStream;
   reset(stream);
else
   rand('seed',net.seed);
   randn('seed',net.seed);
end
for i=1:numel(net.pool)
    vecsize = [1 net.pool(i).nunits];
    net.pool(i).activation(:) = 0;%repmat(0,vecsize);
    net.pool(i).netinput(:) = 0;%repmat(0,vecsize);
    pools = {net.pool.name};
    if ~isempty(net.pool(i).proj)
        matx = net.pool(i).nunits;
         for j=1:numel(net.pool(i).proj)
             from = net.pool(i).proj(j).frompool;
             [tf,np] = ismember(from,pools);
             maty = [net.pool(np).nunits];
             matsz = [matx maty];
           switch lower(net.pool(i).proj(j).constraint_type)
            case 'scalar'
                 net.pool(i).proj(j).weight(:) = net.pool(i).proj(j).constraint; %repmat(1,matsz) .* ...
%                                               net.pool(i).proj(j).constraint;
            case 'random'
                 net.pool(i).proj(j).weight = rand(matsz);
                 sumwt = sum(net.pool(i).proj(j).weight,2);
                 ncols = size(net.pool(i).proj(j).weight,2);
                 net.pool(i).proj(j).weight = net.pool(i).proj(j).weight ./...
                                              repmat(sumwt,1,ncols);                  
            case 'tbias'
                  tbias = net.pool(i).proj(j).topbias;
                  sc = 2*net.pool(i).proj(j).ispread^2; 
                  nconst = 2*pi*net.pool(i).proj(j).ispread^2; %normalizing constant for Gaussian                       
                  [si,sj] = ind2sub(net.pool(np).geometry,...
                            (1:net.pool(np).nunits));
                  [ri,rj] = ind2sub(net.pool(i).geometry,...
                                     (1:net.pool(i).nunits));                           
                 %If the either grid is larger, then center the other grid
                 %on the larger grid
                 if ~isequal(net.pool(np).geometry,net.pool(i).geometry)
                    diffgeo = net.pool(np).geometry - net.pool(i).geometry;
                    if diffgeo(1) > 0 %if input grid is larger in X dim
                        ri = ri + diffgeo(1)/2;
                    else              %if input grid is smaller in X dim
                        si = si - diffgeo(1)/2;
                    end
                    if diffgeo(2) > 0 %if input grid is larger in Y dim
                        rj = rj + diffgeo(2)/2;
                    else             %if input grid is smaller in Y dim
                        sj = sj - diffgeo(2)/2;
                    end
                 end
                 %Non-topograhic part of the weights, uniform then
                 %normalized
                 net.pool(i).proj(j).weight = rand(matsz);
                 sumwt = sum(net.pool(i).proj(j).weight,2);
                 ncols = size(net.pool(i).proj(j).weight,2);
                 net.pool(i).proj(j).weight = net.pool(i).proj(j).weight ./...
                                              repmat(sumwt,1,ncols);          
                 %Add topographic part of the weights
                 p1 = repmat(si,numel(ri),1);
                 q1 = repmat(ri',1,numel(si));
                 p2 = repmat(sj,numel(rj),1);
                 q2 = repmat(rj',1,numel(sj));
                 d = (p1 - q1) .^2 + (p2 - q2).^2;
                 net.pool(i).proj(j).weight = (1. - tbias) .* ...
                                              net.pool(i).proj(j).weight + ...
                                              tbias .* exp(-d./sc)./ nconst;
           end
          end
     end
end
if ~isempty(findobj('tag','netdisplay'))
   update_display(1);
end
if ~isempty(net.outputfile)
   resetlog;
end
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end