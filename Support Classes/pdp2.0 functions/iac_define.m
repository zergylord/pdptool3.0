function definednet = iac_define(net)
% This function is called for setting up interactive activation and 
% competition networks.'n' is the network struct variable with valid 
% fieldnames for 'iac' networks.'basepool' defines a valid pool structure. 
% 'baseproj' defines valid projection structure.'testopts' defines valid 
% testing parameters. fillfields routine populates these structure 
% variables with values from the given input parameter 'net'.
% setiacdefaultvars initializes the vectors and matrices with default 
% values.
% Global variable PDPAppdata contains  runtime information for the
% current pdptool session. PDPAppdata.networks is a list of all currently 
% loaded networks (feature supported only for pdptool in gui mode).
% 'editable' variable can be used to specify user-modifiable network
% fields. These are then used by the object viewer module when invoked from
% pdptool mainwindow menu item 'Launch object viewer'.
% This is also used only when running pdptool in gui mode.
%
% **Important note for enhancement** : To add a new network type, you would
% need to define struct variables specific to the new type by creating a
% file like this with exactly similar function signature. The easiest method 
% would be copying the file content in its entirety into a new file, change
% function name to <newtype>_define , save it as <newtype>_define.m file. In
% the new file all you have to do is change fields of struct variables 
% 'n','basepool','baseproj','tropts','tstopts' - add/remove fieldnames
% according to your needs for the new type.Make corresponding changes to
% set<newtype>defaultvars subroutine. Type 'help struct' on matlab command
% prompt to get more information on syntax of MATLAB struct function.

global editable PDPAppdata;
networks = PDPAppdata.networks;
namedef=sprintf('net%d',numel(networks)+1);
n= struct('name',namedef,'type','iac','seedmode',1,'seed',0,'pool',[],...
          'num',1,'cycleno',0,'nunits',0,'patno',0,'cpname','',...
          'nancolor',[.97 .97 .97],'templatefile','','outputfile','',...
          'logfile','','testopts',[]);
basepool = struct('name','pool','type','Input','nunits',1,'unames',[],...
                 'proj',[],'activation',[],'extinput',[],'excitation',[],...
                 'inhibition',[],'netinput',[],'noise',0.0);
baseproj = struct('frompool','bias','constraint_type','scalar',...
                  'constraint',0.0,'weight',[]);
tstopts =struct('ncycles',10,'actfunction','st','estr',0.1,'alpha',0.1,...
                'gamma',0.1,'max',1,'min',-0.2,'decay',0.1,'rest',-0.1,...
                'testset','none');
n = fillfields(n,net);
net = bias_define(net);   
p(1:length(net.pool)) = basepool;
for i=1:numel(net.pool)
    p(i) = fillfields(p(i),net.pool(i));
    prj = baseproj;
    if isfield(net.pool(i),'proj')&& numel(net.pool(i).proj)>0
       for j = 1:numel(net.pool(i).proj)
           prj(j) = fillfields(baseproj,net.pool(i).proj(j));
       end
        p(i).proj = prj;            
    end
end
n.pool = p;
n.testopts = tstopts;
if isfield(net,'testopts')
    n.testopts = fillfields(n.testopts,net.testopts);
end
if n.seedmode == 0       % 0 - seed provided, 1 - seed randomly
    if exist('RandStream','file') == 2 % for version 7.8 and above
        stream = RandStream('mt19937ar','Seed',n.seed);
        RandStream.setDefaultStream(stream);
    else
        rand('seed',n.seed); % for backward compatibility
        randn('seed',n.seed);                
    end
else
    if exist('RandStream','file') == 2 % for version 7.8 and above    
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        RandStream.setDefaultStream(stream);
        n.seed = stream.Seed;
    else
       n.seed = rand('seed');
       randn('seed',n.seed);
    end
end
editable.net = {'seed'};
editable.pool = {'activation','extinput','excitation','inhibition','netinput'};
editable.proj = {'constraint','weight'};
editable.testopts = fieldnames(rmfield(tstopts,'testset'));
definednet = setiacdefaultvars(n); 

function net = setiacdefaultvars(net)
global readhandles iacnet;
pinfo = cell(numel(readhandles),2);
for r=1:numel(readhandles)
    args=readhandles(r).arg;
    pinfo(r,:)={args{2} args{3}};
end
iacnet=net;
 for i=1:numel(net.pool)
     vecsize = [1 net.pool(i).nunits];
     net.pool(i).activation = repmat(net.testopts.rest,vecsize);
     net.pool(i).netinput = repmat(0,vecsize);
     net.pool(i).excitation = repmat(0,vecsize);
     net.pool(i).inhibition = repmat(0,vecsize);     
     net.pool(i).extinput = repmat(0,vecsize);
     pools = {net.pool.name};
     if ~isempty(net.pool(i).proj)
         matx = net.pool(i).nunits;
         for j=1:numel(net.pool(i).proj)
             k=[];
             from = net.pool(i).proj(j).frompool;
             if isempty(from)
                continue;
             end
             if ~isempty(readhandles)
                 p=[];
                 for pp=1:size(pinfo,1)
                     if isequal(pinfo(pp,:),{from net.pool(i).name})
                        p=pp;
                        break;
                     end
                 end
                 if ~isempty(p)
                     func= readhandles(p).handle;
                     args = readhandles(p).arg;
                     net.pool(i).proj(j).constraint = func(args{1:end});
                 end
             end
             [tf,np] = ismember(from,pools);
             maty = net.pool(np).nunits;
             matsz = [matx maty];
             net.pool(i).proj(j).weight = repmat(1,matsz) .* ...
                                          net.pool(i).proj(j).constraint;
             if size(net.pool(i).proj(j).weight,1)==size(net.pool(i).proj(j).weight,2) ...
                && isscalar(net.pool(i).proj(j).constraint)   %mutually inhibitory connections
                sz = 1:numel(net.pool(i).proj(j).weight);
                [x,y] = ind2sub(size(net.pool(i).proj(j).weight),sz);
                net.pool(i).proj(j).weight(x==y)=0;
             end
          end
     end
 end
net.pool(1).activation = 1;  %bias  
clear global iacnet;