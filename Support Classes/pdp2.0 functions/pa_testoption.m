function varargout = pa_testoption(varargin)
% PA_TESTOPTION M-file for pa_testoption.fig
% This is called when user clicks on
% (i)  'Set Testing options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Test panel of the network viewer window.
% It presents testing parameters for 'pa' type of network that can be 
% modified and saved.  

% Last Modified 03-May-2007 14:14:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pa_testoption_OpeningFcn, ...
                   'gui_OutputFcn',  @pa_testoption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pa_testoption is made visible.
function pa_testoption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pa_testoption (see VARARGIN)

% The GUI opening function sets current/existing values of testing 
% parameters to the corresponding GUI controls.
movegui('center');
setcurrentvalues();

% Choose default command line output for pa_testoption
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% handles
% get(hObject,'children')
% UIWAIT makes pa_testoption wait for user response (see UIRESUME)
% uiwait(handles.pa_testoption);


% --- Outputs from this function are returned to the command line.
function varargout = pa_testoption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function noiseedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function actfnpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
   
function ecritedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tempedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tstoptnfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% This gets appdata for pa_testoption figure handle by extracting details 
% of test patterns currently in the Test pattern list of the main window.
testpatfiles = testpatternspopup_cfn(hObject);
setappdata (findobj('tag','pa_testoption'),'patfiles',testpatfiles);


function actfnpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function tstoptnfilepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function tstoptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.tstoptnfilepop,handles.pa_testoption);

function setcurrentvalues
% This sets the current testing parameter values to the corresponding GUI
% controls. 
global net;
handles = guihandles;
testopts = net.test_options;
set (handles.ecritedit,'String',testopts.ecrit);
set (handles.noiseedit,'String',testopts.noise);
%set (handles.tempedit,'String',testopts.temp);
%actshort = {'st','li','cs','lt'};
%actlong = {'stochastic','linear','continuous sigmoid','linear threshold'};
%actfn =1;
%ind = find(strcmpi(actshort,testopts.actfunction));
%if isempty(ind)
%   ind = find(strncmpi(testopts.actfunction,actlong, length(testopts.actfunction)));
%   if ~isempty(ind)
%       actfn = ind(1);
%   end
%else
%   actfn = ind;
%end
%set (handles.actfnpop,'Value',actfn);


function applybtn_Callback(hObject, eventdata, handles)

% Calls subroutine that sets network parameters.Does not exit from window.
apply_options();

function okbtn_Callback(hObject, eventdata, handles)

% Applies current options and exits.
apply_options();
delete(handles.pa_testoption);

function cancelbtn_Callback(hObject, eventdata, handles)

% Exits without making any changes
delete(handles.pa_testoption);

function apply_options()

% This reads in the new parameters and applies changes to the network
% testopts structure.
handles = guihandles;
global net PDPAppdata;
n = PDPAppdata.networks;

%if isfield(net,'test_options') 
tstopts = net.test_options;
tempopts = tstopts;
if isfield(net.test_options,'testset')
end
%end
tstopts.ecrit = str2double(get(handles.ecritedit,'String'));
tstopts.noise = str2double(get(handles.noiseedit,'String'));
%afval = get(handles.actfnpop,'Value');
%actshort = {'st','li','cs','lt'};
%tstopts.actfunction = actshort{afval};
tstopts.temp = str2double(get(handles.tempedit,'String'));
tstopts.nepochs = 1; 

patlist = get(handles.tstoptnfilepop,'String');
pfiles = getappdata(handles.pa_testoption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','test');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.tstoptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
     end    
end
plistval = get(handles.tstoptnfilepop,'Value');
tstopts.testset = patlist{plistval};

% -- Calls settestopts command with only the modified fields of the testopts
%    structure. 
update_params = compare_struct(tstopts,tempopts);
if ~isempty(update_params)
   args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
   args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
   settestopts(args{1:end});
   if PDPAppdata.lognetwork
      tststmt = 'settestopts (';
      for i=1:size(update_params,1)
          field_name = update_params{i,1};
          field_val = update_params{i,2};
          if ischar(field_val)
             tststmt = sprintf('%s''%s'',''%s'',',tststmt,field_name,field_val);
          else
             tststmt = sprintf('%s''%s'',%g,',tststmt,field_name,field_val);
          end
      end
      tststmt = sprintf('%s);',tststmt(1:end-1));
      updatelog(tststmt);
   end
end
% 
% tempopts = orderfields(tempopts,tstopts);
% fields = fieldnames(tempopts);
% tempA = struct2cell(tempopts);
% tempB = struct2cell(tstopts);
% diffind = cellfun(@isequal,tempA,tempB);
% diffind = find(~diffind);
% if ~isempty(diffind)
%    tststmt = 'settestopts (';
%    args = cell(1,numel(diffind)*2);
%    [args(1:2:end-1)] = fields(diffind);   
%    for i = 1 : numel(diffind)
%        fval =  tstopts.(fields{diffind(i)});
%        args{i*2} = fval;       
%        if ischar(fval)
%           tststmt = sprintf('%s''%s'',''%s'',',tststmt,fields{diffind(i)},...
%                     fval);
%        else
%           tststmt = sprintf('%s''%s'',%g,',tststmt,fields{diffind(i)},fval);
%        end
%    end
%    settestopts(args{1:end});
%    if PDPAppdata.lognetwork
%       tststmt = sprintf('%s);',tststmt(1:end-1));
%       updatelog(tststmt);
%    end 
% end
set(findobj('tag','testpatpop'),'String',patlist,'Value',plistval);
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.pa_testoption,'patfiles',pfiles);

% -- All edited uicontrols that have KeypressFcn set to change background 
%    color to yellow is changed back to white.This  indicates that modified
%   fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');




function pa_testoption_CloseRequestFcn(hObject, eventdata, handles)

% Hint: delete(hObject) closes the figure
delete(hObject);


function setwritebtn_Callback(hObject, eventdata, handles)

% Calls the module that presents options for writing network output logs.
% Once the setwriteopts window is up, this makes 'test' as the default 
% logging process.
% setwriteopts;
create_edit_log;
logproc = findobj('tag','logprocpop');
if ~isempty(logproc)
    set(logproc,'Value',2);
end


function ecritedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function tempedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function noiseedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');
