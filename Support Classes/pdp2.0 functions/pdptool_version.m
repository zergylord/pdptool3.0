function info_string = pdptool_version(varargin)
%   pdptool_version returns a string containing PDPTool version 
%   information.
%
%   PDPTOOL_VERSION                returns the full version string
%   PDPTOOL_VERSION('date')        returns the release date
%   PDPTOOL_VERSION('release')     returns the release number
%   PDPTOOL_VERSION('description') returns the release description
%   PDPTOOL_VERSION('support')     returns technical support information
if nargin ==0 
   info_string = 'PDPTool version 2.06 (April 2011), <a href="http://www.stanford.edu/group/pdplab/pdptool/Releasenotes.txt">Release notes</a>';
   return;
end
whatinfo = varargin{1};
switch whatinfo
       case 'date'
            info_string = 'Release Date: Apr 9,2011';
       case 'release'
            info_string = '2.06, <a href="http://www.stanford.edu/group/pdplab/pdptool/Releasenotes.txt">Release notes</a>';
       case 'description'
            info_string = sprintf(['User guide and handbook available online <a href="http://www.stanford.edu/group/pdplab/pdphandbook")>here</a>',...
                                   '\n<a href="http://www.stanford.edu/group/pdplab/pdptool/Releasenotes.txt">Release notes</a> provide additional information on changes or fixes.']);
       case 'support'
            info_string = 'Have a question/bug to report ? Write to <a href="mailto:sjohn@cnbc.cmu.edu?cc=sindyjohn@gmail.com,mcclelland@stanford.edu">Sindy John</a> ';            
       case 'about'
             info_string.fullname = 'PDPTool version 2.06 (Apr 2011)';
             info_string.reldate  = 'Release Date: Apr 9,2011';
             info_string.description = 'User guide and handbook available online ';
             info_string.hrefdesc = 'here';
             info_string.handbooklink = 'http://www.stanford.edu/group/pdplab/pdphandbook';
             info_string.support ='Have a question/bug to report ? Write to ';
             info_string.hrefsupport = 'Sindy John';
             info_string.mailtolink = 'mailto:sjohn@cnbc.cmu.edu?cc=sindyjohn@gmail.com,mcclelland@stanford.edu';
                                    
       otherwise
        info_string = 'PDPTool version 2.06 (Apr 2011)';           
end
