function writematfiles
global outputlog binarylog net;
for i=1:numel(binarylog)
    index = binarylog(i).logind;
    if index < 1
       continue;
    end
    lfile = outputlog(index).file;
    x=outputlog(index).cobjects;
    out = binarylog(i).output;
    if isempty(out)
       continue;
    end
%     if exist(lfile,'file')==2
%        z =load(lfile);
%     else
       c=cell(1,numel(x));
       z=cell2struct(c,x,2);
%     end
    for j=1:numel(x)
        cf =x{j};
        lind = 0;
        out{j}=squeeze(out{j});
        if isfield(z,cf) && numel(z.(cf))> 0
           if isvector(z.(cf))               
              lind = numel(z.(cf));
           else
              lind = size(z.(cf),ndims(z.(cf)));
           end
        end
        if isvector(out{j})
           aind = lind + numel(out{j});
           z.(cf)(lind+1:aind)= out{j}(1:end);
        else
           aind = lind + size(out{j},ndims(out{j}));
           if ndims(out{j}) == 2
              z.(cf)(:,lind+1:aind) = out{j}(:,1:end);
           else 
              z.(cf)(:,:,lind+1:aind) = out{j}(:,:,1:end);
           end
        end
    end
    if strcmpi(outputlog(index).logopts,'on')
      if strcmpi(outputlog(index).process,'train')
         z.trainopts = net.trainopts;
      else
         z.testopts = net.testopts;
      end
    end
    save(lfile,'-struct','z');
end
% clear global binarylog;