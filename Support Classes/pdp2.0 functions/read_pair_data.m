function [valid, data,errmsg] = read_pair_data(Filename)
global net srn;
valid=0;
data=[];
errmsg='';
srn = 0;
slocal =0;
ninputs = net.ninputs;
noutputs = net.noutputs;
nunits=ninputs + noutputs;
if numel(net.pools) <=1 %variable name change
   return;
end
fid = fopen (Filename,'r');
tline='';
while isempty(tline)
      tline = strtrim(fgetl(fid));
end
if strncmpi(tline,'seqlocal',8) 
   slocal = 1;
   srn = 1;
end
if strncmpi(tline,'seqpattern',10)
   srn = 1;
end
% grammarstr='';
if srn == 1 %strncmpi(tline,'seqlocal',8)
%    srn=1;
   if slocal == 1
       str = strtrim(tline(9:end));
       grammarstr = strsplit(' ',str);
       z = cellfun('isempty',grammarstr);
       grammarstr = grammarstr(~z);
       tline='';
       while isempty(tline)
             tline = strtrim(fgetl(fid));      
       end
       lineno=1;
       while tline ~= -1  
             [pname rem] = strtok(tline);
             patt_string = regexp(rem,'\S','match');  % get a list of non-white space entries of the line;
             ipattern = patt_string;
             sequence = struct('ipat',[],'tpat',[]);
             for seqn= 1: numel(patt_string)-1
                 sequence(seqn).ipat = double(ismember(grammarstr, patt_string(seqn)));
                 sequence(seqn).tpat = double(ismember(grammarstr, patt_string(seqn+1)));
                 data(lineno).ipattern = ipattern;
                 data(lineno).sequence = sequence;
             end
             data(lineno).pname = pname;
             tline = fgetl(fid);
             if ischar(tline) && isempty(strtrim(tline))
                continue;
             end
            lineno=lineno + 1;
       end   
   else
       tline='';
       while isempty(tline)
             tline = strtrim(fgetl(fid));      
       end
       lineno=1;
       while tline ~= -1 
             data(lineno).pname = strtrim(tline);
             try
                 tline = strtrim(fgetl(fid));
             catch
                 break;
             end
             seqn =0;
             sequence = struct('ipat',[],'tpat',[]);             
             while ~strncmpi(tline,'end',3)
                   pstring = strsplit(' ',tline);  
                   ind = ~cellfun(@isempty,pstring);
                   pstring = pstring(ind);
                   seqn =  seqn +1;
                   sequence(seqn).ipat = (str2num(char(pstring{1:ninputs})))';
                   sequence(seqn).tpat = (str2num(char(pstring{ninputs+1 : end})))';
                   try
                     tline = strtrim(fgetl(fid));
                   catch
                     break;
                   end                   
             end
             data(lineno).ipattern = ' ';
             data(lineno).sequence = sequence;             
             tline = fgetl(fid);
             if ischar(tline) && isempty(strtrim(tline))
                continue;
             end
            lineno=lineno + 1;
       end        
   end
else
    num = nunits;
    textreadcmd='[pname';
    for i=1:num
        textreadcmd= sprintf('%s,c%i',textreadcmd,i);
    end
    textreadcmd= sprintf('%s] = textread(''%s'' ,''',textreadcmd,Filename);
    textreadcmd = sprintf('%s%%s',textreadcmd);
    for i=1:num
        textreadcmd=sprintf('%s%%f',textreadcmd);
    end
    textreadcmd=sprintf('%s'',''emptyvalue'',1);',textreadcmd);
    try
        eval(textreadcmd)
    catch
        e = lasterror;
        errmsg = sprintf(' %s ,%s',Filename,e.message);
        return;
    end
    ip=c1;
    arraycatcmd= sprintf('ip =[ip ');
    for i=2:num
        arraycatcmd = sprintf('%s c%d',arraycatcmd,i);
    end
    arraycatcmd = sprintf('%s];',arraycatcmd);
    eval(arraycatcmd);
    for i=1:numel(pname)
        data(i).pname=pname{i};
        data(i).ipattern = ip(i,1:ninputs);
        data(i).tpattern = ip(i,ninputs+1:end);
    end
end
valid =1;