function bp_run_fast 
global net outputlog inpools outpools hidpools patn trpatn tstpatn;
global binarylog runparams PDPAppdata;
[options,data,cpno,N,cepoch, lepoch,patfield,orig_tss,orig_tce,appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
inpools = find(strcmpi( {net.pool.type},'input'));
hidpools = find(strcmpi({net.pool.type},'hidden'));
outpools = find(strcmpi({net.pool.type},'output'));
non_input = [hidpools outpools];
if runparams.alltest ==0
    clear_wed();
end
updatecount = 0;
lgraincount = 1;
runmode = 0;
if strncmpi(runparams.process,'train',length(runparams.process))
   runmode =1;
end
for iter = cepoch:lepoch
    abort = PDPAppdata.stopprocess; 
    if ~isempty(abort)
       PDPAppdata.stopprocess = [];         
       PDPAppdata.(appflag) = 1;           
       return;       
    end
    net.epochno = iter;
    net.tss = orig_tss;
    net.tce = orig_tce;    
    if iter > cepoch
       patn = getpatternrange(data,options);  %gets fresh pattern range for next epoch          
       if runmode
          trpatn = patn;
       else
          tstpatn = patn;
       end
    end    
    for p = cpno: N
        pno = patn(p);
        net.(patfield) = p;
        setpattern(pno,data,options);
        compute_output_error(net,net.pool,outpools,[outpools hidpools(end:-1:1)],runmode,1);  
        sumstats();
        if (options.lflag)
            compute_wed();
            if isequal(lower(options.lgrain(1)),'p')
               if isequal(options.lgrainsize,lgraincount)
                  if (options.follow)
                        bp_change_weights_follow_mx(net.pool,options,non_input);
                  else
                        bp_change_weights_mx(net.pool,options,non_input);
                  end
                      lgraincount = 1;                 
               else
                      lgraincount = lgraincount + 1;
               end
            end
        end 
        net.cycleno = 0;
    end
    cpno = 1;
    trpatn = [];
    tstpatn = [];
    if (options.lflag) && isequal(lower(options.lgrain(1)),'e')
        if isequal(options.lgrainsize,lgraincount)
           if (options.follow)
               bp_change_weights_follow_mx(net.pool,options,non_input);
           else
                bp_change_weights_mx(net.pool,options,non_input);           
           end
           lgraincount =1;
        else
            lgraincount = lgraincount+1;
        end
    end
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
       writeoutput(runparams.process,'epoch');
    end    
    if PDPAppdata.gui && strncmpi(runparams.granularity,'epoch',...
       length(runparams.granularity))
       updatecount = update_display_step(updatecount,patn(net.(patfield)));     
    end
    if (strcmpi(options.errmeas,'sse') && (net.tss < options.ecrit)) || (strcmpi(options.errmeas,'cee') && (net.tce < options.ecrit))
       disp('Error criterion reached');
       if PDPAppdata.gui
          update_display(patn(net.(patfield)));
       end
       net.(patfield) = 0;
       break;
    end
    orig_tss = 0.0;
    orig_tce = 0.0;    
end
if PDPAppdata.gui
   update_display(net.(patfield));
end
compute_output_error(-1);%this call to simply unlock file from memory
PDPAppdata.(appflag) = 0;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end

function [opts,dat,spat,npat,sepoch,lepoch,pfield,otss,otce,flag]= initialize_params
global net runparams patn trpatn tstpatn PDPAppdata rangelog cascadeinit
otss = 0.0;
otce = 0.0;
flag ='tstinterrupt_flag';
if PDPAppdata.gui && (~isequal(size(net.nancolor),[1 3]) || ...
   any(net.nancolor > 1) || any(net.nancolor < 0))
   fprintf(1,'net.nancolor has an invalid value,switching to default RGB triplet [0.97 0.97 0.97]\n');
   net.nancolor = [0.97 0.97 0.97];
end
if strcmpi(runparams.process,'test')
   opts = net.testopts;
   dat = PDPAppdata.testData;
   pfield = 'testpatno';
   if PDPAppdata.tstinterrupt_flag ~= 1
      tstpatn = getpatternrange(dat,opts);
   end
   patn = tstpatn;
   pnum = get(findobj('tag','tstpatlist'),'Value');      
   if strncmpi(runparams.mode,'run',length(runparams.mode))
      if (runparams.alltest) 
         net.testpatno = 1;
         npat = numel(dat);
         net.cycleno = 0;               
      else
         net.testpatno = find(patn==pnum,1);
         npat = net.testpatno;         
      end
   else
      if runparams.alltest
         if net.testopts.cascade
            if net.cycleno >= net.testopts.ncycles
               net.cycleno = 0;
            end
            if net.cycleno == 0 && (isempty(cascadeinit) || cascadeinit == 0 )
               net.testpatno = net.testpatno + 1;
               net.pss = 0;                            
            end             
         else
             net.testpatno = net.testpatno + 1;
             net.pss = 0;             
         end
         npat = numel(dat);
         if net.testpatno > npat
            net.testpatno = 1;
         end
         if net.testpatno == 1
            net.tss = 0;
            net.tce = 0;            
         end
         otss = net.tss;
         otce = net.tce;         
      else
         patselected = find(patn==pnum,1);
         if net.testopts.cascade
            if net.testpatno ~= patselected
               net.cycleno = 0;
               cascadeinit = 0;
            else
               if net.cycleno >= net.testopts.ncycles
                  net.cycleno = 0;
               end
            end
         end
         net.testpatno = patselected;         
         net.pss = 0;         
         npat = net.testpatno;
      end       
   end       
   if isempty(runparams.range)
      sepoch = net.epochno;
      lepoch = net.epochno;
      spat = net.testpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);        
   end
else    
   opts = net.trainopts;
   pfield = 'trainpatno';
   dat = PDPAppdata.trainData;
   npat = numel(dat);
   if PDPAppdata.trinterrupt_flag
      otss = net.tss; % in interrupt mode, start with current tss
      otce = net.tce;      
   else
      trpatn = getpatternrange(dat,opts);
   end
   patn = trpatn;
   flag = 'trinterrupt_flag';
   net.trainpatno = net.trainpatno + 1;
   if net.trainpatno > numel(dat)
      net.trainpatno = 1;
   end
   if net.trainpatno == 1
      net.epochno = net.epochno + 1;
      otss = 0.0;
      otce = 0.0;      
   end   
   if isempty(runparams.range)   
      sepoch = net.epochno;
      if isempty(runparams.nepochs)
         lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;          
      else
         net.trainpatno = 1;
         lepoch = sepoch + runparams.nepochs - 1;
      end
      spat = net.trainpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);
   end
end
rangelog = [spat sepoch];

function setpattern(pnum,patterns,procoptions)
global net inpools outpools;
pname = patterns(pnum).pname;
k=1;
for i=inpools
    nu = k+ net.pool(i).nunits-1;
    net.pool(i).activation(1:end) = 0+patterns(pnum).ipattern(k:nu);
    k = nu+1;
end
net.cpname = pname;
%set target
k=1;
for i=outpools
    nu = k+ net.pool(i).nunits-1;
    t = patterns(pnum).tpattern(k:nu);
    t(t==1) = procoptions.tmax;
    t(t==0) = 1 - procoptions.tmax;
    net.pool(i).target(1:end)=t;
    k = nu+1;
end


function retval = logistic(val)
retval = val;
retval(retval > 15.9357739741644) = 15.9357739741644; % returns 0.99999988;
retval(retval < -15.9357739741644) = -15.9357739741644; % returns 0.00000012
retval = 1.0 ./(1.0 + exp(-1 * retval));

function clear_wed()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
%         net.pool(i).proj(j).wed = repmat(0,size(net.pool(i).proj(j).wed));
        net.pool(i).proj(j).wed(:) = 0;    
    end
end


function sumstats()
global net outpools;
net.pss = 0.0;
net.pce = 0.0;
for i=1:numel(outpools)
    ind =outpools(i);
    t = net.pool(ind).error(net.pool(ind).target>=0);
    net.pss = net.pss + sum(t .* t);
    % for computing cross entropy error,use local variable 'act' to limit
    % values between 0.001 and 0.999, then logically index into 'act' for
    % getting logs of activation values for units that have target 1 or
    % 0.Sum of the logs evaluate to cross entropy error
    act = 0 + net.pool(ind).activation;
    act(act < 0.001) = 0.001;
    act(act > 0.999) = 0.999;
    ce = sum(log(act(net.pool(ind).target == 1))) + ...
         sum(log(1 - act(net.pool(ind).target == 0)));
    net.pce = net.pce - ce; % to make pce positive value;
end
net.tss = net.tss + net.pss;
net.tce = net.tce + net.pce;
 
function compute_wed()
global net outpools hidpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
%         from = net.pool(ind).proj(j).frompool;
        s = net.pool(ind).proj(j).fromindex; %strcmpi({net.pool.name},from);
        x= (net.pool(ind).delta' * net.pool(s).activation);
        net.pool(ind).proj(j).wed(1:end) = net.pool(ind).proj(j).wed + x ;...
%         (net.pool(ind).delta' * net.pool(s).activation);
    end
end

function change_weights(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
           lr = opts.lrate;
        end
        if (opts.wdecay)
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  - ... 
                                            opts.wdecay * net.pool(ind).proj(j).weight + ...
                                            opts.momentum * net.pool(ind).proj(j).dweight;
        else
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  + ... 
                                           opts.momentum * net.pool(ind).proj(j).dweight;
        end          
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
                                       net.pool(ind).proj(j).dweight;
        net.pool(ind).proj(j).wed = repmat(0,size(net.pool(ind).proj(j).wed));      
    end
end

function change_weights_follow(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
p_css = net.css;
net.css = 0.0;
dp =0;
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
           lr = opts.lrate;
        end        
        if (opts.wdecay)
            net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  - ... 
                                           opts.wdecay * net.pool(ind).proj(j).weight + ...
                                           opts.momentum * net.pool(ind).proj(j).dweight;
        else
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  + ... 
                                        opts.momentum * net.pool(ind).proj(j).dweight;
        end 
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
                                       net.pool(ind).proj(j).dweight;
        sqwed = net.pool(ind).proj(j).wed .^ 2;
        net.css = net.css + sum(sqwed(1:end));
        dpprod = net.pool(ind).proj(j).wed .*  net.pool(ind).proj(j).pwed;       
        dp = dp + sum(dpprod(1:end));
        net.pool(ind).proj(j).pwed = net.pool(ind).proj(j).wed;
        net.pool(ind).proj(j).wed = repmat(0,size(net.pool(ind).proj(j).wed));         
    end
end
den = p_css * net.css;
if den > 0.0
   net.gcor = dp/(sqrt(den));
else
   net.gcor =0.0;
end
constrain_weights();


function constrain_weights()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        w = net.pool(i).proj(j).weight; %just for readability
        switch lower(net.pool(i).proj(j).constraint_type)
            case 'prandom'
                 net.pool(i).proj(j).weight(w < 0) = 0.0;
            case 'nrandom'
                 net.pool(i).proj(j).weight(w > 0) = 0.0;
        end
    end
end

function currcount = update_display_step(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmp(runparams.mode,'step',length(runparams.mode))
       currcount = -1;
    end
end

function range = getpatternrange(patterns,procoptions)
N = numel(patterns);
switch procoptions.trainmode(1)
    case 's'  %strain
        range = 1:N;
    case 'p'  %ptrain
        range = randperm(N);
    case 'r' %rtrain
        range = ceil(N * rand(1,N));
    otherwise
        range =1: N;
end