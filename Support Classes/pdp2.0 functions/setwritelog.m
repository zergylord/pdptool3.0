function subsrefstmt = setwritelog(varlist)
subsrefstmt = cell(1,numel(varlist));
for i=1:numel(varlist)
    varitem = varlist{i};
    parts = strsplit('.' ,varitem);
    steval ='S=substruct(';
    for npart=1:numel(parts)
        sp = strsplit('(',parts{npart});
        steval=sprintf('%s''.'',''%s'',',steval,sp{1});        
        if numel(sp) >1
           t= sp{2}(1:end-1);
           steval=sprintf('%s''()'',{%s},',steval,t);           
        end
    end
    steval(end) =')';
    subsrefstmt{i} = steval;
end