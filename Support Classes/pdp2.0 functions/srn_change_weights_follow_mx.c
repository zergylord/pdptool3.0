#include "mex.h"
#include "matrix.h"
#include "string.h"
void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
           {
               mxArray *net_ptr,*pools,*proj,*opts;
               double *weight,*wed,*dweight,*plist,*pwed,global_lrate,proj_lrate;
               double p_css,*css,wt_decay,m,den,dp,*gcor;
               int np,npools,nj,wi,wrows,wcols,pnum;
               char *ctype;
               net_ptr = prhs[0];
               pools = mxGetField(net_ptr,0,"pool");
               opts = prhs[1] ;
               plist = mxGetData(prhs[2]);
               npools = mxGetNumberOfElements(prhs[2]);
               global_lrate = mxGetScalar(mxGetField(opts,0,"lrate"));
               wt_decay = mxGetScalar(mxGetField(opts,0,"wdecay"));  
               m = mxGetScalar(mxGetField(opts,0,"momentum"));  
               css = mxGetPr(mxGetField(net_ptr,0,"css"));
               gcor = mxGetPr(mxGetField(net_ptr,0,"gcor"));
               p_css = *css;
               *css = 0;
               dp = 0;
               for (np=0;np< npools; np++)
               {
                   pnum = (int)plist[np]-1;
                   proj = mxGetField(pools,pnum,"proj");
                   for (nj=0;nj < mxGetNumberOfElements(proj);nj++)
                   {   
                        ctype = mxArrayToString(mxGetField(proj,nj,"constraint_type"));
                        if (strcasecmp(ctype,"copyback"))
                        {                         
                            proj_lrate = mxGetScalar(mxGetField(proj,nj,"lrate"));
                            if (mxIsNaN(proj_lrate))
                            {
                               proj_lrate = global_lrate;
                            }
                            weight = mxGetPr(mxGetField(proj,nj,"weight"));
                            dweight = mxGetPr(mxGetField(proj,nj,"dweight"));
                            wed = mxGetPr(mxGetField(proj,nj,"wed"));
                            pwed = mxGetPr(mxGetField(proj,nj,"pwed"));
                            wrows = mxGetM(mxGetField(proj,nj,"weight"));
                            wcols = mxGetN(mxGetField(proj,nj,"weight"));
                            if (wt_decay && m)
                            {
                               for (wi=0;wi < wrows*wcols; wi++)
                               {
                                   dweight[wi] = (proj_lrate * wed[wi]) + (m * dweight[wi]) - wt_decay * weight[wi];
                                   weight[wi] += dweight[wi];
                                   *css += wed[wi]*wed[wi];
                                   dp += wed[wi] * pwed[wi];
                                   pwed[wi] = wed[wi];
                                   wed[wi] = 0.0;
                               }
                            }
                            else if (m)
                            {
                              for (wi=0;wi < wrows*wcols; wi++)
                              {
                                  dweight[wi] = (proj_lrate * wed[wi]) + (m * dweight[wi]);
                                  weight[wi] += dweight[wi];
                                   *css += wed[wi]*wed[wi];
                                   dp += wed[wi] * pwed[wi];
                                   pwed[wi] = wed[wi];
                                   wed[wi] = 0.0;
                              }
                            }
                            else if (wt_decay)
                            {
                              for (wi=0;wi < wrows*wcols; wi++)
                              {
                                  dweight[wi] = (proj_lrate * wed[wi]) - wt_decay * weight[wi];
                                  weight[wi] += dweight[wi];
                                   *css += wed[wi]*wed[wi];
                                   dp += wed[wi] * pwed[wi];
                                   pwed[wi] = wed[wi];
                                   wed[wi] = 0.0;
                              }
                            }
                            else
                            {
                              for (wi=0;wi < wrows*wcols; wi++)
                              {
                                  dweight[wi] = (proj_lrate * wed[wi]);
                                  weight[wi] += dweight[wi];
                                   *css += wed[wi]*wed[wi];
                                   dp += wed[wi] * pwed[wi];
                                   pwed[wi] = wed[wi];
                                   wed[wi] = 0.0;
                              }
                            } 
                           if (strcasecmp(ctype,"prandom")==0) {
                                for (wi=0;wi < wrows*wcols;wi++)
                                {
                                    if (weight[wi] < 0)
                                       weight[wi] = 0.0;
                                }
                            }
                           else if (strcasecmp(ctype,"nrandom")==0) {
                                 for (wi=0;wi < wrows*wcols;wi++)
                                 {
                                    if (weight[wi] > 0)
                                       weight[wi] = 0.0;
                                 }
                            }
                        }
                   mxFree(ctype);
                   }
                }
               den = p_css * (*css);
               if (den > 0)
                  *gcor = dp/sqrt(den);
               else
                  *gcor = 0.0;
}
