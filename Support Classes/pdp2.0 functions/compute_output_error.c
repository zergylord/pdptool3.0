#include "mex.h"
#include "matrix.h"
#include <string.h>
#include <math.h>

static int mark=0;
static double **activp, **errorp,**deltap,**targetp,**netinputp,**ninputp;
static int *nup;

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
           {
               mxArray *net_ptr,*pools,*opts,*proj;              
               double *olist,*blist,*target,*error,*activ,*weight;
               double *delta,*senderr,ttt,x;
               double *wp,*ap;
               int np,nj,npj,npools,n_olist,n_blist,pnum,numproj;
               int trainmode,wrows,wcols,wi,wj,senderidx,lock;
               char *errmeas,*ptype; 
               if (nrhs == 1)
                  lock = mxGetScalar(prhs[0]);
               else
                  lock = mxGetScalar(prhs[5]); 
               if (mexIsLocked())
               {
                   if (lock < 0) 
                   {
                      mexUnlock();
                      mark = 0;  
                      mxFree(activp);
                      mxFree(nup);
                      mxFree(errorp);
                      mxFree(deltap); 
                      mxFree(targetp);
                      mxFree(netinputp);
                      mxFree(ninputp);
                      return;
                   }
                       
               }
               else
               {
                  if (lock > 0) 
                      mexLock();
               }               
               net_ptr = prhs[0];
               pools = prhs[1];
               olist = mxGetData(prhs[2]);
               n_olist = mxGetNumberOfElements(prhs[2]);
               blist = mxGetData(prhs[3]);
               n_blist = mxGetNumberOfElements(prhs[3]);
               trainmode = mxGetScalar(prhs[4]);
               if (trainmode)
                  opts = mxGetField(net_ptr,0,"trainopts");               
               else
                  opts = mxGetField(net_ptr,0,"testopts");    
               errmeas = mxArrayToString(mxGetField(opts,0,"errmeas"));  
               npools = mxGetNumberOfElements(pools);
               if (mark == 0) /*setup begins here*/
               {
                   mark = 1;
                   activp = mxMalloc(npools * sizeof(double *));
                   errorp = mxMalloc(npools * sizeof(double *));               
                   deltap = mxMalloc(npools * sizeof(double *));               
                   targetp = mxMalloc(npools * sizeof(double *));                                
                   netinputp = mxMalloc(npools * sizeof(double *));                                                     
                   nup = mxMalloc(npools * sizeof(int));        
                   ninputp = mxMalloc(npools * sizeof(double *)); 
                   mexMakeMemoryPersistent(activp);
                   mexMakeMemoryPersistent(errorp);                   
                   mexMakeMemoryPersistent(deltap);               
                   mexMakeMemoryPersistent(targetp);   
                   mexMakeMemoryPersistent(netinputp);   
                   mexMakeMemoryPersistent(nup);                  
                   mexMakeMemoryPersistent(ninputp);
                   for (np =0;np < npools;np++)
                   {
                       activp[np] = mxGetPr(mxGetField(pools,np,"activation"));
                       errorp[np] = mxGetPr(mxGetField(pools,np,"error"));                   
                       deltap[np] = mxGetPr(mxGetField(pools,np,"delta"));
                       targetp[np] = mxGetPr(mxGetField(pools,np,"target"));
                       nup[np] = mxGetScalar(mxGetField(pools,np,"nunits"));   
                       netinputp[np] = mxGetPr(mxGetField(pools,np,"netinput"));
                       ninputp[np] = mxCalloc(nup[np],sizeof(double));                                          
                       mexMakeMemoryPersistent(activp[np]);
                       mexMakeMemoryPersistent(errorp[np]);
                       mexMakeMemoryPersistent(deltap[np]);      
                       mexMakeMemoryPersistent(targetp[np]);
                       mexMakeMemoryPersistent(netinputp[np]);
                       mexMakeMemoryPersistent(ninputp[np]);
                   }
               } /*setup ends */
               for (np=0;np<npools;np++)
               {
                   for (nj=0;nj<nup[np];nj++)
                       ninputp[np][nj] = errorp[np][nj] = 0.0;
               }
               /*compute output */
               for (np=n_blist-1;np>=0;np--)
               {
                   pnum = (int)blist[np]-1;
                   proj = mxGetField(pools,pnum,"proj");                       
                   numproj = mxGetNumberOfElements(proj);
                   for (npj=0;npj < numproj;npj++) {
                       senderidx = mxGetScalar(mxGetField(proj,npj,"fromindex"))-1; 
                       weight = mxGetPr(mxGetField(proj,npj,"weight"));
                       wrows = mxGetM(mxGetField(proj,npj,"weight"));
                       wcols = mxGetN(mxGetField(proj,npj,"weight"));
                       for (wi=0;wi < wrows;wi++) {    
                           ttt = 0;                           
                           for (wj=0,wp=weight+wi,ap=activp[senderidx];wj < wcols;wj++,wp+=wrows,ap++) {
                               ttt += (ap[0] * wp[0]); 
                           }
                           ninputp[pnum][wi] += ttt;
                       }                       
                   }
                   for (nj=0;nj < nup[pnum];nj++) {
                       x = netinputp[pnum][nj] = ninputp[pnum][nj];
			           if (x > 15.9357739741644)
			               x =  15.935773974164;
                  	   else if (x < -15.9357739741644)
			                    x =  -15.935773974164;
    	                activp[pnum][nj] = 1.0/(1.0 + exp(-1.0 * x));                       
                   }
               }
               /*compute error */
               for (np=0;np<n_olist;np++)
               {
                   pnum = (int)olist[np]-1;
                   for (nj=0;nj<nup[pnum];nj++)
                   {
                       errorp[pnum][nj] = (targetp[pnum][nj] >= 0) ? targetp[pnum][nj] - activp[pnum][nj] : 0.0;
                   }

               }
               for (np=0;np<n_blist;np++)
               {
                   pnum = (int)blist[np]-1;
                   if ((pnum <= n_olist) && (~strcasecmp(errmeas,"cee")))
                   {
                       for (nj=0;nj<nup[pnum];nj++)
                       {
                           deltap[pnum][nj] = errorp[pnum][nj];
                       }
                   }
                   else
                   {
                       for (nj=0;nj<nup[pnum];nj++)
                       {
                           deltap[pnum][nj] = errorp[pnum][nj]*activp[pnum][nj]*(1-activp[pnum][nj]);

                       } 
                    } 
                   proj = mxGetField(pools,pnum,"proj");                       
                   numproj = mxGetNumberOfElements(proj);
                   for (npj=0;npj < numproj;npj++) {
                       senderidx = mxGetScalar(mxGetField(proj,npj,"fromindex"))-1; 
                       ptype = mxArrayToString(mxGetField(pools,senderidx,"type"));    
                     if (strcasecmp(ptype,"input"))
                       { 
                          weight = mxGetPr(mxGetField(proj,npj,"weight"));
                          wrows = mxGetM(mxGetField(proj,npj,"weight"));
                          wcols = mxGetN(mxGetField(proj,npj,"weight"));
                          for (wj=0;wj < wcols;wj++) {
                              ttt = 0;
                              for (wi=0,ap=deltap[pnum],wp=weight+wrows*wj;wi < wrows;wi++,ap++,wp++) {
/*
                                   ttt += (deltap[pnum][wi] * weight[wi + wj*wrows]); 
*/
                                  ttt += ap[0]*wp[0];
                              }
                              errorp[senderidx][wj] += ttt;
                          }
                       }
                     mxFree(ptype);  
                   }
               } 
            mxFree(errmeas);               
            }
