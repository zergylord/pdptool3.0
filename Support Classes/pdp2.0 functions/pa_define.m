function definednet = pa_define(net)
% This function is called for setting up pattern associator networks.'n' is
% the network struct variable with valid fieldnames for 'pa'
% networks.'basepool' defines a valid pool structure. 'baseproj' defines
% valid projection structure.'trainopts' defines valid training
% parameters.'testopts' defines valid testing parameters. fillfields
% function populates these structure variables with values from the given
% input parameter 'net'.setpadefaultvars initializes the vectors and
% matrices with default values.
%
% Global variable PDPAppdata contains  runtime information for the
% current pdptool session. PDPAppdata.networks is a list of all currently 
% loaded networks (feature supported only for pdptool in gui mode).
% 'editable' variable can be used to specify user-modifiable network
% fields. These are then used by the object viewer module when invoked from
% pdptool mainwindow menu item 'Launch object viewer'.
% This is also used only when running pdptool in gui mode.
%
% **Important note for enhancement** : To add a new network type, you would
% need to define struct variables specific to the new type by creating a
% file like this with exactly similar function signature. The easiest method 
% would be copying the file content in its entirety into a new file, change
% function name to <newtype>define , save it as <newtype>define.m file. In
% the new file all you have to do is change fields of struct variables 
% 'n','basepool','baseproj','tropts','tstopts' - add/remove fieldnames
% according to your needs for the new type.Make corresponding changes to
% set<newtype>defaultvars subroutine. Type 'help struct' on matlab command
% prompt to get more information on syntax of MATLAB struct function.

global editable PDPAppdata;
networks = PDPAppdata.networks;
namedef = sprintf('net%d',numel(networks)+1);
n = struct('name',namedef,'type','pa','seedmode',1,'seed',0,'pool',[],...
          'num',1,'ninputs',0,'noutputs',0,'tss',0.0,'pss',0.0,...
          'vcor',0.0,'nvl',0.0','ndp',0.0,'epochno',0,'cpname','',...
          'trainpatno',0,'testpatno',0,'nancolor',[.97 .97 .97],...
          'templatefile','','outputfile','','logfile','','trainopts',[],...
          'testopts',[]);
basepool = struct('name','pool','type','Input','nunits',1,'unames',[],...
                 'proj',[],'input',[],'output',[],'target',[],'error',[],...
                 'netinput',[]);
baseproj = struct('frompool','bias','constraint_type','scalar',...
                  'constraint',0.0,'weight',[],'wed',[],...
                 'lrate',nan);
tropts = struct('nepochs',100,'trainmode','strain','lflag',1,'lrate',0.5,...
                'lrule','delta','ecrit',0.00,'noise',0.00,'actfunction','st',...
                'temp',1.0,'trainset','none');
tstopts = struct('nepochs',1,'trainmode','strain','lflag',0,'lrule','delta',...
                'lrate',0.5,'ecrit',0.00,'noise',0.00,'actfunction','st',...
                'temp',1,'testset','none');
n = fillfields(n,net);
net = bias_define(net);   
p(1:length(net.pool)) = basepool;
for i=1:numel(net.pool)
    p(i) = fillfields(p(i),net.pool(i));
    prj = baseproj;        
    if isfield(net.pool(i),'proj') && numel(net.pool(i).proj) > 0
       for j=1:numel(net.pool(i).proj)
           prj(j) = fillfields(baseproj,net.pool(i).proj(j));
       end
       p(i).proj = prj;            
    end
end
n.pool = p;
n.trainopts = tropts;
if isfield(net,'trainopts')
    n.trainopts = fillfields(n.trainopts,net.trainopts);  
end
n.testopts = tstopts;
if isfield(net,'testopts')
    n.testopts = fillfields(n.testopts,net.testopts);
end
if n.seedmode == 0       % 0 - seed provided, 1 - seed randomly
    if exist('RandStream','file') == 2 % for version 7.8 and above
        stream = RandStream('mt19937ar','Seed',n.seed);
        RandStream.setDefaultStream(stream);
    else
        rand('seed',n.seed); % for backward compatibility
        randn('seed',n.seed);                
    end
else
    if exist('RandStream','file') == 2 % for version 7.8 and above    
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        RandStream.setDefaultStream(stream);
        n.seed = stream.Seed;
    else
       n.seed = rand('seed');
       randn('seed',n.seed);
    end
end
editable.net = {'seed'};
editable.pool = {'input','output','target','error','netinput'};
editable.proj = {'constraint','weight','wed','lrate'};
editable.trainopts = fieldnames(rmfield(tropts,'trainset'));
editable.testopts = fieldnames(rmfield(tstopts,'testset'));

definednet = setpadefaultvars(n);


function net = setpadefaultvars(net)
types={net.pool.type};
inpools = find(strcmpi(types,'input'));
outpools = find(strcmpi(types,'output'));
net.inindx=inpools;
net.outindx = outpools;
if ~isempty(inpools)
    net.ninputs= sum(cell2mat({net.pool(inpools).nunits}));
end
if ~isempty(outpools)
    net.noutputs = sum(cell2mat({net.pool(outpools).nunits}));
end
 for i=1:numel(net.pool)
     vecsize = [1 net.pool(i).nunits];
     net.pool(i).input = repmat(0,vecsize);
     net.pool(i).output = repmat(0,vecsize);
     net.pool(i).error = repmat(0,vecsize);
     net.pool(i).target = repmat(0,vecsize);
     net.pool(i).netinput = repmat(0,vecsize);
    try
     np=0;
     if ~isempty(net.pool(i).proj)
         matx = net.pool(i).nunits;
         pools = {net.pool.name};
         for j=1:numel(net.pool(i).proj)
             from = net.pool(i).proj(j).frompool;
             [tf,np] = ismember(from,pools);
             maty = [net.pool(np).nunits];
             matsz = [matx maty];
             net.pool(i).proj(j).weight = repmat(1,matsz) .* ...
                                          net.pool(i).proj(j).constraint;
             net.pool(i).proj(j).wed = repmat(0,matsz);
          end
     end
    catch
        if (np==0)
           fprintf(1,'\nnet.pool(%d).proj(%d).frompool is not a valid pool name',i,j);
        else
           fprintf(1,'\n%s',lasterror);
        end
    end
 end
 net.pool(1).output=1;