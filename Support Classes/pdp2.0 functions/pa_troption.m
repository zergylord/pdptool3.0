function varargout = pa_troption(varargin)
% PA_TROPTION M-file for pa_troption.fig
% This is called when user clicks on
% (i)  'Set Training options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Train panel of the network viewer window.
% It presents training parameters for 'pa' type of network that can be 
% modified and saved.  


% Last Modified 03-May-2007 10:11:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pa_troption_OpeningFcn, ...
                   'gui_OutputFcn',  @pa_troption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pa_troption is made visible.
function pa_troption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pa_troption (see VARARGIN)

% The GUI opening function sets current/existing values of training 
% parameters to the corresponding GUI controls.
movegui('center');
setcurrentvalues;

% Choose default command line output for pa_troption
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% handles
% get(hObject,'children')
% UIWAIT makes pa_troption wait for user response (see UIRESUME)
% uiwait(handles.pa_troption);


% --- Outputs from this function are returned to the command line.
function varargout = pa_troption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function nepochsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function lrateedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function lrulepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ecritedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    
function noiseedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function actfnpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tempedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function trmodepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function troptnfilepop_CreateFcn(hObject, eventdata, handles)
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
trpatfile = trainpatternspopup_cfn(hObject);
setappdata(findobj('tag','pa_troption'),'patfiles',trpatfile);

function learnchk_Callback(hObject, eventdata, handles)
if (get(hObject,'Value') == 0)
    state = 'off';
else
    state = 'on';
end
set(handles.lrateedit,'enable',state);
set(handles.lrulepop,'enable',state);


function lrulepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function actfnpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function nepochsedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function lrateedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function ecritedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function tempedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function noiseedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function trmodepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function setcurrentvalues

% This sets the current training parameter values to the corresponding GUI
% controls. 
global net;
handles = guihandles;
tropts = net.train_options;
trmode =1;
set(handles.nepochsedit,'String',tropts.nepochs);
modeopts = cellstr(get(handles.trmodepop,'String'));
ind = find(strncmpi(tropts.trainmode,modeopts,length(tropts.trainmode)));
if ~isempty(ind)
    trmode = ind;
end
tropts.trainmode = modeopts{trmode};
set (handles.trmodepop,'Value',trmode);
set (handles.learnchk,'Value',tropts.lflag);
set (handles.lrateedit,'String',tropts.lrate);
lrule = 1;
rules = lower(get(handles.lrulepop,'String'));
ind = find(strncmpi(tropts.lrule,rules,length(tropts.lrule)));
if ~isempty(ind)
    lrule = ind;
end
tropts.lrule = rules{lrule};
set (handles.lrulepop,'Value',lrule);
set (handles.ecritedit,'String',tropts.ecrit);
set (handles.noiseedit,'String',tropts.noise);
set (handles.tempedit,'String',tropts.temp);
%actshort = {'st','li','cs','lt'};
%actlong = {'stochastic','linear','continuous sigmoid','linear threshold'};
%actfn =1;
%ind = find(strcmpi(actshort,tropts.actfunction));
%if isempty(ind)
%   ind = find(strncmpi(tropts.actfunction,actlong, length(tropts.actfunction)));
%   if ~isempty(ind)
%       actfn = ind(1);
%   end
%else
%   actfn = ind;
%end
%tropts.actfunction = actshort{actfn};    
%set (handles.actfnpop,'Value',actfn);
net.train_options = tropts;

function applybtn_Callback(hObject, eventdata, handles)

% Calls subroutine that sets network parameters.Does not exit from window.
apply_options();


function okbtn_Callback(hObject, eventdata, handles)
apply_options();
delete(handles.pa_troption);

function apply_options()

% This reads in the new parameters and applies changes to the network
% trainopts structure.
handles = guihandles;
global net PDPAppdata;
prevmode = [];
n = PDPAppdata.networks;
%if isfield(net,'train_options')
tropts = net.train_options;
prevmode = net.train_options.trainmode;    
tempopts = tropts;
%end
tropts.nepochs = str2double(get(handles.nepochsedit,'String'));
tmval = get(handles.trmodepop,'Value');
tmstr = cellstr(get(handles.trmodepop,'String'));
tropts.trainmode = tmstr{tmval}; %get(handles.trmodepop,'Value')-1;
tropts.lflag = get(handles.learnchk,'Value');
tropts.lrate = str2double(get(handles.lrateedit,'String'));
lval = get(handles.lrulepop,'Value');
lstr = cellstr(get(handles.lrulepop,'String'));
tropts.lrule = lower(lstr{lval});
tropts.ecrit = str2double(get(handles.ecritedit,'String'));
tropts.noise = str2double(get(handles.noiseedit,'String'));
afval = get(handles.actfnpop,'Value');
actshort = {'st','li','cs','lt'};
%tropts.actfunction = actshort{afval};
tropts.temp = str2double(get(handles.tempedit,'String'));

patlist= get(handles.troptnfilepop,'String');
pfiles = getappdata(handles.pa_troption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','train');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.troptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
    end
end
plistval = get(handles.troptnfilepop,'Value');
tropts.trainset  = patlist{plistval};

% -- Calls settrainopts command with only the modified fields of the 
%    trainopts structure. 
update_params = compare_struct(tropts,tempopts);
if ~isempty(update_params)
   args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
   args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
   settrainopts(args{1:end});
   if PDPAppdata.lognetwork
      trstmt = 'settrainopts (';
      for i=1:size(update_params,1)
          field_name = update_params{i,1};
          field_val = update_params{i,2};
          if ischar(field_val)
             trstmt = sprintf('%s''%s'',''%s'',',trstmt,field_name,field_val);
          else
             trstmt = sprintf('%s''%s'',%g,',trstmt,field_name,field_val);
          end
      end
      trstmt = sprintf('%s);',trstmt(1:end-1));
      updatelog(trstmt);
   end
end

% tempopts =orderfields(tempopts,tropts);
% fields = fieldnames(tempopts);
% tempA = struct2cell(tempopts);
% tempB = struct2cell(tropts);
% diffind = cellfun(@isequal,tempA,tempB);
% diffind = find(~diffind);
% if ~isempty(diffind)
%     trstmt = 'settrainopts (';
%     args = cell(1,numel(diffind)*2);
%     [args(1:2:end-1)] = fields(diffind);
%     for i = 1 : numel(diffind)
%         fval =  tropts.(fields{diffind(i)});
%         args{i*2} = fval;
%         if ischar(fval)
%            trstmt = sprintf('%s''%s'',''%s'',',trstmt,fields{diffind(i)},...
%                     fval);
%         else
%            trstmt = sprintf('%s''%s'',%g,',trstmt,fields{diffind(i)},fval);
%         end
%     end
%     settrainopts(args{1:end});
%     if PDPAppdata.lognetwork    
%        trstmt = sprintf('%s);',trstmt(1:end-1));
%        updatelog(trstmt);
%    end
% end
set(findobj('tag','trpatpop'),'String',patlist,'Value',plistval);

% -- Reset interrupt flag if figure module is invoked from within
%    train panel of network viewer and training mode has been altered.
netdisp = findobj('tag','netdisplay');
if ~isempty(netdisp)
    if ~isempty(prevmode) && ~strcmpi(prevmode,tropts.trainmode)
        PDPAppdata.trinterrupt_flag = 0;
    end
end
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.pa_troption,'patfiles',pfiles);

% -- All edited uicontrols that have KeypressFcn set to change background 
%    color to yellow is changed back to white.This indicates that modified
%    fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');

function cancelbtn_Callback(hObject, eventdata, handles)

% Exits without making any changes
delete(handles.pa_troption);



function pa_troption_CloseRequestFcn(hObject, eventdata, handles)

% Hint: delete(hObject) closes the figure
delete(hObject);

function troptnfilepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function troptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.troptnfilepop,handles.pa_troption);

function setwritebtn_Callback(hObject, eventdata, handles)
% Calls the module that presents options for writing network output logs.
% Once the setwriteopts window is up, this makes 'train' as the default 
% logging process.

% setwriteopts;
create_edit_log;
logproc = findobj('tag','logprocpop');
if ~isempty(logproc)
    set(logproc,'Value',1);
end
