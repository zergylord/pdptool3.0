function varargout = getannealschedule(varargin)
% GETANNEALSCHEDULE M-file for getannealschedule.fig
%      GETANNEALSCHEDULE, by itself, creates a new GETANNEALSCHEDULE or raises the existing
%      singleton*.
%
%      H = GETANNEALSCHEDULE returns the handle to a new GETANNEALSCHEDULE or the handle to
%      the existing singleton*.
%
%      GETANNEALSCHEDULE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GETANNEALSCHEDULE.M with the given input arguments.
%
%      GETANNEALSCHEDULE('Property','Value',...) creates a new GETANNEALSCHEDULE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before getannealschedule_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to getannealschedule_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help getannealschedule

% Last Modified by GUIDE v2.5 17-Jan-2007 19:04:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @getannealschedule_OpeningFcn, ...
                   'gui_OutputFcn',  @getannealschedule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before getannealschedule is made visible.
function getannealschedule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to getannealschedule (see VARARGIN)

% Choose default command line output for getannealschedule
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes getannealschedule wait for user response (see UIRESUME)
% uiwait(handles.getannealschedule);


% --- Outputs from this function are returned to the command line.
function varargout = getannealschedule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function inittempedit_Callback(hObject, eventdata, handles)
init_t = str2double(strtrim(get(hObject,'String')));
error='';
if isnan(init_t) 
   error ='Invalid init temperture specification';
end
if init_t < 0
    error='Temperatures must be positive';
end
if ~isempty(error)
    set(hObject,'BackgroundColor','red');
    set(handles.errormsgedit,'Visible','on','String',error);
    return;
else
    set(hObject,'BackgroundColor','white');
    set(handles.errormsgedit,'Visible','off');
end
set(handles.timeedit,'enable','on');
set(handles.tempedit,'enable','on');

function inittempedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function timeedit_Callback(hObject, eventdata, handles)


function timeedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
setappdata(hObject,'index',1);


function tempedit_Callback(hObject, eventdata, handles)


function tempedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function timelist_Callback(hObject, eventdata, handles)
val = get(hObject,'Value');
set(handles.templist,'Value',val);

function timelist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function templist_Callback(hObject, eventdata, handles)
val = get(hObject,'Value');
set(handles.timelist,'Value',val);

function templist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function donebtn_Callback(hObject, eventdata, handles)
global net;
timestr=get(handles.timelist,'String');
tempstr=get(handles.templist,'String');
anneal = repmat(0,numel(timestr)+1,2);
anneal(1,:) = [0 str2double(strtrim(get(handles.inittempedit,'String')))];
for i=1:numel(timestr)
    anneal(i+1,:)= [str2double(timestr{i}) str2double(tempstr{i})];
end
setappdata(findobj('tag','annealbtn'),'annealsched',anneal);
% net.testopts.anneal = anneal;
delete(handles.getannealschedule);

function viewbtn_Callback(hObject, eventdata, handles)
editstatus='off';
liststatus='on';
if get(hObject,'Value')==0
   editstatus='on';
   liststatus='off';
end
set(handles.timeedit,'Visible',editstatus);
set(handles.tempedit,'Visible',editstatus);
set(handles.addbtn,'Visible',editstatus);
set(handles.timelist,'Visible',liststatus);
set(handles.templist,'Visible',liststatus);
set(handles.editbtn,'Visible',liststatus);
set(handles.deletebtn,'Visible',liststatus);

function editbtn_Callback(hObject, eventdata, handles)
val = get(handles.templist,'Value');
tempstr = get(handles.templist,'String');
timestr = get(handles.timelist,'String');
set(handles.tempedit,'Visible','on','String',tempstr{val});
set(handles.timeedit,'Visible','on','String',timestr{val});
set(handles.templist,'Visible','off');
set(handles.timelist,'Visible','off');
set(handles.deletebtn,'Visible','off');
set(handles.addbtn,'Visible','on');
setappdata(handles.timeedit,'index',val);
set(hObject,'Visible','off');
uicontrol(handles.timeedit);


function addbtn_Callback(hObject, eventdata, handles)
timestr = get(handles.timelist,'String');
tempstr = get(handles.templist,'String');
temp = str2double(strtrim(get(handles.tempedit,'String')));
time = str2double(strtrim(get(handles.timeedit,'String')));
ind = getappdata(handles.timeedit,'index');
errormsg='';
if isnan(time)
   errormsg=sprintf('%s Time must be numeric and non-empty;',errormsg);
end
if (ind==1 && time <= 0) || (ind > 1 && time < str2double(timestr{ind-1}))   
    errormsg = sprintf('%s Time must be increasing;',errormsg);
end
if ~isempty(errormsg)
   set(handles.timeedit,'BackgroundColor','red');
end
if isnan(temp)
   errormsg=sprintf('%s Temperature must be numeric and non-empty\n',errormsg);
end
if temp < 0
   errormsg = sprintf('%s Temperature must be positive\n',erromsg);
end
if ~isempty(errormsg)
   set(handles.tempedit,'BackgroundColor','red');
   set(handles.errormsgedit,'String',errormsg,'Visible','on');
   return;
end
set(handles.errormsgedit,'String',' ','Visible','off');
listsize = numel(timestr);
tempstr{ind}= strtrim(get(handles.tempedit,'String'));
timestr{ind}=strtrim(get(handles.timeedit,'String'));
set(handles.timelist,'String',timestr);
set(handles.templist,'String',tempstr);
set(handles.timeedit,'String',' ','BackgroundColor','white');
set(handles.tempedit,'String',' ','BackgroundColor','white');
set(handles.viewbtn,'enable','on');
if ind < listsize
   setappdata(handles.timeedit,'index',listsize+1);
else
  setappdata(handles.timeedit,'index',ind+1);
end


function errormsgedit_Callback(hObject, eventdata, handles)


function errormsgedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end
set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));



function deletebtn_Callback(hObject, eventdata, handles)
val = get(handles.timelist,'Value');
tempstr= get(handles.templist,'String');
timestr= get(handles.timelist,'String');
tempstr(val)=[];
timestr(val)=[];
ind=getappdata(handles.timeedit,'index');
if ind > val
    ind = ind-1;
end
setappdata(handles.timeedit,'index',ind);
set(handles.timelist,'String',timestr,'Value',1);
set(handles.templist,'String',tempstr,'Value',1);



function inittempedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'backgroundColor','yellow');

