function setdisplayannotation(varargin)
global PDPAppdata;
annote_types ={'line','rectangle','arrow','dblarrow', 'ellipse'};
annote_obj = PDPAppdata.annoteobjects; %getappdata(pdp_handle,'annoteobjects');
vargs = regexprep(varargin,'''','''''');
astring = sprintf('annotation (%s);',vargs{1:end});
next = numel(annote_obj)+1;
annote_obj(next).astring = astring;
astringtype = regexp(astring,annote_types); 
whichtype = find(~cellfun(@isempty,astringtype));
% commasep = strtrim(strsplit(',',astring));
% switch whichtype
%     case {1,3} %line
%         p = find(strcmpi(commasep,'''X'''));
%         xstr =commasep{p+1};
%         numx = regexp(xstr,'[\d+]');
%         xp = str2num(xstr(numx(1):numx(end)));
%         annote_obj(next).X = xp;
%         p = find(strcmpi(commasep,'''Y'''));
%         ystr =commasep{p+1};
%         numy = regexp(ystr,'[\d+]');
%         yp = str2num(ystr(numy(1):numy(end)));
%         annote_obj(next).Y = yp;        
%     case {2,4} %rectangle
%         p = find(strcmpi(commasep,'''Position'''));
%         rstr = commasep{p+1};
%         numr = regexp(rstr,'[\d+]');
%         rp = str2num(rstr(numr(1):numr(end)));        
%         annote_obj(next).Position = rp;
%     otherwise
%         disp('Invalid annotation object');
%         
% end
annote_obj(next).type = whichtype;
PDPAppdata.annoteobjects = annote_obj;
% setappdata(pdp_handle,'annoteobjects',annote_obj);