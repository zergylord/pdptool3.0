function update_display(pattern_num)
global net;
netdisp = findobj('tag','netdisplay');
frames = getappdata(netdisp,'frames');
cmatind=0;
for i=1:numel(frames)
    src = frames(i).source;
    if ~isempty(src)  %src is emoty for labels
        [pnames obj_value] = getvalue(src);
        childuis = get(frames(i).fhandle,'UserData');
        rgbcolor = getrgbtriplet(obj_value(1:end),frames(i).vcslope);
        for j= 1:numel(childuis)
            c = childuis(j);
            ind = get(c,'UserData');  
            if frames(i).scalarind == 1 %if (strcmpi(get(c,'style'),'text'))
                if isnumeric(obj_value) 
                    if fix(obj_value) ~= obj_value
                        obj_value=sprintf('%.3f',obj_value); %split &&'d conditions because of error "Operands to the || and &&..."
                    end
                end
                fontsz=get(0,'DefaultUiControlFontSize');
                st =ceil(0.8 * fontsz * numel(obj_value));
                ps = get(c,'Position');
                if st > ps(3)
                   ps(3)=st;
                end
                set(c,'String',obj_value,'Position',ps);
                break;
            else
                tooltipstr = num2str(obj_value(ind));
                bcolor = rgbcolor(ind,:);
                value = sprintf('%.2f',obj_value(ind));
                if ~isempty(pnames)
                   tooltipstr=sprintf('%s,%s',pnames{ind},tooltipstr);
                end   
                set(c,'String',value,'tooltip',tooltipstr,...
                   'BackgroundColor',bcolor,'ForegroundColor',bcolor);                
%                 cmatind = cmatind+1;
%                 ccolor(cmatind).val = obj_value(ind);
%                 ccolor(cmatind).chandle = c;
%                 ccolor(cmatind).frameindex = i;                
%                 if ~isempty(pnames)
%                     ccolor(cmatind).pname=pnames{ind};
%                 else
%                     ccolor(cmatind).pname={};
%                 end
            end
        end
    end
end
% if cmatind > 0
%    map = get(netdisp,'Colormap');
%    cmin = -2;
%    cmax=2;
% %    [cmin cmax] = caxis;
%    mat=cell2mat({ccolor.val});
% %    mat(mat>=1)=0.999;
% %    mat(mat<=0)=0.001;
% %    colormat =pdf('Normal',mat,0,1);
%    cind=fix((mat - cmin)./(cmax-cmin) * length(map))+1;
%    cind(cind<1)=1;
%    cind(cind>length(map)) = length(map);
%    for k=1:numel(ccolor)
%        value = sprintf('%.2f',ccolor(k).val);
%        if isnan(cind(k))
%          bcolor = net.nancolor;
%        else
%           bcolor = map(cind(k),:);
%        end
%        tooltipstr = num2str(ccolor(k).val);
%        if ~isempty(ccolor(k).pname)
%            tooltipstr=sprintf('%s,%s',ccolor(k).pname,tooltipstr);
%        end
%         set(ccolor(k).chandle,'String',value,'tooltip',tooltipstr,...
%            'BackgroundColor',bcolor,'ForegroundColor',bcolor);
%     end
% end
if ~strcmpi(get(findobj('tag','countedit'),'enable'),'off')
   set(findobj('tag','curr_epochedit'),'String',net.epochno);
end
% uiwait(netdisp,0.05);
if pattern_num >0
%     try
%         pause(0.05);
% %         uiwait(netdisp,0.05);    
%     catch
%     end
    drawnow;
    ppanel =allchild(findobj('tag','patpanel'));
%    plist = findobj(allchild(findobj('tag','patpanel')),'Visible','on');
   plist = findobj(ppanel,'Visible','on','-and','style','listbox');   
   set(plist,'Value',pattern_num);
end
% uiresume;


function [names field] = getvalue(src)
names={};
global net;
structs = src{1};
indices = src{2};
fname = src{3};
f =  net;
for i= 1:numel(structs)
    if strcmpi(structs{i},'pool') && isfield(net.pool,'unames')
        names=net.pool(indices(i)).unames;
    else
        names={};
    end
    f=getfield(f,structs{i},{indices(i)});
end
field = f.(fname);