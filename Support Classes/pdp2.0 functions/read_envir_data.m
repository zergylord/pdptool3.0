function [valid, data, errmsg] = read_envir_data(Filename)
%READ_ENVIR_DATA validates the environment class file
global net;
valid=0;
data=[];
errmsg='';
srn = 0;
slocal =0;
ninputs = net.ninputs;
noutputs = net.noutputs;
nunits=ninputs + noutputs;
if numel(net.pool) <=1
   return;
end
fid = fopen (Filename,'r');
if fid==-1
    errmsg=sprintf('Environment class file %s not found.',Filename);
    return;
end
[pth Classname ext] = fileparts(Filename);
if ~strcmp(ext,'.m')
    errmsg='Pattern files for tdbp networks must be environment class with extention .m\n';
    return;
end
try
    eval(['envir=',Classname,'();']);
catch exception
    errmsg=sprintf('\nError in environment class constructor:\n%s\n',exception.message);
    return;
end    

% test battery to determine whether environment class functions exist
% getCurrentState()
try
    test = envir.getCurrentState();
catch exception
    if strcmp(exception.identifier,'MATLAB:noSuchMethodOrField')
        errmsg=sprintf('environment class ''%s'' does not define the required function ''getCurrentState()''\n',Filename);
        return;
    else
        errmsg=sprintf('error in environment class ''%s'': \n%s at line %i: %s\n',Filename,exception.stack(1,1).name,exception.stack(1,1).line,exception.message);
        return;
    end
end
if ~all(size(test)==[1,ninputs])
    errmsg=sprintf('output of environment class function ''getCurrentState()'' in class ''%s'' must be a horizontal vector of size %i\n',Classname,ninputs);
    return;
end

% getNextStates()
try
    test = envir.getNextStates();
catch exception
    if strcmp(exception.identifier,'MATLAB:noSuchMethodOrField')
        errmsg=sprintf('environment class ''%s'' does not define the required function ''getNextStates()''\n',Filename);
        return;
    else
        errmsg=sprintf('error in environment class ''%s'': \n%s at line %i: %s\n',Filename,exception.stack(1,1).name,exception.stack(1,1).line,exception.message);
        return;
    end
end

% getCurrentReward()
try
    test = envir.getCurrentReward();
catch exception
    if strcmp(exception.identifier,'MATLAB:noSuchMethodOrField')
        errmsg=sprintf('environment class ''%s'' does not define the required function ''getCurrentReward()''\n',Filename);
        return;
    else
        errmsg=sprintf('error in environment class ''%s'': \n%s at line %i: %s\n',Filename,exception.stack(1,1).name,exception.stack(1,1).line,exception.message);
        return;
    end
end
if ~all(size(test)==[1,noutputs])
    errmsg=sprintf('output of environment class function ''getCurrentReward()'' in class ''%s'' must be a horizontal vector of size %i\n',Classname, noutputs);
    return;
end

% isTerminal()
try
    test = envir.isTerminal();
catch exception
    if strcmp(exception.identifier,'MATLAB:noSuchMethodOrField')
        errmsg=sprintf('environment class ''%s'' does not define the required function ''isTerminal()''\n',Filename);
        return;
    else
        errmsg=sprintf('error in environment class ''%s'': \n%s at line %i: %s\n',Filename,exception.stack(1,1).name,exception.stack(1,1).line,exception.message);
        return;
    end
end

% setNextState()
try
    test = envir.setNextState();
catch exception
    if strcmp(exception.identifier,'MATLAB:noSuchMethodOrField')
        errmsg=sprintf('environment class ''%s'' does not define the required function ''setNextState()''\n',Filename);
        return;
    elseif strcmp(exception.identifier,'MATLAB:inputArgUndefined')  % this is ok since we did not pass an input argument. ignore.
    else
        errmsg=sprintf('error in environment class ''%s'': \n%s at line %i: %s\n',Filename,exception.stack(1,1).name,exception.stack(1,1).line,exception.message);
        return;
    end
end

% reset()
try
    test = envir.reset();
catch exception
    if strcmp(exception.identifier,'MATLAB:noSuchMethodOrField')
        errmsg=sprintf('environment class ''%s'' does not define the required function ''reset()''\n',Filename);
        return;
    else
        errmsg=sprintf('error in environment class ''%s'': \n%s at line %i: %s\n',Filename,exception.stack(1,1).name,exception.stack(1,1).line,exception.message);
        return;
    end
end
data = Classname;
valid = 1;
end

