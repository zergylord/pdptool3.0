function pa_reset
global net PDPAppdata;
net.tss = 0.0;
net.pss=0.0;
net.ndp =0.0;
net.vcor=0.0;
net.nvl=0.0;
net.cpname='';
net.trainpatno=0;
net.testpatno=0;
net.epochno=0;
if exist('RandStream','file') == 2 % for version 7.8 and above    
   stream = RandStream.getDefaultStream;
   reset(stream);
else
   rand('seed',net.seed);
   randn('seed',net.seed);
end
PDPAppdata.trinterrupt_flag = 0;
PDPAppdata.tstinterrupt_flag = 0;
 for i=1:numel(net.pool)
     vecsize = [1 net.pool(i).nunits];
     net.pool(i).input(:) = 0; %zeros(vecsize);
     net.pool(i).output(:) = 0; %zeros(vecsize);
     net.pool(i).error(:) = 0; %zeros(vecsize);
     net.pool(i).target(:) = 0; %zeros(vecsize);
     if ~isempty(net.pool(i).proj)
%          matx = net.pool(i).nunits;
%          pools = {net.pool.name};
         for j=1:numel(net.pool(i).proj)
%              from = net.pool(i).proj(j).frompool;
%              [tf,np] = ismember(from,pools);
%              maty = net.pool(np).nunits;
%              matsz = [matx maty];
             net.pool(i).proj(j).weight(:) = 0; %zeros(matsz);             
          end
     end    
 end
net.pool(1).output=1; 
if ~isempty(findobj('tag','netdisplay'))
   update_display(1);
end
if ~isempty(net.outputfile)
   resetlog;
end
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end
