function iac_run
global net outputlog freqrange logoutflag binarylog runparams PDPAppdata;
[ccycle ,lcycle,pnum, plimit] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails('test');
end
PDPAppdata.stopprocess = [];
updatecount = 0;
if runparams.pattest ~= 0
    for i = pnum:plimit
        if i == plimit && ~isempty(runparams.range)
           lcycle = runparams.range(2,1);
        end
        pname = setinput(i);
        if ~PDPAppdata.tstinterrupt_flag
            refresh_iac();
        end
        breakchk = cycle(ccycle,lcycle); 
        if breakchk && rem(net.cycleno,net.testopts.ncycles) ~= 0
           PDPAppdata.tstinterrupt_flag = 1;
           return;
        end
        net.patno = i;
        net.cpname = pname;
        ccycle = 1;
        lcycle = ccycle + net.testopts.ncycles - 1;  
        if logoutflag && ~isempty(strmatch('pattern',lower(freqrange)))
            writeoutput('test','pattern');
        end        
        if PDPAppdata.gui && strncmp(runparams.granularity,'pattern',...
           length(runparams.granularity))       
           updatecount = checkupdate(updatecount,i);
           if updatecount < 0 && i ~= plimit % if i==plimit, should check if binary output files need to be written
              return;
           end           
        end 
       PDPAppdata.tstinterrupt_flag = 0;
       if breakchk
          break;
       end        
    end    
else
   cycle(ccycle,lcycle);
end
if PDPAppdata.gui 
   updatecount = checkupdate(updatecount,net.patno);
end
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end

function [scyc, lcyc, spat, lpat] = initialize_params
global net runparams rangelog;
spat = 1;
lpat =1;
if isempty(runparams.range)
   net.cycleno = net.cycleno + 1;
   if runparams.pattest ~=0
       if runparams.alltest ==0
          spat = get(findobj('tag','tstpatlist'),'Value');
          lpat = spat;
       else
          pats = cellstr(get(findobj('tag','tstpatlist'),'String'));
          if numel(pats) > 0
             lpat = numel(cellstr(get(findobj('tag','tstpatlist'),'String'))); 
          end
          if net.cycleno > net.testopts.ncycles
             net.cycleno = 1;
          end
          if net.cycleno == 1
             net.patno = net.patno + 1;
          end
          if net.patno > lpat
             net.patno = 1;
          end
          spat = net.patno;
       end
   end
   scyc = net.cycleno;
%    lcyc = net.cycleno + (((fix(net.cycleno/net.testopts.ncycles)+1) *...
%             net.testopts.ncycles) - net.cycleno);
    lcyc = ceil(net.cycleno/net.testopts.ncycles) * net.testopts.ncycles;
    rangelog = scyc;
    if runparams.pattest
       rangelog(end+1) = spat;
    end
else
   scyc = runparams.range(1,1);
   lcyc = runparams.range(2,1);   
   if runparams.pattest ~=0
      spat = runparams.range(1,2);
      lpat = runparams.range(2,2);
      lcyc = net.testopts.ncycles;
   end
end   

function breakind = cycle (ccycle,lcycle)
global logoutflag freqrange runparams PDPAppdata;
updatecount = 0;
breakind = 0;
global net;
pno = 1;
if net.patno > 0
   pno = net.patno;
end
for iter = ccycle:lcycle
    abort = PDPAppdata.stopprocess;
    if ~isempty(abort)
        PDPAppdata.stopprocess = [];
        breakind = 1;
        break;
    end    
    net.cycleno = iter;
    getnet();
    update();
    if logoutflag && ~isempty(strmatch('cycle',lower(freqrange)))
       writeoutput('test','cycle');
    end    
    if PDPAppdata.gui && strncmp(runparams.granularity,'cycle',...
       length(runparams.granularity))
       updatecount = checkupdate(updatecount,pno);
       if updatecount < 0
          breakind = 1;
          break;
       end
    end 
end

function getnet()
global net;
% for i= 1:numel(net.pool)
%      vecsize = [1 net.pool(i).nunits];
%      net.pool(i).excitation = repmat(0,vecsize);
%      net.pool(i).inhibition = repmat(0,vecsize);
% end
pools = {net.pool.name};
for i = 2:numel(net.pool)
    vecsize = [1 net.pool(i).nunits];
    net.pool(i).excitation(:) = 0; %repmat(0,vecsize);
    net.pool(i).inhibition(:) = 0; %repmat(0,vecsize);    
    for j = 1:numel(net.pool(i).proj)
        from = net.pool(i).proj(j).frompool;
        pind = strmatch(from,pools);
        p = net.pool(pind);
        posact = find(p.activation>0);
        if ~isempty(posact)
           wt = net.pool(i).proj(j).weight;            
           for k=1:numel(posact)  %size(wt,1) will be nunits
               ind = posact(k);
               w = wt(:,ind)';
               net.pool(i).excitation(w>0.0)= net.pool(i).excitation(w>0.0)...
                                              + p.activation(ind) ...
                                              .* w(w>0.0);
               net.pool(i).inhibition(w<0.0)= net.pool(i).inhibition(w<0.0)...
                                              + p.activation(ind) ...
                                              .* w(w<0.0);
           end
        end
    end
    net.pool(i).excitation = net.pool(i).excitation * net.testopts.alpha;
    if (net.pool(i).noise)
        net.pool(i).excitation = net.pool(i).excitation +... 
                             random('Normal',0,net.pool(i).noise,vecsize);
    end    
    net.pool(i).inhibition = net.pool(i).inhibition * net.testopts.gamma;
    if strcmpi(net.testopts.actfunction(1:2),'gr') %grossberg's rule
       ext_x = find(net.pool(i).extinput > 0);
       ext_y = find(net.pool(i).extinput < 0);
       if ~isempty(ext_x)
          net.pool(i).excitation(ext_x)= net.pool(i).excitation(ext_x)+ ...
                                         net.testopts.estr * ...
                                         net.pool(i).extinput(ext_x);
       end
       if ~isempty(ext_y)
          net.pool(i).inhibition(ext_y)= net.pool(i).inhibition(ext_y) + ...
                                         net.testopts.estr * ...
                                         net.pool(i).extinput(ext_y);                                           
       end
    else
       net.pool(i).netinput = net.pool(i).excitation + ...
                              net.pool(i).inhibition + ...
                              net.testopts.estr * ...
                              net.pool(i).extinput;
    end    
end
                                       
                                           
function update()
global net;
for i=2: numel(net.pool)
    if strcmpi(net.testopts.actfunction(1:2),'gr') %grossberg's rule
       net.pool(i).activation = net.pool(i).activation + net.pool(i).excitation .* ...
               (net.testopts.max - net.pool(i).activation)...
                + net.pool(i).inhibition .* ...
               (net.pool(i).activation - net.testopts.min)...
                - net.testopts.decay*(net.pool(i).activation - net.testopts.rest);
    else
        z = find(net.pool(i).netinput > 0);
        if ~isempty(z)
             net.pool(i).activation(z)= net.pool(i).activation(z)...
                     + net.pool(i).netinput(z) .* ...
                        (net.testopts.max - net.pool(i).activation(z)) ...
                     - net.testopts.decay*...
                        (net.pool(i).activation(z) - net.testopts.rest);
        end
        y = find(net.pool(i).netinput <= 0);
        if ~isempty(y)
            net.pool(i).activation(y) = net.pool(i).activation(y) ...
                     + net.pool(i).netinput(y) .* ...
                        (net.pool(i).activation(y) - net.testopts.min) ...
                     - net.testopts.decay*...
                        (net.pool(i).activation(y) - net.testopts.rest);
        end
    end
    activ = net.pool(i).activation;
    net.pool(i).activation(activ > net.testopts.max) = net.testopts.max;
    net.pool(i).activation(activ < net.testopts.min) = net.testopts.min;
end
            

function currcount = checkupdate(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmp(runparams.mode,'step',length(runparams.mode))
       currcount = -1;
    end
end


function cpname = setinput(patnum)
global net PDPAppdata;
data = PDPAppdata.testData;
cpname = data(patnum).pname;
x=1;
p= find(strcmpi({net.pool.type},'input'));
for i=1:numel(p)
    z = net.pool(p(i)).nunits + x-1;
    net.pool(p(i)).extinput = data(patnum).ipattern(x:z);
    x = z+1;
end
