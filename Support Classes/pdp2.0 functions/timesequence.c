#include "mex.h"
#include "matrix.h"
#include <string.h>
#include <math.h>
#include <time.h>
#define LOG001 log(.001)

static int mark=0;
static double **activp,**acthistp,**extinpp,**dednetp,**sdEdnethistp;
static double **netinputp,**clampstatesp,**exttargp,**dedap,**ninputp;
static double **hastargetp,**targetp;
static int *nup;
void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
           {
               mxArray *net_ptr,*pools,*proj,*oproj,*opts;
               int npools,ticknum,nticks,numproj,receiveridx,senderidx;
               double dt,omdt,ome,*weight,errmargin,actd,t;
               int trainmode,np,nj,wrows,wcols,wi,wj,rows,index,lock,npj,bias,addextinp;
               char *errmeas,*ptype;
               int pticknum;
	           double *tacthistp,*tsdednetp,*biaswt;
	           double ttt,x,act,a,d;
	           double *ap,*wp;
               double *plist,*tss,*pss,*tce,*pce;
               double *etarg,*acth,err,ce;
	           int tenrows,tenwrows,wcsmiten,wrmiten;
               int pnum,nplist;
               if ((nrhs != 1) && (nrhs != 6))
               {
                  mexErrMsgTxt("Mex function timesequence called with incorrect number of arguments\n");
               }
               if (nrhs == 1)
                  lock = mxGetScalar(prhs[0]);
               else
                  lock = mxGetScalar(prhs[5]); 
               if (mexIsLocked())
               {
                   if (lock < 0) 
                   {
                      mexUnlock();
                      mark = 0;
                      mxFree(activp);
                      mxFree(acthistp);
                      mxFree(extinpp);
                      mxFree(dednetp);
                      mxFree(sdEdnethistp);
                      mxFree(nup);
                      mxFree(clampstatesp);
                      mxFree(hastargetp);
                      mxFree(dedap);
                      mxFree(netinputp);
                      mxFree(exttargp);
                      mxFree(targetp);                      
                      return;
                   }
               }
               else
               {
                  if (lock > 0)
                      mexLock();
               }
               net_ptr = prhs[0];
               pools = prhs[1];
               nticks = mxGetScalar(mxGetField(net_ptr,0,"nticks"));
               npools = mxGetNumberOfElements(pools);
               dt = mxGetScalar(prhs[2]);
               plist = mxGetData(prhs[3]);
               nplist = mxGetNumberOfElements(prhs[3]);               
               trainmode = mxGetScalar(prhs[4]);
               if (trainmode)
                  opts = mxGetField(net_ptr,0,"trainopts");               
               else
                  opts = mxGetField(net_ptr,0,"testopts");
               errmargin = mxGetScalar(mxGetField(opts,0,"errmargin"));
               errmeas = mxArrayToString(mxGetField(opts,0,"errmeas"));
   	           omdt = 1.0 - dt;
	           ome = 1 - errmargin;
               rows = nticks+1;
               tss = mxGetPr(mxGetField(net_ptr,0,"tss"));
               tce = mxGetPr(mxGetField(net_ptr,0,"tce"));
               pss = mxGetPr(mxGetField(net_ptr,0,"pss"));
               pce = mxGetPr(mxGetField(net_ptr,0,"pce"));
               if (mark == 0) /*setup begins here*/
               {
                   mark = 1;
                   activp = mxMalloc(npools * sizeof(double *));
                   acthistp = mxMalloc(npools * sizeof(double *));
                   extinpp = mxMalloc(npools * sizeof(double *));
                   dednetp = mxMalloc(npools * sizeof(double *));
                   sdEdnethistp = mxMalloc(npools * sizeof(double *));
                   netinputp = mxMalloc(npools * sizeof(double *));
                   nup = mxMalloc(npools * sizeof(int));                   
                   clampstatesp = mxMalloc(npools * sizeof(double *));                   
                   hastargetp = mxMalloc(npools * sizeof(double *));                   
                   exttargp = mxMalloc(npools * sizeof(double *));                       
                   dedap = mxMalloc(npools * sizeof(double *));
                   ninputp = mxMalloc(npools * sizeof(double *)); 
                   targetp = mxMalloc(npools * sizeof(double *));                    
                   mexMakeMemoryPersistent(activp);
                   mexMakeMemoryPersistent(acthistp);
                   mexMakeMemoryPersistent(extinpp);
                   mexMakeMemoryPersistent(dednetp);                   
                   mexMakeMemoryPersistent(sdEdnethistp);    
                   mexMakeMemoryPersistent(netinputp);
                   mexMakeMemoryPersistent(nup);
                   mexMakeMemoryPersistent(clampstatesp);                   
                   mexMakeMemoryPersistent(hastargetp);
                   mexMakeMemoryPersistent(exttargp);                                      
                   mexMakeMemoryPersistent(dedap);
                   mexMakeMemoryPersistent(ninputp);
                   mexMakeMemoryPersistent(targetp);                   
                   for (np =0;np < npools;np++)
                   {
                       activp[np] = mxGetPr(mxGetField(pools,np,"activation"));
                       acthistp[np] = mxGetPr(mxGetField(pools,np,"acthistory"));
                       extinpp[np] = mxGetPr(mxGetField(pools,np,"extinput"));         
                       dednetp[np] = mxGetPr(mxGetField(pools,np,"dEdnet"));         
                       sdEdnethistp[np] = mxGetPr(mxGetField(pools,np,"dEdnethistory"));    
                       netinputp[np] = mxGetPr(mxGetField(pools,np,"netinput"));
                       nup[np] = mxGetScalar(mxGetField(pools,np,"nunits"));
                       clampstatesp[np] = mxGetPr(mxGetField(pools,np,"clampstates"));                       
		               hastargetp[np] = mxGetPr(mxGetField(pools,np,"hastargets"));
                       exttargp[np] = mxGetPr(mxGetField(pools,np,"exttarget"));                                              
                       dedap[np] = mxGetPr(mxGetField(pools,np,"dEdA"));
                       targetp[np] = mxGetPr(mxGetField(pools,np,"target"));                       
                       ninputp[np] = mxCalloc(nup[np],sizeof(double));
                       mexMakeMemoryPersistent(activp[np]);
                       mexMakeMemoryPersistent(acthistp[np]);
                       mexMakeMemoryPersistent(extinpp[np]);
                       mexMakeMemoryPersistent(dednetp[np]);
                       mexMakeMemoryPersistent(sdEdnethistp[np]);
                       mexMakeMemoryPersistent(netinputp[np]);
                       mexMakeMemoryPersistent(clampstatesp[np]);   
                       mexMakeMemoryPersistent(exttargp[np]);             
                       mexMakeMemoryPersistent(dedap[np]);  
                       mexMakeMemoryPersistent(ninputp[np]); 
                       mexMakeMemoryPersistent(targetp[np]);                       
                   }
               } /*setup ends here*/
	       /*set initial state*/
               for (np =1;np < npools;np++) {
                   if (clampstatesp[np][0])  {
                       for (nj =0;nj< nup[np];nj++) {
                           act = acthistp[np][nj] = activp[np][nj] = extinpp[np][rows * nj];
                           if (act == 1) act = 0.99999988;
                           else if (act ==0) act = 0.00000012;
                           netinputp[np][nj] = log (act/(1-act));
                       }
                   }
                   else {
                       proj = mxGetField(pools,np,"proj");                       
                       numproj = mxGetNumberOfElements(proj);
                       bias = 0;
                       for (npj=0;npj < numproj;npj++) {
                           senderidx = mxGetScalar(mxGetField(proj,npj,"fromindex"));
                           if (senderidx == 1) {
                               biaswt = mxGetPr(mxGetField(proj,npj,"weight"));
                               bias = 1;
                               break;
                           }
                       }
                       ptype = mxArrayToString(mxGetField(pools,np,"type"));
                       addextinp =0;
                       if ((~strcasecmp(ptype,"input") || ~strcasecmp(ptype,"inout")) && !mxIsNaN(extinpp[np][0]))
                           addextinp = 1;
                       for (nj=0;nj < nup[np];nj++) {
                           netinputp[np][nj] = (bias) ? biaswt[nj] : 0.0;
                           if (addextinp) netinputp[np][nj] += extinpp[np][rows * nj];
                           x = netinputp[np][nj];
                           if (x > 15.9357739741644) x = 15.9357739741644;
                           else if (x < -15.9357739741644) x = -15.9357739741644;
                           acthistp[np][nj] = activp[np][nj] = 1.0 /(1.0 + exp(-1 * x));
                       }
                       mxFree(ptype);
                   }                 
               } /*end of setinitial state*/
               for (ticknum=1,pticknum=0;ticknum <=nticks;ticknum++,pticknum++) {
                   for (np=0;np < npools;np++)
                   {  
		               if (clampstatesp[np][ticknum])  {
		                  for (nj=0,index = ticknum * nup[np];nj <nup[np];nj++,index++) {
			                   act = activp[np][nj] = acthistp[np][index] = extinpp[np][ticknum + (rows * nj)];
                               if (act == 1) act = 0.99999988;
                               else if (act ==0) act = 0.00000012;
                               netinputp[np][nj] = log (act/(1-act));                               
		                  } 
		               }
                       else
                       {
                           if (!mxIsNaN(extinpp[np][ticknum])) {
                              for (nj=0;nj < nup[np];nj++) {
                                  ninputp[np][nj] = extinpp[np][ticknum + (rows *nj)];
                              }
                           }                           
                           else {
                              for (nj=0;nj < nup[np];nj++) {
                                  ninputp[np][nj] = 0.0;
                              }
                          }
                         proj = mxGetField(pools,np,"proj");                       
                         numproj = mxGetNumberOfElements(proj);
                         for (nj=0;nj<numproj;nj++) {
                             senderidx = mxGetScalar(mxGetField(proj,nj,"fromindex"))-1;
			                 tacthistp = acthistp[senderidx];
                             weight = mxGetPr(mxGetField(proj,nj,"weight"));
                             wrows = mxGetM(mxGetField(proj,nj,"weight"));
                             wcols = mxGetN(mxGetField(proj,nj,"weight"));
                             for (wi = 0;wi < wrows;wi++) {
	                             ttt = 0;
                 			     tenrows = 10*rows; tenwrows = 10*wrows; wcsmiten = wcols-10;
			                     for (wj = 0, ap = tacthistp+(pticknum*nup[senderidx]), wp = weight+wi;\
			     /*comment down to "unrolled-end" to bypass unrolling  */
                			         wj <= wcsmiten; wj+=10, ap += 10, wp +=tenwrows) { 
                                     ttt += ap[0]*wp[0]+ap[1]*wp[wrows]+ap[2]*wp[2*wrows]+ap[3]*wp[3*wrows]\
			                   	          +ap[4]*wp[4*wrows]+ap[5]*wp[5*wrows]+ap[6]*wp[6*wrows]\
                  				          +ap[7]*wp[7*wrows]+ap[8]*wp[8*wrows]+ap[9]*wp[9*wrows];                      
			                     }
			                    for (; /*unrolled-end*/
                  				    wj<wcols;wj++,ap++,wp+=wrows) {                 
			                        ttt += ap[0]*wp[0];
			                    }
              		    	    ninputp[np][wi]+=ttt;
			                 }
                         }
                   /*for (np=0;np < npools;np++) {
		               for (nj=0;nj <nup[np];nj++) {
		                    netinputp[np][nj] = dt * (ninputp[np][nj] - netinputp[np][nj]) + netinputp[np][nj];
		               }
		               if (clampstatesp[np][ticknum])  {
		                  for (nj=0,index = ticknum * nup[np];nj <nup[np];nj++,index++) {
			                   activp[np][nj] = acthistp[np][index] = extinpp[np][ticknum + (rows * nj)];
		                  } 
		               }
		               else {*/
		                 for (nj=0,index = ticknum * nup[np];nj <nup[np];nj++,index++) {
                              x = netinputp[np][nj] = dt * (ninputp[np][nj] - netinputp[np][nj]) + netinputp[np][nj];
			                  if (x > 15.9357739741644)
			                      x =  15.935773974164;
                  		      else if (x < -15.9357739741644)
			                          x =  -15.935773974164;
    	                      activp[np][nj] = acthistp[np][index] = 1.0/(1.0 + exp(-1.0 * x));
		                 }
		               }
                   }    
                   for (np=0;np <npools;np++) {
		               if (hastargetp[np][ticknum]) {
        		          for (nj=0,index = ticknum * nup[np];nj <nup[np];nj++,index++) {
			                  t = exttargp[np][ticknum + (rows * nj)];
                              a = acthistp[np][index];
			           /* if a is within the error margin, there is no diff;
			            otherwise, diff is t-a*/
                              if (errmargin > 0.0) {
                                 if      (t ==  1.0) d = ( (a > ome) ? 0 : (ome-a));
                                 else if (t ==  0.0) d = ( (a < errmargin) ? 0 : (errmargin-a));
                                 else if (t == -1.0) d = ( (a < -ome) ? 0 : (-ome-a));
                                 else d = t-a;
                              } 
                              else
                                 d = t-a;
                              if (~strcasecmp(errmeas,"cee")) /* returns 0 if errmeas == cee */
                                 dednetp[np][nj] = sdEdnethistp[np][index] = d * dt;
                              else {
                                 dednetp[np][nj] = sdEdnethistp[np][index] = d * dt * a * (1-a);
                              }
                           }
                       }
		               else { /* what to do if there is no target */
			                for (nj=0,index = ticknum * nup[np];nj <nup[np];nj++,index++) {
			                    dednetp[np][nj] = sdEdnethistp[np][index] = 0.0;
			               }
                       }
		           } /* end of second loop over pools */
	           }/* end of forward loop over ticks */      
      mxFree(errmeas);
      if (trainmode) {
	     for (ticknum=nticks,pticknum=nticks-1;ticknum >= 2;ticknum--,pticknum--) {
             for (np=0;np < npools;np++) {
                 for (nj=0;nj<nup[np];nj++)
                     dedap[np][nj] = 0;
                 if (clampstatesp[np][pticknum] == 0) {
                    oproj = mxGetField(pools,np,"oproj");
                    numproj = mxGetNumberOfElements(oproj);
                    for (nj=0;nj < numproj ;nj++) {
                        receiveridx = mxGetScalar(mxGetField(oproj,nj,"toindex"))-1;
                        tsdednetp = sdEdnethistp[receiveridx];
                        weight = mxGetPr(mxGetField(oproj,nj,"weight"));
                        wrows = mxGetM(mxGetField(oproj,nj,"weight"));
                        wcols = mxGetN(mxGetField(oproj,nj,"weight"));
                        for (wj = 0;wj < wcols;wj++) {
		                     ttt = 0;
             		         tenrows = rows*10; wrmiten=wrows-10;
                		     for (wi = 0, ap = tsdednetp+ticknum*nup[receiveridx],wp = weight+wrows*wj; \
			   /*comment down to "unrolled-end" to bypass unrolling*/
				                  wi<=wrmiten;wi+=10,ap += 10,wp+=10){  
                 			      ttt += ap[0]*wp[0]+ap[1]*wp[1]+ap[2]*wp[2]+ap[3]*wp[3]+ap[4]*wp[4]\
                        			     +ap[5]*wp[5]+ap[6]*wp[6]+ap[7]*wp[7]+ap[8]*wp[8]+ap[9]*wp[9];
			                 }
			                 for(;		\
			   /*unrolled end*/
			                    wi < wrows;wi++,ap++,wp++) {                     
                		        ttt += ap[0]*wp[0]; 
			                    }
			                 dedap[np][wj]+=ttt;                     
			            }
		            }
		         } /* closed clamstatesp if statement */
	         }
	         for (np=0;np < npools;np++) {
	             if (clampstatesp[np][pticknum] == 0) {
            		index = pticknum * nup[np];
		            for (nj=0;nj < nup[np];nj++) {
		                dednetp[np][nj] = sdEdnethistp[np][index] = sdEdnethistp[np][index]	\
              	        + (dt * acthistp[np][index] * (1.0 - acthistp[np][index]) * dedap[np][nj]) \
		                + (omdt * sdEdnethistp[np][ticknum*nup[np] + nj]);
		                index++;
		            }
	             }
	         }
	     }
	  }
/*       *pss = 0;
        *pce = 0; */
       for (np=0;np<nplist; np++)
       {
           pnum = (int)plist[np]-1;
           err = 0.0;
           ce = 0.0;
           for (nj=0,etarg = exttargp[pnum]+(rows-1);nj<nup[pnum];nj++,etarg += rows)
           {
               targetp[pnum][nj] = etarg[0];
           }           
           for (ticknum=0;ticknum <rows;ticknum++)
           {
               if (hastargetp[pnum][ticknum])
               {
                   for (nj=0,etarg = exttargp[pnum]+ticknum,acth = acthistp[pnum]+(ticknum*nup[pnum]);nj < nup[pnum];nj++,etarg +=rows,acth++)
                   {
                        d = etarg[0]-acth[0]; err += d*d; 
                        if (etarg[0]==1) {
                           if (acth[0] < 0.001) ce += LOG001;
                               else ce += log(acth[0]);
                        }
                       else if (etarg[0] ==0) { 
                                if (acth[0] > 0.999) ce += LOG001;
                                else ce += log(1.0-acth[0]);
                       }
                   }
              }
           }
           *pss += dt * err;
           *pce -= dt * ce;
       } 
       *tss += *pss;
       *tce += *pce;
}
