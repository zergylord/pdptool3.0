function read_update(set,proc)
switch proc
    case 'train'
        read_and_fill(set,'train','trpatlist');
    case 'test'
        read_and_fill(set,'test','tstpatlist');
    case 'both',
        read_and_fill(set,'train','trpatlist');
        read_and_fill(set,'test','tstpatlist');
end

function read_and_fill(tset,procname,patlistname)
global net PDPAppdata;
switch net.type
    case {'pa','bp'}
        [valid ,data, errmsg] = read_pair_data(tset);
    case 'srbp'
        [valid ,data, errmsg] = read_pair_srbpdata(tset);        
    case 'rbptt'
        [valid ,data, errmsg] = read_pair_rbpttdata(tset);                
    case {'cs','iac','cl'}
        [valid ,data, errmsg] = read_nonpair_data(tset);
end
panelname=[procname,'panel'];
if (valid ==1)
    appdata=[procname,'Data'];
    patlist=findobj('tag',patlistname);
    PDPAppdata.(appdata) = data;
    set(findobj(findobj('tag',panelname),'style','pushbutton'),'enable',...
        'on');
    fill_patterns(patlist,data);
else
    fprintf(1,errmsg);
    set(findobj(findobj('tag',panelname),'style','pushbutton'),'enable',...
        'off');    
end