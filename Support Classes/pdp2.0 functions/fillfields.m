function retstruct = fillfields(defstruct,givenstruct)
%defstruct is the default struct variable defined in <nettype>define
%function. givenstruct can be a user-defined struct variable loaded from 
%the network script file. This function iterates through each valid field of
%defstruct and assigns it a user-specified value , if givenstruct contains
%the same field. This way, any invalid user-specifid field is silently
%ignored.The return value ,'retstruct' thus contains all fields of default
%struct, having default values for those that are not user-specified.

allf = fieldnames(defstruct);
for i=1:numel(allf)
    if isfield(givenstruct,allf{i}) && ~isstruct(givenstruct.(allf{i})) ...
       && ~isempty(givenstruct.(allf{i}))
       defstruct.(allf{i}) = givenstruct.(allf{i});
    end
end
retstruct = defstruct;