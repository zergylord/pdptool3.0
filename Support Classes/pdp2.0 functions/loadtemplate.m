function loadtemplate(varargin)
if nargin < 1
   error('File name not specified');
end
if nargin > 1
   if ~strncmpi('file',varargin{1},length(varargin{1}))
       fprintf(1,'ERROR: First argument must be property name ''file''\n');
       return;
   end
   filename = varargin{2};
else
   filename = varargin{1};
end
global net PDPAppdata;
PDPAppdata.dispobjects = [];
PDPAppdata.annoteobjects = [];
n= PDPAppdata.networks;
[fid,fopenmesg] = fopen (filename,'rt');
if fid < 0
   fprintf(1,'ERROR: %s - %s\n',filename,fopenmesg);
   return;
end
prefix='setdisplay';
while (feof(fid) ~= 1)
       line = strtrim(fgetl(fid));
       line = removecomments(line);
       if isempty(line)
           continue;
       end
       ind = regexp(line,'\s|(');    
       firstword = line(1:ind-1);
       remword = firstword;
       lookup = findstr(firstword,prefix);
       if numel(lookup) >0 &&  lookup(1)== 1 && (numel(firstword) > numel(prefix))
          remword = firstword(numel(prefix)+1 : end);
       end
       xx = findstr(line,remword);
       switch remword
              case {'object','annotation'}
                   valid=1;
                   line = insertquotes(line(xx(1):end));
              case {'template','cellsize'}
                   valid = 1;
                   line =line(xx(1):end);
              otherwise
                   valid =0;
       end
       if valid==1
          displine = sprintf('%s%s',prefix,line);
          eval(displine);
       end           
end
net.templatefile = filename;
if isfield(net,'num')
   n{net.num} = net;
end
PDPAppdata.networks = n;

function filtered = removecomments(tline)
filtered=tline;
comment = regexp(tline,'%'); 
if ~isempty(comment)
     filtered=tline(1:(comment(1)-1));  %remove comments in line
end

function line =insertquotes(statemnt)
par_sind = regexp(statemnt,'(','start');
par_eind = regexp(statemnt,')','end');
com_word = statemnt(1:par_sind(1)-1);
line = statemnt(par_sind(1)+1 : par_eind(end)-1);
line = sprintf('%s (\''%s'');',com_word,line);