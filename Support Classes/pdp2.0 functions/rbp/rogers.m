% In nogui mode, this script can be run by typing 'pdptool nogui rogers.m'
% on the matlab command line.
pdp;
loadscript('rogers.net');
loadtemplate ('rogers.tem')
loadpattern ('file','features.pat','usefor','both');
settrainopts ('lrate',0.001,'momentum',0,'nepochs',500,'trainmode','ptrain');
% Rogers et al report lrate of .005 but this simulator does not converge
% with that value.
% As in Rogers et al, wdecay is set to x/npatterns (npatterns = 144) x = .001
settrainopts ('wdecay',.0000069444,'wrange',0.25,'lgrain','pattern','errmargin',0.05);
% Wrange and errmarin values were not specified in Rogers et al.
% That paper did not specifically indicate that cross-entropy error was
% used, but this runs best using cross entropy error.  
% Set to 'sse' to see what happens if you used sum squared error.
settrainopts('errmeas','cee','fastrun',1);
%uncomment next two lines to create a log of the train process by epoch
% filename = getfilename('RogersNetLog','.out');
% setoutputlog ('file', filename,'process', 'train','frequency', 'epoch','status','on','writemode','text','onreset','new','objects', {'epochno','tss','tce'});
setseed(12345);
launchnet;
rbp_reset;