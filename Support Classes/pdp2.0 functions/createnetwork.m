function varargout = createnetwork(varargin)
% CREATENETWORK M-file for createnetwork.fig
%      CREATENETWORK, by itself, creates a new CREATENETWORK or raises the existing
%      singleton*.
%
%      H = CREATENETWORK returns the handle to a new CREATENETWORK or the handle to
%      the existing singleton*.
%
%      CREATENETWORK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATENETWORK.M with the given input arguments.
%
%      CREATENETWORK('Property','Value',...) creates a new CREATENETWORK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before createnetwork_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to createnetwork_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help createnetwork

% Last Modified by GUIDE v2.5 28-Sep-2010 16:45:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @createnetwork_OpeningFcn, ...
                   'gui_OutputFcn',  @createnetwork_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before createnetwork is made visible.
function createnetwork_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to createnetwork (see VARARGIN)

% This opening function creates toolbar buttons,resizes window to make
% script panel invisible and creates default UI parameters for existing
% network types. 

% Choose default command line output for createnetwork
handles.output = hObject;
figch = get(hObject,'children');
ht= uitoolbar(hObject,'Tag','toolmenu','Visible','off');
fname = 'cmapout.bmp';
X = imread(fname);
fname1 = 'cmapin.bmp';
X1 = imread(fname1); %X1 is saved as UserData for presenting the second picture when button is toggled
uitoggletool(ht,'tag','scripttoggle','CData',X,...
             'TooltipString','View Script',...
             'UserData',X1,'OnCallback',{@oncallback_toggletool},...
             'OffCallback',{@offcallback_toggletool});
fname = 'save.bmp';
X = imread(fname,'bmp');
uipushtool(ht,'CData',X,'TooltipString','Save Network',...
           'tag','savetool','separator','on',...
           'ClickedCallback',{@savenetwork});
fname = 'StopControl.bmp';
X = imread(fname,'bmp');
uipushtool(ht,'CData',X,'TooltipString','Abort network creation',...
          'tag','aborttool','separator','on',...
          'ClickedCallback',{@abortnetwork});
      
mainfigpos = get(hObject,'Position');
% set(hObject,'Resize','on');
scriptpnlpos = get(handles.scriptpanel,'Position');
mainfigpos(2)= mainfigpos(2) +  scriptpnlpos(4);
mainfigpos(4)= mainfigpos(4) -  scriptpnlpos(4);
cobjs = figch(figch ~= handles.scriptpanel);
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1) x(2) - scriptpnlpos(4) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,{'UserData'},cpos);
set(hObject,'Position',mainfigpos,'UserData',mainfigpos);

types = {'pa','iac','cs','bp','srn','rbp','cl','tdbp'};  % add to this list if new network type is added
setappdata(hObject,'nettypes',types);
panelcntrls = findobj(handles.definepnl,'-property','enable'); % not all objects have 'enable' property
set(panelcntrls,'enable','off');
handles.toolmenu = ht;
%creates 'uiparams' that contains all default uicontrol objects.When
%network type is selected subsequently, network specific uicontrol objects 
%are added to uiparams.
setdefaultuiparams; 

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes createnetwork wait for user response (see UIRESUME)
% uiwait(handles.createnetwork);


% --- Outputs from this function are returned to the command line.
function varargout = createnetwork_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function createnetwork_ResizeFcn(hObject, eventdata)
handles = guihandles;
if strcmpi(get(handles.scriptpanel,'Visible'),'off')
   return;
end
mainfigpos = get(hObject,'Position');
orig_figpos = get(hObject,'UserData');
if isequal(mainfigpos,orig_figpos)
   return;
end
figch = get(handles.createnetwork,'children');
scriptpnlpos = get(handles.scriptpanel,'Position');
nonctrlobjs = findobj(handles.createnetwork,'type','uitoolbar','-or',...
              'type','uicontextmenu');
cobjs = figch(figch ~= handles.scriptpanel);
if ~isempty(nonctrlobjs)
   cobjs = cobjs(~ismember(cobjs,nonctrlobjs));
end
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1)+ orig_figpos(3) - mainfigpos(3) x(2)-(orig_figpos(4) - mainfigpos(4)) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,{'UserData'},cposnew);
scriptpnlpos(1) = 3;
scriptpnlpos(2) = 0.615;
scriptpnlpos(3) = scriptpnlpos(3) + mainfigpos(3) - orig_figpos(3);
scriptpnlpos(4) = scriptpnlpos(4) + mainfigpos(4) - orig_figpos(4);
scripteditpos = get(handles.scriptedit,'Position'); %normalized units
scriptbtnpos = get(handles.scriptclosebtn,'Position'); %normalized units
scriptbtnpos(1) = scripteditpos(1) + scripteditpos(3)-0.02;
scriptbtnpos(2) = scripteditpos(2)+ scripteditpos(4);
scriptbtnpos(3) = 5/scriptpnlpos(3);
scriptbtnpos(4) = 2/scriptpnlpos(4);
set(handles.scriptclosebtn,'Position',scriptbtnpos);

set(handles.scriptpanel,'Position',scriptpnlpos);
set(handles.createnetwork,'UserData',mainfigpos);


function oncallback_toggletool(hObject,eventdata)
% This is called when view script toggle button is clicked when script 
% panel is in 'collapsed mode'. Makes the script panel visible and  
% recalibrates position of each child object of the figure except 
% scriptpanel and the toolbar object(does not have 'Position' property).
handles = guihandles;
refreshscript(handles);
mainfigpos = get(handles.createnetwork,'Position');
spanel = handles.scriptpanel;
set(spanel,'visible','on');
X = get(hObject,'UserData');
Y = get(hObject,'CData');
set(hObject,'TooltipString','Hide Script','CData',X,'UserData',Y);
spanelpos = get(spanel,'Position');
mainfigpos(2)= mainfigpos(2) - spanelpos(4);
mainfigpos(4)= mainfigpos(4) + spanelpos(4);
figch = get(handles.createnetwork,'children');
nonctrlobjs = findobj(handles.createnetwork,'type','uitoolbar','-or',...
              'type','uicontextmenu');
cobjs = figch(figch ~= spanel);
if ~isempty(nonctrlobjs)
   cobjs = cobjs(~ismember(cobjs,nonctrlobjs));
end
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1) x(2) + spanelpos(4) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,{'UserData'},cpos);
set(handles.createnetwork,'Position',mainfigpos,'UserData',mainfigpos);
set(handles.createnetwork,'Resize','on',...
    'ResizeFcn',@createnetwork_ResizeFcn);

function offcallback_toggletool(hObject,eventdata)
% This is called when view script toggle button is clicked when script 
% panel is in 'expanded mode'. Makes the script panel visible and  
% recalibrates position of each child object of the figure except 
% scriptpanel and the toolbar object(does not have 'Position' property.
handles = guihandles;
mainfigpos = get(gcbf,'Position');
set(gcbf,'Resize','off','ResizeFcn','');
spanel = handles.scriptpanel;
set(spanel,'visible','off');
X = get(hObject,'UserData');
Y = get(hObject,'CData');
set(hObject,'TooltipString','View Script','CData',X,'UserData',Y);
spanelpos = get(spanel,'Position');
mainfigpos(2)= mainfigpos(2) + spanelpos(4);
mainfigpos(4)= mainfigpos(4) - spanelpos(4);
figch = get(gcbf,'children');
nonctrlobjs = findobj(handles.createnetwork,'type','uitoolbar','-or',...
              'type','uicontextmenu');
cobjs = figch(figch ~= spanel);% & (figch ~= handles.toolmenu) & ...
        %(figch ~= handles.poolcontextmenu) & (figch ~= handles.poolcontextmenu1));
if ~isempty(nonctrlobjs)
   cobjs = cobjs(~ismember(cobjs,nonctrlobjs));
end
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1) x(2) - spanelpos(4) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,{'UserData'},cpos);
set(gcbf,'Position',mainfigpos);


% --- Executes on selection change in nettypepop.
function nettypepop_Callback(hObject, eventdata, handles)
% hObject    handle to nettypepop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 
% Hints: contents = get(hObject,'String') returns nettypepop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from nettypepop

% This is called when a new network type is selected from the pop-up menu.
% The order of network type names in the 'String' property of nettypepop
% (view fig using guide) and the Appdata 'nettypes' must be the same. After 
% a net type is selected, parameters specific to that type is set. The pop-up
% menu is therefore set to invisible mode on this callback,so no further 
% selection is possible unless user wants to start all over again by 
% clicking on 'Select new' button.
handles = guihandles;
val = get(hObject,'Value');
type = get(hObject,'String');
typepos = get(hObject,'Position');
set(hObject,'visible','off');
set(handles.nettypetxt,'Position',typepos,'visible','on','String',type{val});
set(handles.selectnewbtn,'visible','on');
set(handles.toolmenu,'visible','on');
panelcntrls = findobj(handles.definepnl,'-property','enable'); % not all objects have 'enable' property
set(panelcntrls,'enable','on');
nettypes = getappdata(handles.createnetwork,'nettypes');
netinfo.name = get(handles.netnameedit,'String');
netinfo.type = nettypes{val};
poolinfo(1).name = 'bias';
poolinfo(1).type = 'bias';
poolinfo(1).nunits = 1;
setappdata(handles.createnetwork,'netinfo',netinfo);  % net structure to copy into script edit box
setappdata(handles.createnetwork,'poolinfo',poolinfo); % pool structure to copy into script edit box
switch nettypes{val}
    case 'pa'
          pooltypes = {'input','output'};
          set_pa_params;
    case 'iac'
          pooltypes = {'input','hidden'};
          set_iac_params;
    case 'cs'
          pooltypes = {'input'}; 
          set_cs_params;
    case 'bp'
          pooltypes = {'input','hidden','output'};
          set_bp_params;   
    case 'srn'
          pooltypes = {'input','hidden','output'};
          set_srn_params;     
    case 'rbp'
          pooltypes = {'input','hidden','output','inout'};
          set_rbp_params;
    case 'cl'
          pooltypes = {'input','hidden'};
          set_cl_params;
    case 'tdbp'
          pooltypes = {'input','hidden','output'};
          set_tdbp_params;   
    otherwise
          error(['Invalid network type : Please check that ''String'' ',...
                'property of nettype popup\n and Appdata ''nettypes'' have ',...
                'matching list of types']);
          
end
uiparams = getappdata(handles.createnetwork,'uiparams');
uiparams.poollevel(4).properties{4} = pooltypes;  %'String' property value of pool type pop up.
setappdata(handles.createnetwork,'uiparams',uiparams);
drawuicontrols(uiparams.netlevel);

% --- Executes during object creation, after setting all properties.
function nettypepop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nettypepop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectnewbtn.
function selectnewbtn_Callback(hObject, eventdata, handles)
% hObject    handle to selectnewbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This is called when the 'Select new' button is clicked. This aborts the
% current network creation process ,prompts user and if response is 'yes',
% begins all over again.
qstring = sprintf('This will restart the network creation process.\nDo you want to continue?');
buttons = questdlg(qstring);
if strcmp(buttons,'Yes')
    set(handles.nettypetxt,'visible','off');
    set(handles.nettypepop,'visible','on','Value',1);
    set(handles.netnameedit,'String','net1');
    set(handles.toolmenu,'visible','off');
    set(findobj(handles.toolmenu,'tag','scripttoggle'),'state','off'); %in case script panel is open, setting state of toggle button to off closes script panel
    objs = findobj(allchild(handles.definepnl),'-not','tag','objtypepop',...
           '-not','tag','selecttxt');
    if ~isempty(objs)
       delete(objs);
    end
    uimenus = findobj('type','uicontextmenu');
    if ~isempty(uimenus)
       delete(uimenus);
    end
    set(handles.objtypepop,'Value',1);
    panelcntrls = findobj(handles.definepnl,'-property','enable'); % not all objects have 'enable' property
    set(panelcntrls,'enable','off');    
    set(hObject,'visible','off');
    setdefaultuiparams;
end

function netnameedit_Callback(hObject, eventdata, handles)
% hObject    handle to netnameedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of netnameedit as text
%        str2double(get(hObject,'String')) returns contents of netnameedit as a double

% This is called when the edit box is edited. It updates the netinfo
% structure and refreshes the script panel with the change if script panel
% is in visible state.
netinfo = getappdata(handles.createnetwork,'netinfo');
netinfo.name = get(hObject,'String');
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

% --- Executes during object creation, after setting all properties.
function netnameedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to netnameedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in objtypepop.
function objtypepop_Callback(hObject, eventdata, handles)
% hObject    handle to objtypepop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns objtypepop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from objtypepop

% This pop-up menu allows user to select one set of network objects at a 
% time and configure the properties of elements in that set. Network
% properties are divided into -
% 1. Network components (netlevel/all core net properties)
% 2. Pool components (poollevel/all core pool properties)
% 3. Pool Parameters (poolparamlevel/pool properties specific to net type)
% 4. Projection components (projlevel/all core projection properties
% 5. Projection Parameters (projparamlevel/projection properties specific
%    to net type)
uiparams = getappdata(handles.createnetwork,'uiparams');
poolinfo = getappdata(handles.createnetwork,'poolinfo');
netinfo = getappdata(handles.createnetwork,'netinfo');
%first delete all uicontrols other than pop up menu and corresponding label control
objs = findobj(allchild(handles.definepnl),'-not','tag','objtypepop',...
       '-not','tag','selecttxt');
if ~isempty(objs)
   delete(objs);
end
drawnow;
val = get(hObject,'Value');
switch val
    case 1 
        drawuicontrols(uiparams.netlevel);
    case 2
        drawuicontrols(uiparams.poollevel);
    case 3
        if isempty(uiparams.poolparamlevel)
           return;
        end
        if numel(poolinfo) > 1
            if strcmp(netinfo.type,'tdbp')
                uiparams.poolparamlevel(2).properties{4} = getoutputpools();
            else
                uiparams.poolparamlevel(1).properties{4} = {poolinfo.name};
            end
        end
        drawuicontrols(uiparams.poolparamlevel);
        if isfield(netinfo,'standardcl') && strcmpi(netinfo.standardcl,'off')
           topohandles = findobj('-regexp','tag','^topo*');
           set(topohandles,'Visible','on');
        end        
    case 4
        if numel(poolinfo) > 1
           uiparams.projlevel(5).properties{4} = {poolinfo.name}; % In projlevel selection, populate 'Sender' pool pop-up uicontrol with all pool names
           uiparams.projlevel(6).properties{4} = {poolinfo(2:end).name}; % populate 'Receiver' pool pop-up uicontrol with all pool names except bias
        end
        setappdata(handles.createnetwork,'uiparams',uiparams);
        drawuicontrols(uiparams.projlevel);
    case 5
        if isempty(uiparams.projparamlevel)
           uicontrol('parent',handles.definepnl,'visible','off');
           return;
        end
        drawprojlist;
%         k = 0;        
%         for i=1:numel(poolinfo)
%             if ~isfield(poolinfo,'proj')
%                continue;
%             end
%             for j =1: numel(poolinfo(i).proj)
%                 k = k +1;
%                 liststr{k} = sprintf('%s -> %s',...
%                              poolinfo(i).proj(j).frompool,poolinfo(i).name);  % create 'sender_pool -> receiver_pool' list for projparam level
%                 udata(k,:) = [i j]; % create user data containing ordered indices of sender pools, receiver pools
%             end
%         end
%         if k > 0
%            uiparams.projparamlevel(2).properties{4} = liststr;  
%            uiparams.projparamlevel(2).properties = [uiparams.projparamlevel(2).properties {'UserData',udata}];
%         end
%         drawuicontrols(uiparams.projparamlevel);                
end

function outpools = getoutputpools()
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
poolinfo = getappdata(handles.createnetwork,'poolinfo');
num_outpools = 1;
for i=1:numel(poolinfo)
    if strcmp(poolinfo(i).type,'output')
        outpools{num_outpools} = poolinfo(i).name;
        num_outpools = num_outpools + 1;
    end
end
if num_outpools ==1
    outpools = {'none'};
end



function drawprojlist
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
poolinfo = getappdata(handles.createnetwork,'poolinfo');
k = 0;        
for i = 1:numel(poolinfo)
    if ~isfield(poolinfo,'proj')
       continue;
    end
    for j = 1: numel(poolinfo(i).proj)
        k = k +1;
        liststr{k} = sprintf('%s -> %s',poolinfo(i).proj(j).frompool,...
                    poolinfo(i).name);  % create 'sender_pool -> receiver_pool' list for projparam level
        udata(k,:) = [i j]; % create user data containing ordered indices of sender pools, receiver pools
    end
end
if k > 0
   uiparams.projparamlevel(2).properties{4} = liststr;  
   uiparams.projparamlevel(2).properties = [uiparams.projparamlevel(2).properties {'UserData',udata}];
end
drawuicontrols(uiparams.projparamlevel);  
        
% --- Executes during object creation, after setting all properties.
function objtypepop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to objtypepop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in scriptclosebtn.
function scriptclosebtn_Callback(hObject, eventdata, handles)
% hObject    handle to scriptclosebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = guihandles;
set(h.scripttoggle,'state','off'); % setting state to off calls the offcallback routine that makes the script panel invisible


function scriptedit_Callback(hObject, eventdata, handles)
% hObject    handle to scriptedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of scriptedit as text
%        str2double(get(hObject,'String')) returns contents of scriptedit as a double


% --- Executes during object creation, after setting all properties.
function scriptedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scriptedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function refreshscript(h)
% Populates the script edit field with the current values in netinfo and
% poolinfo appdata variables. This displays the network script creation 
% process in progress,if the script panel is open and visible. 
script='';
snum=1;
netinfo = getappdata(h.createnetwork,'netinfo');
poolinfo = getappdata(h.createnetwork,'poolinfo');
if isempty(netinfo)
    set(h.scriptedit,'String','');
    return;
end
netfields = fieldnames(netinfo);
for neti = 1:numel(netfields)
    if ischar(netinfo.(netfields{neti}))
        script{snum} = sprintf('net.%s = ''%s''',netfields{neti},netinfo.(netfields{neti}));
    else
        script{snum} = sprintf('net.%s = %d', netfields{neti},netinfo.(netfields{neti}));
    end
    snum = snum+1;
end
if isempty(poolinfo)
    set(h.scriptedit,'String',script,'ForegroundColor','Black');
    return;
end
poolfields = fieldnames(poolinfo);
for i = 1: numel(poolinfo)
    for p = 1:numel(poolfields)
        f = poolinfo(i).(poolfields{p});
        if isempty(f)
           continue;
        end
        script{snum} = sprintf('pool(%d).%s',i,poolfields{p});        
        if isstruct(f)
            tscript=script{snum};
            ps = fieldnames(f);
            for j=1:numel(f)
                script{snum} = sprintf('%s(%d).',tscript,j);
                ttscript = script{snum};
                for q= 1:numel(ps)
                    ff = f(j).(ps{q});
                    script{snum} = sprintf('%s%s = ',ttscript,ps{q});
                    if ischar(ff)
                       if strncmp(ff,'read_connection',15)
                          script{snum} = sprintf('%s %s',script{snum},ff);
                       else
                          script{snum} = sprintf('%s''%s''',script{snum},ff);
                       end
                    end
                    if isnumeric(ff)
                        if numel(ff) == 1
                           script{snum} = sprintf('%s%s',script{snum},num2str(ff));
                        else
                          script{snum} = sprintf('%s%s',script{snum},mat2str(ff));
                        end
                    end
                    snum=snum+1;
                end
                snum = snum +1;
            end
            snum=snum+1;
            continue;
        end
                    
        if ischar(f)
           if strncmp(f,'textread',8)
              script{snum} = sprintf('%s = %s',script{snum},f);
           else            
              script{snum} = sprintf('%s = ''%s''',script{snum},f);
           end
        end                     
        if isnumeric(f)
            if numel(f) == 1
               script{snum} = sprintf('%s = %s',script{snum},num2str(f));
            else
               script{snum} = sprintf('%s = %s',script{snum},mat2str(f));
            end
        end
        if iscell(f) %only for cell of strings
            script{snum} = sprintf('%s = {',script{snum});            
            cellvalues = sprintf('''%s'' ',f{1:end});
            script{snum} = sprintf('%s%s}',script{snum},cellvalues);
        end
        snum = snum+1;
    end
end
mt = cellfun('isempty',script);
script = script(~mt);
set(h.scriptedit,'String',script,'ForegroundColor','Black');

function setdefaultuiparams
handles = guihandles;
% set properties of default uicontrol objects to be displayed at various
% levels of network creation process. 'properties' field is a cell array of
% strings that is used for setting all required properties of the uicontrol
% object when it is drawn in the 'drawuicontrols' function.This cell array
% must contain 'property-value' pairs separated by commas.They must be
% valid matlab properties and values for uicontrol objects.
uiparams = struct;
netlevel(1).properties = {'Style','checkbox','String','Seed Randomly ',...
                          'Units','characters',...
                          'HorizontalAlignment','left',...
                          'Value',1,'Callback',@seedcheck_cb};
netlevel(1).width = numel(netlevel(1).properties{4})+ 7;
netlevel(1).height = 1.5;
netlevel(1).posindex = [1 1];

netlevel(end+1).properties = {'Style','text','String','Seed with : ',...
                              'Units','characters',...
                              'HorizontalAlignment','left'};
netlevel(end).width = numel(netlevel(end).properties{4});
netlevel(end).height = 1;
netlevel(end).posindex = [2 1];

netlevel(end+1).properties = {'Style','edit','String','','enable','off',...
                              'Units','characters','tag','seededit',...
                              'HorizontalAlignment','left',...
                              'BackgroundColor','white',...
                              'Callback',@seededit_cb};
netlevel(end).width = 10;
netlevel(end).height = 2;
netlevel(end).posindex = [3 1];

uiparams.netlevel = netlevel;

poollevel(1).properties = {'Style','text','String','Name :',...
                          'Units','characters',...
                          'HorizontalAlignment','left'};
poollevel(1).width = numel(poollevel(1).properties{4})+2;
poollevel(1).height = 2;
poollevel(1).posindex = [1 1];
cmenu = uicontextmenu('tag','poolcontextmenu');
uimenu(cmenu,'Label', 'Edit', 'Callback',@editpool_cb);
poollevel(end+1).properties = {'Style','edit','String','mypool',...
                               'tag','poolnameedit','Units','characters',...
                               'HorizontalAlignment','left',...
                               'BackgroundColor','white',...
                               'TooltipString','Right click to edit',... 
                               'UIContextMenu',cmenu,...
                               'KeypressFcn',@changebg_cb};
poollevel(end).width = 15;
poollevel(end).height = 2;
poollevel(end).posindex = [2 1];

poollevel(end+1).properties = {'Style','text','String','Type :',...
                               'Units','characters',...
                               'HorizontalAlignment','left'};
poollevel(end).width = numel(poollevel(end).properties{4});
poollevel(end).height = 2;
poollevel(end).posindex = [3 1];

poollevel(end+1).properties = {'Style','popupmenu','String',{'input',...
                             'output'},'tag','pooltypepop',...
                             'Units','characters','Value',1,...
                             'BackgroundColor','white',...
                             'Callback',@changebg_cb};
poollevel(end).width = max(cellfun(@numel,poollevel(end).properties));
poollevel(end).height = 2;
poollevel(end).posindex = [4 1];

poollevel(end+1).properties = {'Style','text','String','Nunits :',...
                               'Units','characters',...
                               'HorizontalAlignment','left'};
poollevel(end).width = numel(poollevel(end).properties{4});
poollevel(end).height = 2;
poollevel(end).posindex = [5 1];

poollevel(end+1).properties = {'Style','edit','String','1',...
                               'Units','characters',...
                               'tag','nunitsedit',...
                               'HorizontalAlignment','left',...
                               'BackgroundColor','white',...
                               'KeypressFcn',@changebg_cb};
poollevel(end).width = 8;
poollevel(end).height = 2;
poollevel(end).posindex = [6 1];

poollevel(end+1).properties = {'Style','pushbutton','String','Add',...
                              'Units','characters','tag','addpoolbtn',...
                              'Callback',@addpoolbtn_cb};
poollevel(end).width = numel(poollevel(end).properties{4})+5;
poollevel(end).height = 2;
poollevel(end).posindex = [7 1];
cmenu1 = uicontextmenu('tag','poolcontextmenu1');
uimenu(cmenu1,'Label','Delete','Callback',@deletepool_cb);
poollevel(end+1).properties = {'Style','popupmenu','String',{' '},...
                              'Units','characters','Visible','off',...
                              'BackgroundColor','white',...
                              'TooltipString','Right-Click to delete',...
                              'tag','currplist','UIContextMenu',cmenu1,...
                              'Callback',@currplist_cb};
poollevel(end).width = 10;
poollevel(end).height = 2;
poollevel(end).posindex = [8 1];

projlevel(1).properties = {'Style','text','String','Sender ',...
                           'Units','characters',...
                           'HorizontalAlignment','left'};
projlevel(1).width = 15;
projlevel(1).height = 2;
projlevel(1).posindex = [1 1];


projlevel(end+1).properties = {'Style','text','String','Receiver ',...
                               'Units','characters',...
                               'HorizontalAlignment','left'};
projlevel(end).width = 15;
projlevel(end).height = 2;
projlevel(end).posindex = [2 1];


projlevel(end+1).properties = {'Style','text','String','Constraint Type ',...
                               'Units','characters',...
                               'HorizontalAlignment','left'};
projlevel(end).width = 15;
projlevel(end).height = 2;
projlevel(end).posindex = [3 1];

projlevel(end+1).properties = {'Style','text','String','Value ',...
                               'Units','characters',...
                               'HorizontalAlignment','left'};
projlevel(end).width = 15;
projlevel(end).height = 2;
projlevel(end).posindex = [4 1];


projlevel(end+1).properties = {'Style','popupmenu','String',{'bias'},...
                               'tag','projspop','Units','characters',...
                               'BackgroundColor','white','Value',1,...
                               'Callback',@changebg_cb};
projlevel(end).width = 15;
projlevel(end).height = 1.5;
projlevel(end).posindex = [1 2];


projlevel(end+1).properties = {'Style','popupmenu','String',{' '},...
                               'tag','projrpop','Units','characters',...
                               'BackgroundColor','white','Value',1,...
                               'Callback',@changebg_cb};
projlevel(end).width = 15;
projlevel(end).height = 1.5;
projlevel(end).posindex = [2 2];


projlevel(end+1).properties = {'Style','popupmenu','String',{'Scalar',...
                               'Read file','Transpose file',},...
                               'Units','characters','tag','constrpop',...
                               'BackgroundColor','white',...
                               'Callback',@constraintpop_cb};
projlevel(end).width = 15;
projlevel(end).height = 1.5;
projlevel(end).posindex = [3 2];

projlevel(end+1).properties = {'Style','edit','String',' ',...
                               'HorizontalAlignment','left'...
                               'Units','characters','tag','constredit',...
                               'BackgroundColor','white',...
                               'KeypressFcn',@changebg_cb};
projlevel(end).width = 8;
projlevel(end).height = 1.5;
projlevel(end).posindex = [4 2];

projlevel(end+1).properties = {'Style','pushbutton','String','Add',...
                               'Units','characters',...
                               'Callback',@addprojbtn_cb};
projlevel(end).width = numel(projlevel(end).properties{4})+5;
projlevel(end).height = 2;
projlevel(end).posindex = [5 2];

projparamlevel(1).properties = {'Style','text','String','Projections :',...
                                'Units','characters',...
                                'HorizontalAlignment','left'};
projparamlevel(1).width = 15;
projparamlevel(1).height = 1;
projparamlevel(1).posindex = [1 1];
cmenu = uicontextmenu('tag','projcontextmenu');
uimenu(cmenu,'Label','Delete','Callback',@deleteproj_cb);
projparamlevel(2).properties = {'Style','popupmenu','String',{'None'},...
                                'tag','projlistpop','Units','characters',...
                                'BackgroundColor','white','Value',1,...
                                'ToolTipString','Right Click to delete projection',...
                                'UIContextMenu',cmenu,...
                                'Callback',@changebg_cb};
projparamlevel(2).width = 25;
projparamlevel(2).height = 1.5;
projparamlevel(2).posindex = [2 1];

uiparams.netlevel = netlevel;
uiparams.poollevel = poollevel;
uiparams.projlevel = projlevel;
uiparams.projparamlevel = projparamlevel;

setappdata(handles.createnetwork,'uiparams',uiparams);

function deleteproj_cb(hObject,event)
handles = guihandles;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
projlist = cellstr(get(handles.projlistpop,'String'));
index = get(handles.projlistpop,'Value');
udata = get(handles.projlistpop,'UserData');
if isempty(udata)
   return;
end
poolinfo(udata(index,1)).proj(udata(index,2)) = []; 
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if index == 1 && numel(projlist) == 1
   set(handles.projlistpop,'String',{'None'},'UserData',[],'Value',1);
else
   objs = findobj(allchild(handles.definepnl),'-not','tag','objtypepop',...
          '-not','tag','selecttxt');
   if ~isempty(objs)
      delete(objs);
   end    
   drawprojlist;
end
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function currplist_cb(hObject, event)
handles = guihandles;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
index = get(hObject,'Value') + 1;
ptype = poolinfo(index).type;
typestr = cellstr(get(handles.pooltypepop,'String'));
typeindex = find(strcmpi(typestr,ptype));
set(handles.pooltypepop,'Value',typeindex);
set(handles.nunitsedit,'String',poolinfo(index).nunits);

function constraintpop_cb(hObject,event)
handles = guihandles;
alphadef = 0;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
sval = get(handles.projspop,'Value');
rval = get(handles.projrpop,'Value')+1;
su = poolinfo(sval).nunits;
ru = poolinfo(rval).nunits;
value = get(hObject,'Value');
popstr = cellstr(get(hObject,'String'));
set(hObject,'BackgroundColor','yellow');
switch popstr{value}
    case 'Scalar'
        return;
    case 'Read file'
        [filename, pathname] = uigetfile('*.*','Read array from file');
        if isequal([filename,pathname],[0,0])
           set(hObject,'Value',1);
           return;
        end
        if strcmpi(pathname(1:end-1),pwd)
           File = filename;
        else
           File = fullfile(pathname,filename);
        end
        [wrdcheck] = textread(File,'%s',1);  %check if this is iac word file
        if isnan(str2double(wrdcheck{1}))
           if ~any(strfind(wrdcheck{1},'alpha'))
               return;
           end
           inpnum = sum(strcmpi('input',{poolinfo.type}));
           hp = strcmpi('hidden',{poolinfo.type});
           su = sum(poolinfo(hp==1).nunits);
           formatstr= sprintf('%%%dc%%*[^\\n]',inpnum);
           [readm] = textread(File,formatstr,'headerlines',2);
           sz = [su inpnum];
           alphadef = 1 ;           
        else
            readm =  load(File);
            sz = [ru su];
        end
        if isequal(size(readm),sz)
            matfile.alphadef = alphadef;
            matfile.fname = File;
            setappdata(hObject,'constraintfile',matfile);
        else
            disp(sprintf ('Array in file %s (%s) does not match pool sizes(%s)',...
                File,mat2str(size(readm)),mat2str(sz)));
            set(hObject,'Value',1);
            return;
        end
    case 'Transpose file'
        [filename, pathname] = uigetfile('*.*','Read array from file');
        if isequal([filename,pathname],[0,0])
           set(hObject,'Value',1);
           return;
        end
        if strcmpi(pathname(1:end-1),pwd)
           File = filename;
        else
           File = fullfile(pathname,filename);
        end
        readm =  load(File);
        sz = [su ru];
        if isequal(size(readm),sz)
            matfile.alphadef = alphadef;
            matfile.fname = File;
            setappdata(hObject,'constraintfile',matfile);
        else
            disp(sprintf ('Array in file %s (%s) does not match pool sizes(%s)',...
                File,mat2str(size(readm)),mat2str(sz)));
            set(hObject,'Value',1);
            return;
        end        
end
                       
function poolnameedit_cb(hObject,event)
handles = guihandles;
if  ~strcmpi(get(handles.createnetwork,'SelectionType'),'alt')
    return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
if isempty(poolinfo)
   return;
end
pos = get(hObject,'Position');
set(hObject,'Visible','off');
set(handles.currplist,'Visible','on','Position',pos);

function editpool_cb(hObject,event)
handles = guihandles;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
if isempty(poolinfo)
   return;
end
if numel(poolinfo) == 1 && strcmpi(poolinfo(1).name,'bias')
   warndlg ('No pools defined, bias pool cannot be edited','Create network');
   return;
end
poolnames = {poolinfo(2:end).name};
pos = get(handles.poolnameedit,'Position');
set(handles.poolnameedit,'Visible','off');
set(handles.currplist,'String',poolnames,'Visible','on','Position',pos);
set(handles.addpoolbtn,'String','Done');

function drawuicontrols (drawstruct)
handles = guihandles;
u = get(findobj(handles.definepnl,'tag','objtypepop'),'Units');
set(findobj(handles.definepnl,'tag','objtypepop'),'Units','characters');
pos_ref = get(findobj(handles.definepnl,'tag','objtypepop'),'Position');
set(findobj(handles.definepnl,'tag','objtypepop'),'Units',u);
for i = 1:numel(drawstruct)
    properties = drawstruct(i).properties;
    y_pos = pos_ref(2) - drawstruct(i).posindex(2)* 2.5;
    if i == 1 || (i > 1 && drawstruct(i).posindex(2) ~= ...
                                               drawstruct(i-1).posindex(2))
       x_pos = 2;
    else
       if drawstruct(i).posindex(2) == drawstruct(i-1).posindex(2)
          x_pos = x_extent + 2;
       end
    end
    x_extent = x_pos + drawstruct(i).width;    
    pos_vec = [x_pos y_pos drawstruct(i).width drawstruct(i).height];
    add_prop = {'Position',pos_vec,'Parent',handles.definepnl};
    properties = [properties add_prop];
    uicontrol(properties{1:end});  % this draws each uicontrol object ,setting all properties specified in the 'properties' field of drawstruct.
end

function seedcheck_cb(hObject,event)
handles = guihandles;
netinfo = getappdata(handles.createnetwork,'netinfo');
seedbox = findobj(handles.definepnl,'tag','seededit');
if get(hObject,'Value') == 1
   set(findobj(handles.definepnl,'tag','seededit'),'enable','off');
   if isfield(netinfo,'seed')
      netinfo = rmfield(netinfo,'seed');
   end
else
   set(seedbox,'enable','on');
   if ~isnan(str2double(get(seedbox,'String')))
      netinfo.seed = str2double(get(seedbox,'String'));
   end
end
netinfo.seedmode = get(hObject,'Value');
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function seededit_cb(hObject, event)
handles = guihandles;
netinfo = getappdata(handles.createnetwork,'netinfo');
seedvalue = str2double(get(hObject,'String'));
if isnan(seedvalue)
   return;
end
netinfo.seed = seedvalue;
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function netmodepop_cb(hObject, event)
handles = guihandles;
netinfo = getappdata(handles.createnetwork,'netinfo');
val = get(handles.netmodepop,'Value');
if val == 1
    netmode = 'beforestate';
elseif val == 2
    netmode = 'afterstate';
end
netinfo.netmode = netmode;
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
    refreshscript(handles);
end



function changebg_cb(hObject,event)
set(hObject,'BackgroundColor','yellow');

function addpoolbtn_cb(hObject, event)
handles = guihandles;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolnames = {poolinfo.name};
if strcmpi(get(hObject,'String'),'Done')
   index = get(handles.currplist,'Value') + 1;
else
   index = numel(poolinfo) + 1;
   name = get(handles.poolnameedit,'String');
   if any(strcmpi(poolnames,name))
      warndlg(sprintf('Pool %s already exists. Cannot have two pools with same name',name),'Network creation');
      return;
   end
   poolinfo(index).name = name;
end
val = get(handles.pooltypepop,'Value');
typestr = cellstr(get(handles.pooltypepop,'String'));
poolinfo(index).type = typestr{val};
poolinfo(index).nunits = str2double(get(handles.nunitsedit,'String'));
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(hObject,'String','Add');
set(handles.currplist,'Visible','off');
set(handles.poolnameedit,'Visible','on');
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');

function deletepool_cb(hObject,event)
handles = guihandles;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
if isempty(poolinfo) || numel(poolinfo) < 2
   return;
end
names = cellstr(get(handles.currplist,'String'));
index = get(handles.currplist,'Value');
poolinfo(index+1) = [];
if index == 1
   set(handles.currplist,'String','','Value',1,'Visible','off');
else
   names(index) = [];
   set(handles.currplist,'String',names,'Value',1,'Visible','off');
end
set(handles.poolnameedit,'String','mypool','Visible','on');
set(handles.nunitsedit,'String','');
set(handles.pooltypepop,'Value',1);
set(handles.addpoolbtn,'String','Add');
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
       
function addprojbtn_cb(hObject, event)
handles = guihandles;
rpool = cellstr(get(handles.projrpop,'String')); %get receiver pools
rval = get(handles.projrpop,'Value');
receiver = rpool{rval};
spool = cellstr(get(handles.projspop,'String')); %get sender pools
sval = get(handles.projspop,'Value');
sender = spool{sval};
poolinfo = getappdata(handles.createnetwork,'poolinfo');
pl_index = strmatch(receiver,{poolinfo.name},'exact');
pr_index = 1;
if isfield(poolinfo(pl_index),'proj')
   pr_index = numel(poolinfo(pl_index).proj) + 1;
end
poolinfo(pl_index).proj(pr_index).frompool = sender;
constr_value = get(handles.constrpop,'Value');
constr_popstr = cellstr(get(handles.constrpop,'String'));
constraint = str2double(get(handles.constredit,'String'));
switch constr_popstr{constr_value}
    case 'Scalar'
         poolinfo(pl_index).proj(pr_index).constraint = constraint;
         poolinfo(pl_index).proj(pr_index).constraint_type = 'scalar';
    case 'Read file'
         constrfile = getappdata(handles.constrpop,'constraintfile');
         arrayfile = constrfile.fname;
         args = sprintf('''%s''',arrayfile);
         if constrfile.alphadef == 1  %iac word file
            args = sprintf('%s,''%s'',''%s'',%.2f',args,sender,receiver,...
                           constraint);
         end
         poolinfo(pl_index).proj(pr_index).constraint = ...
                                       sprintf('read_connection(%s)',args); 
         poolinfo(pl_index).proj(pr_index).constraint_type = 'scalar';   
    case 'Transpose file'
         constrfile = getappdata(handles.constrpop,'constraintfile');
         arrayfile = constrfile.fname;
         args = sprintf('''%s'',''t''',arrayfile);
         poolinfo(pl_index).proj(pr_index).constraint = ...
                                       sprintf('read_connection(%s)',args); 
         poolinfo(pl_index).proj(pr_index).constraint_type = 'scalar';          
    case {'Random','pRandom','nRandom','Copyback'}
         poolinfo(pl_index).proj(pr_index).constraint = constraint;
         poolinfo(pl_index).proj(pr_index).constraint_type = ...
                                        lower(constr_popstr{constr_value});
end
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');

function set_pa_params
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
uiparams.poolparamlevel = [];
setappdata(handles.createnetwork,'uiparams',uiparams);
setlrateparams;

function set_iac_params
setunamesparams;

function setlrateparams
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
projparamlevel = uiparams.projparamlevel;
projparamlevel(3).properties = {'Style','text','String','lrate :',...
                                'Units','characters',...
                                'HorizontalAlignment','left'};
projparamlevel(3).width = numel(projparamlevel(3).properties{4});
projparamlevel(3).height = 1;
projparamlevel(3).posindex = [3 1];

projparamlevel(4).properties = {'Style','edit','String','NaN',...
                                'tag','lrateedit','Units','characters',...
                                'HorizontalAlignment','left'...
                                'BackgroundColor','white',...
                                'Callback',@changebg_cb};
projparamlevel(4).width = 8;
projparamlevel(4).height = 1.5;
projparamlevel(4).posindex = [4 1];

projparamlevel(5).properties = {'Style','pushbutton','String','Add',...
                                'Units','characters',...
                                'Callback',@addlratebtn_cb};
projparamlevel(5).width = numel(projparamlevel(5).properties{4})+5;
projparamlevel(5).height = 2;
projparamlevel(5).posindex = [5 1];
uiparams.projparamlevel = projparamlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);

function setlambdaparams
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
projparamlevel = uiparams.projparamlevel;
projparamlevel(6).properties = {'Style','text','String','lambda :',...
                                'Units','characters',...
                                'HorizontalAlignment','left'};
projparamlevel(6).width = numel(projparamlevel(3).properties{4});
projparamlevel(6).height = 1;
projparamlevel(6).posindex = [1 3];

projparamlevel(7).properties = {'Style','edit','String','NaN',...
                                'tag','lambdaedit','Units','characters',...
                                'HorizontalAlignment','left'...
                                'BackgroundColor','white',...
                                'Callback',@changebg_cb};
projparamlevel(7).width = 8;
projparamlevel(7).height = 1.5;
projparamlevel(7).posindex = [2 3];

projparamlevel(8).properties = {'Style','pushbutton','String','Add',...
                                'Units','characters',...
                                'Callback',@addlambdabtn_cb};
projparamlevel(8).width = numel(projparamlevel(5).properties{4})+5;
projparamlevel(8).height = 2;
projparamlevel(8).posindex = [3 3];
uiparams.projparamlevel = projparamlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);

function addlratebtn_cb(hObject,event)
handles = guihandles;
prlist = cellstr(get(handles.projlistpop,'String'));
if strcmpi(prlist{1},'none')
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
val = get(handles.projlistpop,'Value');
udata = get(handles.projlistpop,'UserData');
index = udata(val,:);  %must be a 2 element vector 
poolinfo(index(1)).proj(index(2)).lrate = str2double(get(handles.lrateedit,'String'));
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');

function addgammabtn_cb(hObject,event)
handles = guihandles;
prlist = cellstr(get(handles.outpoolspop,'String'));
if strcmpi(prlist{1},'none')
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
val = get(handles.outpoolspop,'Value');
poolnames = {poolinfo(:).name};
poolname = prlist{val};
pool_index = find(ismember(poolnames,poolname)==1);
poolinfo(pool_index).gamma = str2double(get(handles.gammaedit,'String'));
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(handles.gammaedit,'BackgroundColor','white');

function addactfuncbtn_cb(hObject,event)
handles = guihandles;
prlist = cellstr(get(handles.outpoolspop,'String'));
if strcmpi(prlist{1},'none')
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
val = get(handles.outpoolspop,'Value');
poolnames = {poolinfo(:).name};
poolname = prlist{val};
pool_index = find(ismember(poolnames,poolname)==1);
actlist = get(handles.actfuncpop,'String');
val = get(handles.actfuncpop,'Value');
poolinfo(pool_index).actfunction = actlist{val};
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(handles.actfuncpop,'BackgroundColor','white');

function addlambdabtn_cb(hObject,event)
handles = guihandles;
prlist = cellstr(get(handles.projlistpop,'String'));
if strcmpi(prlist{1},'none')
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
val = get(handles.projlistpop,'Value');
udata = get(handles.projlistpop,'UserData');
index = udata(val,:);  %must be a 2 element vector 
poolinfo(index(1)).proj(index(2)).lambda = str2double(get(handles.lambdaedit,'String'));
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(handles.lambdaedit,'BackgroundColor','white');

function setunamesparams
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolnames = {poolinfo.name};
poolparamlevel(1).properties = {'Style','popupmenu','String',poolnames,...
                                'tag','poollistpop','Units','characters',...
                                'BackgroundColor','white','Value',1,...
                                'Callback',@changebg_cb};
poolparamlevel(1).width = 15;
poolparamlevel(1).height = 1.5;
poolparamlevel(1).posindex = [1 1];

poolparamlevel(2).properties = {'Style','text','String','Unames :',...
                           'HorizontalAlignment','left','Units','characters'};
poolparamlevel(2).width = numel(poolparamlevel(2).properties{4});
poolparamlevel(2).height = 1;
poolparamlevel(2).posindex = [2 1];

poolparamlevel(3).properties = {'Style','popupmenu',...
                                'String',{'Select an option','Read File',...
                                'Set by hand'},'tag','setunamepop',...
                                'Units','characters',...
                                'BackgroundColor','white','Value',1,...
                                'Callback',@setunamepop_cb};
poolparamlevel(3).width = 15;
poolparamlevel(3).height = 1.5;
poolparamlevel(3).posindex = [3 1];

poolparamlevel(4).properties = {'Style','edit','String','',...
                                'TooltipString','Comma-separated list,press ''Tab'' or ''Enter'' key when done'...
                                'tag','setunameedit',...
                                'HorizontalAlignment','left',...
                                'Units','characters',...
                                'BackgroundColor','white',...
                                'Callback',@setunameedit_cb};
poolparamlevel(4).width = 35;
poolparamlevel(4).height = 1.5;
poolparamlevel(4).posindex = [4 1];

uiparams.poolparamlevel = poolparamlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);

function setunamepop_cb(hObject, event)
handles = guihandles;
val = get(hObject,'Value');
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolindex = get(handles.poollistpop,'Value');
nu = poolinfo(poolindex).nunits;
switch val
    case 1
        return;
    case 2
        [filename, pathname] = uigetfile('*.*','Read unames from file');
        if isequal([filename,pathname],[0,0])
           set(hObject,'Value',1);
           return;
        end
        if strcmpi(pathname(1:end-1),pwd)
           File =filename;
        else
           File = fullfile(pathname,filename);
        end
        readm =  textread(File,'%s');
        if numel(readm) == nu
           set(handles.setunameedit,'String',File,'enable','off');
           poolinfo(poolindex).unames = sprintf('textread(''%s'',''%%s'')',File);
        else
           set(hObject,'Value',1);            
           error('List in file %s does not match pool size',File);
           return;
        end        
    case 3
        set(handles.setunameedit,'enable','on','String',' ');
end
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function setunameedit_cb(hObject, event)
handles = guihandles;
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolindex = get(handles.poollistpop,'Value');
nu = poolinfo(poolindex).nunits;
str = get(hObject,'String');
unamelist = textscan(str,'%s','delimiter',','); %this will be a single cell element containing one cell of strings
unamelist = unamelist{1}';
if numel(unamelist) == nu
   poolinfo(poolindex).unames = unamelist;
else
   errordlg('List in edit box does not match pool size');
end
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(findobj('BackgroundColor', 'yellow'),'BackgroundColor', 'white');

function set_cs_params
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
% uiparams.projparamlevel = [];
setappdata(handles.createnetwork,'uiparams',uiparams);
setunamesparams;

function set_bp_params
% add uicontrols at projparamlevel for 'bp' networks
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
uiparams.poolparamlevel = [];
uiparams.projlevel(7).properties{4} = {'Random','pRandom',...
                                       'nRandom','Scalar'};
% projparamlevel(1).properties = {'Style','text','String','Projections :',...
%                                 'Units','characters',...
%                                 'HorizontalAlignment','left'};
% projparamlevel(1).width = 15;
% projparamlevel(1).height = 1;
% projparamlevel(1).posindex = [1 1];
% 
% projparamlevel(2).properties = {'Style','popupmenu','String',{'None'},...
%                                 'tag','projlistpop','Units','characters',...
%                                 'BackgroundColor','white','Value',1,...
%                                 'Callback',@changebg_cb};
% projparamlevel(2).width = 25;
% projparamlevel(2).height = 1.5;
% projparamlevel(2).posindex = [2 1];
% uiparams.projparamlevel = projparamlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);
setlrateparams;

function set_tdbp_params
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
netinfo = getappdata(handles.createnetwork,'netinfo');  % set default netmode
netmode = 'statevalue';
netinfo.netmode = netmode;
setappdata(handles.createnetwork,'netinfo',netinfo);
    
% add sarse/valuefunction mode to netlevel params
netlevel = uiparams.netlevel;
netlevel(end+1).width = 15;
netlevel(end).height = 1;
netlevel(end).posindex = [1 2];
netlevel(end).properties = {'Style','text','tag','netmodetxt','String','Network mode : ','Units',...
                            'characters','HorizontalAlignment','left'};
netlevel(end+1).width = 15;
netlevel(end).height = 1;
netlevel(end).posindex = [2 2];
netlevel(end).properties = {'Style','popupmenu','String',{'beforestate','afterstate'},...
                                'tag','netmodepop',...
                             'Units','characters','Value',1,...
                             'BackgroundColor','white',...
                             'Callback',@netmodepop_cb};
uiparams.netlevel = netlevel;

% add gamma to poolparamlevel and lambda to projparamlevel
uiparams.poolparamlevel = [];
poolparamlevel(1).width = 15;
poolparamlevel(1).height = 1;
poolparamlevel(1).posindex = [1,1];
poolparamlevel(1).properties = {'Style','text','tag','outpooltxt','String','Output pool : ','Units','characters','HorizontalAlignment','left'};
poolparamlevel(end+1).width = 15;
poolparamlevel(end).height = 1;
poolparamlevel(end).posindex = [2,1];
poolparamlevel(end).properties = {'Style','popupmenu','String',{'none'},...
                                'tag','outpoolspop',...
                             'Units','characters','Value',1,...
                             'BackgroundColor','white',...
                             'Callback',@changebg_cb};
poolparamlevel(end+1).width = 15;
poolparamlevel(end).height = 1;
poolparamlevel(end).posindex = [1,2];
poolparamlevel(end).properties = {'Style','text','String','Gamma : ','Units','characters','HorizontalAlignment','left'};
poolparamlevel(end+1).width = 15;
poolparamlevel(end).height = 1.5;
poolparamlevel(end).posindex = [2,2];
poolparamlevel(end).properties = {'Style','edit','String','NaN','tag',...
                                        'gammaedit','Units','characters',...
                                        'HorizontalAlignment','left','Background','white',...
                                        'TooltipString','Right click to edit'};
poolparamlevel(end+1).width = 8;%numel(poolparamlevel(5).properties{4})+5;
poolparamlevel(end).height = 2;
poolparamlevel(end).posindex = [3 2];
poolparamlevel(end).properties = {'Style','pushbutton','String','Add',...
                                'Units','characters',...
                                'Callback',@addgammabtn_cb};
poolparamlevel(end+1).width = 15;
poolparamlevel(end).height = 1;
poolparamlevel(end).posindex = [1 3];
poolparamlevel(end).properties = {'Style','text','tag','actfunctxt','String','Activation function : ','Units','characters','HorizontalAlignment','left'};
poolparamlevel(end+1).width = 15;
poolparamlevel(end).height = 1;
poolparamlevel(end).posindex = [2 3];
poolparamlevel(end).properties = {'Style','popupmenu','String',{'logistic','linear'},...
                                'tag','actfuncpop',...
                             'Units','characters','Value',1,...
                             'BackgroundColor','white',...
                             'Callback',@changebg_cb};
poolparamlevel(end+1).properties = {'Style','pushbutton','String','Add',...
                                    'Units','characters',...
                                    'Callback',@addactfuncbtn_cb};
poolparamlevel(end).width = numel(poolparamlevel(5).properties{4})+5;
poolparamlevel(end).height = 2;
poolparamlevel(end).posindex = [3 3];

uiparams.poolparamlevel = poolparamlevel;
uiparams.projlevel(7).properties{4} = {'Random','pRandom',...
                                       'nRandom','Copyback','Scalar'};
setappdata(handles.createnetwork,'uiparams',uiparams);
setlrateparams;
setlambdaparams;

function set_srn_params
% add uicontrols at projparamlevel for 'srn' networks
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
uiparams.poolparamlevel = [];
uiparams.projlevel(7).properties{4} = {'Random','pRandom',...
                                       'nRandom','Copyback','Scalar'};
% projparamlevel(1).properties = {'Style','text','String','Projections :',...
%                                 'Units','characters',...
%                                 'HorizontalAlignment','left'};
% projparamlevel(1).width = 15;
% projparamlevel(1).height = 1;
% projparamlevel(1).posindex = [1 1];
% 
% projparamlevel(2).properties = {'Style','popupmenu','String',{'None'},...
%                                 'tag','projlistpop','Units','characters',...
%                                 'BackgroundColor','white','Value',1,...
%                                 'Callback',@changebg_cb};
% projparamlevel(2).width = 25;
% projparamlevel(2).height = 1.5;
% projparamlevel(2).posindex = [2 1];
% uiparams.projparamlevel = projparamlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);
setlrateparams;

function set_rbp_params
% add uicontrols at netlevel,projparamlevel for 'rbp' networks
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
uiparams.poolparamlevel = [];
uiparams.projlevel(7).properties{4} = {'Random','pRandom',...
                                       'nRandom','Scalar'};
netlevel = uiparams.netlevel;
next = numel(netlevel)+1;

netlevel(next).properties = {'Style','text','String','nintervals :',...
                             'Units','characters',...
                             'HorizontalAlignment','left'};
netlevel(next).width = numel(netlevel(next).properties{4});
netlevel(next).height = 1;
netlevel(next).posindex = [next 2];

next = next +1;
netlevel(next).properties = {'Style','edit','String',' ',...
                             'tag','nintervalsedit',...
                             'Units','characters',...
                             'HorizontalAlignment','left'...
                             'BackgroundColor','white',...
                             'KeypressFcn',@changebg_cb,...
                             'Callback',@nintervalsedit_cb};
netlevel(next).width = 8;
netlevel(next).height = 1.5;
netlevel(next).posindex = [next 2];

next = next +1;
netlevel(next).properties = {'Style','text',...
                             'String','Ticks per interval :',...
                             'Units','characters',...
                             'HorizontalAlignment','left'};
netlevel(next).width = numel(netlevel(next).properties{4});
netlevel(next).height = 1;
netlevel(next).posindex = [next 2];

next = next +1;
netlevel(next).properties = {'Style','edit','String',' ','tag','tpiedit',...
                             'Units','characters',...
                             'HorizontalAlignment','left'...
                             'BackgroundColor','white',...
                             'KeypressFcn',@changebg_cb,...
                             'Callback',@tpiedit_cb};
netlevel(next).width = 8;
netlevel(next).height = 1.5;
netlevel(next).posindex = [next 2];

uiparams.netlevel = netlevel;

% projparamlevel(1).properties = {'Style','text','String','Projections :',...
%                                 'Units','characters',...
%                                 'HorizontalAlignment','left'};
% projparamlevel(1).width = 15;
% projparamlevel(1).height = 1;
% projparamlevel(1).posindex = [1 1];
% 
% projparamlevel(2).properties = {'Style','popupmenu','String',{'None'},...
%                                 'tag','projlistpop','Units','characters',...
%                                 'BackgroundColor','white','Value',1,...
%                                 'Callback',@changebg_cb};
% projparamlevel(2).width = 25;
% projparamlevel(2).height = 1.5;
% projparamlevel(2).posindex = [2 1];
% uiparams.projparamlevel = projparamlevel;

setappdata(handles.createnetwork,'uiparams',uiparams);
setlrateparams;

function nintervalsedit_cb(hObject,event)
handles = guihandles;
netinfo = getappdata(handles.createnetwork,'netinfo');
nintervals_val = str2double(get(hObject,'String'));
if isnan(nintervals_val)
   return;
end
netinfo.nintervals = nintervals_val;
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(hObject,'BackgroundColor','white');

function tpiedit_cb(hObject,event)
handles = guihandles;
netinfo = getappdata(handles.createnetwork,'netinfo');
tpi_val = str2double(get(hObject,'String'));
if isnan(tpi_val)
   return;
end
netinfo.ticksperinterval = tpi_val;
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end
set(hObject,'BackgroundColor','white');

function set_cl_params
% add uicontrols at netlevel,poolparamlevel,projparamlevel for 'cl'
% networks
handles = guihandles;
uiparams = getappdata(handles.createnetwork,'uiparams');
uiparams.projlevel(7).properties{4} = {'Scalar','Random'};
netlevel = uiparams.netlevel;
next = numel(netlevel)+1;

netlevel(next).properties = {'Style','checkbox','String','Topographic ',...
                             'Units','characters',...
                             'HorizontalAlignment','left',...
                             'Value',0,'Callback',@topocheck_cb};
netlevel(next).width = 20;
netlevel(next).height = 1;
netlevel(next).posindex = [next 1];
uiparams.netlevel = netlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);
setunamesparams;
uiparams = getappdata(handles.createnetwork,'uiparams');  %this must have changed in the call to setunamesparams
poolparamlevel = uiparams.poolparamlevel;
next = numel(poolparamlevel) +1;
poolparamlevel(next).properties = {'Style','text','String','Geometry : ',...
                                   'tag','topogeomtxt',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters','Visible','off'};
poolparamlevel(next).width = numel(poolparamlevel(next).properties{4});
poolparamlevel(next).height = 1;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','text','String','Rows ',...
                                   'tag','toporowstxt',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters','Visible','off'};
poolparamlevel(next).width = numel(poolparamlevel(next).properties{4});
poolparamlevel(next).height = 1;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','edit','String','',...
                                   'tag','toporowsedit',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters',...
                                   'BackgroundColor','white',...
                                   'Visible','off',...
                                   'Callback',@topogeometryedit_cb};
poolparamlevel(next).width = 5;
poolparamlevel(next).height = 1.5;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','text','String','Cols ',...
                                   'tag','topocolstxt',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters','Visible','off'};
poolparamlevel(next).width = numel(poolparamlevel(next).properties{4});
poolparamlevel(next).height = 1;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','edit','String','',...
                                   'tag','topocolsedit',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters',...
                                   'BackgroundColor','white',...
                                   'Visible','off',...
                                   'Callback',@topogeometryedit_cb};
poolparamlevel(next).width = 5;
poolparamlevel(next).height = 1.5;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','text','String','Swan : ',...
                                   'tag','toposwantxt',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters','Visible','off'};
poolparamlevel(next).width = numel(poolparamlevel(next).properties{4});
poolparamlevel(next).height = 1;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','edit','String','',...
                                   'tag','toposwanedit',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters',...
                                   'BackgroundColor','white',...
                                   'Visible','off',...
                                   'Callback',@toposwanedit_cb};
poolparamlevel(next).width = 5;
poolparamlevel(next).height = 1.5;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','text','String','Lrange : ',...
                                   'tag','topolrangetxt',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters','Visible','off'};
poolparamlevel(next).width = numel(poolparamlevel(next).properties{4});
poolparamlevel(next).height = 1;
poolparamlevel(next).posindex = [next 2];

next = next + 1;
poolparamlevel(next).properties = {'Style','edit','String','',...
                                   'tag','topolrangeedit',...
                                   'HorizontalAlignment','left',...
                                   'Units','characters',...
                                   'BackgroundColor','white',...
                                   'Visible','off',...
                                   'Callback',@topolrangeedit_cb};
poolparamlevel(next).width = 5;
poolparamlevel(next).height = 1.5;
poolparamlevel(next).posindex = [next 2];

uiparams.poolparamlevel = poolparamlevel;

% projparamlevel(1).properties = {'Style','text','String','Projections :',...
%                                 'Units','characters',...
%                                 'HorizontalAlignment','left'};
% projparamlevel(1).width = 15;
% projparamlevel(1).height = 1;
% projparamlevel(1).posindex = [1 1];
% 
% projparamlevel(2).properties = {'Style','popupmenu','String',{'None'},...
%                                 'tag','projlistpop','Units','characters',...
%                                 'BackgroundColor','white','Value',1,...
%                                 'Callback',@changebg_cb};
% projparamlevel(2).width = 25;
% projparamlevel(2).height = 1.5;
% projparamlevel(2).posindex = [2 1];

projparamlevel = uiparams.projparamlevel;

projparamlevel(3).properties = {'Style','text','String','Topbias : ',...
                                'tag','topotopbiastxt',...
                                'HorizontalAlignment','left',...
                                'Units','characters'};
projparamlevel(3).width = numel(projparamlevel(3).properties{4});
projparamlevel(3).height = 1;
projparamlevel(3).posindex = [3 1];

projparamlevel(4).properties = {'Style','edit','String','',...
                                'tag','topotopbiasedit',...
                                'Units','characters',...
                                'HorizontalAlignment','left',...
                                'BackgroundColor','white',...
                                'Callback',@topotopbiasedit_cb};
projparamlevel(4).width = 5;
projparamlevel(4).height = 1.5;
projparamlevel(4).posindex = [4 1];

projparamlevel(5).properties = {'Style','text','String','Spread : ',...
                                'tag','topoispreadtxt',...
                                'HorizontalAlignment','left',...
                                'Units','characters'};
projparamlevel(5).width = numel(projparamlevel(5).properties{4});
projparamlevel(5).height = 1;
projparamlevel(5).posindex = [5 1];

projparamlevel(6).properties = {'Style','edit','String','',...
                                'tag','topoispreadedit',...
                                'HorizontalAlignment','left',...
                                'Units','characters',...
                                'BackgroundColor','white',...
                                'Callback',@topoispreadedit_cb};
projparamlevel(6).width = 5;
projparamlevel(6).height = 1.5;
projparamlevel(6).posindex = [6 1];

uiparams.projparamlevel = projparamlevel;
setappdata(handles.createnetwork,'uiparams',uiparams);

function topocheck_cb(hObject,event)
handles = guihandles;
netinfo = getappdata(handles.createnetwork,'netinfo');
if get(hObject,'Value') == 1
   netinfo.standardcl = 'off';
else
   netinfo.standardcl = 'on';
end
setappdata(handles.createnetwork,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function topogeometryedit_cb(hObject,event)
handles = guihandles;
rows = str2double(get(handles.toporowsedit,'String'));
cols = str2double(get(handles.topocolsedit,'String'));
if isnan(rows) || isnan(cols)
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolindex = get(handles.poollistpop,'Value');
poolinfo(poolindex).geometry = [rows cols];
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function toposwanedit_cb(hObject,event)
handles = guihandles;
swan_val = str2double(get(hObject,'String'));
if isnan(swan_val)
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolindex = get(handles.poollistpop,'Value');
poolinfo(poolindex).swan = swan_val;
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function topolrangeedit_cb(hObject,event)
handles = guihandles;
lrange_val = str2double(get(hObject,'String'));
if isnan(lrange_val)
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
poolindex = get(handles.poollistpop,'Value');
poolinfo(poolindex).lrange = lrange_val;
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end


function topotopbiasedit_cb(hObject,event)
handles = guihandles;
topbias_val = str2double(get(hObject,'String'));
if isnan(topbias_val)
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
val = get(handles.projlistpop,'Value');
udata = get(handles.projlistpop,'UserData');
index = udata(val,:);  %must be a 2 element vector 
poolinfo(index(1)).proj(index(2)).topbias = topbias_val;
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function topoispreadedit_cb(hObject,event)
handles = guihandles;
ispread_val = str2double(get(hObject,'String'));
if isnan(ispread_val)
   return;
end
poolinfo = getappdata(handles.createnetwork,'poolinfo');
val = get(handles.projlistpop,'Value');
udata = get(handles.projlistpop,'UserData');
index = udata(val,:);  %must be a 2 element vector 
poolinfo(index(1)).proj(index(2)).ispread = ispread_val;
setappdata(handles.createnetwork,'poolinfo',poolinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function savenetwork(hObject,evendata)
global PDPAppdata;
handles = guihandles;
[netname, pathname] = uiputfile({'*.net','Network Files (*.net)'},...
                                'New Network');
% If "Cancel" is selected then return
if isequal([netname,pathname],[0,0])
	return
else
 File = fullfile(pathname,sprintf('%s', netname));
end
fid = fopen(File, 'wt');
refreshscript(handles); %in case script panel was never 'viewed', it will have no string in it
data = get(handles.scriptedit,'String');
for i = 1:numel(data)
    fprintf(fid,'%s;\n',data{i});
end
fclose(fid);
loadscript(File);
if PDPAppdata.lognetwork
   stmt = sprintf('loadscript (''%s'');',File);
   updatelog(stmt);
end
script = PDPAppdata.netscript; 
netind = size(PDPAppdata.networks,1);
script{netind} = File;
PDPAppdata.netscript = script;
delete(handles.createnetwork);


function abortnetwork(hObject,eventData)
handles = guihandles;
delete(handles.createnetwork);
