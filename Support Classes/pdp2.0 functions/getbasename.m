function varargout = getbasename(varargin)
% GETBASENAME M-file for getbasename.fig
%      GETBASENAME, by itself, creates a new GETBASENAME or raises the existing
%      singleton*.
%
%      H = GETBASENAME returns the handle to a new GETBASENAME or the handle to
%      the existing singleton*.
%
%      GETBASENAME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GETBASENAME.M with the given input arguments.
%
%      GETBASENAME('Property','Value',...) creates a new GETBASENAME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before getbasename_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to getbasename_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help getbasename

% Last Modified by GUIDE v2.5 18-Feb-2008 16:17:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @getbasename_OpeningFcn, ...
                   'gui_OutputFcn',  @getbasename_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before getbasename is made visible.
function getbasename_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to getbasename (see VARARGIN)

defaultstr = varargin{1};
ext = varargin{2};
set(handles.extedit,'String',ext);
set(handles.basenameedit,'String',defaultstr);
setappdata(findobj('tag','setwriteopts'),'basefilename',[]);
% Choose default command line output for getbasename
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes getbasename wait for user response (see UIRESUME)
% uiwait(handles.getbasename);


% --- Outputs from this function are returned to the command line.
function varargout = getbasename_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function basenameedit_Callback(hObject, eventdata, handles)


function basenameedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function extpop_Callback(hObject, eventdata, handles)


function extpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function getbnameokbtn_Callback(hObject, eventdata, handles)
setappdata(findobj('tag','setwriteopts'),'basefilename',get(handles.basenameedit,'String'));
delete(handles.getbasename);

function getbnamecancelbtn_Callback(hObject, eventdata, handles)
setappdata(findobj('tag','setwriteopts'),'basefilename',[]);
delete(handles.getbasename);



function extedit_Callback(hObject, eventdata, handles)


function extedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


