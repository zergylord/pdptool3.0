#include "mex.h"
#include "matrix.h"
#include "string.h"
void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
           {
               mxArray *pools,*proj,*opts;
               double *weight,*dedw,*dweight,*plist,global_lrate,proj_lrate,wt_decay,m;
               int np,npools,nj,wi,wrows,wcols,pnum;
               char *ctype;
               pools = prhs[0];
               opts = prhs[1];
               plist = mxGetData(prhs[2]);
               npools = mxGetNumberOfElements(prhs[2]);
               global_lrate = mxGetScalar(mxGetField(opts,0,"lrate"));
               wt_decay = mxGetScalar(mxGetField(opts,0,"wdecay"));  
               m = mxGetScalar(mxGetField(opts,0,"momentum"));  
               for (np=0;np< npools; np++)
               {
                   pnum = (int)plist[np]-1;
                   proj = mxGetField(pools,pnum,"proj");
                   for (nj=0;nj < mxGetNumberOfElements(proj);nj++)
                   {   
                        ctype = mxArrayToString(mxGetField(proj,nj,"constraint_type"));
                        if (strcasecmp(ctype,"scalar"))
                        {
                            proj_lrate = mxGetScalar(mxGetField(proj,nj,"lrate"));
                            if (mxIsNaN(proj_lrate))
                            {
                               proj_lrate = global_lrate;
                            }
                            weight = mxGetPr(mxGetField(proj,nj,"weight"));
                            dweight = mxGetPr(mxGetField(proj,nj,"dweight"));
                            dedw = mxGetPr(mxGetField(proj,nj,"dEdw"));
                            wrows = mxGetM(mxGetField(proj,nj,"weight"));
                            wcols = mxGetN(mxGetField(proj,nj,"weight"));
                            if (wt_decay && m)
                            {
			                   for (wi=0;wi < wrows*wcols; wi++)
                               {
                                   dweight[wi] = (proj_lrate * dedw[wi]) + (m * dweight[wi]) - wt_decay * weight[wi];
                                   weight[wi] += dweight[wi];
                                   dedw[wi] = 0.0;
                               }
                            }
                            else if (m)
                            {
                              for (wi=0;wi < wrows*wcols; wi++)
                              {
                                  dweight[wi] = (proj_lrate * dedw[wi]) + (m * dweight[wi]);
                                  weight[wi] += dweight[wi];
                                  dedw[wi] = 0.0;
                              }
                            }
                            else if (wt_decay)
                            {
                              for (wi=0;wi < wrows*wcols; wi++)
                              {
                                  dweight[wi] = (proj_lrate * dedw[wi]) - wt_decay * weight[wi];
                                  weight[wi] += dweight[wi];
                                  dedw[wi] = 0.0;
                              }
                            }
                            else
                            {
                              for (wi=0;wi < wrows*wcols; wi++)
                              {
                                  dweight[wi] = (proj_lrate * dedw[wi]);
                                  weight[wi] += dweight[wi];
                                  dedw[wi] = 0.0;
                              }
                            }
                        }
                     mxFree(ctype);
                    }
               }
}
