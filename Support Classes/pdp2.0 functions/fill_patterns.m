function fill_patterns(listbox,data)
str = cell(numel(data),1);
for i=1: numel(data)
    if isnumeric(data(i).ipattern)
       strpat = num2str(data(i).ipattern);
    else
       strpat = char(data(i).ipattern);
    end
    str{i}= sprintf('%s : %s',data(i).pname,strpat);
    if isfield (data(i),'tpattern')
        str{i} = sprintf('%s  |  %s',str{i},num2str(data(i).tpattern));
    end
end
set(listbox,'String',str,'Value',1);