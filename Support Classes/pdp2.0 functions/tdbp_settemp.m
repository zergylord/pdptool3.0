function tdbp_settemp()
global net ltemp ctemp ntemp coolrate;
%annealing stuff
ltemp = size(net.trainopts.annealsched,1);
ntemp = 1;
ctemp=1;
anneal = net.trainopts.annealsched;
if ~isempty(net.trainopts.annealsched) && ltemp ~= ntemp
   ctemp=ntemp;
   ntemp = ntemp+1;
   coolrate = (anneal(ctemp,2) - anneal(ntemp,2))/anneal(ntemp,1);
   net.trainopts.temp = annealing(net.epochno);
elseif ~isempty(net.trainopts.annealsched) && ltemp == 1
   coolrate = 0;
   net.trainopts.temp = anneal(1,2);
end

function temp = annealing(iter)
% Adjusts temperature for softmax policy based on the annealing schedule 
% specified by user. It linearly interpolates between temperature-time
% milestones given by 'anneal' matrix and returns temperature for current
% iteration.

temp = 0.0;
global net ntemp ctemp ltemp coolrate;
anneal = net.trainopts.annealsched;
if isempty (anneal)
   return;
end
if iter >= anneal(ltemp,1) 
   temp = anneal(ltemp,2);
end
if iter >= anneal(ntemp,1)
   temp = anneal(ntemp,2);
   if (ntemp ~= ltemp)
       ctemp = ntemp;
       ntemp = ntemp + 1;
   end
   coolrate = (anneal(ctemp,2) - anneal(ntemp,2))/ ...
              (anneal(ntemp,1) - anneal(ctemp,1));
else
   temp = anneal(ctemp,2) - (coolrate * (iter - anneal(ctemp,1)));
end
