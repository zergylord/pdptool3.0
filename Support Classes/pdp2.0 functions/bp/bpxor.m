% In nogui mode, this script can be run by typing 'pdptool nogui bpxor.m'
% on the matlab command line.
pdp;
loadscript ('xor.net');
loadpattern ('file','xor.pat','usefor','both');
loadtemplate ('xor.tem')
settrainopts ('nepochs',30,'ecrit',0.04,'lgrain','epoch');
launchnet;
loadweights ('xor.wt');
setoutputlog ('file', 'xorlog1.mat','process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','tss'},'onreset', 'new','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', 'xorlog1.mat', 'plotnum', 1, 'yvariables', {'tss'}, 'ylim', [0 1.15]);
runprocess ('process','test','mode','run','granularity','pattern','count',1,'alltest',1,'range',[1 0;4 0]);
% This can be also be written as runprocess('alltest',1, range[1 0;4 0])
% since the rest of the properties have default values as above.