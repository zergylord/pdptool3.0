% In nogui mode, this script can be run by typing 'pdptool nogui bpxor.m'
% on the matlab command line.
pdp;
loadscript ('xor.net');
loadpattern ('file','xor.pat','usefor','both');
loadtemplate ('xor.tem')
settrainopts ('nepochs',30,'ecrit',0.04,'lgrain','epoch');
launchnet;
loadweights ('xor.wt');
filename = getfilename('xorlog','.mat');
setoutputlog ('file', filename,'process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','tss'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'tss'});
filename = getfilename('xoract','.mat');
setoutputlog ('file', filename,'process', 'train','frequency', 'patsbyepoch','status', 'on','writemode', 'binary','objects', {'epochno','pool(4).activation'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'pool(4).activation'}, 'ylim', [0 1]);
runprocess ('process','train','granularity','epoch','count',1,'range',[1 1;4 30]);
% runprocess ('process','train','granularity','epoch','count',1,'range',[1 31;4 60]);
% runprocess ('process','train','granularity','epoch','count',1,'range',[1 61;4 90]);