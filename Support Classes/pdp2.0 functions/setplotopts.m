function varargout = setplotopts(varargin)
% SETPLOTOPTS M-file for setplotopts.fig
%      SETPLOTOPTS, by itself, creates a new SETPLOTOPTS or raises the existing
%      singleton*.
%
%      H = SETPLOTOPTS returns the handle to a new SETPLOTOPTS or the handle to
%      the existing singleton*.
%
%      SETPLOTOPTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETPLOTOPTS.M with the given input arguments.
%
%      SETPLOTOPTS('Property','Value',...) creates a new SETPLOTOPTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setplotopts_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setplotopts_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setplotopts

% Last Modified by GUIDE v2.5 08-Aug-2010 17:45:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setplotopts_OpeningFcn, ...
                   'gui_OutputFcn',  @setplotopts_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before setplotopts is made visible.
function setplotopts_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setplotopts (see VARARGIN)

% Choose default command line output for setplotopts
global outputlog listlog;
handles.output = hObject;
selectedvars = findobj('tag','rightlist');
vars = cellstr(get(selectedvars,'String'));
set(findobj('tag','yvarlist'),'String',vars(2:end));
lval = listlog.index; %get(findobj('tag','logfilepop'),'Value');
plotdata = struct('plotpos',[],'yvariables',{''},'title','','plot3d',0,...
           'az',0,'el',90,'tag','','xlabel','','ylabel','','ylimit',[]);
if lval > 0 && ~isempty(outputlog) && numel(outputlog)> lval && ~isempty(outputlog(lval).graphdata)
   nplots = numel(outputlog(lval).graphdata);
   [plotdata(1:nplots)] = deal(plotdata);   
    for i = 1:nplots
        plotdata(i).plotpos = outputlog(lval).graphdata(i).plotnum;
        plotdata(i).yvariables = outputlog(lval).graphdata(i).yvariables; 
        plotdata(i).title = outputlog(lval).graphdata(i).title;
        plotdata(i).plot3d = outputlog(lval).graphdata(i).plot3d;
        plotdata(i).az = outputlog(lval).graphdata(i).az;
        plotdata(i).el = outputlog(lval).graphdata(i).el;
        plotdata(i).tag = outputlog(lval).graphdata(i).tag;        
        plotdata(i).xlabel = outputlog(lval).graphdata(i).xlabel;        
        plotdata(i).ylabel = outputlog(lval).graphdata(i).ylabel;
        plotdata(i).ylimit = outputlog(lval).graphdata(i).ylim;
    end
   setappdata(hObject,'plotlayout',outputlog(lval).plotlayout);
   entries = cell(outputlog(lval).plotlayout(1)*outputlog(lval).plotlayout(2),1);   
   k = 1;
   for r = 1:outputlog(lval).plotlayout(1)
       for c = 1:outputlog(lval).plotlayout(2)
           entries{k} = sprintf('(%d,%d)',r,c);
           k = k + 1;
       end
   end
   set(findobj('tag','subplotpop'),'String',entries,'Value',1);
   set(allchild(findobj('tag','propertypnl')),'enable','on');
   set(findobj('tag','yvarlist'),'enable','off');
   set(findobj('tag','titleedit'),'String',plotdata(1).title);
   set(findobj('tag','tagedit'),'String',plotdata(1).tag);   
   set(findobj('tag','xlabeledit'),'String',plotdata(1).xlabel);
   set(findobj('tag','ylabeledit'),'String',plotdata(1).ylabel);
   if isempty(plotdata(1).ylimit)
      set(findobj('tag','ylimautochk'),'Value',1);
      set(findobj('tag','minylimedit'),'enable','off');
      set(findobj('tag','maxylimedit'),'enable','off');      
   else
      set(findobj('tag','ylimautochk'),'Value',0);
      set(findobj('tag','minylimedit'),'String',plotdata(1).ylimit(1), 'enable','on');
      set(findobj('tag','maxylimedit'),'String',plotdata(1).ylimit(2), 'enable','on');  
   end
   set(findobj('tag','plot3dchk'),'Value',plotdata(1).plot3d);
   set(findobj('tag','azedit'),'String',plotdata(1).az);
   set(findobj('tag','eledit'),'String',plotdata(1).el);
   set(allchild(findobj('tag','plotlayoutpnl')),'enable','off');
end
setappdata(hObject,'plotdata',plotdata);       
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes setplotopts wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = setplotopts_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function numplotedit_Callback(hObject, eventdata, handles)
numplots = get(hObject,'String');
set(handles.rowsedit,'String',numplots);
set(handles.colsedit,'String',1);

function numplotedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rowsedit_Callback(hObject, eventdata, handles)


function rowsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)


function edit3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function continuebtn_Callback(hObject, eventdata, handles)
plotdata = getappdata(handles.setplotopts,'plotdata');
if strcmpi(get(hObject,'String'),'Start Over')
  qstring = sprintf('This will restart the plot definition process.\nDo you want to continue?');
  buttons = questdlg(qstring);
  if strcmpi(buttons,'Yes')
     set(allchild(handles.propertypnl),'enable','off');
     set(handles.rowsedit,'enable','on');      
     set(handles.colsedit,'enable','on');
     set(hObject,'String','Set axes properties');
     plotdata = struct('plotpos',[],'yvariables',{''},'title','',...
                'plot3d',0,'az',0,'el',90,'tag','','xlabel','',...
                'ylabel','','ylimit',[]);
  end    
else
   numrows = str2double(get(handles.rowsedit,'String'));
   numcols = str2double(get(handles.colsedit,'String'));
   setappdata(handles.setplotopts,'plotlayout',[numrows numcols]);
   entries = cell(numrows*numcols,1);   
   k = 1;
   for r = 1:numrows
       for c = 1:numcols
           entries{k} = sprintf('(%d,%d)',r,c);
           k = k + 1;
       end
   end
   set(handles.subplotpop,'String',entries);
   set(allchild(handles.propertypnl),'enable','on');
   set(handles.yvarlist,'BackgroundColor','yellow');
   set(handles.rowsedit,'enable','off');      
   set(handles.colsedit,'enable','off'); 
   set(handles.ylimautochk,'Value',1);
   set(handles.minylimedit,'enable','off');
   set(handles.maxylimedit,'enable','off');
   set(hObject,'String','Start Over');
end
setappdata(handles.setplotopts,'plotdata',plotdata);     

function cancelbtn_Callback(hObject, eventdata, handles)
delete (handles.setplotopts);

function subplotpop_Callback(hObject, eventdata, handles)
plotdata = getappdata(handles.setplotopts,'plotdata');
val = get(hObject,'value');
positions = {plotdata.plotpos};
posvec = cell2mat(positions);
index = find(posvec == val,1);
if isempty(index)
   return;
end
set(handles.titleedit,'String',plotdata(index).title);
set(handles.tagedit,'String',plotdata(index).tag);   
set(handles.xlabeledit,'String',plotdata(index).xlabel);
set(handles.ylabeledit,'String',plotdata(index).ylabel);
if isempty(plotdata(index).ylimit)
   set(handles.ylimautochk,'Value',1);
   set(handles.minylimedit,'enable','off');
   set(handles.maxylimedit,'enable','off');
else
   set(handles.ylimautochk,'Value',0);
   set(handles.minylimedit,'String',plotdata(index).ylimit(1),'enable','on');
   set(handles.maxylimedit,'String',plotdata(index).ylimit(2),'enable','on');      
end
yvars = plotdata(index).yvariables;
listvars = cellstr(get(handles.yvarlist,'String'));
selected = find(ismember(listvars,yvars));
if isempty(selected)
   selected = 1;
end
set(handles.yvarlist,'Value',selected);
set(handles.plot3dchk,'Value',plotdata(index).plot3d)
set(handles.azedit,'String',plotdata(index).az)
set(handles.eledit,'String',plotdata(index).el)

function subplotpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function yvarlist_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function yvarlist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function titleedit_Callback(hObject, eventdata, handles)


function titleedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tagedit_Callback(hObject, eventdata, handles)


function tagedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function xlabeledit_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');


function xlabeledit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit7_Callback(hObject, eventdata, handles)


function edit7_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function minylimedit_Callback(hObject, eventdata, handles)


function minylimedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function maxylimedit_Callback(hObject, eventdata, handles)


function maxylimedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function savebtn_Callback(hObject, eventdata, handles)


function resetbtn_Callback(hObject, eventdata, handles)
editboxes = findobj(findobj('tag','propertypnl'),'style','edit');
set(editboxes,'String', ' ');
plotdata = getappdata(handles.setplotopts,'plotdata');
v = get(handles.subplotpop,'Value');
if isempty(plotdata(v).yvariables)
   set(editboxes,'BackgroundColor','white');
end
set(handles.yvarlist,'Value',1);

function ylimautochk_Callback(hObject, eventdata, handles)
if get(hObject,'Value') == 0
   set(handles.minylimedit,'enable','on');
   set(handles.maxylimedit,'enable','on');   
else
   set(handles.minylimedit,'enable','off');    
   set(handles.maxylimedit,'enable','off');   
end


function okbtn_Callback(hObject, eventdata, handles)
plotdata = getappdata(handles.setplotopts,'plotdata');
plotlayout = getappdata(handles.setplotopts,'plotlayout');
if ~isempty(plotdata(1).plotpos)
   yvars = cellstr(get(handles.yvarlist,'String'));
   setyvars = {plotdata.yvariables};
   notset = find(cellfun('isempty',setyvars));
   if ~isempty(notset)
      [plotdata(notset).yvariables] = deal(yvars');
   end
   setappdata(findobj('tag','setwriteopts'),'plotdata',plotdata);
   setappdata(findobj('tag','setwriteopts'),'plotlayout',plotlayout);   
end
delete(handles.setplotopts);



function titleedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function tagedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function ylabeledit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');


function applybtn_Callback(hObject, eventdata, handles)
plotdata = getappdata(handles.setplotopts,'plotdata');
val = get(handles.subplotpop,'Value');
positions = {plotdata.plotpos};
posvec = cell2mat(positions);
if isempty(posvec)
   index = 1;
else
  index = find(posvec == val);
  if isempty(index)
   index = numel(plotdata) + 1;
  end
end
plotdata(index) = struct('plotpos',[],'yvariables',{''},'title','',...
                  'plot3d',0,'az',0,'el',90,'tag','','xlabel','',...
                  'ylabel','','ylimit',[]);
plotdata(index).plotpos = val;
yvars = cellstr(get(handles.yvarlist,'String'));
vals = get(handles.yvarlist,'Value');
plotdata(index).yvariables = yvars(vals)';
title = strtrim(get(handles.titleedit,'String'));
if ~isempty(title)
   plotdata(index).title = title;
end
tag = strtrim(get(handles.tagedit,'String'));
if ~isempty(tag)
   plotdata(index).tag = tag;
end
xlabel = strtrim(get(handles.xlabeledit,'String'));
if ~isempty(xlabel)
   plotdata(index).xlabel = xlabel;
end                
ylabel = strtrim(get(handles.ylabeledit,'String'));
if ~isempty(ylabel)
   plotdata(index).ylabel = ylabel;
end
if get(handles.ylimautochk,'Value')== 0
   min = str2double(get(handles.minylimedit,'String'));
   max = str2double(get(handles.maxylimedit,'String'));
   if ~isnan([min max])
      plotdata(index).ylimit = [min max];
   end
end
plotdata(index).plot3d = get(handles.plot3dchk,'Value');
az = str2double(strtrim(get(handles.azedit,'String')));
el = str2double(strtrim(get(handles.eledit,'String')));
if ~isnan(az) && ~isnan(el)
    plotdata(index).az = az;
    plotdata(index).el = el;
end
setappdata(handles.setplotopts,'plotdata',plotdata);
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');

function minylimedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



function maxylimedit_KeyPressFcn(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



% --- Executes on button press in plot3dchk.
function plot3dchk_Callback(hObject, eventdata, handles)
% hObject    handle to plot3dchk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot3dchk
if get(hObject,'Value')
   set(handles.azedit,'String','4');
   set(handles.eledit,'String','4');
else
   set(handles.azedit,'String','0');
   set(handles.eledit,'String','90');
end


function azedit_Callback(hObject, eventdata, handles)
% hObject    handle to azedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of azedit as text
%        str2double(get(hObject,'String')) returns contents of azedit as a double


% --- Executes during object creation, after setting all properties.
function azedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to azedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eledit_Callback(hObject, eventdata, handles)
% hObject    handle to eledit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eledit as text
%        str2double(get(hObject,'String')) returns contents of eledit as a double


% --- Executes during object creation, after setting all properties.
function eledit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eledit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
