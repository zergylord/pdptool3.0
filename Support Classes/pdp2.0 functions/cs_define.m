function definednet = cs_define(net)
% This function is called for setting up constraint satisfaction networks.
% 'n' is the network struct variable with valid fieldnames for 'cs'
% networks.'basepool' defines a valid pool structure. 'baseproj' defines
% valid projection structure.'testopts' defines valid testing parameters. 
% fillfields funciton populates these structure variables with values from 
% the given input parameter 'net'.setcsdefaultvars initializes the vectors 
% and matrices with default values.
%
% Global variable PDPAppdata contains  runtime information for the
% current pdptool session. PDPAppdata.networks is a list of all currently 
% loaded networks (feature supported only for pdptool in gui mode).
% 'editable' variable can be used to specify user-modifiable network
% fields. These are then used by the object viewer module when invoked from
% pdptool mainwindow menu item 'Launch object viewer'.
% This is also used only when running pdptool in gui mode.
%
% **Important note for enhancement** : To add a new network type, you would
% need to define struct variables specific to the new type by creating a
% file like this with exactly similar function signature. The easiest method 
% would be copying the file content in its entirety into a new file, change
% function name to <newtype>_define , save it as <newtype>_ define.m file. In
% the new file all you have to do is change fields of struct variables 
% 'n','basepool','baseproj','tropts','tstopts' - add/remove fieldnames
% according to your needs for the new type.Make corresponding changes to
% set<newtype>defaultvars subroutine. Type 'help struct' on matlab command
% prompt to get more information on syntax of MATLAB struct function.

global editable PDPAppdata;
networks = PDPAppdata.networks;
namedef = sprintf('net%d',numel(networks)+1);
n = struct('name',namedef,'type','cs','seedmode',1,'seed',0,'pool',[],...
          'num',1,'goodness',0.0,'unitno',1,'updateno',0,'cycleno',0,...
          'cuname','','patno',0,'cpname','','nancolor',[.97 .97 .97],...
           'templatefile','','outputfile','','logfile','','testopts',[]);
basepool = struct('name','pool','type','Input','nunits',1,'unames',[],...
                 'proj',[],'activation',[],'intinput',[],'extinput',[],...
                 'netinput',[]);
baseproj = struct('frompool','bias','constraint_type','scalar',...
                  'constraint',0.0,'weight',[]);
tstopts = struct('nupdates',100,'ncycles',10,'actfunction','schema','istr',1,...
                'estr',1,'clamp',0,'harmony',0,'kappa',0,'temp',0,...
                'annealsched',[],'testset','none');  
n = fillfields(n,net);
net = bias_define(net);
p(1:length(net.pool)) = basepool;
for i = 1:numel(net.pool)
    p(i) = fillfields(p(i),net.pool(i));
    prj = baseproj;
    if isfield(net.pool(i),'proj')&& numel(net.pool(i).proj)>0
       for j = 1:numel(net.pool(i).proj)
           prj(j) = fillfields(baseproj,net.pool(i).proj(j));
       end
    p(i).proj = prj;            
    end
end
n.pool = p;
n.testopts = tstopts;
if isfield(net,'testopts')
    n.testopts = fillfields(n.testopts,net.testopts);
end
if n.seedmode == 0       % 0 - seed provided, 1 - seed randomly
    if exist('RandStream','file') == 2 % for version 7.8 and above
        stream = RandStream('mt19937ar','Seed',n.seed);
        RandStream.setDefaultStream(stream);
    else
        rand('seed',n.seed); % for backward compatibility
        randn('seed',n.seed);                
    end
else
    if exist('RandStream','file') == 2 % for version 7.8 and above    
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        RandStream.setDefaultStream(stream);
        n.seed = stream.Seed;
    else
       n.seed = rand('seed');
       randn('seed',n.seed);
    end
end
editable.net = {'seed'};
editable.pool = {'activation','intinput','extinput','netinput'};
editable.proj = {'constraint','weight'};
editable.testopts = fieldnames(rmfield(tstopts,'testset'));
definednet = setcsdefaultvars(n); 
 
function net = setcsdefaultvars(net)
 for i=1:numel(net.pool)
     vecsize = [1 net.pool(i).nunits];
     net.pool(i).activation = repmat(0,vecsize);
     net.pool(i).netinput = repmat(0,vecsize);
     net.pool(i).intinput = repmat(0,vecsize);
     net.pool(i).extinput = repmat(0,vecsize);
     if ~isempty(net.pool(i).proj)
         matx = net.pool(i).nunits;
         pools = {net.pool.name};
         for j=1:numel(net.pool(i).proj)
             from = net.pool(i).proj(j).frompool;
             [tf,np] = ismember(from,pools);
             maty = net.pool(np).nunits;
             matsz = [matx maty];
             net.pool(i).proj(j).weight = repmat(1,matsz) .* ...
                                          net.pool(i).proj(j).constraint;
          end
     end
 end
net.pool(1).activation = 1;  %bias 