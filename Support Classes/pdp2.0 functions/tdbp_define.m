function definednet = tdbp_define(net)
% This function is called for setting up Temporal-Differentfeed-forward backpropagation
% networks. 'n' is the network struct variable with valid fieldnames for 
% 'tdbp' networks. 'basepool' defines a valid pool structure. 
% 'baseproj' defines valid projection structure.'trainopts' defines valid 
% training paramters. 'testopts' defines valid testing parameters.
% fillfields routine populates these structure variables with values from
% the given input parameter 'net'.setbpdefaultvars initializes the vectors 
% and matrices with default values.
%
% Global variable PDPAppdata contains  runtime information for the
% current pdptool session. PDPAppdata.networks is a list of all currently 
% loaded networks (feature supported only for pdptool in gui mode).
% 'editable' variable can be used to specify user-modifiable network
% fields. These are then used by the object viewer module when invoked from
% pdptool mainwindow menu item 'Launch object viewer'.
% This is also used only when running pdptool in gui mode.
%
% **Important note for enhancement** : To add a new network type, you would
% need to define struct variables specific to the new type by creating a
% file like this with exactly similar function signature. The easiest method 
% would be copying the file content in its entirety into a new file, change
% function name to <newtype>_define , save it as <newtype>_define.m file. In
% the new file all you have to do is change fields of struct variables 
% 'n','basepool','baseproj','tropts','tstopts' - add/remove fieldnames
% according to your needs for the new type. Make corresponding changes to
% set<newtype>defaultvars subroutine. Type 'help struct' on matlab command
% prompt to get more information on syntax of MATLAB struct function.

global editable PDPAppdata;
global display_end_state;
display_end_state = 0;

networks = PDPAppdata.networks;
namedef=sprintf('net%d',numel(networks)+1);
n= struct('name',namedef,'type','tdbp','seedmode',1,'seed',0,...
          'pool',[],'num',1,'ninputs',0,'noutputs',0,'epochno',0,...
          'trainpatno',0,'testpatno',0,'cpname','','nancolor',[.97 .97 .97],...
          'templatefile','','outputfile','','logfile','','trainopts',[],...
          'testopts',[],'netmode','','reward',0,'steps',0);
basepool = struct('name','pool','type','input','nunits',1,'unames',[],...   %at run time, an 'error' field is added - this prevents its being selected as a display item
                 'proj',[],'netinput',[],'activation',[],'prevactivation',[],'delta',[],...
                 'gamma',nan,'actfunction',[]);
baseproj = struct('frompool','bias','fromindex',1,'constraint',0.0,...      % at run time, an 'eligtrace' field is added
                  'constraint_type','scalar','weight',[],'dweight',[],...
                  'lrate',nan,'lambda',nan);
tropts = struct('nepochs',500,'lflag',1,'lrate',0.5,'lambda',0.7,'gamma',.9,...
                'wrange',1,'lgrain','pattern','wdecay',0.0,'lgrainsize',1,'mu',0.5,...
                'clearval',0.5,'trainset','none','policy','egreedy','epsilon',0.1,'showvals',0,...
                'annealsched',[],'temp',1,'runstats',0);
tstopts =struct('nepochs',1,'stepcutoff',100,'lflag',0,...
                'mu',0.5,'wdecay',0.0,'clearval',0.5,'lrate',0.5,'lambda',0.7,...
                'lgrain','pattern','lgrainsize',1,...
                'momentum',0.9,'testset','none','policy','greedy','epsilon',0.1,...
                'showvals',0,'temp',1,'runstats',0); 
                
n = fillfields(n,net);
net = bias_define(net);   
p(1:length(net.pool)) = basepool;
for i=1:numel(net.pool)
    p(i) = fillfields(p(i),net.pool(i));
    prj = baseproj;        
    if isfield(net.pool(i),'proj') && numel(net.pool(i).proj)>0
       for j = 1:numel(net.pool(i).proj)
           prj(j) = fillfields(baseproj,net.pool(i).proj(j));
       end
       p(i).proj = prj;            
    end
end
n.pool = p;
n.trainopts = tropts;
if isfield(net,'trainopts')
    n.trainopts = fillfields(n.trainopts,net.trainopts);
end
n.testopts = tstopts;
if isfield(net,'testopts')
    n.testopts = fillfields(n.testopts,net.testopts);
end
if n.seedmode == 0       % 0 - seed provided, 1 - seed randomly
    if exist('RandStream','file') == 2 % for version 7.8 and above
        stream = RandStream('mt19937ar','Seed',n.seed);
        RandStream.setDefaultStream(stream);
    else
        rand('seed',n.seed); % for backward compatibility
        randn('seed',n.seed);                
    end
else
    if exist('RandStream','file') == 2 % for version 7.8 and above    
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        RandStream.setDefaultStream(stream);
        n.seed = stream.Seed;
    else
       n.seed = rand('seed');
       randn('seed',n.seed);
    end
end
editable.net = {'seed'};
editable.pool = {'netinput','activation','delta','target','error'};
editable.proj = {'constraint','weight','dweight','wed','pwed','lrate'};
editable.trainopts = fieldnames(rmfield(tropts,'trainset'));
editable.testopts = fieldnames(rmfield(tstopts,'testset'));
definednet=settdbpdefaultvars(n); 

function net = settdbpdefaultvars(net)
types={net.pool.type};
inpools = find(strcmpi(types,'input'));
outpools = find(strcmpi(types,'output'));
net.inindx = inpools;
net.outindx = outpools;
if ~isempty(inpools)
    net.ninputs= sum(cell2mat({net.pool(inpools).nunits}));
end
if ~isempty(outpools)
    net.noutputs = sum(cell2mat({net.pool(outpools).nunits}));
end
 for i=1:numel(net.pool)
     vecsize = [1 net.pool(i).nunits];
     net.pool(i).netinput = repmat(0,vecsize);     
     net.pool(i).activation = repmat(0,vecsize);
     net.pool(i).prevactivation = repmat(0,vecsize);
     net.pool(i).actderiv = repmat(0,vecsize);
     net.pool(i).delta = repmat(0,vecsize);
     if ~isempty(net.pool(i).proj)
         matx = net.pool(i).nunits;
         pools = {net.pool.name};
         for j=1:numel(net.pool(i).proj)
             from = net.pool(i).proj(j).frompool;
             [tf,np] = ismember(from,pools);
             net.pool(i).proj(j).fromindex = np;
             maty = [net.pool(np).nunits];
             matsz = [matx maty];
             switch lower(net.pool(i).proj(j).constraint_type)
                 case 'scalar'
                       net.pool(i).proj(j).weight = repmat(1,matsz) .* ...
                                          net.pool(i).proj(j).constraint;
                 case 'random'
                       net.pool(i).proj(j).weight = (rand(matsz)-0.5) * ...
                                                      net.trainopts.wrange;  
                 case 'prandom'
                       net.pool(i).proj(j).weight = rand(matsz) * ...
                                                      net.trainopts.wrange;
                       w = net.pool(i).proj(j).weight;
                       net.pool(i).proj(j).weight(w < 0) = 0.0;                                           
                 case 'nrandom'
                       net.pool(i).proj(j).weight = (rand(matsz)-1) * ... 
                                                      net.trainopts.wrange;
                       w = net.pool(i).proj(j).weight;
                       net.pool(i).proj(j).weight(w > 0) = 0.0;  
                 case 'copyback'
                       net.pool(i).proj(j).weight = repmat(0,matsz);
             end
             net.pool(i).proj(j).dweight = repmat(0,matsz);                                      
          end
     end    
 end
net.pool(1).activation = 1;  %bias 