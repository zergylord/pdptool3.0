function refresharrays()
% Must be called for network of type 'cs'
global net;
net.cycleno = 0;
cssettemp();
% %annealing stuff
% ltemp = size(net.testopts.anneal,1);
% ntemp = 1;
% ctemp=1;
% anneal = net.testopts.anneal;
% net.goodness = 0;
% net.updateno = 0;
% net.cuname='';
% if ~isempty(net.testopts.anneal) && ltemp ~= ntemp
%    ctemp=ntemp;
%    ntemp = ntemp+1;
%    coolrate = (anneal(ctemp,2) - anneal(ntemp,2))/anneal(ntemp,1);
%    net.testopts.temperature = annealing(net.cycleno);
% end
for i=1:numel(net.pools)
    vecsize = [1 net.pools(i).unit_count];
    net.pools(i).activation(:) = 0; %repmat(0,vecsize);
    net.pools(i).net_input(:) = 0; %repmat(0,vecsize);
    %net.pools(i).intinput(:) = 0; %repmat(0,vecsize);  %commented this
    %line out
    if (net.test_options.clamp)
       z = net.pools(i).extern_input;
       net.pools(i).activation(z == 1) = 1;
       net.pools(i).activation(z == -1) = 0;
    end
end
net.pools(1).activation=1; %bias unit
    