pdp
loadscript ('3by3grid.net');
loadpattern ('file','trashgrid.m','setname','trashgrid','usefor','both');
loadtemplate ('3by3grid.tem');
launchnet;
settrainopts ('nepochs',350,'lambda',0.5,'gamma',0.7,'trainset','trashgrid','policy','softmax','annealsched',[0 1]);
settestopts ('nepochs',1,'testset','trashgrid','policy','softmax');
setseed(214251);
tdbp_reset;