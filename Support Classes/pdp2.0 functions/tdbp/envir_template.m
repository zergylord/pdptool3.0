classdef envir_template
    % This is the skeletal structure for a PDPTool environment class for
    % use with the TDBP module.  All required and optional methods are
    % present, with explanations of the requisite functions. Replace
    % "environment" after the classdef statement above with the name of the
    % environment, and name this .m file the same name.
    
    properties
        %All class-internal variables go here.
        lflag;  % lflag is the only required property. It allows the modeler
                % to control which states learning takes place on. Its main
                % function is in tasks where the network learns by
                % self-playing. When this variable is 0, the network can
                % choose the next move without the state becoming a part of
                % the network's eligibility trace, and thus participating
                % in temporal credit assignment. There could be a range of
                % tasks in which this behavior is desirable. This variable
                % should be changed in the setNextState() method.
                
        % Place environment variables here. These variables keep track of
        % the current state, the number of steps taken so far in the
        % episode, etc. They will vary based on the nature of the
        % environment.
    end
    
    methods
        % The methods here are divided by their conceptual relation to the
        % operation of the algorithm. Many of the methods are for control
        % purposes and do not have conceptual analogs in the formal
        % specification of reinforcement learning outlined in the handbook.
        % The other methods do have conceptual analogs in the formal
        % characterization. All functions that change variables in this
        % class take the environment object itself as an argument. All
        % properties of the environment class must be referenced within
        % these functions as obj.property_name. 
        
        % ########################
        % CONTROL METHODS
        % ########################
        
        % This is the main constructor for the environment object. The name
        % of this function must match the name of the class as specified
        % after the classdef statement above, so change it accordingly.
        % Other than that necessary change, nothing need be added to this
        % function. This function is called at the beginning of each new
        % training run, but the reset() function will be called after each
        % episode. Thus, it is possible to collect data over the course of
        % many episodes by setting up variables of data structures in this
        % function. These variables will be initialized when a training run
        % starts (i.e. by clicking the Train button in the GUI), and will
        % persist across all episodes in the training run. The data will be
        % lost upon starting another training run. Use the runStats()
        % method to manipulate and display data collected over the course
        % of a training or testing run.
        function obj = environment_template()
            obj = obj.construct();
            
            % Initialize optional variables here that persist over a training run.
        end
        
        % This is the function that should do the majority of setting up
        % each episode of the environment. This function should be called
        % both upon creation of the environment object (i.e. called from
        % the contruction function directly above) and after every episode
        % of the task (i.e. called from the reset(obj) function).
        % Thus, variable initializations occuring in this function will be 
        % reinitialized after each episode.
        function obj = construct(obj)
            obj.lflag = 1;  %This enables learning.
            
            % Place non-persisting (duration of an episode only) 
            % variable initializations here.
        end
        
        % This function resets the environment after each episode. It does
        % this by calling our main initialization function, construct(obj).
        % Before construct(obj) is called, however, you must save any
        % relevant data they may have been saved over the course of the
        % episode in persisting variables that were initialized in the
        % environment construction function, i.e. the one appearing here as
        % environment().
        function obj = reset(obj)
            % 
            obj = obj.construct();
        end
        
        % This optional function allows you to do operations on data that 
        % have been collected over the course of a training or testing run.
        % These variables must be initialized in the main construction 
        % function (appearing here as environment()). You may also wish to 
        % save log files here. This function is optional. You can display 
        % output from this function in the network viewer window by 
        % selecting the "Runstats" checkbox in training or testing options 
        % window. This function must return a cell array on strings to 
        % display in the pattern list at the end of a training or testing run.
        function add_to_list = runStats(obj)
            % Access and manipulate the persisting variables here.
            % Return the results as a cell array of strings. Each string
            % will be printed in the pattern list as a separate line.
            add_to_list = {'This text will appear in the pattern list',...
                'This text will be a second line in the pattern list'};
        end
        
        % This function is an optional function that takes as input the
        % matrix of next states just passed to the network by the
        % getNextStates(obj) function and their associated state values
        % produced by the network. next_states is a matrix with each row
        % being a next input state to the network. state_values is a
        % matrix of corresponding network output, with each row corresponding
        % to the same row in the next_states matrix. This function allows 
        % the modeler to specify a state selection policy not implemented 
        % in the TDBP program. It is especially useful when state selection 
        % must involve consideration of multiple output unit values. See
        % discussion in the handbook.
        function next_state = doPolicy(obj, next_states, state_values)
            % Determine the next state following some user specified policy
            % here. Then return this state to the network in the next_state
            % variable.
        end
        
        % This function should return 1 if the environment is in its
        % terminal state, and 0 otherwise. This is how the TDBP program
        % knows that the episode has ended.
        function endFlag = isTerminal(obj)
            
        end
        
        % ###############
        % FORMAL METHODS
        % ###############
        
        % These methods pass the reward, action, and state signals to the
        % RL agent, and thus have conceptual analogs in the formal
        % specification of the RL agent.
        
        % This function should return a vector of the same length as the
        % number of output units of the network. The vector will be divided
        % across output pools if there is more than one such that the
        % lowest numbered output pool will receive the first portion of the
        % reward vector, the next output pool the second portion, etc. 
        function reward = getCurrentReward(obj)
            
        end
        
        % This function should return a vector of the same length as the
        % number of input units of the network. The vector will be divided
        % across input pools if there is more than one such that the
        % lowest numbered input pool will receive the first portion of the
        % state vector, the next input pool the second portion, etc.
        function state = getCurrentState(obj)

        end
        
        % This function should return a matrix of possible next states or
        % next actions that can be taken based on the current environment
        % state. states is a matrix such that each column corresponds to an
        % input unit of the network and each row is a different environment
        % state. If you are doing SARSA learning, then you will fix a
        % portion of each state vector as the current state, and turn on
        % each of the action units, thereby creating a matrix of
        % state-action pairs. The states will be distributed over multiple
        % input units in the same way described above.
        function states = getNextStates(obj)

        end

        % This function performs the agents action on the environment by
        % setting the current state to the value of next_state. next_state
        % will always be one of the states that was passed to the network
        % for evaluation by the getNextStates(obj) function, or, when you 
        % are using the 'defer' policy, will be a vector of zeros which you
        % should ignore in setting the environment state. In the case of
        % SARSA learning, you will need to test which action unit is on the
        % next_state vector and change the environment accordingly. This is
        % how the network communicates its selected action to your
        % environment class.
        function obj = setNextState(obj, next_state)

        end

        % Other internal functions can be specified for your convenience,
        % but the above functions, unless specified as optional, are
        % necessary for the TDBP program to work correctly. Very cursory
        % tests of these functions will be performed when the environment
        % class is loaded. These tests include only testing if required
        % functions exist, and whether functions returning state or reward
        % vectors are of the correct length, as specified by the number of
        % input and output units in the network. All other testing is the
        % responsibility of the modeler.
        
    end
    
end

