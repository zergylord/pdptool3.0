% In nogui mode, this script can be run by typing 'pdptool nogui cube.m'
% on the matlab command line.
pdp ;
loadscript ('cube.net');
loadtemplate ('cube.tem');
setseed(6543);
launchnet;
settestopts ('actfunction','boltzmann','nupdates',16,'ncycles',20,'istr',1,'estr',1,'annealsched',[0 2.0;20 0.5]);