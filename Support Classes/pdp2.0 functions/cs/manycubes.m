function myhist = manycubes(n,cycles)
global net;
goodlog = 1:n;
x = 0:.5:16;
for i = 1:n
    newstart;
    runprocess ('granularity','cycle','count',cycles,'range', [1 1;cycles 16]);
    goodlog(i) = net.goodness;
end
myhist = histc(goodlog,x);
figure; bar(x,histc(goodlog,x)); set(gca,'xtick',[0:16],'xlim',[-.5 16.5],'ylim',[0 100]);

