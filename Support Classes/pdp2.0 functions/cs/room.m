% In nogui mode, this script can be run by typing 'pdptool nogui room.m'
% on the matlab command line.
pdp ;
loadscript ('room.net');
loadtemplate ('room.tem')
loadpattern ('file','ROOM.PAT','usefor','both');
settestopts ('nupdates',40,'ncycles',50,'istr',0.5,'clamp',1,'annealsched',[0 2;200 0.05]);
launchnet;