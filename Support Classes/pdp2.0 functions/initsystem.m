function initsystem(guiflag)

%This function must only be called internally by the pdptool application
%setup routine. 
%Code summary : guiflag value can be either 1 or 0. 1 means pdptool is to
%be run with gui windows and 0 means non-graphical session. This function
%sets up pdptool session parameters in a global struct variable
%'PDPAppdata'. This also locates MATLAB history.m file for pdptool logging.
%When run in deployed non-Matlab environment, pdptool logging is
%accomplished without look-up of history.m 

global PDPAppdata;
PDPAppdata.networks = [];
PDPAppdata.history = [];
PDPAppdata.lognetwork = 1;
PDPAppdata.logfilename = [];
PDPAppdata.patfiles = [];
PDPAppdata.dispobjects = [];
PDPAppdata.annoteobjects = [];
PDPAppdata.templatesize = [];
PDPAppdata.cellsize = [];
PDPAppdata.netscript = [];
PDPAppdata.trainData = [];
PDPAppdata.testData = [];
PDPAppdata.trinterrupt_flag = 0;
PDPAppdata.tstinterrupt_flag = 0;
PDPAppdata.stopprocess = [];
PDPAppdata.gui = guiflag;

if isdeployed
    PDPAppdata.logfilename = getfilename('pdplog','.m');
else
	pdphistory = [];    
    fn = '';
    if exist('prefdir','builtin')
       fn = prefdir;
    else
       uprofile = getenv('userprofile'); 
       if ~isempty(uprofile)
          releasename = sprintf('R%s',version('-release'));       
          fn = sprintf('%s%s%s%sMathworks%smatlab%s%s',uprofile,filesep,...
               'application Data',filesep,filesep,filesep,releasename);
       end
    end
    if exist(fn,'dir')
       hfilename = sprintf('%s%shistory.m',fn,filesep);
       if exist(hfilename,'file')
          pdphistory.fname = hfilename; %sprintf('%s%shistory.m',fn,filesep);
       end
    end
    if ~isempty(pdphistory) 
       ss = char(textread(pdphistory.fname,'%[^\n]'));             
       pdphistory.histsize = max(1,size(ss,1)-1); % in case ss=[];
       PDPAppdata.logfilename = getfilename('pdplog','.m');
    else
       PDPAppdata.lognetwork = 0;
       fprintf(1,'WARNING : Logging disabled because history file cannot be located\n');
    end
    PDPAppdata.history = pdphistory;
end 
if ~PDPAppdata.gui
   fprintf(1,'Application initialized, ready to load script ...\n');
end