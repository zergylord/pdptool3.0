function n = definenetwork(net)
% n = definenetwork(net)
% 
% This function is part of the pdptool package.It is called from other
% functions within the pdptool application and should not be invoked
% directly through MATLAB command window or outside the pdptool
% environment.
%------------------------------------------------------------------------
% Notes for maintenance/development purposes
%------------------------------------------------------------------------
%
% Overview : This function is called from loadscript.m file when a network 
% is set up by loading a network script or on creating a new network using 
% the create module window in gui mode.
%
% Description : It accepts as input, the variable 'net' defined in the
% script file ,creates a structure of various fields based on its values and
% returns the new structure to the calling routine. The new structure is
% subsquently declared as a global variable 'net' which is used and 
% manipulated for all network functions.The 'net' struct global variable in
% pdptool contains all network information. This is the main object in the
% hierarchy of objects. 'net' contains pools or layers defined as field
% 'pool' of the 'net' variable. net.trainopts contain training paramters,
% net.testopts contain testing parameters.
% A network loaded in pdptool must have a valid type. All valid types are
% represented by abbreviated string literals e.g pa for pattern associator,
% cl for competitive learning etc. The actual network setup is done by 
% functions calle from this file. It first sets up function name to be 
% invoked and uses str2func to create the function handle to be used.
% Thus,depending on the value of 'type' field of input variable 'net', 
% the corresponding function that set up all default network parameters are
% invoked.
%
% **Important note for enhancement** : To add a new network type, you would
% need to start here. Add a new entry to the net_types cell array of
% strings and write a new function in a separate file for the new type. The 
% function signature must be similar to the existing <nettype>_define 
% functions and its name must be <newtype>_define. For further guidance,
% look at each <nettype>_define.m file.

net_types={'pa','iac','cs','bp','srn','rbp','cl','tdbp'};
if ~isfield(net,'type')
    net.type='pa';
end
if ~ismember(net.type,net_types)
    fprintf(1,'\n\n Error : Invalid network type ''%s'', switching to default type ''pa''\n',net.type);    
    net.type='pa';
end
% Function name is created according to type of network being defined.
definefunc=str2func(sprintf('%s_define',net.type));
n = definefunc(net);
