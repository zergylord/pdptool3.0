function varargout = setwriteopts(varargin)
% SETWRITEOPTS M-file for setwriteopts.fig
%      SETWRITEOPTS, by itself, creates a new SETWRITEOPTS or raises the existing
%      singleton*.
%
%      H = SETWRITEOPTS returns the handle to a new SETWRITEOPTS or the handle to
%      the existing singleton*.
%
%      SETWRITEOPTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETWRITEOPTS.M with the given input arguments.
%
%      SETWRITEOPTS('Property','Value',...) creates a new SETWRITEOPTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setwriteopts_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setwriteopts_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setwriteopts

% Last Modified by GUIDE v2.5 05-Feb-2009 17:29:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setwriteopts_OpeningFcn, ...
                   'gui_OutputFcn',  @setwriteopts_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before setwriteopts is made visible.
function setwriteopts_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setwriteopts (see VARARGIN)
global outputlog net listlog PDPAppdata filevarname;
leftstr=cellstr('+ net : <struct>');
leftitemlist(1).parents = [];
leftitemlist(1).sub = 1;
leftitemlist(1).fname = 'net';
setappdata(hObject,'leftlist',leftitemlist);
set(handles.leftlist,'String',leftstr);
[st,ii] = dbstack;
x= numel(st);
freqval =1;
procval=1;
switch net.type
    case 'pa'
        freqlist={'cycle','pattern','epoch'};
        proclist={'train','test','both'};
        if ~isempty(findobj('tag','pa_troption'))
           freqval = 3;
           procval =1;
        else
           if ~isempty(findobj('tag','pa_testoption'))
              freqval =2;
              procval =2;
           end
        end
    case 'cs'
        freqlist={'cycle','update','pattern'};
        proclist={'test'};
    case 'iac'
        freqlist ={'cycle'};
        proclist ={'test'};        
    case 'bp'
        freqlist = {'epoch','pattern','patsbyepoch'};
        proclist = {'train','test','both'};
        if ~isempty(findobj('tag','bp_troption'))        
           freqval = 2;
           procval =1;
        else
           if ~isempty(findobj('tag','bp_testoption'))            
              freqval =2;
              procval =2;
           end
        end
    case 'srn'
        freqlist={'epoch','pattern','sequence'};
        proclist={'train','test','both'};
        if ~isempty(findobj('tag','bp_troption'))                
           freqval = 3;
           procval =1;
        else
           if ~isempty(findobj('tag','bp_testoption'))            
              freqval =2;
              procval =2;
           end
        end
    case 'rbp'
        freqlist={'epoch','pattern','interval','tick','backtick'};
        proclist={'train','test','both'};
        if ~isempty(findobj('tag','rbp_troption'))                
           freqval = 3;
           procval =1;
        else
           if ~isempty(findobj('tag','rbp_testoption'))                        
              freqval =2;
              procval =2;
           end
        end          
    case 'cl'
        freqlist={'epoch','pattern'};
        proclist={'train','test','both'};
        if ~isempty(findobj('tag','cl_testoption'))                    
           freqval =2;
           procval =2;
        end
    case 'tdbp'
        freqlist = {'epoch','pattern'};
        proclist = {'train','test','both'};
        if ~isempty(findobj('tag','tdbp_troption'))
            freqval = 2;
            procval =1;
        else
            if ~isempty(findobj('tag','tdbp_testoption'))
                freqval =2;
                procval =2;
            end
        end
end
set(handles.writefreqpop,'String',freqlist,'Value',freqval);
set(handles.logprocpop,'String',proclist,'Value',procval);
set(allchild(handles.subarraypnl),'Enable','off');
% selpanelctrls = findobj(allchild(findobj('tag','selectpnl')),'-not','type','uipanel');  %uipanel does not have 'enable' property
% set(selpanelctrls,'enable','off');
if isempty(outputlog)
   outputlog=struct('file','','process','','frequency','','status','off',...
                    'writemode','text','objects',{},'cobjects',{},...
                    'onreset','clear','plot','off','graphdata',[],...                    
                    'varstruct',{});
% else
% %     lfiles={outputlog.file};
% %     set(handles.logfilepop,'String',lfiles,'Value',1);
%    if listlog.index > 0
%       setlogvalues(listlog.index);
%       
% %     setlogvalues(1);
%     set(handles.donebtn,'enable','on');
%     set(handles.cancelbtn,'enable','on');
%     set(handles.okbtn,'enable','on');
%    end
end
set(handles.lognameedit,'String',listlog.fname);
if listlog.index > 0
   setlogvalues(listlog.index);
   selpanelctrls = findobj(allchild(findobj('tag','selectpnl')),'-not','type','uipanel');  %uipanel does not have 'enable' property
   set(selpanelctrls,'enable','off');   
   set(handles.donebtn,'enable','on');
   set(handles.cancelbtn,'enable','on');
   set(handles.okbtn,'enable','on');   
else
   set(handles.binarychk,'Value',listlog.binarychk);
   if PDPAppdata.lognetwork
      statement = sprintf('filename = getfilename(''%s'',''%s'');',listlog.base,listlog.ext);
      updatelog(statement);
   end
   filevarname = 'filename';   
end
% Choose default command line output for setwriteopts
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes setwriteopts wait for user response (see UIRESUME)
% uiwait(handles.setwriteopts);


% --- Outputs from this function are returned to the command line.
function varargout = setwriteopts_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function leftlist_Callback(hObject, eventdata, handles)
global net;
if isempty(net)
   return;
end
set(handles.srowpop,'String','1','Value',1);
set(handles.erowpop,'String','1','Value',1);
set(handles.scolpop,'String','1','Value',1);
set(handles.ecolpop,'String','1','Value',1);
rowctrls = findobj(handles.subarraypnl,'-regexp','tag','row');
set(rowctrls,'enable','off'); 
colctrls = findobj(handles.subarraypnl,'-regexp','tag','col');
set(colctrls,'enable','off');
v=get(hObject,'Value');
listitems=get(hObject,'String');
numpads = getleadspaces(listitems{v});
padding(1:numpads)=' ';
index = 1;
currlist = getappdata(handles.setwriteopts,'leftlist');
parents = currlist(v).parents;
fn = currlist(v).fname;
subs = currlist(v).sub;
[t,r]= strtok(listitems{v});  % for struct items t will be + or - and 'r' will be rest of line
if t(1) == '+'
    ind = find(r==':');
    if ~isempty(ind)
        r = r(1:ind-1);
    end
    item = strtrim(r);
    structure =net;
    if ~strcmpi('net', item)
      array_mark = regexp(item,'[()]'); 
      if ~isempty(array_mark)
          index = str2num(item(array_mark(1)+1 : array_mark(2) -1));
      end
      for i=2:numel(parents)
          structure=getfield(structure,parents{i},{subs(i)});
      end
      structure = structure.(fn);
    end
    expstring = expandtree(structure,v,index,padding);
    namecollap = cellstr(sprintf('%s- %s',padding,item));
    newstring= [listitems{1:v-1},namecollap,expstring,listitems{v+1:end}];
    set(hObject,'String',newstring);
    set(handles.addbtn,'enable','off');    
    return;
end
if t(1) == '-'
    item=strtrim(r);
    numremove = collapsetree(v);
    nameexp = {sprintf('%s+ %s : <struct>',padding,item)};
    newstring= [listitems{1:v-1},nameexp,listitems{v+numremove+1:end}];
    set(hObject,'String',newstring);
    set(handles.addbtn,'enable','off');   
    currlist(v+1 : v+ numremove)=[];
    setappdata(handles.setwriteopts,'leftlist',currlist);
    return;
end
structure = net;
for i=2:numel(parents)
    structure=getfield(structure,parents{i},{subs(i)});
end
arritem = structure.(fn);
if ~ischar(arritem) 
    if size(arritem,1) > 1
       set(rowctrls,'enable','on');        
       set(handles.srowpop,'String',1:size(arritem,1),'Value',1);
       set(handles.erowpop,'String',1:size(arritem,1),'Value',size(arritem,1));
    end
    if size(arritem,2) > 1
       set(colctrls,'enable','on');         
       set(handles.scolpop,'String',1:size(arritem,2),'Value',1);
       set(handles.ecolpop,'String',1:size(arritem,2),'Value',size(arritem,2));
    end
end
set(handles.addbtn,'enable','on');

function newitems = expandtree(expstruct,val,ind,pad)
handles = guihandles;
currentlist =  getappdata(handles.setwriteopts,'leftlist');
parents  = currentlist(val).parents;
par_ind = numel(parents) + 1;
parents{par_ind} = currentlist(val).fname;
newitems='';
allitems = fieldnames(expstruct(ind));
k=1;
for i=1:numel(allitems)
    suff='';
    field = allitems{i};
    f= expstruct(ind).(field);
    if isstruct(f)
       suff = '+';
       for j=1:numel(f)  % for array of structures
          item = sprintf('%s(%d) : %s',field,j,'<struct>');
          newitems{k} = sprintf('%s  %s %s',pad,suff,item);
          if val  > 1
              currentlist(val+k+1 : end+1) = currentlist(val+k:end);
          end
          currentlist(val+k).parents = parents;
          currentlist(val+k).fname = field; %fieldname
          currentlist(val+k).sub = [currentlist(val).sub,j];
          k = k +1;
       end
    else
       if ~ischar(f)
          if isscalar(f) && isnumeric(f)
             f = num2str(f,'%0.5g');
          else
            if isempty(f)
                f='[ ]';
            else    
                f= sprintf('%d x %d array',size(f,1),size(f,2));
            end
          end
       end
       item = sprintf('%s : %s',field,f);   
       newitems{k} =sprintf('%s  %s %s',pad,suff,item);
       if (val > 1)
          currentlist(val+k+1 : end+1) = currentlist(val+k:end);
       end    
       currentlist(val+k).parents = parents;
       currentlist(val+k).fname = field; %fieldname
       currentlist(val+k).sub = [currentlist(val).sub];
       k = k +1;
    end
end
setappdata(handles.setwriteopts,'leftlist',currentlist);

function numtoremove = collapsetree(v)
handles = guihandles;
currlist = getappdata(handles.setwriteopts,'leftlist');
checkp = currlist(v).parents;
numtoremove = 0;
if isempty(checkp)
    numtoremove = numel(currlist) - 1;
    return;
end
for i = v+1:numel(currlist)
    temp = currlist(i).parents{end};
    if ~isempty(strmatch(temp,checkp,'exact'))
       return;
    else       
        numtoremove = numtoremove + 1;
    end
end
    

function num=getleadspaces(str)
num=0;
k = strfind(str,'+');
l = strfind(str,'-');
if ~isempty(k)
    num = k -1;
end
if ~isempty(l)
    num = l -1;
end
if num == 0
    k= regexp(str,'\w');
    num = k(1) - 1;
end 


function leftlist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rightlist_Callback(hObject, eventdata, handles)
set(handles.removebtn,'enable','on');

function rightlist_CreateFcn(hObject, eventdata, handles)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function addbtn_Callback(hObject, eventdata, handles)
selected =cellstr(get(handles.rightlist,'String'));
z =isspace(selected{1});
if numel(find(z))  == numel(selected{1})
   num=1;
else
   num= numel(selected)+1;
end
currentlist =  getappdata(handles.setwriteopts,'leftlist');
val=get(handles.leftlist,'Value');
parents  = currentlist(val).parents;
subs=currentlist(val).sub;
itemstr='';
if numel(parents) > 1
   for i=2:numel(parents)
       itemstr = sprintf('%s%s(%d).',itemstr,parents{i},subs(i));
   end
end
itemstr=sprintf('%s%s',itemstr,currentlist(val).fname);

%sets up selected item with array indices
erows = cellstr(get(handles.erowpop,'String'));
erowval = get(handles.erowpop,'Value');
ecols = cellstr(get(handles.ecolpop,'String'));
ecolval = get(handles.ecolpop,'Value');
srowval = get(handles.srowpop,'Value');
scolval = get(handles.scolpop,'Value');
if strcmpi(get(handles.srowpop,'enable'),'on') && strcmpi(get(handles.scolpop,'enable'),'on') %matrix
   if ~(srowval == 1 && erowval == numel(erows) && scolval ==1 && ecolval == numel(ecols))
      itemstr = sprintf('%s(%d:%d,%d:%d)',itemstr,srowval,erowval,scolval,ecolval);
   end
else
   if strcmpi(get(handles.srowpop,'enable'),'on') %row vector
      if ~(srowval == 1 && erowval == numel(erows))  %setup string with indices only if subset of vector is selected
         itemstr = sprintf('%s(%d:%d)',itemstr,srowval,erowval);
      end
   end
   if strcmpi(get(handles.scolpop,'enable'),'on')
      if ~(scolval == 1 && ecolval == numel(ecols))
         itemstr = sprintf('%s(%d:%d)',itemstr,scolval,ecolval);
      end
   end
end
selected{num} = itemstr;
set(handles.rightlist,'String',selected);

if num==1
   set(handles.removebtn,'enable','on');
end




function removebtn_Callback(hObject, eventdata, handles)
index  = get(handles.rightlist,'Value');
rpanelstrs = get(handles.rightlist,'String');
rpanelstrs(index)=[];
if numel(rpanelstrs) == 0
   rpanelstrs = {' '};
   set(hObject,'enable','off');
end
set(handles.rightlist,'String',rpanelstrs,'Value',1);



function donebtn_Callback(hObject, eventdata, handles)
apply_options();

function logfilepop_Callback(hObject, eventdata, handles)
global outputlog;
val = get(hObject,'Value');
if isempty(outputlog) || (val > numel(outputlog))
    set(handles.rightlist,'String',' ');    
    return;
end
setlogvalues(val);

function logfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function startchk_Callback(hObject, eventdata, handles)


function stopchk_Callback(hObject, eventdata, handles)


function writefreqpop_Callback(hObject, eventdata, handles)

function writefreqpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function logfilebtn_Callback(hObject, eventdata, handles)
global net filevarname PDPAppdata;
prefix=sprintf('%slog',net.name);
bincheck = get(handles.binarychk,'Value');
if bincheck ==1
   ext = '.mat';
   defaultfile = getfilename(prefix,'.mat');  
   while ~isempty(strmatch(defaultfile,net.outputfile))  %to generate new name in case previous logs have been setup but not actually created.
         dotpos = regexp(defaultfile,'\.');
         numpos = regexp(defaultfile,'\d+\.');
         numstr = defaultfile(numpos : dotpos-1);
         newindex = str2double(numstr)+1;
         defaultfile = sprintf('%s%d.mat',defaultfile(1:numpos-1),newindex);
   end   
   filterstr={'*.mat','matlab binary file(*.mat)'};
else
   ext = '.out';
   defaultfile = getfilename(prefix,'.out');
   while ~isempty(strmatch(defaultfile,net.outputfile))%to generate new name in case previous logs have been setup but not actually created.
         dotpos = regexp(defaultfile,'\.');
         numpos = regexp(defaultfile,'\d+\.');
         numstr = defaultfile(numpos : dotpos-1);
         newindex = str2double(numstr)+1;
         defaultfile = sprintf('%s%d.out',defaultfile(1:numpos-1),newindex);
   end    
   filterstr={'*.out','pdp output file (*.out)'};
end
getbasename(prefix,ext);
uiwait(findobj('tag','getbasename'));  %wait till getbasename figure is cleared
base = getappdata(handles.setwriteopts,'basefilename');
if isempty(base)
   return;
end
filename = getfilename(base,ext);
% [filename, pathname] = uiputfile(filterstr,'Save output file',defaultfile);
% if isequal([filename,pathname],[0,0])
%     return;
% end
val =get(handles.logfilepop,'Value');
lfiles=cellstr(get(handles.logfilepop,'String'));
if val==1 && strcmpi(lfiles{val},'none')
   index=1;
else
   index=numel(lfiles)+1;
end
selpanelctrls = findobj(findobj('tag','selectpnl'),'-depth',1,'-not','type','uipanel');
set(selpanelctrls,'enable','on');
currpath = eval('pwd');
% if strcmpi(pathname(1:end-1),currpath)  %last element of pathname is filesep
    File=filename;
% else
%     File = fullfile(pathname,filename);
% end
% if exist(File,'file')
%    delete(File);
% end
set(handles.onradio,'Value',1);
lfiles{index}=File;
set(handles.logfilepop,'String',lfiles,'Value',index);
set(handles.rightlist,'String',' ');
if PDPAppdata.lognetwork
   statement = sprintf('filename = getfilename(''%s'',''%s'');',base,ext);
   updatelog(statement);
end
filevarname = 'filename';

function setwriteopts_CloseRequestFcn(hObject, eventdata, handles)
% Hint: delete(hObject) closes the figure
delete(hObject);

function setlogvalues(num)
global outputlog;
handles=guihandles;
currentlog=outputlog(num);
if isempty(currentlog.status)
    set(handles.rightlist,'String',' ');    
    return;
end
if strcmpi(currentlog.status,'on')
   set(handles.onradio,'Value',1);    
   set(handles.offradio,'Value',0);
else
   set(handles.onradio,'Value',0);    
   set(handles.offradio,'Value',1);
end
set(handles.rightlist,'String',currentlog.objects);
freq = get(handles.writefreqpop,'String');
t= strmatch(currentlog.frequency,freq,'exact');
if isempty(t)
   t=1;
end
set(handles.writefreqpop,'Value',t);
if isequal(currentlog.writemode(1),'b')
    set(handles.binarychk,'Value',1);
else
    set(handles.binarychk,'Value',0);
end
[t,procval] = ismember(currentlog.process,get(handles.logprocpop,'String'));
if isequal(procval,[1 2])
    procval=3;
end
if isempty(procval)
    procval=1;
end
set(handles.logprocpop,'Value',procval);
if strcmpi(currentlog.logopts,'on')
   set(handles.logoptschk,'Value',1);
else
   set(handles.logoptschk,'Value',0);
end
if strcmpi(currentlog.plot,'on')
   set(handles.plotchk,'Value',1);
   set(handles.setplotoptsbtn,'enable','on');
else
   set(handles.plotchk,'Value',0);
   set(handles.setplotoptsbtn,'enable','off');   
end
panelstat = 'off';
if isempty(currentlog.graphdata)
   panelstat = 'on';
else
   ploth = cell2mat({currentlog.graphdata.fighandle});
   if isempty(ploth)
      panelstat = 'on';
   end
end
selpanelctrls = findobj(allchild(handles.selectpnl),'-not','type','uipanel');  %uipanel does not have 'enable' property
set(selpanelctrls,'enable',panelstat);

if strcmpi(currentlog.onreset,'clear')
   set(handles.clearradio,'Value',1);
else
   set(handles.newradio,'Value',1);
end
if ~isempty(currentlog.plotlayout)
   setappdata(handles.setwriteopts,'plotlayout',currentlog.plotlayout);
end

function logprocpop_Callback(hObject, eventdata, handles)


function logprocpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function binarychk_Callback(hObject, eventdata, handles)
fnames= cellstr(get(handles.logfilepop,'String'));
if strcmpi(fnames{1},'none')
   return;
end
v = get(handles.logfilepop,'Value');
selfile = fnames{v};
[t,r]=strtok(selfile,'.');
if isempty(r)
   return;
end
if get(hObject,'Value')==1
   if ~strcmpi(r,'.mat')
       selfile=sprintf('%s.mat',t);
   end
else
   if ~strcmpi(r,'.out')
       selfile=sprintf('%s.out',t);
   end
end
fnames{v}=selfile;
set(handles.logfilepop,'String',fnames)



function okbtn_Callback(hObject, eventdata, handles)
apply_options();
delete(handles.setwriteopts);

function cancelbtn_Callback(hObject, eventdata, handles)
delete(handles.setwriteopts);

function apply_options()
global net;
handles = guihandles;
% val =get(handles.logfilepop,'Value');
vars= get(handles.rightlist,'String');
z =isspace(vars);
if numel(find(z))  == numel(vars)
   return;
end
vars = vars';
if get(handles.binarychk,'Value') ==1
   writemode='binary';
else
   writemode='text';
end
% lfiles=cellstr(get(handles.logfilepop,'String'));
% if strcmpi(lfiles{val},'none')
%    prefix=sprintf('%slog',net.name);
%    if writemode(1) == 'b'
%       fname = getfilename(prefix,'.mat');
%    else
%       fname=getfilename(prefix,'.out');
%    end
%    lfiles{val} = fname;
%    set(handles.logfilepop,'String',lfiles);
% else
%    fname = lfiles{val};
% end
fname = get(handles.lognameedit,'String');
if get(handles.onradio,'Value')==1
   status='on';
else
   status='off';
end
freqpop=get(handles.writefreqpop,'String');
freqval = get(handles.writefreqpop,'Value');
procoptions = get(handles.logprocpop,'String');
procval = get(handles.logprocpop,'Value');
prname=procoptions{procval};
freq=freqpop{freqval};
plotdata = getappdata(handles.setwriteopts,'plotdata');
if strcmpi(get(handles.plotchk,'enable'),'on') && get(handles.plotchk,'Value') ==1
   plotstatus = 'on';
%    if isempty(plotdata)
%        if numel(vars) > 1
%            plotdata = struct('plotpos',1,'yvariables',vars(2:end),'title','',...
%                       'tag','','xlabel','','ylabel','','ylimit',[]);
%        end
%    end
else
   plotstatus = 'off';
end
if get(handles.clearradio,'Value') == 1
   resetact = 'clear';
else
   resetact = 'new';
end
if get(handles.logoptschk,'Value') ==1
   logoptsval='on';
else
   logoptsval='off';
end
layout = getappdata(handles.setwriteopts,'plotlayout');
setoutputlog('file',fname,'process',prname,'frequency',freq,'status',...
             status,'writemode',writemode,'logopts',logoptsval,'objects',vars,...
             'plot',plotstatus,'plotlayout',layout,'onreset',resetact); 

if ~isempty(plotdata)
   for i = 1:numel(plotdata)
       args = {'file',fname,'plotnum',plotdata(i).plotpos,'yvariables',plotdata(i).yvariables};       
       if ~isempty(plotdata(i).title)
          args = {args{1:end},'title',plotdata(i).title};
       end
       if ~isempty(plotdata(i).tag)
          args = {args{1:end},'tag',plotdata(i).tag};
       end
       if ~isempty(plotdata(i).xlabel)
          args = {args{1:end},'xlabel',plotdata(i).xlabel};
       end        
       if ~isempty(plotdata(i).ylabel)
          args = {args{1:end},'ylabel',plotdata(i).ylabel};
       end
       if ~isempty(plotdata(i).ylimit)
          args = {args{1:end},'ylim',plotdata(i).ylimit};
       end
       if plotdata(i).plot3d
          args = {args{1:end},'plot3d',plotdata(i).plot3d};
       end
       if ~isequal([plotdata(i).az plotdata(i).el],[0 90])
          args = {args{1:end},'az',plotdata(i).az,'el',plotdata(i).el};
       end
       setplotparams(args{1:end});
   end
end

         
function logstatuspnl_SelectionChangeFcn(hObject, eventdata, handles)
%Plot must happen only if log status is on
if get(handles.onradio,'Value') == 1
   set(handles.plotchk,'Enable','on');
   if numel(cellstr(get(handles.rightlist,'String'))) > 1
      set(handles.setplotoptsbtn,'Enable','on');
   end
else
   set(handles.plotchk,'Enable','off');
   set(handles.setplotoptsbtn,'Enable','off');   
end



function plotchk_Callback(hObject, eventdata, handles)
if get(hObject,'Value') == 1 && numel(cellstr(get(handles.rightlist,'String'))) > 1
   set(handles.setplotoptsbtn,'enable','on');
else
   set(handles.setplotoptsbtn,'enable','off');
end

function srowpop_Callback(hObject, eventdata, handles)


function srowpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function erowpop_Callback(hObject, eventdata, handles)


function erowpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function scolpop_Callback(hObject, eventdata, handles)


function scolpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ecolpop_Callback(hObject, eventdata, handles)


function ecolpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function setplotopts_Callback(hObject, eventdata, handles)
setplotopts;



function lognameedit_Callback(hObject, eventdata, handles)


function lognameedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in setplotoptsbtn.
function setplotoptsbtn_Callback(hObject, eventdata, handles)
% hObject    handle to setplotoptsbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setplotopts;



function checkbox6_Callback(hObject, eventdata, handles)




function logoptschk_Callback(hObject, eventdata, handles)


