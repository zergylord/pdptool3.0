function enablelog(checkstatus)
global PDPAppdata;
logmenu_h = findobj('tag','filelog');
value = 0;
if strcmpi(checkstatus,'on')
    value = 1;
    if isempty(PDPAppdata.logfilename)
      PDPAppdata.logfilename = getfilename('pdplog','.m');
   end
end
set(logmenu_h,'checked',checkstatus);
PDPAppdata.lognetwork = value;