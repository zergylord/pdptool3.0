function setseed(varargin)
global net;
if nargin==0
   rand('seed',sum(100*clock))
else
   rand('seed',varargin{1});
end
stream = RandStream('mt19937ar','Seed',varargin{1});
RandStream.setGlobalStream(stream);
net.seed=rand('seed');
