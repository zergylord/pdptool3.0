function cs_run
global net outputlog freqrange logoutflag binarylog runparams PDPAppdata;
[ccycle lcycle cupdate lupdate pnum plimit] = initialize_params;
uprange = [cupdate lupdate];
logoutflag=0;
freqrange={};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails('test');
end
PDPAppdata.stopprocess = [];
updatecount = 0;
if runparams.pattest ~=0
    for i = pnum:plimit
        if i == plimit && ~isempty(runparams.range)
           lcycle = runparams.range(2,1);
        end
        pname = setinput(i);
        if ~PDPAppdata.tstinterrupt_flag
           refresharrays();
        end
        breakchk = cycle(uprange,ccycle,lcycle);
        if breakchk && rem(net.cycleno,net.testopts.ncycles) ~= 0
           PDPAppdata.tstinterrupt_flag = 1;
           return;
        end
        net.patno = i;
        net.cpname = pname;
        ccycle = 1;
        lcycle = ccycle + net.testopts.ncycles - 1;
        if logoutflag && ~isempty(strmatch('pattern',lower(freqrange)))
            writeoutput('test','pattern');
        end         
        if PDPAppdata.gui && strncmp(runparams.granularity,'pattern',...
           length(runparams.granularity))       
           updatecount = checkupdate(updatecount,i);
           if updatecount < 0 && i ~= plimit % if i==plimit, should check if binary output files need to be written
              return;
           end
        end
        PDPAppdata.tstinterrupt_flag = 0;
        if breakchk
           break;
        end
    end
else
   cycle(uprange,ccycle,lcycle);
end
if PDPAppdata.gui
   updatecount = checkupdate(updatecount,net.patno);
end
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end

function [scyc lcyc sup lup spat lpat] = initialize_params
global net runparams rangelog;
spat = 1;
lpat =1;
if isempty(runparams.range)
    net.updateno = net.updateno + 1;
    if net.updateno > net.testopts.nupdates
       net.updateno = 1;
    end
    if net.updateno == 1
       net.cycleno = net.cycleno + 1;
    end    
   if runparams.pattest ~=0
      if runparams.alltest == 0
         spat = get(findobj('tag','tstpatlist'),'Value');
         lpat = spat;
         net.patno = spat;
      else
         lpat = numel(cellstr(get(findobj('tag','tstpatlist'),'String'))); 
         if net.cycleno > net.testopts.ncycles
            net.cycleno = 1;
         end
         if net.cycleno == 1 && net.updateno == 1
            net.patno = net.patno + 1;
         end
         if net.patno > lpat
            net.patno = 1;
         end
         spat = net.patno;
      end
   end
   scyc = net.cycleno;
%    lcyc = net.cycleno + (((fix(net.cycleno/net.testopts.ncycles)+1) *...
%             net.testopts.ncycles) - net.cycleno); 
   lcyc = ceil(net.cycleno/net.testopts.ncycles) * net.testopts.ncycles;        
   sup = net.updateno;
   rangelog = [scyc sup];   
   if runparams.pattest
      rangelog(end+1) = spat;
   end
else
   scyc = runparams.range(1,1);
   lcyc = runparams.range(2,1);   
   sup = runparams.range(1,2);
   if runparams.pattest ~=0
      spat = runparams.range(1,3);
      lpat = runparams.range(2,3);
      lcyc = net.testopts.ncycles;
   end
end
lup = net.testopts.nupdates;

function breakind = cycle (uprange,ccycle,lcycle)
global logoutflag freqrange runparams PDPAppdata;
updatecount=0;
breakind=0;
global net;
pno = 1;
if net.patno > 0
   pno = net.patno;
end
for iter=ccycle:lcycle
    net.cycleno = iter;
    if strncmpi(net.testopts.actfunction,'boltzmann',length(net.testopts.actfunction)) ||...
       (net.testopts.harmony==1)
        net.testopts.temp = annealing(iter);
    end
    breakind = rupdate(uprange);
    if breakind==1
       net.goodness=get_goodness();        
       break;
    end
    if iter == lcycle && ~isempty(runparams.range)
       uprange = [1 runparams.range(2,2)];
    else
       uprange=[1 net.testopts.nupdates];
    end
    if logoutflag && ~isempty(strmatch('cycle',lower(freqrange)))
       writeoutput('test','cycle');
    end     
    if PDPAppdata.gui && strncmp(runparams.granularity,'cycle',...
       length(runparams.granularity))
       updatecount = checkupdate(updatecount,pno);
       if updatecount < 0 
          breakind = 1;
          break;
       end
    end
end
% if net.cycleno==0
%     net.cycleno=net.testopts.ncycles;
% end
% if net.updateno==0
%     net.updateno=net.testopts.nupdates;
% end
% if PDPAppdata.gui
%    update_display(0);
% end


function retbreak = rupdate(range)
global net logoutflag freqrange runparams PDPAppdata;
retbreak=0;
poolnums =cell2mat({net.pool.nunits});
totunits = sum(poolnums(2:end));
updatecount = 0;
pno =1;
if net.patno > 0
   pno = net.patno;
end
for n=range(1):range(2)
    abort = PDPAppdata.stopprocess;
    if ~isempty(abort)
       PDPAppdata.stopprocess = [];
       retbreak=1;
       break;
    end    
    net.updateno=n;
    unum = ceil(totunits.* rand(1,1));
    [pindex uindex] = getpoolindex(unum,poolnums);
    inti=0.0;
    pool = net.pool(pindex);
    if (net.testopts.harmony)
        neti = 0.0;
        if (pool.extinput(uindex) == 0.0)
            if probability(logistic(neti))  
               pool.activation(uindex) = 1;
            else
               pool.activation(uindex) = -1;
            end
        else
            if pool.extinput(uindex) < 0.0
               pool.activation(uindex) = 1;
            end
            if pool.extinput(uindex) > 0.0
               pool.activation(uindex) = -1;
            end
        end
    else
        if (net.testopts.clamp)
            if pool.extinput(uindex) > 0
               pool.activation(uindex) = 1;
               net.pool(pindex) = pool;
               if logoutflag && ~isempty(strmatch('update',lower(freqrange)))
                  writeoutput('test','update');
               end               
               if PDPAppdata.gui && strncmpi(runparams.granularity,...
                  'update',length(runparams.granularity))
                  updatecount = checkupdate(updatecount,pno);
                  if updatecount < 0
                     retbreak = 1;
                     return;
                  end
               end 
               continue;
            end
            if pool.extinput(uindex) < 0
               pool.activation(uindex) = 0;
               net.pool(pindex) = pool;
               if logoutflag && ~isempty(strmatch('update',lower(freqrange)))
                  writeoutput('test','update');
               end               
               if PDPAppdata.gui && strncmpi(runparams.granularity,...
                  'update',length(runparams.granularity))
                  updatecount = checkupdate(updatecount,pno);
                  if updatecount < 0
                     retbreak = 1;
                     return;
                  end
               end
               continue;
            end
        end
        pools = {net.pool.name};
        for prjn=1:numel(pool.proj)
            from = pool.proj(prjn).frompool;
            np = strmatch(from,pools,'exact');
            wt = pool.proj(prjn).weight(uindex,:);
            inti = inti + sum(net.pool(np).activation .* wt);
        end
        if (net.testopts.clamp==0)
            neti = (net.testopts.istr * inti) + (net.testopts.estr * pool.extinput(uindex));
        else
            neti = net.testopts.istr * inti;
        end
        pool.netinput(uindex) = neti;
        pool.intinput(uindex) = inti;
        if strncmpi(net.testopts.actfunction,'boltzmann',length(net.testopts.actfunction))
            if (probability(logistic(neti)))
                pool.activation(uindex) = 1.0;
            else
                pool.activation(uindex) = 0.0;
            end
        else
            if neti > 0
               if pool.activation(uindex) < 1.0
                   acti = pool.activation(uindex);
                   dt = acti + (neti * (1.0 - acti));
                   if dt > 1.0
                      pool.activation(uindex) = 1.0;
                   else
                      pool.activation(uindex) = dt;
                   end
               end
            else
               if pool.activation(uindex) > 0.0
                  acti = pool.activation(uindex);
                  dt = acti + (neti * acti);
                  if (dt < 0.0)
                      pool.activation(uindex) = 0.0;
                  else
                      pool.activation(uindex) = dt;
                  end
               end
            end
        end
    end
    if ~isempty(pool.unames)
        net.cuname= pool.unames{uindex};
    end
    net.pool(pindex) = pool;
   if logoutflag && ~isempty(strmatch('update',lower(freqrange)))
      writeoutput('test','update');
   end    
   if PDPAppdata.gui && strncmpi(runparams.granularity,'update',...
      length(runparams.granularity))       
      updatecount = checkupdate(updatecount,pno);
      if updatecount < 0
         retbreak = 1;
         return;
      end
   end   
end


function [p u] = getpoolindex(unitnum,poolunits)
p=[];
u=[];
x=1;
parray=cell(1,numel(poolunits)-1);
for i=2:numel(poolunits)
    k=i-1;
    parray{k}=x:(poolunits(i)+x-1);
    z=find(parray{k}==unitnum);
    if ~isempty(z)
        p = i;
        u = z;
    end
    x = poolunits(i)+1;
end

function retval = logistic(val)
global net;
if (net.testopts.temp <=0)
    retval = val >0;
    return;
else
    val = val /net.testopts.temp;
end
if (val > 15.9357739741644)
    retval = 0.99999988;
else
    if val <  -15.9357739741644
       retval = 0.00000012;
    else
       retval = 1.0 ./(1.0 + exp(-1 * val));
    end
end


function prob = probability(val)
prob=0;
randval = rand(1,1);
if randval < val
   prob = 1;
end

function currcount = checkupdate(currcount,pno)
global net runparams;
net.goodness = get_goodness();
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if  strncmpi(runparams.mode,'step',length(runparams.mode))
        currcount = -1;
    end
end

function dg = get_goodness()
global net;
activ=[];
exti=[];
dg =0.0;
if (net.testopts.harmony ==1 || numel(net.pool) <= 1 )
   return;
end
pools = {net.pool.name};
for plnum=2:numel(net.pool)
    p = net.pool(plnum);
    for prjn=1:numel(p.proj)
        from = p.proj(prjn).frompool;
        np = strmatch(from,pools);
        if np ==1  %bias connection
           dg = dg + sum(p.proj(prjn).weight .* p.activation');
           continue;
        end
        fromp = net.pool(np);
        wt = p.proj(prjn).weight;
        for i = 1:size(wt,1)
            if np == plnum %self block
               jstart = i+1;
            else
               jstart = 1;
            end
            for j=jstart:size(wt,2)
                dg = dg + (wt(i,j) * p.activation(i) * fromp.activation(j));
            end
        end
    end
    activ(plnum-1,:)=p.activation;
    exti(plnum-1,:)=p.extinput;
end
if net.testopts.clamp ==0
  dg = dg * net.testopts.istr;
  for i=1:size(activ,1)
      dg =dg + sum(activ(i,:) .* (exti(i,:) * net.testopts.estr));
  end
end

function cpname = setinput(patnum)
global net PDPAppdata;
data = PDPAppdata.testData;
cpname = data(patnum).pname;
x=1;
for i=2:numel(net.pool)
    z = net.pool(i).nunits+x-1;
    net.pool(i).extinput = data(patnum).ipattern(x:z);
    x = z+1;
end
