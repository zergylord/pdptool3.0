function diff_fields = compare_struct(st1,st2)
diff_fields=cell(1);
if ~isstruct(st1) && ~isstruct(st2)
   fprintf(1,'Invalid argument : both input arguments must be struct variables\n');
end
%get fieldnames
st1_f = fieldnames(st1);
st2_f = fieldnames(st2);
st2_all_vals = struct2cell(st2); 
st1_all_vals = struct2cell(st1);
%find common fields
[tf loc] = ismember(st1_f,st2_f);
if any(tf) && any(loc) % this means there are common fields
   common_fields = st1_f(tf);
   st1_vals = st1_all_vals(tf);
   st2_vals = st2_all_vals(loc(tf));
   diff_ind = find(~cellfun(@isequal,st1_vals,st2_vals));
   diff_fields = cell(numel(diff_ind),3);
   for i=1:numel(diff_ind)
       diff_fields{i,1} = common_fields{diff_ind(i)};
       diff_fields{i,2} = st1_vals{diff_ind(i)};
       diff_fields{i,3} = st2_vals{diff_ind(i)};
   end
end
