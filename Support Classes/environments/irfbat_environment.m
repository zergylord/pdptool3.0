classdef irfbat_environment < rl_environment

    properties
        lvl = 1;
        map = zeros(49);
        center = {25 25};
        irfbat;
        
        screen = zeros(9);
%         top left corner of screen
        row;
        col;
        
        Aud;
        Vis;
        
    end
    
    methods 
        function obj = irfbat_environment()
            obj.set_initial_state;
        end
        function TF = at_terminal(obj)
            TF = obj.screen(5,5);
            if TF
%                 obj.lvl = obj.lvl +1;
            end
        end
        function patterns = state(obj)
            bs = obj.screen;
            bs(~bs) = -1;
            patterns = struct('pool','vis','type','H','pattern',reshape(bs,1,[]));
%             patterns = struct('pool','vis','type','H','pattern',reshape(obj.screen,1,[]));
%             patterns = [patterns struct('pool','vis','type','H','pattern',reshape(obj.screen,1,[]))];
        end
        function patterns = reward(obj)
            if ~isempty(find(obj.screen))%irfbat on screen
               [r c] = find(obj.screen);
               score = 9;
               score = score - (abs(r-5) + abs(c-5));  
            else
                score = 0;
            end
            patterns = struct('pool','out','type','T','pattern',score);
        end
        function perform_action(obj,action)
            switch find(action)
                case 1%up
                    if obj.row~=1
                        obj.row = obj.row -1;
                    end
                case 2%left
                    if obj.col ~= 1
                    obj.col = obj.col -1;
                    end
                case 3%down
                    if obj.row~=49-8
                    obj.row = obj.row +1;
                    end
                    
                case 4%right
                    if obj.col~=49-8
                    obj.col = obj.col +1;
                    end
            end
                        obj.screen = obj.map(obj.row:obj.row+8,obj.col:obj.col+8);
%                         imshow(obj.screen);
        end
        function actions = possible_actions(obj)       %returns set of action pool activations
            actions(1,:) = [1 0 0 0];
            actions(2,:) = [0 1 0 0];
            actions(3,:) = [0 0 1 0];
            actions(4,:) = [0 0 0 1];
        end
        function set_initial_state(obj)
            obj.lvl = 1;
            obj.reset;
        end
        function obj = reset(obj)
            obj.row = obj.center{1}-4;
            obj.col = obj.center{2}-4;
            obj.map = zeros(49);
            obj.irfbat = randi(4);
            switch obj.irfbat %type determines initial placement
                case 1%up
                    obj.map(obj.center{1}-obj.lvl,obj.center{2}) = 1;
                case 2%left
                    obj.map(obj.center{1},obj.center{2}-obj.lvl) = 1;
                case 3%down
                    obj.map(obj.center{1}+obj.lvl,obj.center{2}) = 1;
                case 4%%right
                    obj.map(obj.center{1},obj.center{2}+obj.lvl) = 1;
            end
                        obj.screen = obj.map(obj.row:obj.row+8,obj.col:obj.col+8);
        end
    end
    
end