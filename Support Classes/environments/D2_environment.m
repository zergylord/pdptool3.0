classdef D2_environment < rl_environment
    
    properties
        pic;
        robot;
        tool;
        hp=1;
        prev_hp = 1;
        exp=0;
        screen;
        prev_exp = 0;
        near_death = .02;
        left_click;
        right_click;
        esc;
        shift;
        vertical_mouse_offset = 35;
        interval_index;
        steps;
        resolution;
        non_GUI_res;
        rbm_weights;
        rbm_bias;
        
        really_dead = 0;
        
%         action = 0;
%         prev_action = 0;
        stopped = false;
        hp_signal = 0;
        
        yay = false;
        
    end
    methods (Access = protected)
        function rbm2screen(obj)
            input = rgb2gray(obj.pic);
            input = input(11:end-110,141:end-140,:);
            input = impyramid(impyramid(impyramid( normalize(input),'reduce'),'reduce'),'reduce');
            input = reshape(input,1,[]);
            filterdata = 1./(1 + exp(-input*obj.rbm_weights{1} - obj.rbm_bias{1}));
            filterdata = 1./(1 + exp(-filterdata*obj.rbm_weights{2} - obj.rbm_bias{2}));
            filterdata = 1./(1 + exp(-filterdata*obj.rbm_weights{3} - obj.rbm_bias{3}));
            filterdata = 1./(1 + exp(-filterdata*obj.rbm_weights{4} - obj.rbm_bias{4}));
            filterdata = 1./(1 + exp(-filterdata*obj.rbm_weights{5} - obj.rbm_bias{5}));
            obj.screen = filterdata;
             
        end
        function update_state(obj)
            import java.io.*;
            import java.awt.*;
            img = obj.robot.createScreenCapture(Rectangle(obj.resolution(2),obj.resolution(1)));
            obj.pic = java_img2mat(img);
%             obj.pic = imread('img4.jpg');
            obj.rbm2screen;

            %CHECK TO SEE IF UI EXISTS; SEE STORED_PIC{4} FOR DETAILS
            obj.prev_hp = obj.hp;
            obj.hp = get_hp(obj.pic);
            obj.prev_exp = obj.exp;
            obj.exp = get_exp(obj.pic);
            
            if obj.really_dead == 3
                obj.hp_signal = 0;%max(obj.hp - obj.prev_hp,0)*100 - 100);
            else
                obj.hp_signal = (min(obj.hp - obj.prev_hp,0)*20 + obj.stopped*1);
            end
            disp('reward:');
            disp(obj.hp_signal);
        end
        
        function click_on_portal(obj)
            obj.robot.mouseMove(obj.non_GUI_res(2)/2-15,obj.non_GUI_res(1)/2+15);
            obj.robot.mousePress(obj.left_click);
            obj.robot.mouseRelease(obj.left_click);
            pause(.5);
            obj.robot.mousePress(obj.left_click);
            obj.robot.mouseRelease(obj.left_click);
            pause(.5);
            obj.robot.mousePress(obj.right_click);
            obj.robot.mouseRelease(obj.right_click);
        end
        
    end
    methods
        function obj = D2_environment()
            
            import java.io.*;
            import java.awt.*;
            import java.awt.event.InputEvent;
            import java.awt.event.KeyEvent;
            
            obj.resolution = [480 640];
            obj.non_GUI_res = obj.resolution - [100 0];
            
            obj.left_click = InputEvent.BUTTON1_MASK;
            obj.right_click = InputEvent.BUTTON3_MASK;
            obj.esc = KeyEvent.VK_ESCAPE;
            obj.shift = KeyEvent.VK_SHIFT;
            obj.robot = Robot;
            obj.tool = Toolkit.getDefaultToolkit();
        end
        
        function ind = get.interval_index(obj)
            ind = obj.steps;
        end
        
        function set_initial_state(obj)
            import java.io.*;
            import java.awt.*;
            obj.really_dead = 0;
            obj.update_state;
            
            
%             obj.click_on_portal;
            pause(3);
            obj.steps = 1;
            
            %start moving up
            obj.robot.mouseMove(obj.non_GUI_res(2)/2,obj.non_GUI_res(1)/2-100);
            obj.robot.mousePress(obj.left_click);
        end
        
        function TF = at_terminal(obj)
            TF = false;
            if obj.hp < obj.near_death
                disp('guess Im dead... :(');
                if obj.really_dead == 3
                    TF = true;
                    disp('yup, totally dead');
                else
                    obj.really_dead = obj.really_dead +1;
                end
            else
                obj.really_dead = 0;
            end
        end
        
        function patterns = state(obj)
            patterns = struct('pool','screen','type','H','pattern',reshape(obj.screen, 1, numel(obj.screen)));
        end
        
        function patterns = reward(obj)
            patterns = struct('pool','','type','T','pattern',obj.hp_signal);
        end
        
        function perform_action(obj,action)
%             obj.prev_action = obj.action;
%             obj.action = find(action);
disp('action:');            
disp(action);
obj.yay = false;
            switch find(action)
                case 1 %up
                    
                    obj.robot.mouseMove(obj.non_GUI_res(2)/2,obj.non_GUI_res(1)/2-100);
                    if obj.stopped
                        obj.robot.keyRelease(obj.shift);
                        obj.stopped = false;
                    end
                    %                    obj.robot.mouseRelease(obj.left_click);
                case 2 %down
                    
                    obj.robot.mouseMove(obj.non_GUI_res(2)/2,obj.non_GUI_res(1)/2+100);
                    if obj.stopped
                        obj.robot.keyRelease(obj.shift);
                        obj.stopped = false;
                    end
                    %                    obj.robot.mousePress(obj.left_click);
                    %                    obj.robot.mouseRelease(obj.left_click);
                case 3 %left
                    
                    obj.robot.mouseMove(obj.non_GUI_res(2)/2-100,obj.non_GUI_res(1)/2+obj.vertical_mouse_offset);
                    obj.yay = true;
                    if obj.stopped
                        obj.robot.keyRelease(obj.shift);
                        obj.stopped = false;
                    end
                case 4 %right
                    
                    obj.robot.mouseMove(obj.non_GUI_res(2)/2+100,obj.non_GUI_res(1)/2+obj.vertical_mouse_offset);
                    if obj.stopped
                        obj.robot.keyRelease(obj.shift);
                        obj.stopped = false;
                    end
                case 5
                    obj.robot.mouseMove(obj.non_GUI_res(2)/2,obj.non_GUI_res(1)/2+obj.vertical_mouse_offset);
                    if ~obj.stopped
%                         obj.robot.mouseRelease(obj.left_click)
                        obj.robot.keyPress(obj.shift);
                        
                        obj.stopped = true;
                    end
            end
            
            obj.update_state;
            obj.steps = obj.steps + 1;
        end
        
        function actions = possible_actions(obj)
            actions(1,:) = [1 0 0 0];
            actions(2,:) = [0 1 0 0];
            actions(3,:) = [0 0 1 0];
            actions(4,:) = [0 0 0 1];
            actions(5,:) = [0 0 0 0];
        end
        
        function obj = reset(obj)
            obj.robot.mouseRelease(obj.left_click);
            obj.robot.keyPress(obj.esc);
            obj.robot.keyRelease(obj.esc);
            pause(1);
        end
    end
    
end