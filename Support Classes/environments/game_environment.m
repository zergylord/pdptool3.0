classdef game_environment < rl_environment

    properties
        steps;
        wasd = 'wasd';
    end
    
    methods 
        function obj = game_environment()
            obj.set_initial_state;
            assignin('base','highscore',-intmax);
            assignin('base','lowmove',intmax);
%             if exist('highscore.mat','file')
%                 load highscore
%             else
%                 assignin('base','highscore',-intmax);
%             end
%             if exist('lowmove.mat','file')
%                 load lowmove
%             else
%                 assignin('base','lowmove',intmax);
%             end
        end
        function TF = at_terminal(obj)
            TF = evalin('base','won');
        end
        function patterns = state(obj)
            evalin('base','get_cheap_rep');
            patterns = struct('pool','in','type','H','pattern',evalin('base','data'));
        end
        function patterns = reward(obj)
            patterns = struct('pool','out','type','T','pattern',evalin('base','reward'));
        end
        function perform_action(obj,action)
            assignin('base','pick',obj.wasd(logical(action)));
            assignin('base','move',obj.steps);
            
            evalin('base','move_and_render');
            obj.steps = obj.steps + 1;
        end
        function actions = possible_actions(obj)       %returns set of action pool activations
            actions(1,:) = [1 0 0 0];
            actions(2,:) = [0 1 0 0];
            actions(3,:) = [0 0 1 0];
            actions(4,:) = [0 0 0 1];
        end
        function set_initial_state(obj)
            obj.steps = 1;
            
            evalin('base','make_map');
            assignin('base','reward',0);
            assignin('base','won',false);
            assignin('base','animate',0);
            assignin('base','memdecay',0);
            assignin('base','display',0);
            assignin('base','human',0);
        end
        function obj = reset(obj)
            obj.set_initial_state;
        end
    end
    
end