classdef bloodline_game < rl_environment
    properties
    end
    methods
        function TF = at_terminal(obj)
            TF = false;
        end
           patterns = state(obj);
           patterns = reward(obj);
           perform_action(obj,action);
           possible_actions(obj);       %returns set of action pool activations
           set_initial_state(obj)
    end
end