classdef manual_environment < environment
    properties
        
        interval_index = 1;
        sequences = struct('name','1');
        sequence_index = 1;
        in = [0 0];
        out = [1];
        patterns = [];
        pattern_block = [];
    end
    
    methods
        function obj = manual_environment()
%             obj.in = foo;
        end
        function ret = current_patterns(obj)
            if ~isempty(obj.pattern_block)
                ret = obj.pattern_block{obj.sequence_index};
            elseif obj.patterns
                ret = obj.patterns;
            else
                ret = [struct('pool','','type','H','pattern',obj.in) ...
                    struct('pool','','type','T','pattern',obj.out)];
            end
        end
        function reset(obj)
        end
        function set.pattern_block(obj,value)
            %must take the form of a cell array
            %each cell has 2 col, first in, second out
            obj.pattern_block = [];
            obj.pattern_block = cell(size(value,1),1);
            for i = 1:size(value,1)
				obj.sequences(i)=struct('name',num2str(i));
                [curin, curout] = value{i,:};
                obj.pattern_block{i} = [struct('pool','','type','H','pattern',curin) ...
                    struct('pool','','type','T','pattern',curout)];
            end
        end
    end
end