classdef rl_environment < environment
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function patterns = current_patterns(obj)
            patterns = [obj.state obj.reward];
        end 

    end
    methods (Abstract)
           TF = at_terminal(obj);
           patterns = state(obj);
           patterns = reward(obj);
           perform_action(obj,action);
           actions = possible_actions(obj);       %returns set of action pool activations
           set_initial_state(obj)
    end
    
end

