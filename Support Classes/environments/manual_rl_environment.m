classdef manual_rl_environment < rl_environment
    properties
        
        interval_index = 1;
        sequences = 1; %use for modifying how many patterns to show between weight changes
        sequence_index = 1;
        in = [0 0];
        out = [1];
        patterns = [];
%         pattern_block = [];
        actions;
        current_action;
        terminal = false;
    end
    
    methods
        function obj = manual_rl_environment(pos_act)
                obj.actions = pos_act;
%             obj.in = foo;
        end
%         function ret = current_patterns(obj)
%                 ret = [struct('pool','','type','H','pattern',obj.in) ...
%                     struct('pool','','type','T','pattern',obj.out)];
%         end
        function reset(obj)
        end
        function perform_action(obj,action)
            obj.current_action = action;
        end
        function pattern = reward(obj)
            pattern = struct('pool','','type','T','pattern',obj.out);
        end
        function pattern = state(obj)
            pattern = struct('pool','','type','H','pattern',obj.in);
        end
        function pos_actions = possible_actions(obj)
            pos_actions = obj.actions;
        end
        
        function set_initial_state(obj)
            
        end
        
        function TF = at_terminal(obj)
            TF = obj.terminal;
        end
        
    end
end