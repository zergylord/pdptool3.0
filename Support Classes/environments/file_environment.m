%TO DO: networks should have 2 possible environments, one for train and one
%for test
%DO TO: BUILDIN ORDERING RANDOMIZATION
%LET A NET SET THE OPTIONS OF THE ENVIRONMENT
classdef file_environment < environment
     properties% (Access = protected)
         using_sequence_names;
     end
     properties
%         patterns;
%         input_size;
%         output_size;
         interval_index = 1;
%         current_pattern;
%         current_feedback;
         sequences = struct('name','','weight',1,'intervals',struct('name','1','clamps',[]));
         sequence_index = 1;
         ordering = [];

     end
     %Parser assumptions:
     %1)an event line is at least 3 tokens long
     %
     methods% (Access = protected)
         function tf = q_specEpisode(obj,toks)
             tf = true;
             if length(toks) > 2
                 tf = false;
             end
             obj.using_sequence_names = tf;
         end
         
         function setup_episode(obj,toks)
             if obj.using_sequence_names
                 switch length(toks)
                     case 1
                         obj.sequences(obj.sequence_index).name = toks{1};
                         obj.sequences(obj.sequence_index).weight = 1;
                     case 2
                         obj.sequences(obj.sequence_index).name = toks{1};
                         obj.sequences(obj.sequence_index).weight = toks{2};
                 end
             else
                 obj.sequences(obj.sequence_index).name = int2str(obj.sequence_index);
                 obj.sequences(obj.sequence_index).weight = 1;
             end
         end
         
         function tf = q_endEpisode(obj,toks)
          tf = true;
          switch length(toks)  
             case 1
                 if ~strcmpi('end',toks{1})
                     error('PDPtool:FileParseError','End statement mistyped');
                 end
             case 2
                 error('PDPtool:FileParseError','Missing an end statement in pattern file.');
             otherwise
                 tf = false;
          end
         end
         
         function tokened_lines = parse_whitespace(obj,filename)
             tokened_lines = [];
             lines = textscan(fopen(filename), '%s','delimiter','\n','BufSize',8*36000);lines = lines{1};
             for i=1:length(lines)
                 if isempty(lines{i})
                     continue
                 end
                 parsed_line = textscan(lines{i},'%s');
                 tokened_lines = [tokened_lines parsed_line];
             end
         end
         
         function parse_epoch(obj,lines)
             %set index vars
             obj.sequence_index = 1;
             obj.interval_index = 1;
             
             %check if episode tags are used
             obj.using_sequence_names = obj.q_specEpisode(lines{1});
             
             first = true;
             episode_start = 1;
             episode_end = 1;
             
             for i=1:length(lines)
                 cur_line = lines(i); cur_line = cur_line{1};
                 if first
                     obj.setup_episode(cur_line);
                     if ~obj.using_sequence_names
                         %always a single line episode
                         obj.parse_sequence({cur_line});
                     else
                         obj.q_specEpisode(cur_line);
                         first = false;
                         episode_start = episode_start+1;
                         episode_end = episode_end+1;
                     end
                 else
                     if obj.q_endEpisode(cur_line)
                         obj.parse_sequence(lines(episode_start:episode_end-1));
                         first = true;
                         episode_start = episode_end+1;
                         episode_end = episode_start;
                     else
                         episode_end = episode_end +1;
                     end
                 end 
             end
         end
         
         function parse_sequence(obj, lines)
             obj.interval_index = 0;
             i=1;
             while i <= length(lines)                
                 event = lines{i};
                 j=1;
                 while strcmpi(event{end},'...')%check for continuations
                     event = [event(1:end-1); lines{i+j}];
                     j = j+1;
                 end
                 i = i + j - 1; %skip the lines cat'd into one event
                 j=1; 
                 while i+j <= length(lines) && strcmpi(lines{i+j}{1},'|')%check for the next line wanting to continue
                     event = [event ; lines{i+j}];
                     j = j+1;
                 end
                 i = i + j - 1 + 1; %skip the lines cat'd into one event
                 [header event] = get_header_toks(event,'[',']');
                 obj.parse_event_header(header);         
                 while ~isempty(event)
                     [event_clamp event] = toktok(event,'|');
                     if isempty(event_clamp)
                         event_clamp = event;
                         event = [];
                     end
                     obj.parse_clamp(event_clamp);
                 end                 
             end
             obj.sequence_index = obj.sequence_index + 1;
             obj.interval_index = 1;
         end
         
         function parse_event_header(obj, toks)
            switch length(toks)
                case 0
                    name = num2str(obj.interval_index+1);
                    number = obj.interval_index+1;
                case 1
                    name = toks{1};
                    number = str2num(toks{1});
                    if isempty(number)
                        number = obj.interval_index+1;
                    end
                case 2
                    name = toks{1};
                    number = str2num(toks{2});
                otherwise
                    error('PDPtool:FileParseError','Event header needs 1 or 2 args.');
            end
            
            obj.interval_index = number;
            %if intervals between cur interval and number aren't allocated,
            %allocate them
            obj.sequences(obj.sequence_index).intervals(end+1:number) = struct('name','no_clamp','clamps',[]);
            obj.sequences(obj.sequence_index).intervals(number).name = name;
            obj.interval_index = number;
         end
         
         
         function parse_clamp(obj, toks)
             [header body] = get_header_toks(toks,'(',')');
             %parse body
             pattern = [];
             for i=1:length(body)
                 pattern = [pattern str2double(body{i})];
             end
             
             %defaults
             duration = 1;
             %if first clamp, then type is 'H', else 'T'
             if isempty(obj.sequences(obj.sequence_index).intervals(obj.interval_index).clamps)
                 type = 'H';
             else
                 type = 'T';
             end
             pool_name = '';
             
             %parse header
             switch length(header)
                 case 0 
                     %uses defaults
                 case 1
                     if isempty(str2num(header{1}))
                         if strcmpi(header{1},'H') || strcmpi(header{1},'S') || strcmpi(header{1},'T')
                             type = header{1};
                         else
                             pool_name = header{1};
                         end
                     else
                         duration = str2num(header{1});
                     end
                 case 2
                     if strcmpi(header{1},'H') || strcmpi(header{1},'S') || strcmpi(header{1},'T')
                         type = header{1};
                         if isempty(str2num(header{2}))
                             pool_name = header{2};
                         else
                             duration = str2num(header{2});
                         end
                     else
                         duration = str2num(header{1});
                         pool_name = header{2};
                     end
                 case 3
                     type = header{1};
                     duration = str2num(header{2});
                     pool_name = header{3};
                 otherwise
                     error('PDPtool:FileParseError','clamp header needs 1,2, or 3 args.');
             end
             
             %unroll duration and make pattern structs
             for i=1:duration
                 %if not allocated, allocate them
                    obj.sequences(obj.sequence_index).intervals(end+1:obj.interval_index + i - 1) = struct('name','no_clamp','clamps',[]);
                obj.sequences(obj.sequence_index).intervals(obj.interval_index + i - 1).clamps ...
                    = [obj.sequences(obj.sequence_index).intervals(obj.interval_index + i - 1).clamps struct('pool',pool_name,'type',type,'pattern',pattern)];
             end
         end
         
         function parse_sequence_OLD(obj, strcell)
             if length(obj.sequences) < obj.sequence_index
                 obj.sequences(obj.sequence_index) = struct('name','','intervals',cell(1));
             end
             obj.interval_index = 0;
             for i=1:length(strcell) %for each interval in the sequence
                 
                 parsed_line = textscan(strcell{i},'%s','delimiter',')'); parsed_line = parsed_line{1};
                 if length(parsed_line) ==2 %has info on what interval the pattern is for
                     obj.interval_index = str2double(parsed_line{1});
                     parsed_line = textscan(parsed_line{2},'%s','delimiter','|'); parsed_line = parsed_line{1};
                 else %no interval info so its 1 past the previous interval
                     obj.interval_index = obj.interval_index+1;
                     parsed_line = textscan(parsed_line{1},'%s','delimiter','|'); parsed_line = parsed_line{1};
                 end
                 if length(obj.sequences(obj.sequence_index).intervals) < obj.interval_index
                     obj.sequences(obj.sequence_index).intervals{obj.interval_index} = [];
                 end
                 
                 for j=1:length(parsed_line) %for each pattern set during interval i
                     cur_pattern = textscan(parsed_line{j},'%s','delimiter',':'); cur_pattern = cur_pattern{1};
                     if length(cur_pattern)==2  % has header info aka a pool name
                         %FOR LATER: handle headers with missing info
                         header = textscan(cur_pattern{1},'%s');header = header{1};
                         cur_pattern = str2num(cur_pattern{2});
                         for k = 1:str2double(header{2})
                             if length(obj.sequences(obj.sequence_index).intervals) < obj.interval_index - 1 + k
                                 obj.sequences(obj.sequence_index).intervals{obj.interval_index - 1 + k} = [];
                             end
                             obj.sequences(obj.sequence_index).intervals{obj.interval_index - 1 + k} = [obj.sequences(obj.sequence_index).intervals{obj.interval_index - 1 + k} ...
                                 struct('pool',header{3},'type',header{1},'pattern',cur_pattern)];
                         end
                         
                     else %no pool name, so is of the form input pattern | output pattern
                         cur_pattern = str2num(cur_pattern{1});
                         if j==1
                             obj.sequences(obj.sequence_index).intervals{obj.interval_index} = [obj.sequences(obj.sequence_index).intervals{obj.interval_index} ...
                                 struct('pool','','type','H','pattern',cur_pattern)];
                         else
                             obj.sequences(obj.sequence_index).intervals{obj.interval_index} = [obj.sequences(obj.sequence_index).intervals{obj.interval_index} ...
                                 struct('pool','','type','T','pattern',cur_pattern)];
                         end
                     end
                 end
             end
             obj.sequence_index = obj.sequence_index + 1;
             obj.interval_index = 1;
         end
     end
    methods   
        function obj = file_environment(filename)
            if nargin < 1
                return
            end
           obj.parse_epoch(obj.parse_whitespace(filename));
           obj.ordering = randperm(length(obj.sequences));
            obj.sequence_index = 1;
            obj.interval_index = 1;
		end
		%DESTRUCTIVE method (calling might change future value). Only to be
		%called in compute_output. Index directly if othewise needed:
		% obj.sequences(sind).interval(iind).clamps
        function patterns = current_patterns(obj)
            switch obj.trainmode
                case 's'
                    patterns = obj.sequences(obj.sequence_index).intervals(obj.interval_index).clamps;
					obj.true_sequence_index = obj.sequence_index;
                case 'p'
					ind = obj.ordering(obj.sequence_index);
                    patterns = obj.sequences(ind).intervals(obj.interval_index).clamps;
					obj.true_sequence_index = ind;
                    if obj.sequence_index == length(obj.sequences)
                        obj.ordering = randperm(length(obj.sequences));
                    end
                case 'r'
                    ind = randi(length(obj.sequences));
                    patterns = obj.sequences(ind).intervals(obj.interval_index).clamps;
					obj.true_sequence_index = ind;
            end
        end
        %OLD
        function patterns = next_patterns(obj)
            if obj.interval_index == obj.num_intervals
                obj.interval_index = 1;
                if obj.sequence_index == length(obj.sequences) %changed
                    obj.sequence_index = 1;
                else
                    obj.sequence_index = obj.sequence_index +1;
                end
            else
                obj.interval_index = obj.interval_index + 1;
            end
            
            patterns = obj.current_patterns;
        end
        
        function num = num_intervals(obj)
            num = length(obj.sequences(obj.sequence_index).intervals);
        end
        
        function obj = reset(obj)
            obj.sequence_index = 1;
            obj.interval_index = 1;
        end
    end
end