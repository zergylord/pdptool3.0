classdef image_environment < environment
   properties

         interval_index = 1;
         sequences = struct('name','','intervals',cell(1));
         sequence_index = 1;
   end
   
   methods
       function obj = image_environment(pairs)
           switch class(pairs)
               case 'double'
               case 'uint8'
                   pairs = double(pairs)/255;
               case 'logical'
                   pairs = double(pairs);
           end
           obj.sequence_index = 0;
           for i=1:2:length(pairs)
               obj.sequence_index = obj.sequence_index +1;
               obj.sequences(obj.sequence_index).intervals{obj.interval_index} = ...
                   [struct('pool','','type','H','pattern',pairs{i}(:)') ...
                   struct('pool','','type','T','pattern',pairs{i+1}(:)')];
           end
           obj.sequence_index = 1;
       end
       function patterns = current_patterns(obj)
          patterns = obj.sequences(obj.sequence_index).intervals{obj.interval_index};
       end
       function reset(obj)
           
       end
   end
end