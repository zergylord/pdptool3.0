classdef recurrent_example_file < example_file_environment
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sequences;
        %patterns;
        %input_size;
        %output_size;
        sequence_index = 1;
    end
    
    methods
        function obj = recurrent_example_file(filename)
            obj = obj@example_file_environment();
            line = textscan(fopen(filename), '%s','delimiter','\n');
            line = line{1};
            obj.sequences{obj.sequence_index}.name = line{1};
            newseq = 0;
            for i=2:length(line)
                if newseq
                    newseq = 0;
                    obj.pattern_index = 1;
                    obj.sequence_index = obj.sequence_index + 1;
                    obj.sequences{obj.sequence_index}.name = line{i};
                    continue
                end
                if strcmpi(line{i},'end')
                    newseq = 1;
                    continue
                end
                parts = textscan(line{i},'%s','delimiter','|');                
                parts = parts{1};
                input = textscan(parts{1},'%f');
                input = input{1}';
                output = textscan(parts{2},'%f');
                output = output{1}';
                 obj.sequences{obj.sequence_index}.data{obj.pattern_index} = struct('input',input,'output',output);
                 obj.pattern_index = obj.pattern_index +1;
            end
            obj.sequence_index = 1;
            obj.pattern_index = 1;
            obj.patterns = obj.sequences{1}.data;
            obj.input_size = length(obj.patterns{1}.input);
            obj.output_size = length(obj.patterns{1}.output);
        end
        function pattern = next_pattern(obj)
            obj.pattern_index = mod(obj.pattern_index + 1,length(obj.patterns)+1);
            if obj.pattern_index == 0
                obj.pattern_index = 1;
                obj.sequence_index = mod(obj.sequence_index + 1,length(obj.sequences)+1);
                if obj.sequence_index == 0
                    obj.sequence_index = 1;
                end
            end
            obj.patterns = obj.sequences{obj.sequence_index}.data;
            pattern = obj.patterns{obj.pattern_index}.input;
        end
    end
    
end

