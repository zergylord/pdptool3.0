%%
% clear all;
%%
global net
pdpinit('gui');

pool('in',7,'input');
pool('hid',10,'hidden');
pool('context',10,'copyback',hid);
pool('out',7,'output');
e = file_environment('srn_gsm21.pat'); %changed pat file
hid.connect(in);
hid.connect(context);
out.connect(hid);

net = srn_net([in context hid out],1337,'wrange',.1);
net.pools(2).projections = [];
net.environment = e;
net.train_options.lrate = .1;
net.train_options.nepochs = 50;
net.train_options.mu = 0;
net.train_options.trainmode = 'p';
net.train_options.momentum = 0;

net.test_options.mu = 0;

loadtemplate srn_gsm_new.tem;
launchnet;

%%
% net.first_pattern = 0;
%%
%  net.train;