%%
% clear all
%%
global net
prepare_gui;
pool('visual',41,'input');
% visual.activation_reset_value = -.1;
visual.unames = {'01_Jets' '02_Sharks' '03_in20s' '04_in30s' '05_in40s' '06_JH' '07_HS' '08_College' '09_Single' '10_Married' '11_Divorced' '12_Pusher' '13_Burglar' '14_Bookie'...
    '15_Art' '16_Al' '17_Sam' '18_Clyde' '19_Mike' '20_Jim' '21_Greg' '22_John' '23_Doug' '24_Lance' '25_George' '26_Pete' '27_Fred' '28_Gene' '29_Ralph'...
    '30_Phil' '31_Ike' '32_Nick' '33_Don' '34_Ned' '35_Karl' '36_Ken' '37_Earl' '38_Rick' '39_Ol' '40_Neal' '41_Dave'};
%visual.unit_names = something

pool('hidpool',27,'hidden');
% hidpool.activation_reset_value = -.1;
hidpool.unames = {'01_Art' '02_Al' '03_Sam' '04_Clyde' '05_Mike' '06_Jim' '07_Greg' '08_John' '09_Doug' '10_Lance' '11_George' '12_Pete' '13_Fred' '14_Gene' '15_Ralph'...
    '16_Phil' '17_Ike' '18_Nick' '19_Don' '20_Ned' '21_Karl' '22_Ken' '23_Earl' '24_Rick' '25_Ol' '26_Neal' '27_Dave'};

net = iac_net([visual hidpool],1337);

%moved this call up for order of projections (shouldn't matter)
hid2hid = hidpool.connect(hidpool);
hid2hid.weights(:) = load('constr_hid'); %untransposed 

vis2hid = hidpool.connect(visual);
vis2hid.weights = load('constr_hid_vis')'; %constr_hid_vis is transposed for vis2hid projection

vis2vis = visual.connect(visual); 
vis2vis.weights =load('constrvis'); 

hid2vis = visual.connect(hidpool); 
hid2vis.weights = load('constr_hid_vis'); 



net.test_options.ncycles = 20;
net.test_options.rest = -.1;

filename = getfilename('Jetslog','.mat');
myel = 4; myaz = 4;
setoutputlog ('file', filename,'process', 'test','frequency', 'cycle','status', 'on','writemode', 'binary',...
    'objects', {'cycleno','pools(2).activation(1:2)','pools(2).activation(3:5)','pools(2).activation(6:8)','pools(2).activation(9:11)',...
    'pools(2).activation(12:14)','pools(2).activation(15:41)','pools(3).activation(1:15)','pools(3).activation(16:27)'},...
    'onreset', 'clear','plot', 'on','plotlayout', [5 3]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'pools(2).activation(1:2)'},  'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Gang', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 2, 'yvariables', {'pools(2).activation(15:41)'},'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Name', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 3, 'yvariables', {'pools(3).activation(1:15)'}, 'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Jets-Inst', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 4, 'yvariables', {'pools(2).activation(3:5)'},  'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Age', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 7, 'yvariables', {'pools(2).activation(6:8)'},  'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Edu', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 10,'yvariables', {'pools(2).activation(9:11)'}, 'plot3d', 1,'az', myaz, 'el', myel, 'title', 'MarStat', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 13,'yvariables', {'pools(2).activation(12:14)'},'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Occ', 'ylim', [-0.2 1]);
setplotparams ('file', filename, 'plotnum', 6, 'yvariables', {'pools(3).activation(16:27)'},'plot3d', 1,'az', myaz, 'el', myel, 'title', 'Shk-Inst', 'ylim', [-0.2 1]);
% setcolormap 'jmap.mat';
% setcolorbar 'on';

loadtemplate iac_jets.tem;
launchnet;

%%
% net.test;