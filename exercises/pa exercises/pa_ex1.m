%%
%clear all;
%%
global net
prepare_gui;

 pool('p1',8,'input');
 p1.activation_function = 'linear';
 pool('p3',8,'output');
 p3.activation_function = 'linear';
 e = file_environment('pa_ex1_li.pat');
 
p3.connect(p1);
p1.outgoing_projections(1).using(1).constraint_type = 'scalar'; %for first exercise
p1.outgoing_projections(1).using(1).constraint = 0.0; %for first exercise

mynet = pa_net([p1 p3],1337,'manual_bias',true);
net = mynet;
%mynet.pools(2).outgoing_projections(1).using(1).constraint_type = 'scalar'; %for first exercise
%mynet.pools(2).outgoing_projections(1).using(1).constraint = 0.0;

mynet.environment = e;
mynet.train_options.nepochs = 1;
mynet.train_options.lrule = 'Hebb'; %for first exercise
mynet.train_options.lrate = 0.125; %for first exercise

logfname = getfilename('cttsslog','.mat');
setoutputlog ('file',logfname,'process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','tss'},'onreset', 'new','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', logfname, 'plotnum', 1, 'yvariables', {'tss'}, 'ylim', [0 40]);

loadtemplate pa_8x8.tem;
launchnet;
%%
% mynet.test;
%%
% mynet.train;
