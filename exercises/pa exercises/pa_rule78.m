%%
%clear all;
%%

%this driver uses the 8x8 template

global net
prepare_gui;

 pool('p1',8,'input');
 p1.activation_function = 'linear';
 pool('p3',8,'output');
 p3.activation_function = 'stochastic';
 e = file_environment('pa_rule78.pat');
 
p3.connect(p1);
p1.outgoing_projections(1).using(1).constraint_type = 'scalar'; 
p1.outgoing_projections(1).using(1).constraint = 0.0; 

mynet = pa_net([p1 p3],1337,'manual_bias',true);

mynet.environment = e;
mynet.train_options.nepochs = 10; 
mynet.temp = 1.0;
mynet.environment.trainmode = 'p';
mynet.train_options.lrate = 0.05; 


net = mynet;

loadtemplate pa_8x8.tem;
launchnet;

%%
% mynet.test;
%%
% mynet.train;
