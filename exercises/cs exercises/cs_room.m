global net
pdpinit('gui');

pool('input',40,'input');
input.unames = textread('roomunames.txt','%s');


e = file_environment('cs_room.pat');

input.connect(input);

mynet = cs_net([input],1337);


mynet.pools(2).projections(1).using(1).weights = dlmread('csroombiaswt');
mynet.pools(2).projections(1).using(1).weights = dlmread('csroomwt');

mynet.environment = e;
mynet.test_options.temp = 2;
mynet.test_options.nupdates = 40;
mynet.test_options.ncycles = 50;
mynet.test_options.istr = 0.5;
mynet.test_options.clamp = 0;
mynet.test_options.annealsched = [0 2; 200 0.05];

net = mynet;

loadtemplate cs_room.tem;
launchnet;