function myhist = manycubes(n,cycles)
global net;
goodlog = 1:n;
x = 0:.5:16;
net.test_options.ncycles = cycles;
for i = 1:n
    newstart;
    net.test;
    goodlog(i) = net.goodness;
    if (mod(i,1000)==0)
        fprintf('\n');
    elseif (mod(i,50)==0)
        fprintf('.');
    end
end
myhist = histc(goodlog,x);
figure; bar(x,histc(goodlog,x)); set(gca,'xtick',[0:16],'xlim',[-.5 16.5],'ylim',[0 n]);
drawnow;

