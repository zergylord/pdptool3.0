global net
prepare_gui;

pool('inp',16);
inp.connect(inp); %moved to after net 
mynet = cs_net(inp,6543,'nupdates',16,'ncycles',20,'istr',0.4,'estr',0.4,'schedule',[0 2.0;20 0.5]);

mynet.pools(2).unames = {'Abul', 'Abur', 'Aful', 'Afur', 'Abll', 'Ablr', 'Afll', 'Aflr', 'Bbul', 'Bbur', 'Bful', 'Bfur', ...
    'Bbll', 'Bblr', 'Bfll', 'Bflr'};
mynet.pools(1).outgoing_projections(1).using(1).weights = ones(16,1)*.5; %changed
mynet.pools(2).outgoing_projections(1).using(1).weights = dlmread('cscubewt'); %changed
mynet.test_options.ncycles = 20;

net = mynet;

loadtemplate cs_cube.tem;
launchnet;