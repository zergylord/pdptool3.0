global net
pdpinit('gui'); %or 'nogui'
pool('inp',16);
inp.connect(inp);
mynet = cs_net(inp,6543,'actfunction','boltzmann','nupdates',16,'ncycles',20,'istr',1,'estr',1,'schedule',[0 0;20 0]);%[0 2.0;20 0.5]);

mynet.pools(1).outgoing_projections(1).using(1).weights = ones(16,1)*.5;
mynet.pools(2).outgoing_projections(1).using(1).weights = dlmread('cscubewt');

mynet.pools(2).unames = {'Abul', 'Abur', 'Aful', 'Afur', 'Abll', 'Ablr', 'Afll', 'Aflr', 'Bbul', 'Bbur', 'Bful', ...
    'Bfur', 'Bbll', 'Bblr', 'Bfll', 'Bflr'};

net = mynet;

loadtemplate cs_cube.tem;
launchnet;