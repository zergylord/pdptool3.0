%%
% clear all;
%%
global net
prepare_gui;

%e = file_environment('NEWmyfeatures5.pat');
pool('name',40,'inout');
pool('verbal_descriptors',112,'inout');
pool('visual_features',64,'inout');
pool('semantics',64,'hidden');

name.connect(semantics);
verbal_descriptors.connect(semantics);
visual_features.connect(semantics);
semantics.connect(name);
semantics.connect(verbal_descriptors);
semantics.connect(visual_features);
semantics.connect(semantics);



mynet = rbp_net([name verbal_descriptors visual_features semantics],1337,'wdecay',.0000069444,'wrange',0.25,'errmargin',0.05,'lgrain','sequence');



mynet.environment = file_environment('rbp_features.pat');
mynet.environment.trainmode = 's';

mynet.ticks_per_interval = 4;

mynet.train_options.lrate = .001;
mynet.train_options.errmeas = 'cee';
mynet.train_options.momentum = 0;
mynet.train_options.nepochs = 500;

net = mynet;

loadtemplate rbp_rogers.tem;
launchnet;
% loadweights rogtest.wt
% get_weights(net);

%%
% net.train