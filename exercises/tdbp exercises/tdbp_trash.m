%% IT WORKS! WHO KNOWS IF ANNEALING WORKS THOUGH, SINCE THEY DON'T USE IT...
% clear all;
%%
global net
prepare_gui;

pool('grid',9,'input');
pool('action',4,'input');
pool('hid',5);
pool('out',1,'output');
out.activation_function = 'linear';
e = trashgrid();
hid.connect(grid);
hid.connect(action);
out.connect(hid);
seed = randi(2^32);
% seed = 214251;
net = tdbp_net([grid  action hid out],214251,'manual_bias',true);
net.environment = e;
net.netmode = 'afterstate';
net.train_options.gamma = .7;
net.train_options.lambda = .5;
net.train_options.nepochs = 350;
net.train_options.policy = 'softmax';
net.test_options.policy = 'greedy';
% net.train_options.annealsched = [0 1];
net.schedule = [0 5;300 0];

%for off-policy training with Q-learning
% net.schedule = [0 5;3000 0];
% obj.offPolicyLearning = true;

loadtemplate tdbp_trash.tem;
launchnet;
filename = getfilename('xorlog','.mat');
setoutputlog ('file', filename,'process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','cum_reward','temp'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'cum_reward','temp'});
%%
% net.first_pattern = 0;
%%
%  net.train;