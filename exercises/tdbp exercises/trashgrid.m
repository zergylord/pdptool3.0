classdef trashgrid < rl_environment
    
    properties
        %All class-internal variables go here.
        lflag;
        
        %non-persisting variables
        current_location;       %vector of x, y location of the robot
        grid_size;              %vector of width, height of the grid world
        end_location;           %vector of x, y location of end square
        grid;                   %matrix of grid
        trash_flag;             %logical value indicating whether robot picked up trash
        steps;                  %number of steps elapsed in current episode
        trash_location;         %vector of x, y location of the trash
        num_trash;              
        num_trash_collected;
        
        %persisting variables
        trash_collected_data;
        
        interval_index;
        
    end
    
    methods
        
        %functions to conform to rl_environment
        function patterns = state(obj)
            patterns = struct('pool','grid','type','H','pattern',reshape(obj.grid, 1, numel(obj.grid)));
        end
        function patterns = reward(obj)
            patterns = struct('pool','','type','T','pattern',obj.getCurrentReward);
        end
        function perform_action(obj,action)
            obj.setNextState([zeros(1,numel(obj.grid)) action]);
        end
        function actions = possible_actions(obj)
           actions = obj.getNextStates;
           actions = actions(:,end-3:end);
        end
        function ind = get.interval_index(obj)
            ind = obj.steps;
        end
        function set_initial_state(obj)
            
        end
        
        
        function obj = trashgrid()
            obj = obj.construct();
            %setup our persisting variables
            obj.trash_collected_data = zeros(0,1);
        end

        function obj = construct(obj)
            obj.lflag = 1;  %This enables learning.
            
            %setup our non-persisting variables
            obj.num_trash = 1;
            obj.num_trash_collected = 0;
            obj.steps = 0;
            obj.trash_flag = 0;
            obj.current_location = [1,1];
            obj.grid_size = [3,3];
            obj.grid = zeros(obj.grid_size);
            obj.end_location = [2,3];
            obj.trash_location = [3,1];
            obj.grid(obj.current_location) = 1; %place a 1 in the robot's
                                                %current location
            obj.grid(obj.trash_location(1), obj.trash_location(2)) = .5;     %add trash to trash location
        end

        function obj = reset(obj)
            obj.trash_collected_data(end+1) = obj.num_trash_collected;
            obj = obj.construct();
        end
        
        function add_to_list = runStats(obj)
            avg_trash_collected = mean(obj.trash_collected_data);
            add_to_list = {sprintf('avg trash: %f', avg_trash_collected)};
        end
        
        function next_state = doPolicy(obj, next_states, state_values)

        end
        
        function endFlag = at_terminal(obj)
            endFlag = all(obj.current_location == obj.end_location);
        end

        function reward = getCurrentReward(obj)
            reward = 0; %intialize to default value - don't change
            if obj.trash_flag   %intermediate reward for trash
                reward = 3.5;
            elseif obj.at_terminal() %terminal reward
                reward = 12 - obj.steps;
                 if reward < 0
                     reward = 0;
                 end
            end
        end
        
        function state = getCurrentState(obj)
            state = reshape(obj.grid, 1, numel(obj.grid));
            state = [state, zeros(1,4)];
        end
        
        function states = getNextStates(obj)
            % We build next states by concatenating the current grid with
            % possible actions represented in a length 4 vector. The
            % action vector represents the actions move left, move right,
            % move up, and move down in that order.
            state_num = 1;
            if obj.current_location(1) ~= 1 %are we against left wall?
                states(state_num,:) = [reshape(obj.grid, 1, numel(obj.grid)), [1 0 0 0]];
                state_num = state_num + 1;
            end
            if obj.current_location(1) ~= size(obj.grid,1) %are we against right wall?
                states(state_num,:) = [reshape(obj.grid, 1, numel(obj.grid)), [0 1 0 0]];
                state_num = state_num + 1;
            end
            if obj.current_location(2) ~= 1 %are we against top wall?
                states(state_num,:) = [reshape(obj.grid, 1, numel(obj.grid)), [0 0 1 0]];
                state_num = state_num + 1;
            end
            if obj.current_location(2) ~= size(obj.grid,2) %are we against bottom wall?
                states(state_num,:) = [reshape(obj.grid, 1, numel(obj.grid)), [0 0 0 1]];
            end
        end

        function obj = setNextState(obj, next_state)
            action_vector = next_state(numel(obj.grid)+1:end);
            obj.grid(obj.current_location(1),obj.current_location(2)) = 0;
            obj.trash_flag = 0;
            if action_vector(1) %move left
                obj.current_location = obj.current_location - [1,0];
            elseif action_vector(2) %move right
                obj.current_location = obj.current_location + [1,0];
            elseif action_vector(3) %move up
                obj.current_location = obj.current_location - [0,1];
            elseif action_vector(4) %move down
                obj.current_location = obj.current_location + [0,1];
            end
            obj.grid(obj.current_location(1),obj.current_location(2)) = 1;
            trash = numel(find(obj.grid == .5));
            if trash < (obj.num_trash - obj.num_trash_collected)
                obj.num_trash_collected = obj.num_trash_collected + 1;
                obj.trash_flag = 1; %let's us know that we just picked up trash
                                %so we can give intermediate reward in the reward()
                                %method
            end
            obj.steps = obj.steps + 1;
        end
        
    end
    
end

