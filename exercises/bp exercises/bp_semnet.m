global net
prepare_gui;

pool('item',8,'input');
pool('relation',4,'input');
pool('representation',8,'hidden');
pool('hid',15,'hidden');
pool('attribution',36,'output');


item.unames =  {'Pine' 'Oak' 'Rose' 'Daisy' 'Robin' 'Canary' 'Sunfish' 'Salmon'};
relation.unames = {'ISA' 'Is' 'Can' 'Has'};
attribution.unames = {'Living thing' 'Plant' 'Animal' 'Tree' 'Flower' 'Bird' 'Fish' 'Pine' 'Oak' 'Rose' 'Daisy' 'Robin' 'Canary'...
    'Sunfish' 'Salmon' 'Pretty' 'Big' 'Living' 'Green' 'Red' 'Yellow' 'Grow' 'Move' 'Swim' 'Fly' 'Sing' 'Skin' 'Roots' 'Leaves' ...
    'Bark' 'Branch' 'Petals' 'Wings' 'Feathers' 'Gills' 'Scales'};


%item.projections = [];
%relation.projections = [];

representation.connect(item);
hid.connect(relation);
hid.connect(representation);
attribution.connect(hid);


net = bp_net([item relation representation hid attribution], 1337,'wrange',0.9);


net.environment = file_environment('bp_semnet_new.pat');


net.train_options.trainmode = 'p';
net.train_options.lrate = 0.1;
net.train_options.momentum = 0;
net.train_options.tmax = 1.0;


%net.pools(4).projections(1).lr = NaN;
net.pools(4).projections(1).constraint = 0.0;

%net.pools(4).connect(net.pools(2)); %representation to item
%net.pools(4).projections(2).lr = NaN;
net.pools(4).projections(2).constraint = 0.0;

net.pools(5).projections(1).constraint_type = 'scalar';
%net.pools(5).projections(1).lr = NaN;
net.pools(5).projections(1).constraint = 0.0;

%net.pools(5).connect(net.pools(3)); %hid to relation
%net.pools(5).projections(2).lr = NaN;
net.pools(5).projections(2).constraint = 0.0;

%net.pools(5).connect(net.pools(4)); %hid to representation 
%net.pools(5).projections(3).lr = NaN;
net.pools(5).projections(3).constraint = 0.0;

%net.pools(6).projections(1).constraint_type = 'scalar';
%net.pools(6).projections(1).lr = 0;
net.pools(6).projections(1).constraint = -2;

%net.pools(6).connect(net.pools(5)); %attribution to hid
%net.pools(6).projections(2).lr = NaN;
net.pools(6).projections(2).constraint = 0.0;

loadtemplate bp_semnet.tem;
launchnet;
