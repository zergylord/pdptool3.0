
bp_xor
setoutputlog ('file', 'xorlog3.mat','process', 'train','frequency', 'epoch','status', 'on','objects', {'epochno','tss'},'plot', 'on','plotlayout', [1 1]);
setplotparams ('file', 'xorlog3.mat', 'yvariables', {'tss'});
setoutputlog ('file', 'xoract3.mat','process', 'train','frequency', 'patsbyepoch','status', 'on','objects', {'epochno','pools(4).activation'},'plot', 'on','plotlayout', [1 1]);
setplotparams ('file', 'xoract3.mat', 'yvariables', {'pools(4).activation'}, 'ylim', [0 1]);