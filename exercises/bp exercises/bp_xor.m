%%
%clear all;
%%
global net
pdpinit('gui'); %or 'nogui'
pool('p1',2,'input');
pool('p2',2,'hidden');
pool('p3',1,'output');
e = file_environment('bp_xor.pat');
p2.connect(p1);
p3.connect(p2);
mynet = bp_net([p1 p2 p3],1337,'wrange',1);
mynet.environment = e;


net = mynet;


net.train_options.nepochs = 30;
net.train_options.ecrit = 0.04;
net.train_options.lgrain = 'epoch';

filename = getfilename('xorlog','.mat');
setoutputlog ('file', filename,'process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','tss'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'tss'});
filename = getfilename('xoract','.mat');
setoutputlog ('file', filename,'process', 'train','frequency', 'patsbyepoch','status', 'on','writemode', 'binary','objects', {'epochno','pools(4).activation'},'onreset', 'clear','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', filename, 'plotnum', 1, 'yvariables', {'pools_by_name(''p3'').activation'}, 'ylim', [0 1]);


loadtemplate bp_xor.tem;

launchnet;
loadweights ('xor.wt');
runprocess ('granularity','pattern','count',1,'alltest',1,'range',[]);
%%
% mynet.test;
%%
% mynet.train;