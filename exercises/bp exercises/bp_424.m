global net
prepare_gui;

pool('p1',4,'input');
pool('p2',2,'hidden');
pool('p3',4,'output');

p2.connect(p1);
p3.connect(p2);

net = bp_net([p1 p2 p3],1337,'wrange',1);

inpool = net.pools_by_name('p1');
inpool.projections = [];
net.train_options.ecrit = 0.04;
net.environment = file_environment('bp_424.pat');
loadweights('424.wt');

loadtemplate bp_424.tem;
launchnet;