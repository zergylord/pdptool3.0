classdef pool < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    %TO DO: SET SOME PROPS TO BE PRIVATE
    properties
        name;
        unit_count;
        index;
        
        activation;
        activation_reset_value;
        clamped_activation = 0; %0 no clamp, 1 soft clamp, 2 hard clamp
        activation_history;
        activation_function = 'logistic';
        
       % elig_trace;
        
        net_input;
        net_input_reset_value;
        
        %all projections now incoming
        projections = [];
        outgoing_projections = []; %just used for rbp error
        
        error; % same is dEd net for rbp
        error_reset_value; %non-zero for connection type pools only
        error_history;
        clamp_states;
        target_states;
        has_target;
        has_clamp;
        
        target;
        %display_target; %added
        clamped_error = false;
        
        delta;
        delta_reset_value
        

        copyback = false;
        copy_from;
        
        type = 'hidden';
        
        %for iac
        excitation;
        inhibition;
        unames;
        noise; %added 
        %for cs
        extern_input = 0;
        %for pa
        



    end
    
    methods
        function obj = pool(name,count,type,copy_from)
            obj.name = name;
            assignin('caller', name, obj);
            obj.unit_count = count;
            obj.error = zeros(1,count);
            obj.error_reset_value = zeros(1,count);
            obj.target = NaN(1,count); %zeros(1,count);%; %changed
            %obj.display_target = zeros(1,count); %added
            obj.delta = zeros(1,count);
            obj.delta_reset_value = zeros(1,count);
            obj.activation = zeros(1,count);
            obj.activation_reset_value = zeros(1,count);
            obj.net_input = zeros(1,count);
            obj.net_input_reset_value = zeros(1,count);
            obj.activation_history(:,1) = zeros(1,count);
            obj.error_history(:,1) = zeros(1,count);
            obj.extern_input = zeros(1,count);
            obj.excitation = zeros(1,count);
            obj.inhibition = zeros(1,count);
            obj.noise = 0; %added
            if nargin > 2
                obj.type = type;
                if nargin == 4
                    obj.clamped_activation = 2;
                    obj.activation = ones(1,count)/2;
                    obj.copy_from = copy_from;
                    %CHECK TO SEE WHY NOT JUST HAVE A COPYBACK TYPE
                    %NO REASON NOT TO
                    obj.copyback = true;
                end
            else
                obj.type = 'hidden';
            end
            switch obj.type
                case 'input'
                    obj.clamped_activation = 2;
                case 'output'
                    obj.clamped_error = true;
                case 'bias'
                    obj.clamped_activation = 2;
                    obj.activation = 1;
                case 'connection'
                    %FILL THIS IN NEXT +++++++++++++++++
                    
                case 'hidden'
                case 'copyback'
            end
            
        end
        
        function proj = connect(obj,send_pool,proj_var_OR_type,con)
            %see if you need a new projection or if the user provided one
                proj = projection(obj, send_pool);
            if nargin > 2
                if ischar(proj_var_OR_type)
                    proj.constraint_type = proj_var_OR_type;
                    if nargin >3
                        proj.constraint = con;
                    end
                else
                     proj = proj_var_OR_type;                       
                end
            else

            end
            obj.projections = [obj.projections struct('from',send_pool,'using',proj)];
            send_pool.outgoing_projections = [send_pool.outgoing_projections struct('to',obj,'using',proj)];
        end
        

        function value =  get.error(obj)
            value = obj.error;
        end

    end
    
end

