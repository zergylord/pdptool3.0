classdef iac_net < bp_net %SWITCH TO SUBCLASSING NET ONCE A GENERAL NET CLASS EXISTS
    %NEED TO CARE ABOUT EXTERNAL INPUTS
   properties
        next_cycleno = 1;
   end
   methods
       function obj = iac_net(varargin)
            obj = obj@bp_net(varargin{:},'manual_bias',true);
            obj.test_options =struct('ncycles',10,'actfunction','st','estr',0.4,'alpha',0.1,... %changed estr to 0.4 per exercises
                'gamma',0.1,'max',1,'min',-0.2,'decay',0.1,'rest',-0.1,...
                'testset','none');
            obj.type = 'iac';
			for i = 2:length(obj.pools) %doesn't reset bias
				obj.pools(i).activation = zeros(size(obj.pools(i).activation))+obj.test_options.rest;
			end
            %SETUP WEIGHTS HERE BASED ON FILES GIVEN?
       end
       
       function cycle(obj)
           obj.getnet;
           obj.update;
       end
       
       
       function reset_net(obj)
            obj.next_cycleno = 1;
            obj.cycleno = 0;
            obj.epochno = 1;
            obj.patno = 0;
            obj.next_patno = 1;
            obj.next_epochno = 1;
%             for i = 2:length(obj.pools) %doesn't reset bias
%                 obj.pools(i).activation = obj.pools(i).activation_reset_value;
% 			end
			for i = 2:length(obj.pools) %doesn't reset bias
				obj.pools(i).activation = zeros(size(obj.pools(i).activation))+obj.test_options.rest;
			end
            resetlog;
            if obj.gui
                update_display(0);
            end
       end
       
       function getnet(obj)
           for i=1:length(obj.pools) %changed start from 1 to 2 
               obj.pools(i).excitation(:) = 0;
               obj.pools(i).inhibition(:) = 0;
               for j=1:length(obj.pools(i).projections)
                   from = obj.pools(i).projections(j).from;
                   proj = obj.pools(i).projections(j).using;
                   posact = find(from.activation>0);
%                    for k=1:numel(posact)
%                        ind = posact(k);
%                        w = proj.weights(:,ind);
%                        obj.pools(i).excitation(w>0.0) = obj.pools(i).excitation(w>0.0)...
%                            + from.activation(ind) .* w(w>0.0)';
%                        obj.pools(i).inhibition(w<0.0) = obj.pools(i).inhibition(w<0.0)...
%                            + from.activation(ind) .* w(w<0.0)';
%                    end
                       if posact
                           w = proj.weights(:,posact);
                           pos_w = max(w,0);
                           obj.pools(i).excitation = obj.pools(i).excitation...
                               + from.activation(posact) * pos_w';
                           neg_w = min(w,0);
                           obj.pools(i).inhibition = obj.pools(i).inhibition...
                               + from.activation(posact) * neg_w';
                       end

               end
               
               obj.pools(i).excitation = obj.pools(i).excitation * obj.test_options.alpha; %missing before
               if (obj.pools(i).noise) %missing before
                  obj.pools(i).excitation = obj.pools(i).excitation +... 
                  obj.pools(i).noise*randn(1,obj.pools(i).unit_count);
               end 
               obj.pools(i).inhibition = obj.pools(i).inhibition * obj.test_options.gamma; %missing before
               
               switch obj.test_options.actfunction
                   case 'st'
                       obj.pools(i).net_input = obj.pools(i).excitation + ...
                           obj.pools(i).inhibition + ...
                           obj.test_options.estr * ...
                           obj.pools(i).extern_input;
                   case 'gr'
                       posext = find(obj.pools(i).extern_input > 0);
                       negext = find(obj.pools(i).extern_input < 0);
                       if ~isempty(posext)
                           obj.pools(i).excitation(posext)= obj.pools(i).excitation(posext)+ ...
                               obj.test_options.estr * ...
                               obj.pools(i).extern_input(posext);
                       end
                       if ~isempty(negext)
                           obj.pools(i).inhibition(negext)= obj.pools(i).inhibition(negext) + ...
                               obj.test_options.estr * ...
                               obj.pools(i).extern_input(negext);
                       end
                   
               end
           end
       end
       
       function update(obj)
           for i=1:length(obj.pools)
               switch obj.test_options.actfunction
                   case 'st'
                       z = find(obj.pools(i).net_input > 0);
                       if ~isempty(z)
                           obj.pools(i).activation(z) = obj.pools(i).activation(z) ...
                               + obj.pools(i).net_input(z) .* (obj.test_options.max - obj.pools(i).activation(z)) ...
                               - obj.test_options.decay * (obj.pools(i).activation(z) - obj.test_options.rest);
                       end
                       y = find(obj.pools(i).net_input <= 0);
                       if ~isempty(y)
                           obj.pools(i).activation(y) = obj.pools(i).activation(y) ...
                               + obj.pools(i).net_input(y) .* (obj.pools(i).activation(y) - obj.test_options.min) ...
                               - obj.test_options.decay * (obj.pools(i).activation(y) - obj.test_options.rest);
                       end
                   case 'gr'
                       obj.pools(i).activation = obj.pools(i).activation + obj.pools(i).excitation .* (obj.test_options.max - obj.pools(i).activation) ...
                           + obj.pools(i).inhibition .* (obj.pools(i).activation - obj.test_options.min) ...
                           - obj.test_options.decay * (obj.pools(i).activation - obj.test_options.rest);
               end
               activ = obj.pools(i).activation; %changed this
               obj.pools(i).activation(activ > obj.test_options.max) = obj.test_options.max;
               obj.pools(i).activation(activ < obj.test_options.min) = obj.test_options.min;
           end
       end
       
       function test(obj)
           cycle_limit = (floor(obj.cycleno/obj.test_options.ncycles)+1)*obj.test_options.ncycles;
          while obj.next_cycleno <= cycle_limit
              obj.cycleno = obj.next_cycleno;
              obj.cycle;
              obj.next_cycleno = obj.cycleno + 1;
                if obj.done_updating_cycleno
                    return
                end
          end
       end
       
       function train(obj) %SHOULD THERE BE A TRAIN?
           obj.test(obj)
       end
       
       
      
   end
    
end