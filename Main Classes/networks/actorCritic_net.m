classdef actorCritic_net < tdbp_net
   properties
       actor;
   end
   methods
       function obj = actorCritic_net(varargin)
           obj = obj@tdbp_net(varargin{2:end});
           obj.actor = varargin{1}; %should be a BP or SRN network
          %WILL CAUSE ERROR WHEN GIVING THESE PROPS INTIALLY
          obj.train_options.beta = 0.01;%actor must learn slowwwwwly
       end
        function choose_next_action(obj)
           obj.actor.pools(2).activation = obj.pools(2).activation;
            obj.actor.compute_output;
           values = obj.actor.pools(end).activation;
           values = exp(values);
           if find(isinf(values))
               [~, choice] = max(obj.actor.pools(end).activation);
           elseif find(isinf(1./values))
               [~, choice] = max(obj.actor.pools(end).activation);
           else
               total = sum(values);
               values = values / total;
               choice = tdbp_net.linear_stochastic_choice(values);
           end
           obj.action = choice;
        end
        function updateActor(obj)
           obj.actor.pools(end).target =  obj.actor.pools(end).activation; %no error for unchosen actions
           obj.actor.pools(end).target(obj.action) = obj.actor.pools(end).target(obj.action) + obj.train_options.beta*obj.pools(end).error;%beta*critic TD error for selected action
           obj.actor.compute_error;
%            disp(obj.actor.pools(4).error);
           obj.actor.sumstats;
           obj.actor.compute_weds;
           obj.actor.change_weights;
        end
        function compute_error(obj)
            compute_error@tdbp_net(obj);
            obj.updateActor;
        end
   end
end