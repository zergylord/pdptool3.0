classdef rbp_net < bp_net & gui_usable_net
    properties
        ticks_per_interval = 4;
        numticks;
        numintervals;
        dt=1/4;
        tickno = 1;
        next_tickno = 1;
        next_seqno = 1;
        intervalno; %environment.sequences(seqno).intervals(#) only in test mode does it need to be displayed
        stateno = 0; %=tick but only needs display for tick grain in test mode
    end
        methods(Access = protected)
        
        function add_bias_projections(obj)
           for i=2:length(obj.pools)
               if strcmp(obj.pools(i).type,'input') || strcmp(obj.pools(i).type,'bias')
                   continue
               end
               obj.pools(i).connect(obj.pools(1),'scalar',-2);
           end
        end
    end
    methods
        function obj = rbp_net(varargin)
            obj = obj@bp_net(varargin{:});
            obj.type = 'rbp';
            if ~isfield(obj.train_options,'lgrainsize')
                obj.train_options.lgrainsize = 1;
            end
        end
        
        function set.ticks_per_interval(obj,val)
            obj.ticks_per_interval = val;
            obj.dt = 1/val;
        end
        function set_initial_state(obj)
            obj.cpname = obj.environment.sequences(obj.environment.sequence_index).name; %added
            obj.numticks = length(obj.environment.sequences(1).intervals)*obj.ticks_per_interval;
            obj.tickno = 1;
            obj.next_tickno = 2;
            obj.environment.interval_index = 1;
            obj.clamp_pools;
            obj.pss = 0;
            obj.pce = 0;
            for i=1:length(obj.pools)
                p = obj.pools(i);
                p.target(:) = nan;
                p = obj.pools(i);
                p.activation_history = zeros(p.unit_count,obj.numticks+1);
                p.error_history = zeros(p.unit_count,obj.numticks+1);
                p.has_target = zeros(1,obj.numticks+1);
                p.has_clamp = zeros(1,obj.numticks+1);
                p.target_states = zeros(p.unit_count,obj.numticks+1);
                p.clamp_states = zeros(p.unit_count,obj.numticks+1);
                if ~(p.clamped_activation == 2)
                    bias_proj = find([p.projections.from] == obj.pools(1), 1);
                    if ~isempty(bias_proj)
                        p.net_input = p.projections(bias_proj).using.weights'; %add net_input from bias weight, since activation =1
                    end
                    p.activation = rbp_net.logistic(p.net_input);
                else
                   p.has_clamp(1) = 1;
                   p.clamp_states(:,1) = p.activation;
                end
                p.activation_history(:,1) = p.activation;
            end
        end
        
        function reset_net(obj)
            obj.next_tickno = 1;
            for i = 2:length(obj.pools) %bias pool stays on
                obj.pools(i).target(:) = NaN; 
			end
			reset_net@bp_net(obj)
        end
        function clamp_targets(obj) %deals with only targets (call 1 tick delayed)
            pat = obj.environment.current_patterns;
            
            for i=2:length(obj.pools) %added
                obj.pools(i).target(:) = nan; %added
            end
            %[obj.pools.target] = deal(NaN);
            for i=1:length(pat)
                if isempty(pat(i).pool) %if no pool name given, assume default pools (i.e. in first, out last)
                    if strcmp(pat(i).type,'T')%if setting an output pool, pick the last pool
                        cur_pool = obj.pools(length(obj.pools));
                    else %if setting an input pool, pick the first non-bias pool
                        cur_pool = obj.pools(2);
                    end
                else
                    cur_pool = obj.pools_by_name(pat(i).pool);
                end
                switch pat(i).type
                    case 'T'
                        %PUT ERR MARGIN CODE HERE
                        %                           cur_pool.error = @(x)(pat(i).pattern - cur_pool.activation);
                        cur_pool.clamped_activation = 0; %added
                        cur_pool.target = pat(i).pattern;
                        cur_pool.clamped_error = true;
                    otherwise
                end
            end
        end
        function clamp_pools(obj) %doesn't clamp targets
            pat = obj.environment.current_patterns;
            
            for i=2:length(obj.pools) %added
%                 obj.pools(i).target(:) = nan; %added
                obj.pools(i).clamped_activation = 0;
            end
            %[obj.pools.target] = deal(NaN);
            for i=1:length(pat)
                if isempty(pat(i).pool) %if no pool name given, assume default pools (i.e. in first, out last)
                    if strcmp(pat(i).type,'T')%if setting an output pool, pick the last pool
                        cur_pool = obj.pools(length(obj.pools));
                    else %if setting an input pool, pick the first non-bias pool
                        cur_pool = obj.pools(2);
                    end
                else
                    cur_pool = obj.pools_by_name(pat(i).pool);
                end
%                 if obj.epochno ~= 0 %don't want to reset and clamp pools
                    switch pat(i).type
                        case 'H'
                            cur_pool.activation = pat(i).pattern;
                            act = cur_pool.activation;
                            act(act == 1) = 0.99999988;
                            act(act == 0) = 0.00000012;
                            cur_pool.net_input = log(act ./(1-act));
                            cur_pool.clamped_activation = 2;
                        case 'S'
                            cur_pool.net_input = pat(i).pattern;
                            cur_pool.clamped_activation = 1;
                            %just clamps netinput for some reason... cur_pool.activation = pat(i).pattern;
                        otherwise %ignore patterns with ill-formed types       
                    end
%                 else
%                     cur_pool.activation(:) = 0;
%                     cur_pool.target(:) = nan;
%                     cur_pool.clamped_activation = 0;
%                     
%                 end
                
                %added to display targets when targets are NaN
                %if isnan(cur_pool.target) 
                %    cur_pool.display_target = zeros(1,cur_pool.unit_count);
                %else
                %    cur_pool.display_target = cur_pool.target;
                %end
                
            end 
        end
        
        function compute_output(obj,tnum)

                for i=1:numel(obj.pools)
                    p = obj.pools(i);
                    prevnet = p.net_input;
                    if ~(p.clamped_activation == 2) %not hard clamp
                        if p.clamped_activation == 1
                            ninput = p.net_input;
                        else
                            ninput = zeros(1,p.unit_count);
                        end
                        for j=1:length(p.projections)
                            from = p.projections(j).from;
                            proj = p.projections(j).using;
                            z = ( proj.weights * from.activation_history(:,tnum-1) )';
                            ninput = ninput + z;
                        end
                        p.net_input = obj.dt*(ninput - prevnet) + prevnet;
                        p.activation = logistic(p.net_input);
                    else
                        p.has_clamp(tnum) = 1;
                        p.clamp_states(:,tnum) = p.activation;
                    end
                    p.activation_history(:,tnum) = p.activation;
                    if ~isnan(p.target)
                        p.has_target(tnum) = 1;
                        p.target_states(:,tnum) = p.target;
                        d = zeros(1,p.unit_count);
                        a = p.activation;
                        emg = obj.train_options.errmargin;
                        if emg > 0
                            ome = 1 - emg;
                            %when target is 1.0;
                            isone = (p.target == 1.0);
                            x = (a <= ome) & (isone);
                            d(x) = ome - a(x);
                            %when target is 0.0
                            iszero = (p.target == 0.0);
                            x = (a >= emg) & (iszero);
                            d((x)) = emg - a((x));
                            %when target is -1.0
                            isnegone = (p.target == -1.0);
                            x =  (a >= -ome) & (isnegone);
                            d((x)) = -ome - a((x));
                            %when target is none of the above
                            other = ~(isone | iszero | isnegone);
                            d(other) = p.target(other) - a(other);
                        else
                            d = p.target - a;
                        end
                        %SHOULD HANDLE ERR MARGINS HERE
                        switch obj.train_options.errmeas
                            case 'cee'
                                p.error = obj.dt .* d;
                            case 'sse'
                                p.error = obj.dt .* d .* ...
                                    p.activation .* (1-p.activation);
                        end
                        p.error_history(:,tnum) = p.error;
                    else
                        no_err = zeros(1,p.unit_count);
                        p.error = no_err;
                        p.error_history(:,tnum) = no_err;
                    end
                end
                if ~mod(tnum-1,obj.ticks_per_interval)&& tnum<=obj.numticks
                    obj.environment.interval_index = 1+(tnum-1)/obj.ticks_per_interval;
                    obj.clamp_targets;
                end
        end        
        function compute_error(obj) %for 1 sequence; must call ntick times for full backward pass
            for tnum=obj.numticks:-1:2
                for i=1:length(obj.pools)
                    p = obj.pools(i);
                    dEdA = zeros(1,p.unit_count);
                    if ~p.has_clamp(tnum) %not hard clamp
                        for j=1:length(p.outgoing_projections)
                            to_pool = p.outgoing_projections(j).to;
                            if ~to_pool.has_clamp(tnum+1)
                                dEdA = dEdA + to_pool.error_history(:,tnum+1)' * ...
                                    p.outgoing_projections(j).using.weights;
                            end
                            
                        end
                        
                        activ = p.activation_history(:,tnum)';
                        p.error = p.error_history(:,tnum)' + ...
                            (obj.dt * activ .* (1-activ) .* dEdA) + ...
                            (1-obj.dt) .* p.error_history(:,tnum+1)';
                    else
                        p.error(:) = 0;
                    end
                    p.error_history(:,tnum) = p.error;
                end
            end
            %restores activations to correct values
%             for i=1:length(obj.pools)
%                p = obj.pools(i);
%                p.activation = p.activation_history(:,end);
%                p.error = p.error_history(:,end);
%             end
        end
        function compute_weds(obj)
            for i=1:length(obj.pools)
                p = obj.pools(i);
                for j=1:length(p.projections)
                    from = p.projections(j).from;
                    proj = p.projections(j).using;
                    proj.weds = proj.weds + ...
                        p.error_history(:,2:end) * ...
                        from.activation_history(:,1:end-1)';
                end
            end
        end
        function sumstats(obj)
            obj.pss = 0.0;
            obj.pce = 0.0;
            if obj.seqno == 1 %added because tss was increasing too much
                obj.tss = 0.0;
                obj.tce = 0.0;
            end
            for i=1:numel(obj.pools)
                p = obj.pools(i);
                if strcmpi(p.type,'hidden')
                    continue
                end
                t = find(p.has_target);
                ext = p.target_states(:,t);
                tacthist = p.activation_history(:,t);
                obj.pss = obj.pss + obj.dt*sum(sum((ext - tacthist) .^ 2));
                ton = (ext==1);
                toff = (ext==0);
                %to prevent ce from blowing up, cut off act hist at .999 and .001:
                tacthist(tacthist < .001) = .001;
                tacthist(tacthist > .999) = .999;
                ce = obj.dt*(sum(log(tacthist(ton))) + sum(log(1-tacthist(toff))));
                obj.pce = obj.pce - ce; % to make pce positive value;
            end
            if obj.alltest 
                obj.tss = obj.tss + obj.pss;
                obj.tce = obj.tce + obj.pce;
            else
                obj.tss = NaN;
                obj.tce = NaN;
            end
        end
        function train(obj)
            epoch_limit = (floor(obj.epochno/obj.train_options.nepochs)+1)*obj.train_options.nepochs;
            while obj.next_epochno <= epoch_limit
                obj.epochno = obj.next_epochno;
                while obj.next_seqno <= length(obj.environment.sequences)
                    obj.seqno = obj.next_seqno;
                    obj.environment.sequence_index = obj.seqno;
                    if obj.next_tickno == 1
                        obj.set_initial_state;
                        interval_change = true;
                        if obj.done_updating_tickno
                            return
                        end
                    else
                        interval_change = false;
                    end
                    while obj.next_tickno <= obj.numticks+1
                        obj.tickno = obj.next_tickno;
                        if ~mod(obj.tickno-1,obj.ticks_per_interval) && obj.tickno<=obj.numticks
                            obj.environment.interval_index = 1+(obj.tickno-1)/obj.ticks_per_interval;
                            obj.clamp_pools;
                            interval_change = true;
                        end
                        obj.compute_output(obj.tickno);
                        obj.next_tickno = obj.next_tickno + 1;
                        if obj.done_updating_tickno
                            return
                        end
                        if interval_change
                            interval_change = false;
                            if obj.done_updating_patno
                                return
                            end
                        end
                    end
                    obj.next_tickno = 1;
                    obj.compute_error;
                    obj.compute_weds;                   
                    obj.sumstats;
                    if strcmpi(obj.train_options.lgrain,'sequence') && ~mod(obj.seqno,obj.train_options.lgrainsize)
                        obj.change_weights;
                    end
%                      save(['newnet' num2str(obj.seqno)],'obj');
                    obj.next_seqno = obj.seqno + 1;
                    if obj.done_updating_seqno
                        return
                    end
                end
                if strcmpi(obj.train_options.lgrain,'epoch') && ~mod(obj.epochno,obj.train_options.lgrainsize)
                    obj.change_weights;
                end
                obj.next_epochno = obj.epochno + 1;
                obj.next_seqno = 1;
                if obj.done_updating_epochno
                    return
                end
            end
        end
        function test(obj)
            epoch_limit = obj.next_epochno;
            while obj.next_epochno <= epoch_limit
                obj.epochno = obj.next_epochno;
                while obj.next_seqno <= length(obj.environment.sequences)
                    obj.seqno = obj.next_seqno;
                    obj.environment.sequence_index = obj.seqno;
                    if obj.next_tickno == 1
                        obj.set_initial_state;
                        interval_change = true;
                        if obj.done_updating_tickno
                            return
                        end
                    else
                        interval_change = false;
                    end
                    obj.cpname = obj.environment.sequences(obj.environment.sequence_index).name; %added
                    while obj.next_tickno <= obj.numticks+1
                        obj.tickno = obj.next_tickno;
                        if ~mod(obj.tickno-1,obj.ticks_per_interval) && obj.tickno<=obj.numticks
                            obj.environment.interval_index = 1+(obj.tickno-1)/obj.ticks_per_interval;
                            obj.clamp_pools;
                            interval_change = true;
                        end
                        obj.compute_output(obj.tickno);
                        obj.next_tickno = obj.next_tickno + 1;
                        if obj.done_updating_tickno
                            return
                        end
                        if interval_change
                            interval_change = false;
                            if obj.done_updating_patno
                                return
                            end
                        end
                    end
                    obj.next_tickno = 1;              
                    obj.sumstats;
%                     save(['newnet' num2str(obj.seqno)],'obj');
                    if obj.alltest
                        obj.next_seqno = obj.seqno + 1;
                    end
                    if obj.done_updating_seqno || ~obj.alltest
                        return
                    end
                end
                obj.next_epochno = obj.epochno + 1;
                obj.next_seqno = 1;
                if obj.done_updating_epochno
                    return
                end
            end
        end
    end
    

    
    
    
    
end