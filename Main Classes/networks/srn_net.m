classdef srn_net < bp_net
	%UNTITLED4 Summary of this class goes here
	%   Detailed explanation goes here
	
	properties
		first_pattern = true;
		next_seqno = 1;
		sss; %added
	end
	
	methods
		function obj = srn_net(varargin)
			obj = obj@bp_net(varargin{:});
			obj.type = 'srn';
		end
		
		function reset_net(obj)
			obj.seqno = 0;
			obj.next_seqno = 1;
			obj.sss = 0.0;
			reset_net@bp_net(obj)
		end
		
		%CHANGE TO A MODDED SETUP_POOLS
		function compute_output(obj)
			copy = obj.pools([obj.pools.copyback]);
			if ~isempty(copy)
				for i=1:length(copy)
					send = copy(i).copy_from;
					copy(i).activation = copy(i).activation * obj.train_options.mu + send.activation;
				end
			end
			compute_output@bp_net(obj);
		end
		
		function sumstats(obj)
			obj.pss = 0.0;
			obj.pce = 0.0;
			if obj.patno == 1
				obj.sss = 0.0;
			end
			if obj.seqno == 1
				obj.tss = 0.0;
			end
			outpools = find(strcmpi({obj.pools.type},'output'));
			for i=1:numel(outpools)
				ind =outpools(i);
				t = obj.pools(ind).error(obj.pools(ind).target>=0);
				obj.pss = obj.pss + sum(t .* t);
				% for computing cross entropy error,use local variable 'act' to limit
				% values between 0.001 and 0.999, then logically index into 'act' for
				% getting logs of activation values for units that have target 1 or
				% 0.Sum of the logs evaluate to cross entropy error
				act = obj.pools(ind).activation;
				act(act < 0.001) = 0.001;
				act(act > 0.999) = 0.999;
				ce = sum(log(act(obj.pools(ind).target == 1))) + ...
					sum(log(1 - act(obj.pools(ind).target == 0)));
				obj.pce = obj.pce - ce; % to make pce positive value;
			end
			obj.sss = obj.sss + obj.pss;
			obj.tss = obj.tss + obj.sss;
			obj.tce = obj.tce + obj.pce;
		end
		
		function train(obj,varargin)
			epoch_limit = (floor(obj.epochno/obj.train_options.nepochs)+1)*obj.train_options.nepochs;
			while obj.next_epochno <= epoch_limit
				obj.epochno = obj.next_epochno;
				while obj.next_seqno <= length(obj.environment.sequences)
					obj.seqno = obj.next_seqno;
					obj.environment.sequence_index = obj.seqno;
					npats = length(obj.environment.sequences(obj.seqno).intervals);
					while obj.next_patno <= npats
						obj.patno = obj.next_patno;
						obj.environment.interval_index = obj.patno;
						obj.clamp_pools;
						%                         disp(obj.pools(2).activation);
						obj.compute_output;
						obj.compute_error;
						obj.compute_weds;
						obj.change_weights;
						obj.sumstats;
						obj.next_patno = obj.patno + 1;
						if obj.done_updating_patno
							return
						end
					end
					obj.next_seqno = obj.seqno + 1;
					obj.next_patno = 1;
					if obj.done_updating_seqno
						return
					end
				end
				obj.next_epochno = obj.epochno +1;
				obj.next_seqno = 1;
				if obj.done_updating_epochno
					return
				end
			end
		end
		%                         function train(obj,varargin)
		%             while obj.next_epochno <= obj.train_options.nepochs
		%                 obj.epochno = obj.next_epochno;
		%                 while obj.next_patno <= length(obj.environment.sequences)
		%                     obj.patno = obj.next_patno;
		%                     obj.environment.sequence_index = obj.patno;
		%                     obj.clamp_pools;
		%                     obj.compute_output;
		%                     obj.compute_error;
		%                     obj.sumstats;
		%                     obj.compute_weds;
		%                     if strcmpi(obj.train_options.lgrain,'pattern')
		%                         obj.change_weights;
		%                     end
		%                     obj.next_patno = obj.patno + 1;
		%                     if obj.done_updating_patno
		%                         return
		%                     end
		%                 end
		%                 if strcmpi(obj.train_options.lgrain,'epoch')
		%                     obj.change_weights;
		%                 end
		%                 obj.next_patno = 1;
		%                 obj.next_epochno = obj.epochno + 1;
		%                 if obj.done_updating_epochno
		%                     return
		%                 end
		%             end
		%         end
		function test(obj,varargin)
			%while obj.next_epochno <= obj.train_options.nepochs
			%   obj.epochno = obj.next_epochno;
			while obj.next_seqno <= length(obj.environment.sequences)
				obj.seqno = obj.next_seqno;
				obj.environment.sequence_index = obj.seqno;
				npats = length(obj.environment.sequences(obj.seqno).intervals);
				while obj.next_patno <= npats
					obj.patno = obj.next_patno;
					obj.environment.interval_index = obj.patno;
					obj.clamp_pools;
					%                         disp(obj.pools(2).activation);
					obj.compute_output;
					obj.compute_error;
					obj.sumstats;
					obj.next_patno = obj.patno + 1;
					if obj.done_updating_patno
						return
					end
				end
				obj.next_seqno = obj.seqno + 1;
				obj.next_patno = 1;
				if obj.done_updating_seqno
					return
				end
			end
			obj.next_seqno = 1;
			%  obj.next_epochno = obj.epochno +1;
			%  obj.next_seqno = 1;
			%  if obj.done_updating_epochno
			%      return
			%  end
			%end
		end
		
		%         function test(obj)
		%             while obj.next_epochno <= obj.test_options.nepochs
		%                 obj.epochno = obj.next_epochno;
		%                 while obj.next_patno <= length(obj.environment.sequences)
		%                     obj.patno = obj.next_patno;
		%                     obj.environment.sequence_index = obj.patno;
		%                     obj.clamp_pools;
		%                     obj.compute_output;
		%                     obj.compute_error;
		%                     obj.sumstats;
		%                     obj.next_patno = obj.patno + 1;
		%                     if obj.done_updating_patno
		%                         return
		%                     end
		%                 end
		%                 obj.next_patno = 1;
		%                 obj.next_epochno = obj.epochno + 1;
		%                 if obj.done_updating_epochno
		%                     return
		%                 end
		%             end
		%         end
		
	end
	
end

