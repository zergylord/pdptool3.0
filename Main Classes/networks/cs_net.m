classdef cs_net < bp_net & annealing_process 
    properties
        goodness = 0;
        unitno = 1;
        cuname; %added
        next_cycleno = 1;
        next_updateno = 1;
        total_unit_count;
    end
    methods
        function obj = cs_net(pools,seed,varargin)
            obj = obj@bp_net(pools,seed,varargin{:},'manual_bias',false);
            obj = obj@annealing_process();
            obj.test_options = struct('nupdates',100,'ncycles',10,'actfunction','schema','istr',1,...
                'estr',1,'clamp',0,'harmony',0,'kappa',0,'temp',0,...
                'annealsched',[],'testset','none');
            obj.total_unit_count = sum([obj.pools.unit_count]);
            obj.set_test_params(varargin{:});
            rand('seed',seed)%is this needed?
            obj.type = 'cs';
            obj.set_temp(0);
        end
        
        
        function reset_net(obj)
            obj.cycleno = 0; 
            obj.goodness = 0; %added
            %obj.updateno = 0;
            obj.next_updateno = 1;
            obj.next_cycleno = 1;
            obj.set_temp(0); %added
            obj.cuname = ''; %added
            for i = 2:length(obj.pools) %changed start from 1 to 2; bias pool should stay on
                obj.pools(i).activation = obj.pools(i).activation_reset_value;
            end
%             if ~isempty(obj.environment) %added
%                 obj.clamp_pools;
%             end
            resetlog;
            if obj.gui
                update_display(0);
            end
             if exist('RandStream','file') == 2 % for version 7.8 and above    
               stream = RandStream.getGlobalStream; % add semicolon once finished debugging
               reset(stream);
            else
               rand('seed',net.seed)
               randn('seed',net.seed)
            end
        end
        
        
        function clamp_pools(obj)
            pat = obj.environment.current_patterns;
            for i=1:length(pat)
                if isempty(pat(i).pool) %if no pool name given, assume default pools (i.e. in order)
                    cur_pool = obj.pools(i+1);%count from 2 due to bias
                else
                    cur_pool = obj.pools_by_name(pat(i).pool);
                end
                cur_pool.extern_input = pat(i).pattern;
                cur_pool.clamped_activation = 1;
            end
        end
        
        function early_term = cycle(obj)
                if strcmpi(obj.test_options.actfunction,'boltzmann') || obj.test_options.harmony
                    obj.annealing(obj.cycleno);
                end
                early_term = obj.rupdate();
                obj.calc_goodness();         
        end
        
        function early_term = rupdate(obj)
			early_term = false;
            %obj.updateno = 1;
            while obj.next_updateno <= obj.test_options.nupdates 
                obj.updateno = obj.next_updateno;
                abs_index = ceil((obj.total_unit_count-1) .* rand(1,1));
                [pool uindex] = get_unit_from_pools(obj.pools(2:end),abs_index);
                %disp(uindex);
                obj.cuname= pool.unames{uindex}; %changed
                if obj.test_options.harmony
                    pool.net_input(uindex) = 0.0;
                    if pool.extern_input(uindex) == 0
						if obj.temp <= 0
							activation = pool.net_input(uindex) > 0;
						else
							activation = logistic(pool.net_input(uindex)/obj.temp);
						end
                        if rand() < activation
                            pool.activation(uindex) = 1;
                        else
                            pool.activation(uindex) = -1;
                        end
                    else
                        if pool.extern_input(uindex) < 0.0
                            pool.activation(uindex) = 1;
                        end
                        if pool.extern_input(uindex) > 0.0
                            pool.activation(uindex) = -1;
                        end
                    end
                else
                    if obj.test_options.clamp
                        if pool.extern_input(uindex) > 0
                            pool.activation(uindex) = 1;
                            continue;
                        end
                        if pool.extern_input(uindex) < 0
                            pool.activation(uindex) = 0;
                            continue;
                        end
                    end
                    
                    inti = 0;
                    for i=1:length(pool.projections)
                        wt = pool.projections(i).using.weights(uindex,:);
                        from_act = pool.projections(i).from.activation;
                        inti = inti + sum(from_act .* wt);
                    end
                    if ~obj.test_options.clamp
                        pool.net_input(uindex) = (obj.test_options.istr * inti) + (obj.test_options.estr * pool.extern_input(uindex));
                    else
                        pool.net_input(uindex) = (obj.test_options.istr * inti);
                    end
                    if strcmpi(obj.test_options.actfunction,'boltzmann')
						if obj.temp <= 0
							activation = pool.net_input(uindex) > 0;
						else
							activation = logistic(pool.net_input(uindex)/obj.temp);
						end
                        if rand() < activation
                            pool.activation(uindex) = 1.0;
                        else
                            pool.activation(uindex) = 0.0;
                        end
                    else
                        if pool.net_input(uindex) > 0
                            if pool.activation(uindex) < 1
                                dt = pool.activation(uindex) + ( pool.net_input(uindex) * (1 - pool.activation(uindex)) );
                                if dt > 1
                                    pool.activation(uindex) = 1;
                                else
                                    pool.activation(uindex) = dt;
                                end
                            end
                        else
                            if pool.activation(uindex) > 0.0
                                dt = pool.activation(uindex) + ( pool.net_input(uindex) * pool.activation(uindex) );
                                if dt < 0
                                    pool.activation(uindex) = 0;
                                else
                                    pool.activation(uindex) = dt;
                                end
                            end
                        end
                    end
				end
				obj.next_updateno = obj.updateno + 1;
				if obj.done_updating_updateno
					early_term = true;
					return
				end
			end
            obj.next_updateno = 1;
            %obj.updateno = obj.updateno - 1; %for correct updateno display
        end
        
        function calc_goodness(obj)
            dg = 0.0;
            if (obj.test_options.harmony ==1 || numel(obj.pools) <= 1 )
                obj.goodness = 0.0;
                return;
            end
            for i=2:length(obj.pools)
                p = obj.pools(i);
                for j=1:length(p.projections)
                    from = p.projections(j).from;
                    if strcmpi(from.type,'bias')  %bias connection
                        dg = dg + sum(p.projections(j).using.weights .* p.activation');
                        continue;
                    end
                    wt = p.projections(j).using.weights;
                    for k = 1:size(wt,1)
                        if strcmpi(from.name,p.name) %self block
                            iter_start = k+1;
                        else
                            iter_start = 1;
                        end
                        for l=iter_start:size(wt,2)
                            dg = dg + (wt(k,l) * p.activation(k) * from.activation(l));
                        end
                    end
                end
                a(i-1).activation = p.activation;
                a(i-1).extern_input = p.extern_input;
            end
            if obj.test_options.clamp == 0
                dg = dg * obj.test_options.istr;
                for i=1:size(a,1)
                    dg =dg + sum(a(i).activation .* (a(i).extern_input * obj.test_options.estr));
                end
                obj.goodness = dg;
            end   
        end
        
        function test(obj)
            %obj.cycleno = 1;
            cycle_limit = (floor(obj.cycleno/obj.test_options.ncycles)+1)*obj.test_options.ncycles;
            while obj.next_cycleno <= cycle_limit
                obj.cycleno = obj.next_cycleno;
                if ~isempty(obj.environment) %added
                    obj.clamp_pools;
				end
				if obj.cycle() %if termination occurs at the granularity of updates, then return
					return
				end
                obj.next_cycleno = obj.cycleno + 1;
                if obj.done_updating_cycleno
                  return
                end  
            end
            %update_display(0);
            %obj.next_cycleno = 1; 
        end
        
    end
end