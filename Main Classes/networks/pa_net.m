classdef pa_net < bp_net & annealing_process
   properties
       ndp = 0.0; %added
       nvl= 0.0; %added
       vcor = 0.0; %added
       
       
   end    
    methods
       function obj = pa_net(pools,seed,varargin)
          obj@bp_net(pools,seed,varargin{:});
          obj.train_options.lrule = 'delta';
          obj.type = 'pa';
       end
       
       function clamp_pools(obj)
            
            pat = obj.environment.current_patterns;
            [obj.pools.target] = deal(NaN);
            for i=1:length(pat)
                if isempty(pat(i).pool) %if no pool name given, assume default pools (i.e. in first, out last)
                    if strcmp(pat(i).type,'T')%if setting an output pool, pick the last pool
                        cur_pool = obj.pools(length(obj.pools));
                    else %if setting an input pool, pick the first non-bias pool
                        cur_pool = obj.pools(2);
                    end
                else
                    cur_pool = obj.pools_by_name(pat(i).pool);
                end
                switch pat(i).type
                    case 'H'
                        cur_pool.activation = pat(i).pattern + ((1 -2*rand(1,cur_pool.unit_count)) * obj.train_options.noise);
                        act = cur_pool.activation;
                        act(act == 1) = 0.99999988;
                        act(act == 0) = 0.00000012;
                        cur_pool.net_input = log(act ./(1-act));
                        cur_pool.clamped_activation = 2;
                    case 'S'
                        cur_pool.net_input = pat(i).pattern;
                        cur_pool.clamped_activation = 1;
                        %just clamps netinput for some reason... cur_pool.activation = pat(i).pattern;
                    case 'T'
                        %PUT ERR MARGIN CODE HERE
                        %                           cur_pool.error = @(x)(pat(i).pattern - cur_pool.activation);
                        cur_pool.target = pat(i).pattern + ((1 -2*rand(1,cur_pool.unit_count)) * obj.train_options.noise);
                        cur_pool.clamped_error = true;
                    otherwise %ignore patterns with ill-formed types
                        
                end
            end
        end
       
       function compute_output(obj)
            [obj.pools([obj.pools.clamped_activation] ~= 2).activation] = obj.pools([obj.pools.clamped_activation] ~= 2).activation_reset_value;
            [obj.pools.net_input] = obj.pools.net_input_reset_value;
            
            for i=1:numel(obj.pools)
                if obj.pools(i).clamped_activation == 2
                    continue
                end
                
                
                for j=1:length(obj.pools(i).projections)
                    from = obj.pools(i).projections(j).from;
                    obj.pools(i).net_input = obj.pools(i).net_input + (from.activation * obj.pools(i).projections(j).using.weights');
                end
                switch obj.pools(i).activation_function
                    case 'logistic'
                        obj.pools(i).activation = obj.pools(i).activation + logistic(obj.pools(i).net_input);
                    case 'linear'
                        obj.pools(i).activation = obj.pools(i).net_input; %linear activation: activation= net_input
                    case 'stochastic'
                        logout = logistic(obj.pools(i).net_input ./ obj.temp);
                        r = rand(1,numel(logout));
                        obj.pools(i).activation(r < logout) = 1.0;
                        obj.pools(i).activation(r >= logout) = 0.0;
                    case 'threshold'
                        obj.pools(i).activation(obj.pools(i).net_input > 0) = 1;
                        obj.pools(i).activation(obj.pools(i).net_input <= 0) = 0;
                    otherwise
                        error('Invalid activation function - %s',obj.pools(i).activation_function); %changed
                end
            end 
       end
       
       function compute_error(obj)
           for i=1:length(obj.pools)
                if ~isnan(obj.pools(i).target)
                    obj.pools(i).error = obj.pools(i).target - obj.pools(i).activation;
                end
           end
       end
        function compute_weds(obj)
            %weight error derivatives simply the errors here
        end
        
       function change_weights(obj)
           
           
           for i=1:length(obj.pools)
                if ~isnan(obj.pools(i).target)
                    if strcmpi(obj.train_options.lrule,'delta')
                        scalewith = obj.pools(i).error;
                    else
                        obj.pools(i).activation = obj.pools(i).target;
                        scalewith = obj.pools(i).activation;
                    end
                    for j=1:length(obj.pools(i).projections)
                        if isempty(obj.pools(i).projections(j).using.lr)
                            lr = obj.train_options.lrate;
                        else
                            lr = obj.pools(i).projections(j).using.lr;
                        end
                        obj.pools(i).projections(j).using.weights = obj.pools(i).projections(j).using.weights + ...
                            (scalewith' * obj.pools(i).projections(j).from.activation * lr);
                    end
                end
           end
       end
       
       function sumstats(obj)
            obj.pss = 0.0;
            obj.ndp = 0.0;
            obj.vcor = 0.0;
            obj.nvl = 0.0;
            if obj.patno == 1 %added because tss was increasing too much
                obj.tss = 0.0;
            end
            outpools = find(strcmpi({obj.pools.type},'output'));
            noutputs = 0;
            px = 0;
            py = 0;
            pz = 0;
            for i=1:numel(outpools)
                ind = outpools(i);
                p = obj.pools(ind);
                noutputs = noutputs + p.unit_count;
                obj.pss = obj.pss + sum(p.error.^2); %changed
                obj.ndp = obj.ndp + dot(p.target,p.activation);
                px = px + sum(p.target .^2);
                py = py + sum(p.activation .^ 2);
                pz = pz + sum(p.target .* p.activation);
            end
            obj.ndp = obj.ndp/noutputs;
            obj.tss = obj.tss + obj.pss;
            obj.nvl = sqrt(py/noutputs);
            if (px==0) || (py== 0)
                obj.vcor = 0;
            else
                obj.vcor = pz/sqrt(px*py);
            end
       end
        
       function reset_net(obj)
            obj.ndp = 0.0;
            obj.vcor = 0.0;
            obj.nvl =0.0;
		   reset_net@bp_net(obj);
        end
   end
    
end