classdef bp_net < net & gui_usable_net
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %         gui = false;
        name; %added
        cpname=''; %added
        gcor = 0.0; %added
        next_patno=1;
        next_epochno=1;
        pools;
        pools_by_name;
        %projections;
        environment;
        train_options = struct('nepochs',100,'ncycles',50,'lflag',1,...
            'lrate',0.5,'follow',0,'cascade',0,'ecrit',0.00,...
            'crate',0.05,'wrange',1,'lgrain','pattern','errmeas','sse',...
            'wdecay',0.0,'lgrainsize',1,'momentum',0.9,'mu',0.5,'tmax',1,...
            'clearval',0.5,'fastrun',0,'trainset','none','annealsched',[],'temp',1,'epochno',0,'noise',0); %added noise for pa and iac; changed epochno to 0
        test_options = struct('nepochs',100,'ncycles',50,'lflag',0,...
            'cascade',0,'ecrit',0.00,'crate',0.05,'mu',0.5,'tmax',1,...
            'errmeas','sse','wdecay',0.0,'clearval',0.5,'lrate',0.5,...
            'lgrain','pattern','lgrainsize',1,... %no follow for test
            'momentum',0.9,'fastrun',0,'testset','none','noise',0); %added noise for pa and iac
        app_data = struct('history',[],...
            'lognetwork',1,...
            'logfilename',[],...
            'dispobjects',[],...
            'annoteobjects',[],...
            'templatesize',[],...
            'cellsize',[]);
        seed = NaN;
        manual_bias = false;
        pss = 0.0;
        pce = 0.0;
        tss = 0.0;
        tce = 0.0;
        css = 0.0; %added
        nancolor = [.97 .97 .97]; %added for display of initial NaN targets
    end
    properties (Access = private)
        
    end
    
    %JSUT USE THE UTILITIE LOGISTIC FUNCZTION
    methods(Static)
        function retval = logistic(val)
            retval = val;
            retval(retval > 15.9357739741644) = 15.9357739741644; % returns 0.99999988;
            retval(retval < -15.9357739741644) = -15.9357739741644; % returns 0.00000012
            retval = 1.0 ./(1.0 + exp(-1 * retval));
        end
        
        
        
    end
    methods(Access = protected)
        
        function add_bias_projections(obj)
            for i=2:length(obj.pools)
                if strcmp(obj.pools(i).type,'input') || strcmp(obj.pools(i).type,'bias')
                    continue
                end
                obj.pools(i).connect(obj.pools(1));
            end
        end
        
        
    end
    methods       
        function set_train_params(obj,varargin)
            for i=1:2:length(varargin)
                if isfield(obj.train_options,varargin{i})
                    obj.train_options.(varargin{i}) = varargin{i+1};
                else
                    if isprop(obj,varargin{i})
                        obj.(varargin{i}) = varargin{i+1};
                    else
                        obj.train_options.(varargin{i}) = varargin{i+1}; %add as training option
                    end
                end
            end
        end
        
        function set_test_params(obj,varargin)
            for i=1:2:length(varargin)
                if isfield(obj.test_options,varargin{i})
                    obj.test_options.(varargin{i}) = varargin{i+1};
                else
                    if isprop(obj,varargin{i})
                        obj.(varargin{i}) = varargin{i+1};
                    else
                        obj.test_options.(varargin{i}) = varargin{i+1}; %add as training option
                    end
                end
            end
        end
        
        function obj = bp_net(pools, seed, varargin)
            obj.type = 'bp';
            if nargin > 1
                obj.seed = seed;
                if nargin > 2
                    obj.set_train_params(varargin{:});
                end
            end
            if ~isnan(obj.seed)
                RandStream.setDefaultStream(RandStream('mt19937ar','Seed',obj.seed));
            end
            obj.pools = [pool('bias',1,'bias') pools];
            if ~obj.manual_bias
                obj.add_bias_projections;
            end
            for i=1:length(obj.pools)
                obj.pools(i).index = i;
            end
            obj.pools_by_name = containers.Map({obj.pools.name},num2cell(obj.pools));
            for i=1:length(obj.pools)
                for j=1:length(obj.pools(i).projections)
                    switch obj.pools(i).projections(j).using.constraint_type
                        case 'random'
                            obj.pools(i).projections(j).using.weights = (rand(size(obj.pools(i).projections(j).using.weights))-.5) * ...
                                obj.train_options.wrange;
                        case 'scalar'
                            obj.pools(i).projections(j).using.weights = ones(size(obj.pools(i).projections(j).using.weights)) .* ...
                                obj.pools(i).projections(j).using.constraint;
                    end
                end
            end
        end
        
        function reset_weights(obj)
            for i=1:length(obj.pools)
                for j=1:length(obj.pools(i).projections)
                    switch obj.pools(i).projections(j).using.constraint_type
                        case 'random'
                            obj.pools(i).projections(j).using.weights = (rand(size(obj.pools(i).projections(j).using.weights))-.5) * ...
                                obj.train_options.wrange;
                        case 'scalar'
                            obj.pools(i).projections(j).using.weights = ones(size(obj.pools(i).projections(j).using.weights)) .* ...
                                obj.pools(i).projections(j).using.constraint;
                    end
                    %                      disp(obj.pools(i).projections(j).using.weights);
                end
            end
        end
        
        
        %resets net_input in all pools to 0
        function reset_net_input(obj)
            for i=1:length(obj.pools)
                current_pool = obj.pools(i);
                current_pool.net_input = zeros(1,length(current_pool.net_input));
            end
            
        end
        
        function reset_net(obj)
            RandStream.setDefaultStream(RandStream('mt19937ar','Seed',obj.seed));
            obj.epochno = 0; %changed
            obj.patno = 0;
            obj.next_patno = 1;
            obj.next_epochno = 1;
            obj.tss = 0.0;
            obj.pss = 0.0;
            obj.gcor = 0.0;
            obj.css = 0.0;
            obj.cpname = '';
            for i = 2:length(obj.pools) %bias pool stays on
                obj.pools(i).activation = obj.pools(i).activation_reset_value;
            end
            obj.reset_net_input(); %added for bp display
            obj.environment.sequence_index = obj.next_patno;
            obj.clamp_pools;
            if obj.gui
                update_display(1);
            end
        end
        
        
        function clamp_pools(obj)
            
            pat = obj.environment.current_patterns;
            [obj.pools.target] = deal(NaN);
            for i=1:length(pat)
                if isempty(pat(i).pool) %if no pool name given, assume default pools (i.e. in first, out last)
                    if strcmp(pat(i).type,'T')%if setting an output pool, pick the last pool
                        cur_pool = obj.pools(length(obj.pools));
                    else %if setting an input pool, pick the first non-bias pool
                        cur_pool = obj.pools(2);
                    end
                else
                    cur_pool = obj.pools_by_name(pat(i).pool);
                end
                switch pat(i).type
                    case 'H'
                        cur_pool.activation = pat(i).pattern;
                        act = cur_pool.activation;
                        act(act == 1) = 0.99999988;
                        act(act == 0) = 0.00000012;
                        cur_pool.net_input = log(act ./(1-act));
                        cur_pool.clamped_activation = 2;
                    case 'S'
                        cur_pool.net_input = pat(i).pattern;
                        cur_pool.clamped_activation = 1;
                        %just clamps netinput for some reason... cur_pool.activation = pat(i).pattern;
                    case 'T'
                        %PUT ERR MARGIN CODE HERE
                        %                           cur_pool.error = @(x)(pat(i).pattern - cur_pool.activation);
                        cur_pool.target = pat(i).pattern;
                        cur_pool.clamped_error = true;
                    otherwise %ignore patterns with ill-formed types
                        
                end
            end
        end
        
        function compute_output(obj)
            
            [obj.pools([obj.pools.clamped_activation] ~= 2).activation] = obj.pools([obj.pools.clamped_activation] ~= 2).activation_reset_value;
            [obj.pools.net_input] = obj.pools.net_input_reset_value;
            for i=1:numel(obj.pools)
                if obj.pools(i).clamped_activation == 2
                    continue
                end
                
                
                for j=1:length(obj.pools(i).projections)
                    from = obj.pools(i).projections(j).from;
                    obj.pools(i).net_input = obj.pools(i).net_input + (from.activation * obj.pools(i).projections(j).using.weights');%'
                end
                switch obj.pools(i).activation_function
                    case 'logistic'
                        obj.pools(i).activation = obj.pools(i).activation + bp_net.logistic(obj.pools(i).net_input);
                    case 'linear'
                        obj.pools(i).activation = obj.pools(i).activation + (obj.pools(i).net_input);
                    otherwise
                end
            end
        end
        
        function compute_error(obj)
            [obj.pools.delta] = obj.pools.delta_reset_value;
            [obj.pools.error] = obj.pools.error_reset_value;
            for i=length(obj.pools):-1:1
                if ~isnan(obj.pools(i).target)
                    obj.pools(i).error =   obj.pools(i).error + obj.pools(i).target - obj.pools(i).activation;
                end
                switch obj.train_options.errmeas
                    case 'cee'
                        obj.pools(i).delta = obj.pools(i).error;
                    case 'sse'
                        obj.pools(i).delta = obj.pools(i).error .* ...
                            obj.pools(i).activation .*...
                            (1 - obj.pools(i).activation);
                end
                
                for j=1:length(obj.pools(i).projections)
                    from = obj.pools(i).projections(j).from;
                    from.error = from.error + (obj.pools(i).delta * ...
                        obj.pools(i).projections(j).using.weights);
                end
                if strcmpi(obj.pools(i).type,'connection')
                    obj.pools(i).error_reset_value = obj.pools(i).error_reset_value +obj.pools(i).error;
                end
            end
        end
        
        function compute_weds(obj)
            for i=1:length(obj.pools)
                for j=1:length(obj.pools(i).projections)
                    proj = obj.pools(i).projections(j).using;
                    proj.weds = proj.weds + (obj.pools(i).delta' * ...%'
                        obj.pools(i).projections(j).from.activation);
                    %                                         disp(proj.weds);
                end
            end
        end
        
        function change_weights(obj)
            
            if obj.train_options.follow %added
                p_css = obj.css;
                obj.css = 0.0;
                dp =0;
            end
            
            mo = obj.train_options.momentum;
            decay = obj.train_options.wdecay;
            for i=1:length(obj.pools)
                for j=1:length(obj.pools(i).projections)
                    proj = obj.pools(i).projections(j).using;
                    if isempty(proj.lr)
                        lr = obj.train_options.lrate;
                    else
                        lr = proj.lr;
                    end
                    proj.weight_deltas = lr*proj.weds - decay*proj.weights + ...
                        mo*proj.weight_deltas;
                    
                    if obj.train_options.follow %added
                        sqwed = proj.weds .^ 2;
                        obj.css = obj.css + sum(sqwed(1:end));
                        dpprod = proj.weds .*  proj.prev_weds;
                        dp = dp + sum(dpprod(1:end));
                    end
                    proj.prev_weds = proj.weds; %for follow option
                    
                    proj.weights = proj.weights + proj.weight_deltas;
                    proj.weds(:) = 0;
                end
            end
            
            if obj.train_options.follow %added
                den = p_css * obj.css;
                if den > 0.0
                    obj.gcor = dp/(sqrt(den));
                else
                    obj.gcor =0.0;
                end
            end
            
        end
        
        function sumstats(obj)
            obj.pss = 0.0;
            obj.pce = 0.0;
            if obj.patno == 1 %added because tss was increasing too much
                obj.tss = 0.0;
            end
            outpools = find(strcmpi({obj.pools.type},'output'));
            for i=1:numel(outpools)
                ind =outpools(i);
                t = obj.pools(ind).error(obj.pools(ind).target>=0);
                obj.pss = obj.pss + sum(t .* t);
                % for computing cross entropy error,use local variable 'act' to limit
                % values between 0.001 and 0.999, then logically index into 'act' for
                % getting logs of activation values for units that have target 1 or
                % 0.Sum of the logs evaluate to cross entropy error
                act = obj.pools(ind).activation;
                act(act < 0.001) = 0.001;
                act(act > 0.999) = 0.999;
                ce = sum(log(act(obj.pools(ind).target == 1))) + ...
                    sum(log(1 - act(obj.pools(ind).target == 0)));
                obj.pce = obj.pce - ce; % to make pce positive value;
            end
            obj.tss = obj.tss + obj.pss;
            obj.tce = obj.tce + obj.pce;
        end
        
        function train(obj,varargin)
            epoch_limit = (floor(obj.epochno/obj.train_options.nepochs)+1)*obj.train_options.nepochs;
            while obj.next_epochno <= epoch_limit
                %             while obj.next_epochno <= obj.train_options.nepochs
                obj.epochno = obj.next_epochno;
                while obj.next_patno <= length(obj.environment.sequences)
                    obj.patno = obj.next_patno;
                    obj.environment.sequence_index = obj.patno;
                    obj.cpname = obj.environment.sequences(obj.environment.sequence_index).name; %added
                    obj.clamp_pools;
                    obj.compute_output;
                    obj.compute_error;
                    obj.sumstats;
                    obj.compute_weds;
                    if strcmpi(obj.train_options.lgrain,'pattern')
                        obj.change_weights;
                    end
                    obj.next_patno = obj.patno + 1;
                    if obj.done_updating_patno
                        return 
                    end
                end
                if strcmpi(obj.train_options.lgrain,'epoch')
                    obj.change_weights;
                end
                obj.next_patno = 1;
                obj.epochno = obj.next_epochno;
                obj.next_epochno = obj.next_epochno + 1;
                if obj.done_updating_epochno
                    return
                end
            end
        end
        
        function test(obj)
            
            
            
            %while obj.next_epochno <= obj.test_options.nepochs
            %    obj.epochno = obj.next_epochno;
                while obj.next_patno <= length(obj.environment.sequences)
                    obj.patno = obj.next_patno;
                    obj.environment.sequence_index = obj.patno;
                    obj.cpname = obj.environment.sequences(obj.environment.sequence_index).name; %added
                    obj.clamp_pools;
                    obj.compute_output;
                    obj.compute_error;
                    obj.sumstats;
                    obj.next_patno = obj.patno + 1;
                    if obj.done_updating_patno
                        return
                    end
                end
                obj.next_patno = 1;
                obj.tss =0;
                %obj.next_epochno = obj.epochno + 1;
                %if obj.done_updating_epochno
                %    return
                %end
            %end
        end
        
        function set.environment(obj,value)
            obj.environment = value;
            obj.clamp_pools;
        end
        %         function train(obj,varargin)
        %             for epochno = obj.next_epochno:obj.train_options.nepochs
        %                 obj.epochno = epochno;
        %                 for patno = obj.next_patno:length(obj.environment.sequences)
        %                     obj.patno = patno;
        %                     obj.environment.sequence_index = patno;
        %                     obj.clamp_pools;
        %                     obj.compute_output;
        %                     obj.compute_error;
        %                     obj.sumstats;
        %                     obj.compute_weds;
        %                     if strcmpi(obj.train_options.lgrain,'pattern')
        %                         obj.change_weights;
        %                     end
        %                     obj.next_patno = patno + 1;
        %                     if obj.done_updating_patno
        %                         return
        %                     end
        %                 end
        %                 if strcmpi(obj.train_options.lgrain,'epoch')
        %                     obj.change_weights;
        %                 end
        %                 obj.next_patno = 1;
        %                 obj.next_epochno = epochno + 1;
        %                 if obj.done_updating_epochno
        %                     return
        %                 end
        %             end
        %         end
        %
        %         function test(obj)
        %             for epochno = obj.next_epochno:obj.train_options.nepochs
        %                 obj.epochno = epochno;
        %                 for patno = obj.next_patno:length(obj.environment.sequences)
        %                     obj.patno = patno;
        %                     obj.environment.sequence_index = obj.patno;
        %                     obj.clamp_pools;
        %                     obj.compute_output;
        %                     obj.compute_error;
        %                     obj.sumstats;
        %                     obj.next_patno = obj.patno + 1;
        %                     if obj.done_updating_patno
        %                         return
        %                     end
        %                 end
        %                 obj.next_patno = 1;
        %                 obj.next_epochno = obj.epochno + 1;
        %                 if obj.done_updating_epochno
        %                     return
        %                 end
        %             end
        %         end
    end
    
end

