classdef tdbp_net < srn_net & annealing_process
    properties
        stepno = 0;
        next_stepno = 1;
        step_limit = 200;
        action;
        counter;
        netmode = 'afterstate'; %added, but not implemented yet
        reward = 0; %just for reference/display
        cum_reward = 0; %for display
        offPolicyLearning = false; %if true, then use Q-learning (no traces)
        greedyAction = []; %Q-learning needs this for the weight update
        trainMode = true; %set in train() and test() TODO: extend to all networks
    end
    methods (Access = protected, Static = true)
        
        function choice = linear_stochastic_choice(state_values)
            choice = [];
            total_value = sum(state_values);
            value = total_value * rand;
            end_range = 0;
            for i=1:numel(state_values)
                end_range = end_range + state_values(i);
                if (value < end_range)
                    choice = i;
                    break;
                end
            end
            if isempty(choice)
                disp(state_values);
                disp('cant decide :(');
                choice = 1;
            end
        end
    end
    methods
        function obj = tdbp_net(varargin)
            obj = obj@srn_net(varargin{:});
            obj = obj@annealing_process();
            %WILL CAUSE ERROR WHEN GIVING THESE PROPS INTIALLY
            obj.train_options.gamma = .9;
            obj.train_options.lambda = .9;
            obj.train_options.epsilon = .1;
            obj.test_options.epsilon = .1;
            obj.train_options.policy = 'greedy';
            obj.type = 'tdbp';
            obj.action = zeros(1,obj.pools_by_name('action').unit_count);
            obj.counter = 0;
        end
        
        function reset_net(obj)
            reset_elig_trace(obj);
            obj.counter = 0;
            obj.set_temp(obj.counter);
            obj.seqno = 0;
            obj.next_seqno = 1;
            obj.epochno = 0; %changed
            %obj.patno = 0;
            %obj.next_patno = 1;
            obj.next_epochno = 1;
            obj.tss = 0.0;
            obj.pss = 0.0;
            obj.sss = 0.0;
            for i = 2:length(obj.pools) %bias pool stays on
                obj.pools(i).activation = obj.pools(i).activation_reset_value;
                obj.pools(i).error = 0;
                obj.pools(i).delta = zeros(size(obj.pools(i).activation));
            end
            obj.reset_net_input(); %added for bp display
            obj.reset_weights();
            %obj.environment.sequence_index = obj.next_patno;
            %obj.clamp_pools; %don't want to have pools automatically clamped
            resetlog;
            if obj.gui
                update_display(0);
            end
        end
               
        function save_output(obj)
           for i=1:length(obj.pools)
               if ~isnan(obj.pools(i).target)
                obj.pools(i).activation_history(:,1) = obj.pools(i).activation;
               end
           end
        end            
        
        
        function compute_error(obj)
            if obj.offPolicyLearning %compute outpute overwrites activations, so save beforehand
                onPolicyActivations = {obj.pools.activation};
                onPolicyAction = obj.action;
                obj.action = obj.greedyAction;
                obj.compute_output();
            end
            for i=1:length(obj.pools)
                if ~isnan(obj.pools(i).target)
                    obj.pools(i).error = obj.pools(i).error_reset_value;
                else
                    obj.pools(i).error = [];
                end
            end
            for i=length(obj.pools):-1:1
                gamma = obj.train_options.gamma;
               if ~isnan(obj.pools(i).target)
                   if obj.stepno == 1
                       obj.pools(i).error = zeros(1,obj.pools(i).unit_count);
                   else
                       obj.reward = obj.environment.getCurrentReward();
                       obj.cum_reward = obj.cum_reward + obj.reward;
                       if obj.environment.at_terminal
                           obj.pools(i).error = obj.pools(i).target - obj.pools(i).activation_history(:,1);
                       else
                           obj.pools(i).error = obj.pools(i).target + gamma * obj.pools(i).activation - obj.pools(i).activation_history(:,1);
                       end
                   end
%                    disp(obj.pools(i).target);
               end
               for j=1:length(obj.pools(i).projections)
                   from = obj.pools(i).projections(j).from;
                   from.error = [from.error obj.pools(i).error];
               end
            end
            if obj.offPolicyLearning%restore onPolicy activations and action
                obj.action = onPolicyAction;
                [obj.pools.activation] = onPolicyActivations{:};
            end
        end
        
        function compute_delta(obj)
            for i=1:length(obj.pools)
                if ~isnan(obj.pools(i).target)
                    obj.pools(i).delta = obj.pools(i).delta_reset_value;
                else
                    obj.pools(i).delta = [];
                end
            end
            for i=length(obj.pools):-1:1
               if ~isnan(obj.pools(i).target)
                    %handle other activation functions here
                   switch obj.pools(i).activation_function
                       case 'logistic'
                            obj.pools(i).delta = obj.pools(i).activation .* (1 - obj.pools(i).activation);
                       case 'linear'
                           obj.pools(i).delta = ones(1,obj.pools(i).unit_count);
                       otherwise
                   end
                   for j=1:length(obj.pools(i).projections)
                       proj = obj.pools(i).projections(j).using;
                       from = obj.pools(i).projections(j).from;
                        new_delta = zeros(length(obj.pools(i).error), from.unit_count);
                        for k=1:length(obj.pools(i).error)
                             %handle other activation functions for non-output here
                            new_delta(k,:) =( obj.pools(i).delta(k) * proj.weights(k,:) ) .* ...
                                (from.activation .* (1 - from.activation));
                        end
                        from.delta = [from.delta; new_delta];
                   end
               else
                   for j=1:length(obj.pools(i).projections)
                       proj = obj.pools(i).projections(j).using;
                       from = obj.pools(i).projections(j).from;
                       new_delta = zeros(length(obj.pools(i).error), from.unit_count);
                       for k=1:length(obj.pools(i).error)
                           for l=1:obj.pools(i).unit_count
                               for m=1:from.unit_count
                                   new_delta(k,m) = ( obj.pools(i).delta(k,l) * proj.weights(l,m) ) * ...
                                       ( from.activation(m) * ( 1-from.activation(m) ) );
                               end
                           end
                       end
                       from.delta = [from.delta; new_delta];
                   end
               end
                   
            end
        end
        
        function compute_elig_trace(obj)
            for i=1:length(obj.pools)
                for j=1:length(obj.pools(i).projections)
                    proj = obj.pools(i).projections(j).using;
                    lambda = obj.train_options.lambda;
                    from = obj.pools(i).projections(j).from;
                    if ~isnan(obj.pools(i).target)
                        for k=1:length(obj.pools(i).error)
                            for l=1:from.unit_count
                                proj.elig_trace(k,l) = lambda * proj.elig_trace(k,l) + ...
                                    obj.pools(i).delta(k) * from.activation(l);
                            end
                        end
                    else
                        for k=1:length(obj.pools(i).error)
                            for l=1:from.unit_count
                                for m=1:obj.pools(i).unit_count
                                    proj.elig_trace(m,l,k) = lambda * proj.elig_trace(m,l,k) + ...
                                        obj.pools(i).delta(k,m) * from.activation(l);
                                end
                            end
                        end
                    end
                    
                end
            end
        end
        
        function reset_elig_trace(obj)
           for i = 1:length(obj.pools)
               for j = 1:length(obj.pools(i).projections)
                   from = obj.pools(i).projections(j).from;
                   proj = obj.pools(i).projections(j).using;
                   if isnan(obj.pools(i).target)
                       proj.elig_trace = zeros(obj.pools(i).unit_count,from.unit_count);
                   else
                       proj.elig_trace = zeros(obj.pools(i).unit_count,from.unit_count,length(obj.pools(i).error));
                   end
               end
           end
        end
        
        function compute_weight_change(obj)
            lr = obj.train_options.lrate;
            decay = obj.train_options.wdecay;
            for i=1:length(obj.pools)
                if ~isnan(obj.pools(i).target)
                    for j=1:length(obj.pools(i).projections)
                        proj = obj.pools(i).projections(j).using;
                        %DELETE LOOP AND DO ALL K AT ONCE?
                        for k=1:length(obj.pools(i).error)
                            proj.weight_deltas(k,:) = (lr*proj.elig_trace(k,:) * obj.pools(i).error(k) ) - ...
                                decay*proj.weights(k,:) + proj.weight_deltas(k,:);
                        end
                    end
                else
                    for j=1:length(obj.pools(i).projections)
                        proj = obj.pools(i).projections(j).using;
                        for k=1:length(obj.pools(i).error)
                            proj.weight_deltas = (lr*proj.elig_trace(:,:,k) * obj.pools(i).error(k) ) - ...
                                decay*proj.weights + proj.weight_deltas;
                        end
                    end                   
                end
            end
        end
        
        function change_weights(obj)
            
            for i=1:length(obj.pools)
                for j=1:length(obj.pools(i).projections)
                    proj = obj.pools(i).projections(j).using;
                    proj.weights = proj.weights + proj.weight_deltas;
                    proj.weight_deltas = zeros(size(proj.weight_deltas));
                end
            end
        end
        
        function clamp_pools(obj)
           clamp_pools@srn_net(obj);
           %MAKE THIS MORE FLEXIBLE
           action_pool = obj.pools_by_name('action');
           action_pool.activation = obj.action;
           action_pool.clamped_activation = 2;
        end
        
        function choose_next_action(obj)
            outpools = [];
            for i=1:length(obj.pools)
                if ~isnan(obj.pools(i).target)
                    outpools = [outpools obj.pools(i)];
                end
            end
            pos_actions = obj.environment.possible_actions;
           rewards = zeros(size(pos_actions,1),length(outpools));
           for i=1:length(pos_actions(:,1))
               obj.action = pos_actions(i,:);
               obj.compute_output;
               si = 1;
               cur_rewards = zeros(1,length(outpools));
               for j=1:length(outpools)
                   ei = si + outpools(j).unit_count-1;
                   cur_rewards(si:ei) = outpools(j).activation;
                   si = ei+1;
               end
               rewards(i,:) = cur_rewards;
           end
%            disp(rewards);
           if obj.trainMode
               policyType = obj.train_options.policy;
           else
               policyType = obj.test_options.policy;
           end
           switch policyType
               case 'greedy'
                   [~, choice] = max(rewards(:,1));
               case 'egreedy'
                   
                   if obj.train_options.epsilon > rand
                       choice = ceil(rand * size(pos_actions,1));
                   else
                    [~, choice] = max(rewards(:,1));
                   end
               case 'softmax'
%                    if obj.temp < .00001    % if temp is 0, then just do greedy policy
%                        [val choice] = max(rewards(:,1));
%                    else
                       values = rewards / obj.temp;
                       values = exp(values);
                       if find(isinf(values))
                           [~, choice] = max(rewards(:,1));
                       elseif find(isinf(1./values))
                           [~, choice] = max(rewards(:,1));
                       else
                           total = sum(values);
                           values = values / total;
                           choice = tdbp_net.linear_stochastic_choice(values);
                       end
%                    end
               case 'linearwt'
                   choice = tdbp_net.linear_stochastic_choice(rewards);
               case 'defer'
                   
                
               case 'userdef'
                %for 'userdef' policies, the policy is defined in the chooseAction method of the
                %environment
                   choice = obj.environment.chooseAction(rewards);
               otherwise
           end
           obj.action = pos_actions(choice,:);
           %offpolicy learning needs the greedy action for the weight
           %update
           if obj.offPolicyLearning
              [~, choice] = max(rewards(:,1));
              obj.greedyAction = pos_actions(choice,:);
           end
% 		   disp(obj.action);
		end
        function train(obj)
            obj.trainMode = true;
            epoch_limit = (floor(obj.epochno/obj.train_options.nepochs)+1)*obj.train_options.nepochs;
            while obj.next_epochno <= epoch_limit
                flag = 0;
                obj.epochno = obj.next_epochno;

                while obj.next_stepno <= obj.step_limit && ~flag
                    obj.stepno = obj.next_stepno;
                    obj.set_temp(obj.counter);
                    obj.counter = obj.counter +1;
                    obj.clamp_pools;
                    if obj.environment.at_terminal
                        flag = 1;
                    else
                        obj.choose_next_action;
					end
                    obj.compute_output;
                    obj.compute_error;
                    if obj.stepno ~= 1
                        obj.compute_weight_change;
                        obj.change_weights;
                        obj.compute_output;
                        obj.save_output;
                    else
                        obj.reset_elig_trace;
                        obj.save_output;
                    end
                    obj.compute_delta;
                    obj.compute_elig_trace;
                    if ~flag
                        obj.environment.perform_action(obj.action);
                        obj.next_stepno = obj.stepno+1;
                    end
             
                    if obj.done_updating_stepno
                        if flag
                            obj.terminate;
                        end
                        return
                    end
                end
                if obj.done_updating_epochno
                    obj.terminate;
                        return
                end
                
                obj.terminate;
            end
        end
        function terminate(obj)
            endstring = sprintf('steps: %i terminal r: %s',obj.stepno,num2str(obj.cum_reward));
                disp(endstring);
                obj.environment.reset;
                obj.next_epochno = obj.epochno +1;
                obj.next_stepno = 1;
                obj.cum_reward = 0;
        end
        function set.action(obj,value)
           obj.action = value;
           action_pool = obj.pools_by_name('action');
           action_pool.activation = obj.action;
           action_pool.clamped_activation = 2;
		end   
        function test(obj)
            obj.trainMode = false;
            epoch_limit = (floor(obj.epochno/obj.train_options.nepochs)+1)*obj.train_options.nepochs;
            while obj.next_epochno <= epoch_limit
                flag = 0;
                obj.epochno = obj.next_epochno;

                while obj.next_stepno <= obj.step_limit && ~flag
                    obj.stepno = obj.next_stepno;
                    obj.set_temp(obj.counter);
                    obj.counter = obj.counter +1;
                    obj.clamp_pools;
                    if obj.environment.at_terminal
                        flag = 1;
                    else
                        obj.choose_next_action;
					end
                    obj.compute_output;
                    obj.compute_error;
%                     if obj.stepno ~= 1
%                         obj.compute_weight_change;
%                         obj.change_weights;
%                         obj.compute_output;
%                         obj.save_output;
%                     else
%                         obj.reset_elig_trace;
%                         obj.save_output;
%                     end
%                     obj.compute_delta;
%                     obj.compute_elig_trace;
                    if ~flag
                        obj.environment.perform_action(obj.action);
                        obj.next_stepno = obj.stepno+1;
                    end
             
                    if obj.done_updating_stepno
                        if flag
                            obj.terminate;
                        end
                        return
                    end
                end
                if obj.done_updating_epochno
                    obj.terminate;
                        return
                end
                obj.terminate;
            end
        end
        function set.offPolicyLearning(obj,val)
           obj.offPolicyLearning = val;
           if val
               obj.train_options.lambda = 0;
           end
        end
    end

         
        
        
    
    
end