%rename?
classdef projection < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties

        weights;
        weight_deltas;
        weds;
        prev_weds; %for follow
        
        constraint_type = 'random';
        constraint = [];
        lr = [];
        elig_trace;
		

    end
    
    methods
        function obj = projection(rec,send,type,con)
            if nargin >2
                obj.constraint_type = type;
                if nargin > 3
                    obj.constraint = con;
                end
            end
                
%           obj.sending_pool = send;
%           obj.receiving_pool = rec;
%           send.outgoing_projections = [send.outgoing_projections obj];
%           rec.incoming_projections = [rec.incoming_projections obj];
           obj.weights = ones(send.unit_count,rec.unit_count)'; %init weights in net constructor
           obj.weds = zeros(send.unit_count,rec.unit_count)';
           obj.prev_weds = obj.weds; %added for follow
           obj.weight_deltas = zeros(send.unit_count,rec.unit_count)';
%copyback now pool based
%            if nargin > 2
%                obj.constraint_type = type;
%            else
%                obj.constraint_type = 'random';
%            end
%            if strcmpi(obj.constraint_type,'copyback') 
%                rec.clamped_activation = 1;
%                rec.activation = ones(1,rec.unit_count)/2;
%            end
        end
    end
    
end

